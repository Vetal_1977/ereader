#include "pch.h"
#include "LiveTile.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Media::Animation;

Platform::String^ const ScrollerPartName = "Scroller";
Platform::String^ const CurrentPartName = "Current";
Platform::String^ const NextPartName = "Next";
Platform::String^ const TranslatePartName = "Translate";
Platform::String^ const StackPartName = "Stack";

LiveTile::LiveTile() : m_currentIndex(0)
{
    DefaultStyleKey = LiveTile::typeid->FullName;

    m_sizeChangedToken = SizeChanged += ref new SizeChangedEventHandler(this, &LiveTile::OnLiveTileSizeChanged);
    m_loadedToken = Loaded += ref new RoutedEventHandler(this, &LiveTile::OnLiveTileLoaded);
    m_unloadedToken = Unloaded += ref new RoutedEventHandler(this, &LiveTile::OnLiveTileUnLoaded);
}

LiveTile::~LiveTile()
{
    SizeChanged -= m_sizeChangedToken;
    Loaded -= m_loadedToken;
    Unloaded -= m_unloadedToken;
    if (nullptr != m_timer)
    {
        m_timer->Tick -= m_timerTickToken;
    }
}

static DependencyProperty^ m_itemsSourceProperty =
    DependencyProperty::Register("ItemsSource", 
    TypeName(Object::typeid), 
    TypeName(LiveTile::typeid), 
    ref new PropertyMetadata(nullptr, ref new PropertyChangedCallback(&LiveTile::OnItemsSourcePropertyChanged)));

DependencyProperty^ LiveTile::ItemsSourceProperty::get()
{
    return m_itemsSourceProperty;
}

Object^ LiveTile::ItemsSource::get()
{
    return dynamic_cast<Object^>(GetValue(ItemsSourceProperty));
}

void LiveTile::ItemsSource::set(Object^ value)
{
    SetValue(ItemsSourceProperty, value);
}

static DependencyProperty^ m_itemTemplateProperty =
    DependencyProperty::Register("ItemTemplate", 
    TypeName(DataTemplate::typeid), 
    TypeName(LiveTile::typeid), 
    nullptr);

DependencyProperty^ LiveTile::ItemTemplateProperty::get()
{
    return m_itemTemplateProperty;
}

DataTemplate^ LiveTile::ItemTemplate::get()
{
    return dynamic_cast<DataTemplate^>(GetValue(ItemTemplateProperty));
}

void LiveTile::ItemTemplate::set(DataTemplate^ value)
{
    SetValue(ItemTemplateProperty, value);
}

static DependencyProperty^ m_directionProperty =
    DependencyProperty::Register("Direction", 
    TypeName(SlideDirection::typeid), 
    TypeName(LiveTile::typeid), 
    ref new PropertyMetadata(SlideDirection::Up));

DependencyProperty^ LiveTile::DirectionProperty::get()
{
    return m_directionProperty;
}

SlideDirection LiveTile::Direction::get()
{
    return static_cast<SlideDirection>(GetValue(DirectionProperty));
}

void LiveTile::Direction::set(SlideDirection value)
{
    SetValue(DirectionProperty, value);
}

// Event Handlers

void LiveTile::OnItemsSourcePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto ctrl = dynamic_cast<LiveTile^>(sender);
    auto value = dynamic_cast<IBindableObservableVector^>(e->NewValue);
    if (nullptr != value)
    {
        if (nullptr != ctrl->m_currentElement && nullptr != ctrl->m_nextElement)
        {
            ctrl->Start();
        }
    }
    else if (nullptr != ctrl->m_timer)
    {
        ctrl->m_timer->Stop();
    }
}

void LiveTile::OnApplyTemplate()
{
    m_scroller = dynamic_cast<FrameworkElement^>(GetTemplateChild(ScrollerPartName));
    m_currentElement = dynamic_cast<FrameworkElement^>(GetTemplateChild(CurrentPartName));
    m_nextElement = dynamic_cast<FrameworkElement^>(GetTemplateChild(NextPartName));
    m_translate = dynamic_cast<TranslateTransform^>(GetTemplateChild(TranslatePartName));
    m_stackPanel = dynamic_cast<StackPanel^>(GetTemplateChild(StackPartName));
    if (nullptr != m_stackPanel)
    {
        if (Direction == SlideDirection::Up)
        {
            m_stackPanel->Orientation = Orientation::Vertical;
        }
        else
        {
            m_stackPanel->Orientation = Orientation::Horizontal;
        }
    }
    if(nullptr != ItemsSource)
    {
        Start();
    }
    Control::OnApplyTemplate();
}

void LiveTile::OnLiveTileLoaded(Object^ sender, RoutedEventArgs^ e)
{
    if (nullptr != m_timer)
    {
        m_timer->Start();
    }
}

void LiveTile::OnLiveTileUnLoaded(Object^ sender, RoutedEventArgs^ e)
{
    if (nullptr != m_timer)
    {
        m_timer->Stop();
    }

    if(nullptr != m_translate)
    {
        m_translate->Y = 0;
    }
}

void LiveTile::OnLiveTileSizeChanged(Object^ sender, SizeChangedEventArgs^ e)
{
    if (nullptr != m_currentElement && nullptr != m_nextElement)
    {
        m_currentElement->Width = e->NewSize.Width;
        m_nextElement->Width = e->NewSize.Width;
        m_currentElement->Height = e->NewSize.Height; 
        m_nextElement->Height = e->NewSize.Height;
    }
    //Set content area to twice the size in the slide direction
    if (nullptr != m_scroller)
    {
        if(Direction == SlideDirection::Up)
        {
            m_scroller->Height = e->NewSize.Height * 2;
        }
        else
        {
            m_scroller->Width = e->NewSize.Width * 2;
        }
    }
    //Set clip to control
    auto geometry = ref new RectangleGeometry();
    geometry->Rect = Rect(Point(), e->NewSize);
    Clip = geometry;
}

void LiveTile::OnTimerTick(Object^ sender, Object^ e)
{
    //std::mt19937 rand(static_cast<unsigned int>(time(NULL)));
    //std::uniform_int_distribution<int> gen(0, 5); // uniform, unbiased
    //m_currentIndex++;
    //m_timer->Interval = FromSeconds(gen(rand) + 5); //randomize next flip
    //UpdateNextItem();
}

void LiveTile::Start()
{
    //std::mt19937 rand(static_cast<unsigned int>(time(NULL)));
    //std::uniform_int_distribution<int> gen(0, 5); // uniform, unbiased
    m_currentIndex = 0;
    if (nullptr != m_currentElement)
    {
        auto current = GetCurrent();
        m_currentElement->DataContext = current;
    }
    if (nullptr != m_nextElement)
    {
        auto next = GetNext();
        m_nextElement->DataContext = next;
    }
    /*if (nullptr == m_timer)
    {
        m_timer = ref new DispatcherTimer();
        m_timer->Interval = FromSeconds(1);
        m_timerTickToken = m_timer->Tick += ref new EventHandler<Object^>(this, &LiveTile::OnTimerTick);
        m_timer->Interval = FromSeconds(gen(rand) + 5);
    }
    m_timer->Start();*/
}

void LiveTile::UpdateNextItem()
{
    //auto sb = ref new Storyboard();
    //if (nullptr != m_translate)
    //{
    //    auto anim = ref new DoubleAnimation();
    //    anim->Duration = Duration(FromMilliseconds(500));
    //    anim->From = 0.0;
    //    if(Direction == SlideDirection::Up)
    //    {
    //        anim->To = -ActualHeight;
    //    }
    //    else if (Direction == SlideDirection::Left)
    //    {
    //        anim->To = -ActualWidth;
    //    }

    //    anim->FillBehavior = FillBehavior::HoldEnd;
    //    auto ease = ref new CubicEase();
    //    ease->EasingMode = EasingMode::EaseOut;
    //    anim->EasingFunction = ease;
    //    Storyboard::SetTarget(anim, m_translate);
    //    if(Direction == SlideDirection::Up)
    //    {
    //        Storyboard::SetTargetProperty(anim, "Y");
    //    }
    //    else
    //    {
    //        Storyboard::SetTargetProperty(anim, "X");
    //    }
    //    sb->Children->Append(anim);
    //}
    //sb->Completed += ref new EventHandler<Object^>([this](Object^ sender, Object^ e)
    //{
    //    //Reset back and swap images, getting the next image ready
    //    auto sb = dynamic_cast<Storyboard^>(sender);
    //    sb->Stop();
    //    if (nullptr != m_translate)
    //    {
    //        m_translate->X = 0;
    //        m_translate->Y = 0;
    //    }
    //    if(nullptr != m_currentElement)
    //    {
    //        m_currentElement->DataContext = GetCurrent();
    //    }
    //    if(nullptr != m_nextElement)
    //    {
    //        m_nextElement->DataContext = GetNext();
    //    }
    //});
    //sb->Begin();
}

Object^ LiveTile::GetCurrent()
{
    return GetItemAt(m_currentIndex);
}

Object^ LiveTile::GetNext()
{
    return GetItemAt(m_currentIndex + 1);
}

Object^ LiveTile::GetItemAt(unsigned int index)
{
    if (nullptr != ItemsSource)
    {
        auto list = dynamic_cast<IBindableObservableVector^>(ItemsSource);
        if (nullptr != list)
        {
            if (list->Size > 0)
            {
                index = index % list->Size;
                return list->GetAt(index);
            }
        }
    }
    return nullptr;
}

TimeSpan LiveTile::FromSeconds(long long value)
{
    return FromInterval(value, 1000);
}


Windows::Foundation::TimeSpan LiveTile::FromMilliseconds(long long value)
{
    return FromInterval(value, 1);
}

Windows::Foundation::TimeSpan LiveTile::FromInterval(long long value, unsigned int scale)
{
    long long num = value * static_cast<long long>(scale) + (value >= static_cast<long long>(0.0) ? static_cast<long long>(0.5) : static_cast<long long>(-0.5));
    TimeSpan span;
    span.Duration = num * 10000LL;
    return span;
}