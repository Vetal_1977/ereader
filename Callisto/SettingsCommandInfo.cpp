#include "pch.h"
#include "SettingsCommandInfo.h"

using namespace Callisto::Controls::SettingsManagement;
using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;

SettingsCommandInfo::SettingsCommandInfo(Platform::String^ headerText, double width, Windows::UI::Xaml::Controls::UserControl^ instance) : m_headerText(headerText), m_width(width), m_instance(instance)
{
}

Platform::String^ SettingsCommandInfo::HeaderText::get()
{
    return m_headerText;
}

double SettingsCommandInfo::Width::get()
{
    return m_width;
}

UserControl^ SettingsCommandInfo::Instance::get()
{
    return m_instance;
}

