#include "pch.h"
#include "CustomDialog.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;

CustomDialog::CustomDialog() 
{
    DefaultStyleKey = CustomDialog::typeid->FullName;
}

CustomDialog::~CustomDialog()
{
    Window::Current->SizeChanged -= m_windowSizeChangedToken;
    RemoveBackButtonClickHandler();
}

// Dependency Properties

static DependencyProperty^ _isOpenProperty =
    DependencyProperty::Register("IsOpen", 
    TypeName(bool::typeid), 
    TypeName(CustomDialog::typeid), 
    ref new PropertyMetadata(false, ref new PropertyChangedCallback(&CustomDialog::OnIsOpenPropertyChanged)));

DependencyProperty^ CustomDialog::IsOpenProperty::get()
{
    return _isOpenProperty;
}

bool CustomDialog::IsOpen::get()
{
    return static_cast<bool>(GetValue(IsOpenProperty));
}

void CustomDialog::IsOpen::set(bool value)
{
    SetValue(IsOpenProperty, value);
}

static DependencyProperty^ _backButtonVisibilityProperty =
    DependencyProperty::Register("BackButtonVisibility", 
    TypeName(Visibility::typeid), 
    TypeName(CustomDialog::typeid), 
    ref new PropertyMetadata(Visibility::Collapsed));

DependencyProperty^ CustomDialog::BackButtonVisibilityProperty::get()
{
    return _backButtonVisibilityProperty;
}

Visibility CustomDialog::BackButtonVisibility::get()
{
    return static_cast<Windows::UI::Xaml::Visibility>(GetValue(BackButtonVisibilityProperty));
}

void CustomDialog::BackButtonVisibility::set(Windows::UI::Xaml::Visibility value)
{
    SetValue(BackButtonVisibilityProperty, value);
}

static DependencyProperty^ _titleProperty =
    DependencyProperty::Register("Title", 
    TypeName(String::typeid), 
    TypeName(CustomDialog::typeid), 
    nullptr);

DependencyProperty^ CustomDialog::TitleProperty::get()
{
    return _titleProperty;
}

String^ CustomDialog::Title::get()
{
    return dynamic_cast<String^>(GetValue(TitleProperty));
}

void CustomDialog::Title::set(String^ value)
{
    SetValue(TitleProperty, value);
}

static DependencyProperty^ _backButtonCommandProperty =
    DependencyProperty::Register("BackButtonCommand", 
    TypeName(ICommand::typeid), 
    TypeName(CustomDialog::typeid), 
    ref new PropertyMetadata(DependencyProperty::UnsetValue));

DependencyProperty^ CustomDialog::BackButtonCommandProperty::get()
{
    return _backButtonCommandProperty;
}

ICommand^ CustomDialog::BackButtonCommand::get()
{
    return dynamic_cast<ICommand^>(GetValue(BackButtonCommandProperty));
}

void CustomDialog::BackButtonCommand::set(ICommand^ value)
{
    SetValue(BackButtonCommandProperty, value);
}

static DependencyProperty^ _backButtonCommandParameterProperty =
    DependencyProperty::Register("BackButtonCommandParameter", 
    TypeName(Object::typeid), 
    TypeName(CustomDialog::typeid), 
    ref new PropertyMetadata(DependencyProperty::UnsetValue));

DependencyProperty^ CustomDialog::BackButtonCommandParameterProperty::get()
{
    return _backButtonCommandParameterProperty;
}

Object^ CustomDialog::BackButtonCommandParameter::get()
{
    return dynamic_cast<ICommand^>(GetValue(BackButtonCommandParameterProperty));
}

void CustomDialog::BackButtonCommandParameter::set(Object^ value)
{
    SetValue(BackButtonCommandParameterProperty, value);
}

// Overrides

void CustomDialog::OnApplyTemplate()
{
    m_rootGrid = dynamic_cast<Grid^>(GetTemplateChild("PART_RootGrid"));
    m_rootBorder = dynamic_cast<Border^>(GetTemplateChild("PART_RootBorder"));
    RemoveBackButtonClickHandler();
    m_backButton = dynamic_cast<Button^>(GetTemplateChild("PART_BackButton"));

    ResizeContainers();

    if (m_backButton != nullptr)
    {
        m_backButtonClickedToken = m_backButton->Click += ref new RoutedEventHandler(this, &CustomDialog::OnBackButtonClicked);
    }

    m_windowSizeChangedToken = Window::Current->SizeChanged += ref new WindowSizeChangedEventHandler(this, &CustomDialog::OnWindowSizeChanged);

    ContentControl::OnApplyTemplate();
}

// Event Handlers

void CustomDialog::OnIsOpenPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto boxedBool = dynamic_cast<Box<bool>^>(e->NewValue);
    auto boolValue = (boxedBool != nullptr && boxedBool->Value);
    if (boolValue)
    {
        auto dialog = dynamic_cast<CustomDialog^>(sender);
        if (dialog != nullptr)
        {
            dialog->ApplyTemplate();
        }
    }
}

void CustomDialog::OnWindowSizeChanged(Object^ sender, WindowSizeChangedEventArgs^ e)
{
    ResizeContainers();
}

void CustomDialog::OnBackButtonClicked(Object^ sender, RoutedEventArgs^ e)
{
    if (m_backButtonClickedObserved)
    {
        BackButtonClicked(sender, e);
    }
    else
    {
        IsOpen = false;
    }
}

// Private Methods

void CustomDialog::ResizeContainers()
{
    if (m_rootGrid != nullptr)
    {
        m_rootGrid->Width = Window::Current->Bounds.Width;
        m_rootGrid->Height = Window::Current->Bounds.Height;
    }

    if (m_rootBorder != nullptr)
    {
        m_rootBorder->Width = Window::Current->Bounds.Width;
    }
}

void CustomDialog::RemoveBackButtonClickHandler()
{
    if (m_backButton != nullptr)
    {
        m_backButton->Click -= m_backButtonClickedToken;
    }
}
