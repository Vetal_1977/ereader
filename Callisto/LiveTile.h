#pragma once

namespace Callisto
{
    namespace Controls
    {
        public enum class SlideDirection
        {
            Up,
            //Down,
            Left,
            //Right
        };

        [Windows::UI::Xaml::TemplatePartAttribute(Name = "Scroller", Type=Windows::UI::Xaml::FrameworkElement::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = "Current", Type = Windows::UI::Xaml::FrameworkElement::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = "Next", Type = Windows::UI::Xaml::FrameworkElement::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = "Translate", Type = Windows::UI::Xaml::Media::TranslateTransform::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = "Stack", Type = Windows::UI::Xaml::Controls::StackPanel::typeid)]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class LiveTile sealed : public Windows::UI::Xaml::Controls::Control
        {
        public:
            LiveTile();
            virtual ~LiveTile();

            static property Windows::UI::Xaml::DependencyProperty^ ItemsSourceProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::Object^ ItemsSource
            {
                Platform::Object^ get();
                void set(Platform::Object^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ ItemTemplateProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::DataTemplate^ ItemTemplate
            {
                Windows::UI::Xaml::DataTemplate^ get();
                void set(Windows::UI::Xaml::DataTemplate^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ DirectionProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property SlideDirection Direction
            {
                SlideDirection get();
                void set(SlideDirection value);
            }

        internal:
            static void OnItemsSourcePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        protected:
            virtual void OnApplyTemplate() override;

        private:
            unsigned int m_currentIndex;
            Windows::UI::Xaml::DispatcherTimer^ m_timer;
            Windows::UI::Xaml::FrameworkElement^ m_currentElement;
            Windows::UI::Xaml::FrameworkElement^ m_nextElement;
            Windows::UI::Xaml::FrameworkElement^ m_scroller;
            Windows::UI::Xaml::Media::TranslateTransform^ m_translate;
            Windows::UI::Xaml::Controls::StackPanel^ m_stackPanel;

            Windows::Foundation::EventRegistrationToken m_sizeChangedToken;
            Windows::Foundation::EventRegistrationToken m_loadedToken;
            Windows::Foundation::EventRegistrationToken m_unloadedToken;
            Windows::Foundation::EventRegistrationToken m_timerTickToken;

            void OnLiveTileSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);
            void OnLiveTileLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnLiveTileUnLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnTimerTick(Platform::Object^ sender, Platform::Object^ e);

            void UpdateNextItem();
            Platform::Object^ GetCurrent();
            Platform::Object^ GetNext();
            Platform::Object^ GetItemAt(unsigned int index);
            void Start();

            Windows::Foundation::TimeSpan FromSeconds(long long value);
            Windows::Foundation::TimeSpan FromMilliseconds(long long value);
            Windows::Foundation::TimeSpan FromInterval(long long value, unsigned int scale);
        };
    }
}

