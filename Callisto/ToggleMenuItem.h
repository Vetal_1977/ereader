#pragma once
#include "MenuItem.h"

namespace Callisto
{
    namespace Controls
    {
        [Windows::UI::Xaml::TemplateVisualState(Name = "Base", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Hover", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Pressed", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Disabled", GroupName = "Common")]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class ToggleMenuItem sealed : public MenuItemInternal
        {
        public:
            ToggleMenuItem();

            static property Windows::UI::Xaml::DependencyProperty^ IsCheckedProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property bool IsChecked
            {
                bool get();
                void set(bool value);
            }
        };
    }
}

