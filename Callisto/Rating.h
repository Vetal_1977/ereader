#pragma once
#include "IUpdateVisualState.h"
#include "RatingSelectionMode.h"
#include "ValueChangedEventArgs.h"

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            class InteractionHelper;
            class ItemsControlExtension;
        }

        ref class RatingItem;

        [Windows::UI::Xaml::TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "PointerOver", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "PointerPressed", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Disabled", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Focused", GroupName = "FocusStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Unfocused", GroupName = "FocusStates")]
        [Windows::UI::Xaml::StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = RatingItem::typeid)]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class Rating sealed : public Windows::UI::Xaml::Controls::ItemsControl, Callisto::Controls::Common::IUpdateVisualState
        {
        public:
            
            event ValueChangedEventHandler^ ValueChanged;

            static property Windows::UI::Xaml::DependencyProperty^ DisplayValueProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double DisplayValue
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ ItemCountProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property unsigned int ItemCount
            {
                unsigned int get();
                void set(unsigned int value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ SelectionModeProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property RatingSelectionMode SelectionMode
            {
                RatingSelectionMode get();
                void set(RatingSelectionMode value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ ValueProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Value
            {
                double get();
                void set(double value);
            }

            property double WeightedValue
            {
                double get();
            }

            static property Windows::UI::Xaml::DependencyProperty^ PointerOverFillProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Media::SolidColorBrush^ PointerOverFill
            {
                Windows::UI::Xaml::Media::SolidColorBrush^ get();
                void set(Windows::UI::Xaml::Media::SolidColorBrush^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ PointerPressedFillProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Media::SolidColorBrush^ PointerPressedFill
            {
                Windows::UI::Xaml::Media::SolidColorBrush^ get();
                void set(Windows::UI::Xaml::Media::SolidColorBrush^ value);
            }

            virtual void UpdateVisualState(bool useTransitions) = Callisto::Controls::Common::IUpdateVisualState::UpdateVisualState;

        protected:
            virtual void OnValueChanged(double oldValue, double newValue);
            virtual void OnItemContainerStyleChanged(Windows::UI::Xaml::Style^ newValue);
            virtual void OnItemsChanged(Platform::Object^ e) override;
            virtual void OnSelectionModeChanged(RatingSelectionMode oldValue, RatingSelectionMode newValue);
            virtual void OnApplyTemplate() override;
            virtual void OnPointerEntered(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerExited(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerPressed(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerReleased(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnRatingItemValueSelected(RatingItem^ ratingItem, double newValue);
            virtual void OnKeyDown(Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e) override;

            virtual Windows::UI::Xaml::DependencyObject^ GetContainerForItemOverride() override;
            virtual bool IsItemItsOwnContainerOverride(Platform::Object^ item) override;
            virtual void PrepareContainerForItemOverride(Windows::UI::Xaml::DependencyObject^ element, Platform::Object^ item) override;
            virtual void ClearContainerForItemOverride(Windows::UI::Xaml::DependencyObject^ element, Platform::Object^ item) override;
            
        internal:
            static void OnDisplayValuePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnItemCountPropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnItemContainerStylePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnSelectionModePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnValuePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

            std::vector<RatingItem^> GetRatingItems();
            void SelectRatingItem(RatingItem^ selectedRatingItem);

        private:
            RatingItem^ m_hoveredRatingItem;
            std::shared_ptr<Common::InteractionHelper> m_interactionHelper;
            std::shared_ptr<Common::ItemsControlExtension> m_itemsControlHelper;
            double m_weightedValue;
            Windows::Foundation::EventRegistrationToken m_layoutUpdatedToken;
            Windows::Foundation::EventRegistrationToken m_ratingItemTappedToken;
            Windows::Foundation::EventRegistrationToken m_ratingItemPointerEnterToken;
            Windows::Foundation::EventRegistrationToken m_ratingItemPointerExitedToken;
            
            void OnDisplayValueChanged();
            void OnItemCountChanged(unsigned int newValue);
            void OnLayoutUpdated(Platform::Object^ sender, Platform::Object^ e);
            void OnRatingItemPointerEnter(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
            void OnRatingItemPointerExited(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
            void OnRatingItemTapped(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

            void UpdateDisplayValues();
            void UpdateValues();
            void UpdateHoverStates();
            RatingItem^ GetSelectedRatingItem();
            RatingItem^ GetRatingItemAtOffsetFrom(RatingItem^ ratingItem, int offset);
        };
    }
}
