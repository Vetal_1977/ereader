#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            extern Platform::String^ const GroupCommon;
            extern Platform::String^ const StateNormal;
            extern Platform::String^ const StateReadOnly;
            extern Platform::String^ const StatePointerOver;
            extern Platform::String^ const StatePointerPressed;
            extern Platform::String^ const StateDisabled;

            extern Platform::String^ const GroupFocus;
            extern Platform::String^ const StateUnfocused;
            extern Platform::String^ const StateFocused;

            extern Platform::String^ const StateWatermarked;
            extern Platform::String^ const StateUnwatermarked;

            class VisualStates
            {
            public:
                static void GoToState(Windows::UI::Xaml::Controls::Control^ control, bool useTransitions, const std::vector<Platform::String^>& stateNames);
            };
        }
    }
}