#pragma once
#include "IUpdateVisualState.h"

namespace Callisto
{
    namespace Controls
    {
        ref class Rating;
        namespace Common
        {
            class InteractionHelper;
        }

        [Windows::UI::Xaml::TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "PointerOver", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "PointerPressed", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Disabled", GroupName = "CommonStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Focused", GroupName = "FocusStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Unfocused", GroupName = "FocusStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Filled", GroupName = "FillStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Empty", GroupName = "FillStates")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Partial", GroupName = "FillStates")]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class RatingItem sealed : public Windows::UI::Xaml::Controls::Primitives::ButtonBase, Common::IUpdateVisualState
        {
        public:
            RatingItem();

            // Dependency Properties
            static property Windows::UI::Xaml::DependencyProperty^ DisplayValueProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double DisplayValue
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ PointerOverFillProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Media::SolidColorBrush^ PointerOverFill
            {
                Windows::UI::Xaml::Media::SolidColorBrush^ get();
                void set(Windows::UI::Xaml::Media::SolidColorBrush^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ PointerPressedFillProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Media::SolidColorBrush^ PointerPressedFill
            {
                Windows::UI::Xaml::Media::SolidColorBrush^ get();
                void set(Windows::UI::Xaml::Media::SolidColorBrush^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ ValueProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Value
            {
                double get();
                void set(double value);
            }

            virtual void UpdateVisualState(bool useTransitions) = Common::IUpdateVisualState::UpdateVisualState;

        protected:
            virtual void OnPointerPressed(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerReleased(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerEntered(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerExited(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnTapped(Windows::UI::Xaml::Input::TappedRoutedEventArgs^ e) override;

        internal:
            property Rating^ ParentRating 
            { 
                Rating^ get(); 
                void set(Rating^ value); 
            }

            static void OnDisplayValuePropertyChanged(Windows::UI::Xaml::DependencyObject^ sender, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

            void SelectValue();

        private:
            bool m_settingDisplayValue;
            Platform::WeakReference m_parentRating;
            std::shared_ptr<Common::InteractionHelper> m_interactionHelper;

            void OnDisplayValueChanged(double oldValue, double newValue);
        };
    }
}

