#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class FlipViewIndicator sealed : public Windows::UI::Xaml::Controls::ListBox
        {
        public:
            FlipViewIndicator();
            virtual ~FlipViewIndicator();

            static property Windows::UI::Xaml::DependencyProperty^ FlipViewProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Controls::FlipView^ FlipView
            {
                Windows::UI::Xaml::Controls::FlipView^ get();
                void set(Windows::UI::Xaml::Controls::FlipView^ value);
            }

        internal:
            static void OnFlipViewPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        private:
            Windows::Foundation::EventRegistrationToken m_selectionChangedToken;
            Windows::UI::Xaml::Controls::FlipView^ m_flipView;
            
            void OnFlipViewPropertyChangedCore(Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            void OnSelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
        };
    }
}
