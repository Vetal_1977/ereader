#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class DynamicTextBlock sealed : public Windows::UI::Xaml::Controls::ContentControl
        {
        public:
            DynamicTextBlock();
            virtual ~DynamicTextBlock();

            // Dependency Properties
            static property Windows::UI::Xaml::DependencyProperty^ TextProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::String^ Text
            {
                Platform::String^ get();
                void set(Platform::String^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ TextWrappingProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::TextWrapping TextWrapping
            {
                Windows::UI::Xaml::TextWrapping get();
                void set(Windows::UI::Xaml::TextWrapping value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ LineHeightProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double LineHeight
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ LineStackingStrategyProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::LineStackingStrategy LineStackingStrategy
            {
                Windows::UI::Xaml::LineStackingStrategy get();
                void set(Windows::UI::Xaml::LineStackingStrategy value);
            }

        internal:
            static void OnTextPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnTextWrappingPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnLineHeightPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnLineStackingStrategyPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        protected:
            virtual Windows::Foundation::Size MeasureOverride(Windows::Foundation::Size availableSize) override;
            virtual Platform::String^ ReduceText(Platform::String^ text);

            virtual void OnTextChanged(Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            virtual void OnTextWrappingChanged(Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            virtual void OnLineHeightChanged(Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            virtual void OnLineStackingStrategyChanged(Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        private:
            Windows::UI::Xaml::Controls::TextBlock^ m_textBlock;
        };
    }
}
