#include "pch.h"
#include "MenuItemSeparator.h"

using namespace Callisto::Controls;

MenuItemSeparator::MenuItemSeparator()
{
    DefaultStyleKey = MenuItemSeparator::typeid->FullName;
    IsTabStop = false;
}