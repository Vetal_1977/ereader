#include "pch.h"
#include "FlipViewIndicator.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;

FlipViewIndicator::FlipViewIndicator() 
{
    DefaultStyleKey = FlipViewIndicator::typeid->FullName;
}

FlipViewIndicator::~FlipViewIndicator()
{
    if (m_selectionChangedToken.Value != 0 && m_flipView != nullptr)
    {
        m_flipView->SelectionChanged -= m_selectionChangedToken;
    }
}

// Properties

static DependencyProperty^ _flipViwProperty =
    DependencyProperty::Register("FlipView", 
    TypeName(Windows::UI::Xaml::Controls::FlipView::typeid),
    TypeName(FlipViewIndicator::typeid), 
    ref new PropertyMetadata(nullptr, ref new PropertyChangedCallback(&FlipViewIndicator::OnFlipViewPropertyChanged)));

DependencyProperty^ FlipViewIndicator::FlipViewProperty::get()
{
    return _flipViwProperty;
}

Windows::UI::Xaml::Controls::FlipView^ FlipViewIndicator::FlipView::get()
{
    return dynamic_cast<Windows::UI::Xaml::Controls::FlipView^>(GetValue(FlipViewProperty));
}

void FlipViewIndicator::FlipView::set(Windows::UI::Xaml::Controls::FlipView^ value)
{
    SetValue(FlipViewProperty, value);
}

// Event Handlers

void FlipViewIndicator::OnFlipViewPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e)
{
    auto flipViewInidicator = dynamic_cast<FlipViewIndicator^>(d);
   flipViewInidicator->OnFlipViewPropertyChangedCore(e);
}

void FlipViewIndicator::OnFlipViewPropertyChangedCore(DependencyPropertyChangedEventArgs^ e)
{
    if (m_selectionChangedToken.Value != 0 && m_flipView != nullptr)
    {
        m_flipView->SelectionChanged -= m_selectionChangedToken;
    }

    m_flipView = dynamic_cast<Windows::UI::Xaml::Controls::FlipView^>(e->NewValue);

    // this is a special case where ItemsSource is set in code
    // and the associated FlipView's ItemsSource may not be available yet
    // if it isn't available, let's listen for SelectionChanged 
    m_selectionChangedToken = m_flipView->SelectionChanged += ref new SelectionChangedEventHandler(this, &FlipViewIndicator::OnSelectionChanged);

    ItemsSource = m_flipView->ItemsSource;

    // create the element binding source
    auto binding = ref new Binding();
    binding->Mode = BindingMode::TwoWay;
    binding->Source = m_flipView;
    binding->Path = ref new PropertyPath("SelectedItem");

    // set the element binding to change selection when the FlipView changes
    SetBinding(FlipViewIndicator::SelectedItemProperty, binding);
}

void FlipViewIndicator::OnSelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
    ItemsSource = m_flipView->ItemsSource;
}