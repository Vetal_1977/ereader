#include "pch.h"
#include "SettingChangedEventArgs.h"

using namespace Callisto::Controls::SettingsManagement;
using namespace Platform;

SettingChangedEventArgs::SettingChangedEventArgs(String^ settingsName) : m_settingsName(settingsName)
{
}

String^ SettingChangedEventArgs::SettingsName::get()
{
    return m_settingsName;
}

void SettingChangedEventArgs::SettingsName::set(String^ value)
{
    m_settingsName = value;
}