#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class MenuItemBase : public Windows::UI::Xaml::Controls::Control
        {
        internal:
            MenuItemBase();

        public:
            static property Windows::UI::Xaml::DependencyProperty^ MenuTextMarginProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Thickness MenuTextMargin
            {
                Windows::UI::Xaml::Thickness get();
                void set(Windows::UI::Xaml::Thickness value);
            }
        };
    }
}

