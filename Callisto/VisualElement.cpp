#include "pch.h"
#include "VisualElement.h"

using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI;
using namespace std;

struct colorcomp {
    bool operator() (String^ lhs, String^ rhs) const
    {
        wstring left = lhs->Data();
        wstring right = rhs->Data();
        std::transform(left.begin(), left.end(), left.begin(), toupper);
        std::transform(right.begin(), right.end(), right.begin(), toupper);
        return left < right;
    }
};

map<String^, Color, colorcomp> create_map()
{
    std::map<String^, Color, colorcomp> colors;
    colors["AliceBlue"] = Colors::AliceBlue;
    colors["AntiqueWhite"] = Colors::AntiqueWhite;
    colors["Aqua"] = Colors::Aqua;
    colors["Aquamarine"] = Colors::Aquamarine;
    colors["Azure"] = Colors::Azure;
    colors["Beige"] = Colors::Beige;
    colors["Bisque"] = Colors::Bisque;
    colors["Black"] = Colors::Black;
    colors["BlanchedAlmond"] = Colors::BlanchedAlmond;
    colors["Blue"] = Colors::Blue;
    colors["BlueViolet"] = Colors::BlueViolet;
    colors["Brown"] = Colors::Brown;
    colors["BurlyWood"] = Colors::BurlyWood;
    colors["CadetBlue"] = Colors::CadetBlue;
    colors["Chartreuse"] = Colors::Chartreuse;
    colors["Chocolate"] = Colors::Chocolate;
    colors["Coral"] = Colors::Coral;
    colors["CornflowerBlue"] = Colors::CornflowerBlue;
    colors["Cornsilk"] = Colors::Cornsilk;
    colors["Crimson"] = Colors::Crimson;
    colors["Cyan"] = Colors::Cyan;
    colors["DarkBlue"] = Colors::DarkBlue;
    colors["DarkCyan"] = Colors::DarkCyan;
    colors["DarkGoldenrod"] = Colors::DarkGoldenrod;
    colors["DarkGray"] = Colors::DarkGray;
    colors["DarkGreen"] = Colors::DarkGreen;
    colors["DarkKhaki"] = Colors::DarkKhaki;
    colors["DarkMagenta"] = Colors::DarkMagenta;
    colors["DarkOliveGreen"] = Colors::DarkOliveGreen;
    colors["DarkOrange"] = Colors::DarkOrange;
    colors["DarkOrchid"] = Colors::DarkOrchid;
    colors["DarkRed"] = Colors::DarkRed;
    colors["DarkSalmon"] = Colors::DarkSalmon;
    colors["DarkSeaGreen"] = Colors::DarkSeaGreen;
    colors["DarkSlateBlue"] = Colors::DarkSlateBlue;
    colors["DarkSlateGray"] = Colors::DarkSlateGray;
    colors["DarkTurquoise"] = Colors::DarkTurquoise;
    colors["DarkViolet"] = Colors::DarkViolet;
    colors["DeepPink"] = Colors::DeepPink;
    colors["DeepPink"] = Colors::DeepPink;
    colors["DeepSkyBlue"] = Colors::DeepSkyBlue;
    colors["DimGray"] = Colors::DimGray;
    colors["DodgerBlue"] = Colors::DodgerBlue;
    colors["Gainsboro"] = Colors::Gainsboro;
    colors["GhostWhite"] = Colors::GhostWhite;
    colors["Gold"] = Colors::Gold;
    colors["Goldenrod"] = Colors::Goldenrod;
    colors["Gray"] = Colors::Gray;
    colors["Green"] = Colors::Green;
    colors["GreenYellow"] = Colors::GreenYellow;
    colors["Honeydew"] = Colors::Honeydew;
    colors["HotPink"] = Colors::HotPink;
    colors["IndianRed"] = Colors::IndianRed;
    colors["Indigo"] = Colors::Indigo;
    colors["Ivory"] = Colors::Ivory;
    colors["Khaki"] = Colors::Khaki;
    colors["Lavender"] = Colors::Lavender;
    colors["LavenderBlush"] = Colors::LavenderBlush;
    colors["LawnGreen"] = Colors::LawnGreen;
    colors["LemonChiffon"] = Colors::LemonChiffon;
    colors["LightBlue"] = Colors::LightBlue;
    colors["LightCoral"] = Colors::LightCoral;
    colors["LightCyan"] = Colors::LightCyan;
    colors["LightGoldenrodYellow"] = Colors::LightGoldenrodYellow;
    colors["LightGray"] = Colors::LightGray;
    colors["LightGreen"] = Colors::LightGreen;
    colors["LightPink"] = Colors::LightPink;
    colors["LightSalmon"] = Colors::LightSalmon;
    colors["LightSeaGreen"] = Colors::LightSeaGreen;
    colors["LightSkyBlue"] = Colors::LightSkyBlue;
    colors["LightSlateGray"] = Colors::LightSlateGray;
    colors["LightSteelBlue"] = Colors::LightSteelBlue;
    colors["LightYellow"] = Colors::LightYellow;
    colors["Lime"] = Colors::Lime;
    colors["LimeGreen"] = Colors::LimeGreen;
    colors["Linen"] = Colors::Linen;
    colors["Magenta"] = Colors::Magenta;
    colors["Maroon"] = Colors::Maroon;
    colors["MediumAquamarine"] = Colors::MediumAquamarine;
    colors["MediumBlue"] = Colors::MediumBlue;
    colors["MediumOrchid"] = Colors::MediumOrchid;
    colors["MediumPurple"] = Colors::MediumPurple;
    colors["MediumSeaGreen"] = Colors::MediumSeaGreen;
    colors["MediumSlateBlue"] = Colors::MediumSlateBlue;
    colors["MediumSpringGreen"] = Colors::MediumSpringGreen;
    colors["MediumTurquoise"] = Colors::MediumTurquoise;
    colors["MediumVioletRed"] = Colors::MediumVioletRed;
    colors["MidnightBlue"] = Colors::MidnightBlue;
    colors["MintCream"] = Colors::MintCream;
    colors["MistyRose"] = Colors::MistyRose;
    colors["Moccasin"] = Colors::Moccasin;
	colors["NavajoWhite"] = Colors::NavajoWhite;
	colors["Navy"] = Colors::Navy;
	colors["OldLace"] = Colors::OldLace;
	colors["Olive"] = Colors::Olive;
	colors["OliveDrab"] = Colors::OliveDrab;
	colors["Orange"] = Colors::Orange;
	colors["OrangeRed"] = Colors::OrangeRed;
	colors["Orchid"] = Colors::Orchid;
	colors["PaleGoldenrod"] = Colors::PaleGoldenrod;
	colors["PaleGreen"] = Colors::PaleGreen;
	colors["PaleTurquoise"] = Colors::PaleTurquoise;
	colors["PaleVioletRed"] = Colors::PaleVioletRed;
	colors["PapayaWhip"] = Colors::PapayaWhip;
	colors["PeachPuff"] = Colors::PeachPuff;
	colors["Peru"] = Colors::Peru;
	colors["Pink"] = Colors::Pink;
	colors["Plum"] = Colors::Plum;
	colors["PowderBlue"] = Colors::PowderBlue;
	colors["Purple"] = Colors::Purple;
	colors["Red"] = Colors::Red;
	colors["RosyBrown"] = Colors::RosyBrown;
	colors["RoyalBlue"] = Colors::RoyalBlue;
	colors["SaddleBrown"] = Colors::SaddleBrown;
	colors["Salmon"] = Colors::Salmon;
	colors["SandyBrown"] = Colors::SandyBrown;
	colors["SeaGreen"] = Colors::SeaGreen;
	colors["SeaShell"] = Colors::SeaShell;
	colors["Sienna"] = Colors::Sienna;
	colors["Silver"] = Colors::Silver;
	colors["SkyBlue"] = Colors::SkyBlue;
	colors["SlateBlue"] = Colors::SlateBlue;
	colors["SlateGray"] = Colors::SlateGray;
	colors["Snow"] = Colors::Snow;
	colors["SpringGreen"] = Colors::SpringGreen;
	colors["SteelBlue"] = Colors::SteelBlue;
	colors["Tan"] = Colors::Tan;
	colors["Teal"] = Colors::Teal;
	colors["Thistle"] = Colors::Thistle;
	colors["Tomato"] = Colors::Tomato;
	colors["Transparent"] = Colors::Transparent;
	colors["Turquoise"] = Colors::Turquoise;
	colors["Violet"] = Colors::Violet;
	colors["Wheat"] = Colors::Wheat;
	colors["White"] = Colors::White;
	colors["WhiteSmoke"] = Colors::WhiteSmoke;
	colors["Yellow"] = Colors::Yellow;
	colors["YellowGreen"] = Colors::YellowGreen;
	
	return colors;
}

static map<String^, Color, colorcomp> m_colors(create_map()); 

String^ VisualElement::DisplayName::get()
{
    return m_displayName;
}

void VisualElement::DisplayName::set(String^ value)
{
    m_displayName = value;
}

String^ VisualElement::Description::get()
{
    return m_description;
}

void VisualElement::Description::set(String^ value)
{
    m_description = value;
}

Uri^ VisualElement::LogoUri::get()
{
    return m_logoUri;
}

void VisualElement::LogoUri::set(Uri^ value)
{
    m_logoUri = value;
}

Uri^ VisualElement::SmallLogoUri::get()
{
    return m_smallLogoUri;
}

void VisualElement::SmallLogoUri::set(Uri^ value)
{
    m_smallLogoUri = value;
}

String^ VisualElement::BackgroundColorAsString::get()
{
    return m_backgroundColorAsString;
}

void VisualElement::BackgroundColorAsString::set(String^ value)
{
    m_backgroundColorAsString = value;
}

Color VisualElement::BackgroundColor::get()
{
    wstring backGroundColor = m_backgroundColorAsString->Data();
    auto found = backGroundColor.find(L"#");

    if (found == wstring::npos)
    { 
        // may be a named color, attempt that first
        auto foundColor = m_colors[m_backgroundColorAsString];
        return foundColor;
    }

    wstring hexValue = backGroundColor.replace(found, 1, L"");

    // some loose validation (not bullet-proof)
    if (hexValue.length() < 6)
    {
        throw ref new Platform::InvalidArgumentException("This does not appear to be a proper hex color number");
    }

    byte a = 255;
    byte r = 255;
    byte g = 255;
    byte b = 255;

    int startPosition = 0;
    wstringstream val;
    // the case where alpha is provided
    if (hexValue.length() == 8)
    {
        a = ParseHexValue(hexValue.substr(0, 2));
        startPosition = 2;
    }

    r = ParseHexValue(hexValue.substr(startPosition, 2));
    g = ParseHexValue(hexValue.substr(startPosition + 2, 2));
    b = ParseHexValue(hexValue.substr(startPosition + 4, 2));

    Color color;
    color.A = a;
    color.B = b;
    color.R = r;
    color.G = g;

    return color;
}

byte VisualElement::ParseHexValue(std::wstring hexValue)
{
    unsigned int intVal;
    wstringstream val;
    
    val << std::hex;
    val << hexValue.substr(0, 2);
    val >> intVal;

    byte b = intVal;

    return b;
}