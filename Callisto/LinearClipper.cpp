#include "pch.h"
#include "LinearClipper.h"

using namespace Callisto::Controls::Primitives;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml;

static DependencyProperty^ _expandDirectionProperty =
    DependencyProperty::Register("ExpandDirection", 
    TypeName(Callisto::Controls::ExpandDirection::typeid), 
    TypeName(LinearClipper::typeid), 
    ref new PropertyMetadata(Callisto::Controls::ExpandDirection::Right, ref new PropertyChangedCallback(&LinearClipper::OnExpandDirectionPropertyChanged)));

DependencyProperty^ LinearClipper::ExpandDirectionProperty::get()
{
    return _expandDirectionProperty;
}

Callisto::Controls::ExpandDirection LinearClipper::ExpandDirection::get()
{
    return static_cast<Callisto::Controls::ExpandDirection>(GetValue(ExpandDirectionProperty));
}

void LinearClipper::ExpandDirection::set(Callisto::Controls::ExpandDirection value)
{
    SetValue(ExpandDirectionProperty, value);
}

void LinearClipper::OnExpandDirectionPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e)
{
    auto linearClipper = dynamic_cast<LinearClipper^>(d);
    if (nullptr != linearClipper)
    {
        linearClipper->OnExpandDirectionChanged(static_cast<Callisto::Controls::ExpandDirection>(e->OldValue), static_cast<Callisto::Controls::ExpandDirection>(e->NewValue));
    }
}

void LinearClipper::OnExpandDirectionChanged(Callisto::Controls::ExpandDirection oldValue, Callisto::Controls::ExpandDirection newValue)
{
    ClipContent();
}

void LinearClipper::ClipContent()
{
    if (ExpandDirection == Callisto::Controls::ExpandDirection::Right)
    {
        float width = RenderSize.Width * RatioVisible;

        Rect rect(0.0f, 0.0f, width, RenderSize.Height);
        RectangleGeometry^ rectGeo = ref new RectangleGeometry();
        rectGeo->Rect = rect;
        Clip = rectGeo;
    }
    else if (ExpandDirection == Callisto::Controls::ExpandDirection::Left)
    {
        float width = RenderSize.Width * RatioVisible;
        float rightSide = RenderSize.Width - width;

        Rect rect(rightSide, 0.0f, width, RenderSize.Height);
        RectangleGeometry^ rectGeo = ref new RectangleGeometry();
        rectGeo->Rect = rect;
        Clip = rectGeo;
    }
    else if (ExpandDirection == Callisto::Controls::ExpandDirection::Up)
    {
        float height = RenderSize.Height * RatioVisible;
        float bottom = RenderSize.Height - height;

        Rect rect(0.0f, bottom, RenderSize.Width, height);
        RectangleGeometry^ rectGeo = ref new RectangleGeometry();
        rectGeo->Rect = rect;
        Clip = rectGeo;
    }
    else if (ExpandDirection == Callisto::Controls::ExpandDirection::Down)
    {
        float height = RenderSize.Height * RatioVisible;

        Rect rect(0.0f, 0.0f, RenderSize.Width, height);
        RectangleGeometry^ rectGeo = ref new RectangleGeometry();
        rectGeo->Rect = rect;
        Clip = rectGeo;
    }
}