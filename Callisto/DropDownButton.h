#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class DropDownButton sealed : public Windows::UI::Xaml::Controls::Button
        {
        public:
            DropDownButton();
            virtual ~DropDownButton();

        protected:
            virtual void OnApplyTemplate() override;

        private:
            Windows::UI::Xaml::Controls::TextBlock^ m_arrowGlyph;
        };
    }
}
