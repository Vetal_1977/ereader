#include "pch.h"
#include "ColorBrightnessConverter.h"

using namespace Callisto::Converters;
using namespace Platform;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI;
using namespace std;

Object^ ColorBrightnessConverter::Convert(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    // parameter should be the brightness factor in double.
    double factor = 0.0;
    auto str = dynamic_cast<String^>(parameter);
    wstring data = str->Data();
    wstringstream strstream;
    strstream << data;
    strstream >> factor;

    //auto factor = static_cast<float>(parameter);
    auto brush = dynamic_cast<SolidColorBrush^>(value);
    
    Color newColor;
    newColor.A = brush->Color.A;
    newColor.B = static_cast<byte>(brush->Color.B * factor);
    newColor.G = static_cast<byte>(brush->Color.G * factor);
    newColor.R = static_cast<byte>(brush->Color.R * factor);

    return ref new SolidColorBrush(newColor);
}

Object^ ColorBrightnessConverter::ConvertBack(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    return value;
}
