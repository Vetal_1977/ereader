#include "pch.h"
#include "NumericUpDown.h"
#include "VisualStates.h"
#include <cctype>

using namespace Callisto::Controls;
using namespace Callisto::Controls::Common;
using namespace Windows::UI::Xaml;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Input;
using namespace std;

#undef min
#undef max

NumericUpDown::NumericUpDown() : m_delay(500), m_interval(100), m_requestedMax(100), m_initialMax(100), m_requestedInc(1), m_initialInc(1)
{
    DefaultStyleKey = NumericUpDown::typeid->FullName;
    m_lostFocusEvent = LostFocus += ref new RoutedEventHandler(this, &NumericUpDown::OnLostFocus);
    m_gotFocusEvent = GotFocus += ref new RoutedEventHandler(this, &NumericUpDown::OnGotFocus);
    m_textChangedEvent = TextChanged += ref new TextChangedEventHandler(this, &NumericUpDown::OnTextChanged);
}

NumericUpDown::~NumericUpDown()
{
    LostFocus -= m_lostFocusEvent;
    GotFocus -= m_gotFocusEvent;
    TextChanged -= m_textChangedEvent;

    if (m_incrementButton != nullptr)
    {
        m_incrementButton->SizeChanged -= m_incrementButtonSizeChangedEvent;
        m_incrementButton->Click -= m_incrementClickedEvent;
    }

    if (m_decrementButton != nullptr)
    {
        m_decrementButton->SizeChanged -= m_decrementButtonSizeChangedEvent;
        m_decrementButton->Click -= m_decrementClickedEvent;
    }
}

// Properties

int NumericUpDown::Delay::get()
{
    return m_delay;
}

void NumericUpDown::Delay::set(int value)
{
    m_delay = value;
    m_incrementButton->Delay = m_delay;
    m_decrementButton->Delay = m_delay;
}

int NumericUpDown::Interval::get()
{
    return m_interval;
}

void NumericUpDown::Interval::set(int value)
{
    m_interval = value;
    m_incrementButton->Interval = m_interval;
    m_decrementButton->Interval = m_interval;
}

static DependencyProperty^ m_maximumProperty =
    DependencyProperty::Register("Maximum", 
    TypeName(double::typeid), 
    TypeName(NumericUpDown::typeid), 
    ref new PropertyMetadata(100.0, ref new PropertyChangedCallback(&NumericUpDown::OnMaximumPropertyChanged)));

DependencyProperty^ NumericUpDown::MaximumProperty::get()
{
    return m_maximumProperty;
}

double NumericUpDown::Maximum::get()
{
    return static_cast<double>(GetValue(MaximumProperty));
}

void NumericUpDown::Maximum::set(double value)
{
    SetValue(MaximumProperty, value);
}

static DependencyProperty^ m_minimumProperty =
    DependencyProperty::Register("Minimum", 
    TypeName(double::typeid), 
    TypeName(NumericUpDown::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&NumericUpDown::OnMinimumPropertyChanged)));

DependencyProperty^ NumericUpDown::MinimumProperty::get()
{
    return m_minimumProperty;
}

double NumericUpDown::Minimum::get()
{
    return static_cast<double>(GetValue(MinimumProperty));
}

void NumericUpDown::Minimum::set(double value)
{
    SetValue(MinimumProperty, value);
}

static DependencyProperty^ m_valueProperty =
    DependencyProperty::Register("Value", 
    TypeName(double::typeid), 
    TypeName(NumericUpDown::typeid),
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&NumericUpDown::OnValuePropertyChanged)));

DependencyProperty^ NumericUpDown::ValueProperty::get()
{
    return m_valueProperty;
}

double NumericUpDown::Value::get()
{
    return static_cast<double>(GetValue(ValueProperty));
}

void NumericUpDown::Value::set(double value)
{
    SetValue(ValueProperty, value);
}

static DependencyProperty^ m_decimalPlacesProperty =
    DependencyProperty::Register("DecimalPlaces", 
    TypeName(int::typeid), 
    TypeName(NumericUpDown::typeid), 
    ref new PropertyMetadata(ref new Box<int>(0), ref new PropertyChangedCallback(&NumericUpDown::OnDecimalPlacesPropertyChanged)));

DependencyProperty^ NumericUpDown::DecimalPlacesProperty::get()
{
    return m_decimalPlacesProperty;
}

int NumericUpDown::DecimalPlaces::get()
{
    return static_cast<int>(GetValue(DecimalPlacesProperty));
}

void NumericUpDown::DecimalPlaces::set(int value)
{
    SetValue(DecimalPlacesProperty, value);
}

static DependencyProperty^ m_incrementProperty =
    DependencyProperty::Register("Increment", 
    TypeName(double::typeid), 
    TypeName(NumericUpDown::typeid), 
    ref new PropertyMetadata(1.0, ref new PropertyChangedCallback(&NumericUpDown::OnIncrementPropertyChanged)));

DependencyProperty^ NumericUpDown::IncrementProperty::get()
{
    return m_incrementProperty;
}

double NumericUpDown::Increment::get()
{
    return static_cast<double>(GetValue(IncrementProperty));
}

void NumericUpDown::Increment::set(double value)
{
    SetValue(IncrementProperty, value);
}

// Event Handlers

void NumericUpDown::OnMaximumPropertyChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    ValidateDoubleValue(d, e);
    auto nud = dynamic_cast<NumericUpDown^>(d);
    double oldValue = static_cast<double>(e->OldValue);
    double newValue = static_cast<double>(e->NewValue);
    nud->OnMaximumChanged(oldValue, newValue);
}

void NumericUpDown::OnMaximumChanged(double oldValue, double newValue)
{
    if (m_levelsFromRootCall == 0)
    {
        m_requestedMax = newValue;
        m_initialMax = oldValue;
        m_initialVal = Value;
    }
    m_levelsFromRootCall++;

    CoerceMaximum();
    CoerceValue();

    m_levelsFromRootCall--;
    if (m_levelsFromRootCall == 0)
    {
        SetValidIncrementDirection();
    }
}

void NumericUpDown::OnMinimumPropertyChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    ValidateDoubleValue(d, e);
    auto nud = dynamic_cast<NumericUpDown^>(d);
    double oldValue = static_cast<double>(e->OldValue);
    double newValue = static_cast<double>(e->NewValue);
    nud->OnMinimumChanged(oldValue, newValue);
}

void NumericUpDown::OnMinimumChanged(double oldValue, double newValue)
{
    if (m_levelsFromRootCall == 0)
    {
        m_requestedMin = newValue;
        m_initialMin = oldValue;
        m_initialMax = Maximum;
        m_initialVal = Value;

        m_levelsFromRootCall++;
        if (Minimum != m_requestedMin)
        {
            Minimum = m_requestedMin;
        }
        m_levelsFromRootCall--;
    }
    m_levelsFromRootCall++;

    CoerceMaximum();
    CoerceValue();

    m_levelsFromRootCall--;
    if (m_levelsFromRootCall == 0)
    {
        SetValidIncrementDirection();
    }
}

void NumericUpDown::OnValuePropertyChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    auto nud = dynamic_cast<NumericUpDown^>(d);
    nud->OnValueChanged();
    nud->SetValidIncrementDirection();
}

void NumericUpDown::OnValueChanged()
{
    SetTextBoxText();
}

void NumericUpDown::OnDecimalPlacesPropertyChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    auto nud = dynamic_cast<NumericUpDown^>(d);
    int oldValue = static_cast<int>(e->OldValue);
    int newValue = static_cast<int>(e->NewValue);
    nud->OnDecimalPlacesChanged(oldValue, newValue);
}

void NumericUpDown::OnDecimalPlacesChanged(int oldValue, int newValue)
{
    int decimalPlaces = newValue;
    if (decimalPlaces < 0 || decimalPlaces > 15)
    {
        m_levelsFromRootCall++;
        DecimalPlaces = oldValue;
        m_levelsFromRootCall--;

        throw ref new InvalidArgumentException("Not a valid Double value");
    }
    m_levelsFromRootCall++;
    SetTextBoxText();
    m_levelsFromRootCall--;
}

void NumericUpDown::OnIncrementPropertyChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    ValidateIncrementValue(d, e);
    auto nud = dynamic_cast<NumericUpDown^>(d);
    double oldValue = static_cast<double>(e->OldValue);
    double newValue = static_cast<double>(e->NewValue);
    nud->OnIncrementChanged(oldValue, newValue);
}

void NumericUpDown::OnIncrementChanged(double oldValue, double newValue)
{
    if (m_levelsFromRootCall == 0)
    {
        m_requestedInc = newValue;
        m_initialInc = oldValue;

        m_levelsFromRootCall++;
        if (Increment != m_requestedInc)
        {
            Increment = m_requestedInc;
        }
        m_levelsFromRootCall--;
    }
}

void NumericUpDown::OnLostFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
    ProcessUserInput();
}

void NumericUpDown::OnGotFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
    if (Text != nullptr)
    {
        if (SelectionLength == 0 && Text != nullptr)
        {
            Select(0, Text->Length());
        }
    }
}

void NumericUpDown::OnTextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
    ProcessUserInput();
}

void NumericUpDown::OnPartButtonSizeChanged(Object^ sender, SizeChangedEventArgs^ e)
{
    auto b = dynamic_cast<RepeatButton^>(sender);
    if (b != nullptr)
    {
        double currentWidth = b->Width;
        
        if (currentWidth != e->NewSize.Height)
        {
            b->Width = e->NewSize.Height;
        }
    }
}

void NumericUpDown::OnIncrementClicked(Object^ sender, RoutedEventArgs^ e)
{
    DoIncrement();
}

void NumericUpDown::OnDecrementClicked(Object^ sender, RoutedEventArgs^ e)
{
    DoDecrement();
}

void NumericUpDown::OnKeyDown(KeyRoutedEventArgs^ e)
{
    TextBox::OnKeyDown(e);

    if (e->Handled)
    {
        return;
    }

    switch (e->Key)
    {
    case Windows::System::VirtualKey::Up:
        DoIncrement();
        e->Handled = true;
        break;
    case Windows::System::VirtualKey::Down:
        DoDecrement();
        e->Handled = true;
        break;
    }
}

void NumericUpDown::OnApplyTemplate()
{
    TextBox::OnApplyTemplate();

    m_incrementButton = dynamic_cast<RepeatButton^>(GetTemplateChild("PART_IncrementButton"));
    m_decrementButton = dynamic_cast<RepeatButton^>(GetTemplateChild("PART_DecrementButton"));

    if (m_incrementButton != nullptr)
    {
        m_incrementButtonSizeChangedEvent = m_incrementButton->SizeChanged += ref new SizeChangedEventHandler(this, &NumericUpDown::OnPartButtonSizeChanged);
        m_incrementButton->Delay = m_delay;
        m_incrementButton->Interval = m_interval;
        m_incrementClickedEvent = m_incrementButton->Click +=  ref new RoutedEventHandler(this, &NumericUpDown::OnIncrementClicked);
    }
    if (m_decrementButton != nullptr)
    {
        m_decrementButtonSizeChangedEvent = m_decrementButton->SizeChanged += ref new SizeChangedEventHandler(this, &NumericUpDown::OnPartButtonSizeChanged);
        m_decrementButton->Delay = m_delay;
        m_decrementButton->Interval = m_interval;
        m_decrementClickedEvent = m_decrementButton->Click +=  ref new RoutedEventHandler(this, &NumericUpDown::OnDecrementClicked);
    }

    SetValidIncrementDirection();
}

// Private Methods

void NumericUpDown::ProcessUserInput()
{
    /*if (Text != nullptr)
    {*/
        if (m_text != Text)
        {
            // retain caret position
            unsigned int caretPosition = SelectionStart;

            m_text = Text;
            ApplyValue(m_text);

            if (caretPosition < Text->Length())
            {
                // set back caret position.
                SelectionStart = caretPosition;
            }
        }
    //}
}

bool is_number(const std::wstring& str)
{
    for (unsigned int i = 0; i < str.length(); i++) {
        if (!std::isdigit(str[i]))
            return false;
    }

    return true;
}

void NumericUpDown::ApplyValue(Platform::String^ text)
{
    if (text != nullptr || text != "")
    {
        wstring data = text->Data();
        wstringstream strstream;
        double regularParsedValue = 0.0;
        strstream << std::dec;
        strstream << data;
        strstream >> regularParsedValue;

        if (is_number(data) && regularParsedValue >= Minimum && regularParsedValue <= Maximum)
        {
            Value = regularParsedValue;
        }
        SetTextBoxText();
    }
    else
    {
        Value = Minimum;
        Text = "";
    }
}

void NumericUpDown::SetTextBoxText()
{
    /*auto txt = Text;
    if (txt != nullptr)
    {*/
        m_text = FormatValue();
        Text = m_text;

        SelectionStart = m_text->Length();
    //}
}

void NumericUpDown::SetValidIncrementDirection()
{
    double value = Value;
    double max = Maximum;
    double min = Minimum;
    stringstream ss;
    int places = DecimalPlaces;
    ss << setprecision(places) << std::fixed << value;
    ss >> value; 
    ss.clear();
    ss << setprecision(places) << std::fixed << max;
    ss >> max; 
    ss.clear();
    ss << setprecision(places) << std::fixed << min;
    ss >> min; 

    if (Value < Maximum)
    {
        VisualStateManager::GoToState(this, "IncrementEnabled", true);
        m_canIncrement = true;
    }
    if (Value > Minimum)
    {
        VisualStateManager::GoToState(this, "DecrementEnabled", true);
        m_canDecrement = true;
    }
    if (value == max)
    {
        VisualStateManager::GoToState(this, "IncrementDisabled", true);
        m_canIncrement = false;
    }
    if (value == min)
    {
        VisualStateManager::GoToState(this, "DecrementDisabled", true);
        m_canDecrement = false;
    }
}

Platform::String^ NumericUpDown::FormatValue()
{
    wstring data;
    wstringstream strstream;
    double value = Value;
    int places = DecimalPlaces;
    strstream << setprecision(places) << std::fixed << value;
    strstream >> data;

    return ref new String(data.c_str());
}

void NumericUpDown::ValidateDoubleValue(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    NumericUpDown::ValidateDoubleValue(d, e->Property, e->OldValue, e->NewValue);
}

void NumericUpDown::ValidateDoubleValue(DependencyObject^ d, DependencyProperty^ property, Object^ oldValue, Object^ newValue)
{
    auto nud = dynamic_cast<NumericUpDown^>(d);
    double number;
    if (!NumericUpDown::IsValidDoubleValue(newValue, &number))
    {
        // revert back to old value
        nud->m_levelsFromRootCall++;
        nud->SetValue(property, oldValue);
        nud->m_levelsFromRootCall--;
        
        throw ref new InvalidArgumentException("Not a valid Double value");
    }
}

bool NumericUpDown::IsValidDoubleValue(Platform::Object^ value, double* number)
{
    *number = static_cast<double>(value);
	 double max = numeric_limits<double>::max();
    double min = numeric_limits<double>::min();
    return *number != numeric_limits<double>::quiet_NaN() && *number != numeric_limits<double>::infinity()
        && *number <= max && *number >= min;
}

void NumericUpDown::ValidateIncrementValue(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
    auto nud = dynamic_cast<NumericUpDown^>(d);
    double number;
    if (!NumericUpDown::IsValidDoubleValue(e->NewValue, &number) || number <= 0)
    {
        nud->m_levelsFromRootCall++;
        nud->SetValue(e->Property, e->OldValue);
        nud->m_levelsFromRootCall--;

        throw ref new InvalidArgumentException("Not a valid Double value");
    }
}

void NumericUpDown::CoerceMaximum()
{
    double minimum = Minimum;
    double maximum = Maximum;

    if (m_requestedMax != maximum)
    {
        if (m_requestedMax >= minimum)
        {
            SetValue(MaximumProperty, m_requestedMax);
        }
        else if (maximum != minimum)
        {
            SetValue(MaximumProperty, minimum);
        }
    }
    else if (maximum < minimum)
    {
        SetValue(MaximumProperty, minimum);
    }
}

void NumericUpDown::CoerceValue()
{
    double minimum = Minimum;
    double maximum = Maximum;
    double value = Value;

    if (m_requestedVal != value)
    {
        if (m_requestedVal >= minimum && m_requestedVal <= maximum)
        {
            SetValue(ValueProperty, m_requestedVal);
        }
        else if (m_requestedVal < minimum && value != minimum)
        {
            SetValue(ValueProperty, minimum);
        }
        else if (m_requestedVal > maximum && value != maximum)
        {
            SetValue(ValueProperty, maximum);
        }
    }
    else if (value < minimum)
    {
        SetValue(ValueProperty, minimum);
    }
    else if (value > maximum)
    {
        SetValue(ValueProperty, maximum);
    }
}

void NumericUpDown::DoIncrement()
{
    if (m_canIncrement)
    {
        Value = Value + Increment;
        m_requestedVal = Value;
    }
}

void NumericUpDown::DoDecrement()
{
    if (m_canDecrement)
    {
        Value = Value - Increment;
        m_requestedVal = Value;
    }
}