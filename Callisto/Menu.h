#pragma once

namespace Callisto
{
    namespace Controls
    {
        ref class MenuItem;

        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class Menu sealed : public Windows::UI::Xaml::Controls::Control
        {
        public:
            Menu();
            virtual ~Menu();

            property Windows::Foundation::Collections::IObservableVector<Windows::UI::Xaml::Controls::Control^>^ Items
            {
                Windows::Foundation::Collections::IObservableVector<Windows::UI::Xaml::Controls::Control^>^ get();
            }

        protected:
            virtual void OnApplyTemplate() override;
            //virtual void OnKeyDown(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ args) override;

        private:
            Platform::Collections::Vector<Windows::UI::Xaml::Controls::Control^>^ m_items;
            std::map<Windows::UI::Xaml::Controls::Control^, Windows::Foundation::EventRegistrationToken> m_tokenMap;
            Windows::UI::Xaml::Controls::ItemsControl^ m_itemContainerList;
            Windows::Foundation::EventRegistrationToken m_itemChangeToken;
            Windows::Foundation::EventRegistrationToken m_loadedToken;
            Windows::Foundation::EventRegistrationToken m_keyDownToken;

            struct MenuItemEvent
            {
                Windows::Foundation::EventRegistrationToken Token;
                Callisto::Controls::MenuItem^ MenuItem;
            };

            std::vector<MenuItemEvent> m_itemTokens;

            void OnItemsCollectionChanged(Windows::Foundation::Collections::IObservableVector<Windows::UI::Xaml::Controls::Control^>^ sender, Windows::Foundation::Collections::IVectorChangedEventArgs^ e);
            void OnLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);	
            void OnMenuKeyDown(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ args);
            void OnMenuItemSelected(Platform::Object^ sender, Windows::UI::Xaml::Input::TappedRoutedEventArgs^ args);

            unsigned int GetNextItemIndex(int startIndex, bool ascending);

            void ChangeFocusedItem(bool ascendIndex);
            void PageFocusedItem(bool top);
        };
    }
}

