#include "pch.h"
#include "RatingItem.h"
#include "Rating.h"
#include "VisualStates.h"
#include "InteractionHelper.h"

using namespace Callisto::Controls;
using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace std;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;

Platform::String^ const StateFilled = "Filled";
Platform::String^ const StateEmpty = "Empty";
Platform::String^ const StatePartial = "Partial";
//Platform::String^ const GroupFill = "FillStates";

RatingItem::RatingItem()
{
    DefaultStyleKey = RatingItem::typeid->FullName;
    m_interactionHelper = std::make_shared<InteractionHelper>(this);
}

// Properties

static DependencyProperty^ m_displayValueProperty =
    DependencyProperty::Register("DisplayValue", 
    TypeName(double::typeid),
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&RatingItem::OnDisplayValuePropertyChanged)));

DependencyProperty^ RatingItem::DisplayValueProperty::get()
{
    return m_displayValueProperty;
}

double RatingItem::DisplayValue::get()
{
    return static_cast<double>(GetValue(DisplayValueProperty));
}

void RatingItem::DisplayValue::set(double value)
{
    m_settingDisplayValue = true;
    try
    {
        SetValue(DisplayValueProperty, value);
    }
    catch(...)
    {
        m_settingDisplayValue = false;
    }
}


static DependencyProperty^ m_pointerOverFillProperty =
    DependencyProperty::Register("PointerOverFill", 
    TypeName(SolidColorBrush::typeid), 
    TypeName(Rating::typeid), 
    nullptr);

DependencyProperty^ RatingItem::PointerOverFillProperty::get()
{
    return m_pointerOverFillProperty;
}

SolidColorBrush^ RatingItem::PointerOverFill::get()
{
    return static_cast<SolidColorBrush^>(GetValue(PointerOverFillProperty));
}

void RatingItem::PointerOverFill::set(SolidColorBrush^ value)
{
    SetValue(PointerOverFillProperty, value);
}

static DependencyProperty^ m_pointerPressedFillProperty =
    DependencyProperty::Register("PointerPressedFill", 
    TypeName(SolidColorBrush::typeid), 
    TypeName(Rating::typeid), 
    nullptr);

DependencyProperty^ RatingItem::PointerPressedFillProperty::get()
{
    return m_pointerPressedFillProperty;
}

SolidColorBrush^ RatingItem::PointerPressedFill::get()
{
    return static_cast<SolidColorBrush^>(GetValue(PointerPressedFillProperty));
}

void RatingItem::PointerPressedFill::set(SolidColorBrush^ value)
{
    SetValue(PointerPressedFillProperty, value);
}

static DependencyProperty^ m_valueProperty =
    DependencyProperty::Register("Value", 
    TypeName(double::typeid), 
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(0.0));

DependencyProperty^ RatingItem::ValueProperty::get()
{
    return m_valueProperty;
}

double RatingItem::Value::get()
{
    return static_cast<double>(GetValue(ValueProperty));
}

void RatingItem::Value::set(double value)
{
    SetValue(ValueProperty, value);
}

Rating^ RatingItem::ParentRating::get()
{
    return m_parentRating.Resolve<Rating>();
}

void RatingItem::ParentRating::set(Rating^ value)
{
    m_parentRating = value;
}

// Event Handlers

void RatingItem::OnDisplayValuePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto source = dynamic_cast<RatingItem^>(sender);
    source->OnDisplayValueChanged(static_cast<double>(e->OldValue), static_cast<double>(e->NewValue));
}

void RatingItem::OnDisplayValueChanged(double oldValue, double newValue)
{
    if (!m_settingDisplayValue)
    {
        m_settingDisplayValue = true;

        DisplayValue = oldValue;

        throw ref new InvalidArgumentException("Invalid attempt to change read-only property 'DisplayValue'.");
    }
    else
    {
        vector<Platform::String^> states;
        if (newValue <= 0.0)
        {
            states.push_back(StateEmpty);
            VisualStates::GoToState(this, true, states);
        }
        else if (newValue >= 1.0)
        {
            states.push_back(StateFilled);
            VisualStates::GoToState(this, true, states);
        }
        else
        {
            states.push_back(StatePartial);
            VisualStates::GoToState(this, true, states);
        }
    }
}

void RatingItem::OnPointerPressed(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerPressed(e))
    {
        m_interactionHelper->OnPointerPressedBase();
    }
    Control::OnPointerPressed(e);
}

void RatingItem::OnPointerReleased(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerPressed(e))
    {
        m_interactionHelper->OnPointerReleasedBase();
    }
    Control::OnPointerReleased(e);
}

void RatingItem::OnPointerEntered(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerEnter(e))
    {
        m_interactionHelper->UpdateVisualStateBase(true);
    }
    Control::OnPointerEntered(e);
}

void RatingItem::OnPointerExited(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerEnter(e))
    {
        m_interactionHelper->UpdateVisualStateBase(true);
    }
    Control::OnPointerEntered(e);
}

void RatingItem::OnTapped(Windows::UI::Xaml::Input::TappedRoutedEventArgs^ e)
{
    Control::OnTapped(e);
}

// Methods

void RatingItem::UpdateVisualState(bool useTransitions)
{
    m_interactionHelper->UpdateVisualStateBase(useTransitions);
}

void RatingItem::SelectValue()
{
    if (!IsEnabled)
    {
        Value = 1.0;
        OnTapped(nullptr);
    }
}