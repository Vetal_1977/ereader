#include "pch.h"
#include "AppSettings.h"
#include "SettingChangedEventArgs.h"
#include "AppManifestExtension.h"
#include "VisualElement.h"
#include "SettingsCommandInfo.h"

using namespace Callisto::Controls::Common;
using namespace Callisto::Controls::SettingsManagement;
using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::Foundation;
using namespace Windows::UI::ApplicationSettings;
using namespace Windows::UI::Popups;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI;
using namespace concurrency;
using namespace std;

AppSettings::AppSettings()
{
	m_commandsRequestedToken = SettingsPane::GetForCurrentView()->CommandsRequested += 
		ref new TypedEventHandler<SettingsPane^, SettingsPaneCommandsRequestedEventArgs^>(this, &AppSettings::OnCommandsRequested);
}

AppSettings::~AppSettings()
{
    SettingsPane::GetForCurrentView()->CommandsRequested -= m_commandsRequestedToken;
}

EventRegistrationToken AppSettings::SettingChanged::add(EventHandler<SettingChangedEventArgs^>^ e)
{
    m_settingChangedObserved = true;
    return m_settingChangedEvent += e;
}

void AppSettings::SettingChanged::remove(Windows::Foundation::EventRegistrationToken t)
{
    m_settingChangedObserved = false;
    m_settingChangedEvent -= t;
}

void AppSettings::AddCommand(Windows::UI::Xaml::Controls::SettingsFlyout^ settingsFlyout, Platform::String^ headerKey)
{
	wstring key = headerKey->Data();
    wstring findString = L" ";
    auto found = key.find(findString);
    while (found != wstring::npos)
    {
        key.replace(found, findString.length(), L"");
        found = key.find(findString, found + 1);
    }

    String^ keyStr = ref new String(key.c_str());
    if (m_commands[keyStr] == nullptr)
    {
		m_commands[keyStr] = settingsFlyout;
    }
}

void AppSettings::SignalSettingChanged(Platform::String^ settingName)
{
    if (m_settingChangedObserved)
    {
        auto e = ref new SettingChangedEventArgs(settingName);
        m_settingChangedEvent(this, e);
    }
}

void AppSettings::OnCommandsRequested(SettingsPane^ sender, SettingsPaneCommandsRequestedEventArgs^ e)
{
    for (auto item : m_commands)
    {
        auto command = ref new SettingsCommand(item.first, item.second->Title, 
			ref new UICommandInvokedHandler(this, &AppSettings::OnSettingsCommandInvoked));
        e->Request->ApplicationCommands->Append(command);
    }
}

void AppSettings::OnSettingsCommandInvoked(Windows::UI::Popups::IUICommand^ command)
{
    String^ key = dynamic_cast<String^>(command->Id);
    if (key == nullptr)
    {
        return;
    }
	m_commands[key]->Show();
}