#pragma once
namespace Callisto
{
    namespace Controls
    {
		namespace SettingsManagement
        {
            [Windows::Foundation::Metadata::WebHostHidden]
            public interface class ISettingsCommandInfo
            {
                property Platform::String^ HeaderText
                {
                    Platform::String^ get();
                }

                property Windows::UI::Xaml::Controls::UserControl^ Instance
                {
                    Windows::UI::Xaml::Controls::UserControl^ get();
                }

                property double Width
                {
                    double get();
                }
            };
        }
    }
}
