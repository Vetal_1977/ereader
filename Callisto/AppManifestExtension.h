#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            ref class VisualElement;

            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class AppManifestExtension sealed
            {
            public:
                static Windows::Foundation::IAsyncOperation<VisualElement^>^ GetManifestVisualElementsAsync();

            private:
                static Platform::String^ BuildURIString(Platform::String^ value);
            };
        }
    }
}

