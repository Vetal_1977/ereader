#include "pch.h"
#include "MenuItemInternal.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::System;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;

MenuItemInternal::MenuItemInternal() : m_isActive(false)
{
}

// Dependency Properties
static DependencyProperty^ _textProperty =
    DependencyProperty::Register("Text", 
    TypeName(String::typeid), 
    TypeName(MenuItemInternal::typeid), 
    nullptr);

DependencyProperty^ MenuItemInternal::TextProperty::get()
{
    return _textProperty;
}

String^ MenuItemInternal::Text::get()
{
    return static_cast<String^>(GetValue(TextProperty));
}

void MenuItemInternal::Text::set(String^ value)
{
    SetValue(TextProperty, value);
}

static DependencyProperty^ _commandProperty =
    DependencyProperty::Register("Command", 
    TypeName(ICommand::typeid), 
    TypeName(MenuItem::typeid), 
    nullptr);

DependencyProperty^ MenuItemInternal::CommandProperty::get()
{
    return _commandProperty;
}

ICommand^ MenuItemInternal::Command::get()
{
    return static_cast<ICommand^>(GetValue(CommandProperty));
}

void MenuItemInternal::Command::set(ICommand^ value)
{
    SetValue(CommandProperty, value);
}

static DependencyProperty^ _commandParameterProperty =
    DependencyProperty::Register("CommandParameter", 
    TypeName(Object::typeid), 
    TypeName(MenuItemInternal::typeid), 
    nullptr);

DependencyProperty^ MenuItemInternal::CommandParameterProperty::get()
{
    return _commandParameterProperty;
}

Object^ MenuItemInternal::CommandParameter::get()
{
    return static_cast<Object^>(GetValue(CommandParameterProperty));
}

void MenuItemInternal::CommandParameter::set(Object^ value)
{
    SetValue(CommandParameterProperty, value);
}

// Control Overrides
void MenuItemInternal::OnPointerEntered(PointerRoutedEventArgs^ e)
{
    MenuItemBase::OnPointerEntered(e);
    Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void MenuItemInternal::OnPointerExited(PointerRoutedEventArgs^ e)
{
    MenuItemBase::OnPointerExited(e);
    m_isActive = false;
    UpdateState(true);
}

void MenuItemInternal::OnPointerMoved(PointerRoutedEventArgs^ e)
{
    MenuItemBase::OnPointerMoved(e);
    Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void MenuItemInternal::OnPointerReleased(PointerRoutedEventArgs^ e)
{
    MenuItemBase::OnPointerReleased(e);
    VisualStateManager::GoToState(this, "Base", true);
}

void MenuItemInternal::OnPointerPressed(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{
    MenuItemBase::OnPointerPressed(e);
    VisualStateManager::GoToState(this, "Pressed", true);
}

void MenuItemInternal::OnGotFocus(RoutedEventArgs^ e)
{
    MenuItemBase::OnGotFocus(e);
    m_isActive = true;
    UpdateState(true);
}

void MenuItemInternal::OnLostFocus(RoutedEventArgs^ e)
{
    MenuItemBase::OnLostFocus(e);
    m_isActive = false;
    UpdateState(true);
}

void MenuItemInternal::OnKeyDown(Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e)
{
    MenuItemBase::OnKeyDown(e);

    if (e->Key == VirtualKey::Enter || e->Key == VirtualKey::Space)
    {
        OnTapped(ref new TappedRoutedEventArgs());
    }
}

// Private Methods
void MenuItemInternal::UpdateState(bool useTransitions)
{
    if (!IsEnabled)
    {
        VisualStateManager::GoToState(this, "Disabled", useTransitions);
    }
    else
    {
        if (m_isActive)
        {
            VisualStateManager::GoToState(this, "Hover", useTransitions);
        }
        else
        {
            VisualStateManager::GoToState(this, "Base", useTransitions);
        }
    }
}