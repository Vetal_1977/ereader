#pragma once
namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class ValueChangedEventArgs sealed
        {
        public:
            ValueChangedEventArgs(double oldValue, double newValue);

            property double OldValue
            {
                double get();
            }

            property double NewValue
            {
                double get();
            }

        private:
            double m_oldValue;
            double m_newValue;
        };

        [Windows::Foundation::Metadata::WebHostHidden]
        public delegate void ValueChangedEventHandler(Platform::Object^ sender, ValueChangedEventArgs^ e);
    }
}
