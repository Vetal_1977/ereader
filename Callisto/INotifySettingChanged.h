#pragma once
namespace Callisto
{
    namespace Controls
    {
        namespace SettingsManagement
        {
            ref class SettingChangedEventArgs;

            [Windows::Foundation::Metadata::WebHostHidden]
            public interface class INotifySettingChanged
            {
                event Windows::Foundation::EventHandler<SettingChangedEventArgs^>^ SettingChanged;
            };
        }
    }
}
