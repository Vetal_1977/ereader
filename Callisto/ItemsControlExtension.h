#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            class ItemsControlExtension
            {
            public:
                ItemsControlExtension(Windows::UI::Xaml::Controls::ItemsControl^ control);

                Windows::UI::Xaml::Controls::Panel^ GetItemsHost();
                Windows::UI::Xaml::Controls::ScrollViewer^ GetScrollHost();

                static void PrepareContainerForItemOverride(Windows::UI::Xaml::DependencyObject^ element, Windows::UI::Xaml::Style^ parentItemContainerStyle);

                void OnApplyTemplate();
                void UpdateItemContainerStyle(Windows::UI::Xaml::Style^ itemContainerStyle);
                void ScrollIntoView(Windows::UI::Xaml::FrameworkElement^ element);

            private:
                // TODO: Maybe should change to WeakReference
                Windows::UI::Xaml::Controls::ItemsControl^ m_itemsControl;
                Windows::UI::Xaml::Controls::Panel^ m_itemsHost;
                Windows::UI::Xaml::Controls::ScrollViewer^ m_scrollHost;
            };
        }
    }
}