#include "pch.h"
#include "AppManifestExtension.h"
#include "VisualElement.h"

using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::Data::Xml::Dom;
using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace concurrency;
using namespace std;

IAsyncOperation<VisualElement^>^ AppManifestExtension::GetManifestVisualElementsAsync()
{
    return concurrency::create_async([]()-> task<VisualElement^>
    {
        auto element = ref new VisualElement();
        task<StorageFile^> getFileTask(Windows::ApplicationModel::Package::Current->InstalledLocation->GetFileAsync("AppxManifest.xml"));
        return getFileTask.then([](StorageFile^ manifestFile) {
            return XmlDocument::LoadFromFileAsync(manifestFile);
        }, task_continuation_context::use_arbitrary()).then([element](XmlDocument^ manifestDocument){

            IXmlNode^ node = manifestDocument->SelectSingleNodeNS("//m2:VisualElements", "xmlns:m2='http://schemas.microsoft.com/appx/2013/manifest'");

            element->DisplayName = dynamic_cast<String^>(node->Attributes->GetNamedItem("DisplayName")->NodeValue);
            element->Description = dynamic_cast<String^>(node->Attributes->GetNamedItem("Description")->NodeValue);
            auto logo = dynamic_cast<String^>(node->Attributes->GetNamedItem("Square150x150Logo")->NodeValue);
            auto logoUri = AppManifestExtension::BuildURIString(logo);
            element->LogoUri = ref new Uri(logoUri); 
            auto smallLogo = dynamic_cast<String^>(node->Attributes->GetNamedItem("Square30x30Logo")->NodeValue);
            auto smallLogoUri = AppManifestExtension::BuildURIString(smallLogo);
            element->SmallLogoUri = ref new Uri(smallLogoUri); 
            element->BackgroundColorAsString = dynamic_cast<String^>(node->Attributes->GetNamedItem("BackgroundColor")->NodeValue);
            return element;
        }, task_continuation_context::use_current());
    });
}

String^ AppManifestExtension::BuildURIString(String^ value)
{
    wstring wlogo = value->Data();
    wstring findString = L"\\";
    auto found = wlogo.find(findString);
    while (found != wstring::npos)
    {
        wlogo.replace(found, findString.length(), L"/");
        found = wlogo.find(findString, found + 1);
    }

    wstringstream formattedlogo;
    formattedlogo << L"ms-appx:///" << wlogo;
    return ref new String(formattedlogo.str().c_str());
}