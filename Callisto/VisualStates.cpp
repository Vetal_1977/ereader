#include "pch.h"
#include "VisualStates.h"

using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml;
using namespace std;

String^ const Callisto::Controls::Common::GroupCommon = "CommonStates";
String^ const Callisto::Controls::Common::StateNormal = "Normal";
String^ const Callisto::Controls::Common::StateReadOnly = "ReadOnly";
String^ const Callisto::Controls::Common::StatePointerOver = "PointerOver";
String^ const Callisto::Controls::Common::StatePointerPressed = "PointerPressed";
String^ const Callisto::Controls::Common::StateDisabled = "Disabled";

String^ const Callisto::Controls::Common::GroupFocus = "FocusStates";
String^ const Callisto::Controls::Common::StateUnfocused = "Unfocused";
String^ const Callisto::Controls::Common::StateFocused = "Focused";

String^ const Callisto::Controls::Common::StateWatermarked = "Watermarked";
String^ const Callisto::Controls::Common::StateUnwatermarked = "Unwatermarked";

void VisualStates::GoToState(Control^ control, bool useTransitions, const vector<String^>& stateNames)
{
    for(auto name : stateNames)
    {
        if (VisualStateManager::GoToState(control, name, useTransitions))
        {
            break;
        }
    }
}