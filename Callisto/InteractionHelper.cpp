#include "pch.h"
#include "InteractionHelper.h"
#include "IUpdateVisualState.h"
#include "VisualStates.h"

using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::System;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml;
using namespace std;

const double SequentialClickThresholdInMilliseconds = 500.0;
const double SequentialClickThresholdInPixelsSquared = 3.0 * 3.0;

InteractionHelper::InteractionHelper(Control^ control) : m_control(control)
{
    m_updateVisualState = dynamic_cast<IUpdateVisualState^>(control);

    // Wire up the event handlers for events without a virtual override
    m_controlLoadedToken = m_control->Loaded += ref new RoutedEventHandler([this](Object^ sender, RoutedEventArgs^ e)
    {
        UpdateVisualState(false);
    });
    m_controlIsEnabledChangedToken = m_control->IsEnabledChanged += ref new DependencyPropertyChangedEventHandler([this](Object^ sender, DependencyPropertyChangedEventArgs^ e)
    {
        bool enabled = static_cast<bool>(e->NewValue);
        if (!enabled)
        {
            m_isPointerPressed = false;
            m_isPointerOver = false;
            m_isFocused = false;
        }

        UpdateVisualState(true);
    });
}

InteractionHelper::~InteractionHelper()
{
    m_control->Loaded -= m_controlLoadedToken;
    m_control->IsEnabledChanged -= m_controlIsEnabledChangedToken;
}

Control^ InteractionHelper::GetControl()
{
    return m_control;
}

bool InteractionHelper::IsPointerPressed()
{
    return m_isPointerPressed;
}

bool InteractionHelper::IsPointerOver()
{
    return m_isPointerOver;
}

bool InteractionHelper::IsFocused()
{
    return m_isFocused;
}

void InteractionHelper::UpdateVisualState(bool useTransitions)
{
    m_updateVisualState->UpdateVisualState(useTransitions);
}

bool InteractionHelper::AllowPointerPressed(PointerRoutedEventArgs^ e)
{
    if (nullptr == e)
    {
        throw ref new InvalidArgumentException("e");
    }

    bool enabled = m_control->IsEnabled;
    if (enabled)
    {
        // Get the current position and time
        time_t rawtime;
        struct tm newtime;
        time(&rawtime);
        auto err = gmtime_s(&newtime, &rawtime);
        // TODO do something about the error if need be
        DateTime now;
        now.UniversalTime = rawtime;
        Point position = e->GetCurrentPoint(m_control)->Position;

        // Compute the deltas from the last click
        double timeDelta = 0; (now.UniversalTime - m_lastClickTime.UniversalTime);
        Point lastPosition = m_lastClickPosition;
        double dx = position.X - lastPosition.X;
        double dy = position.Y - lastPosition.Y;
        double distance = dx * dx + dy * dy;

        // Check if the values fall under the sequential click temporal
        // and spatial thresholds
        if (timeDelta < SequentialClickThresholdInMilliseconds &&
            distance < SequentialClickThresholdInPixelsSquared)
        {
            // TODO: Does each click have to be within the single time
            // threshold on WPF?
            m_clickCount++;
        }
        else
        {
            m_clickCount = 1;
        }

        // Set the new position and time
        m_lastClickTime = now;
        m_lastClickPosition = position;

        // Raise the event
        m_isPointerPressed = true;
    }
    else
    {
        m_clickCount = 1;
    }

    return enabled;
}

void InteractionHelper::OnPointerPressedBase()
{
    UpdateVisualState(true);
}

void InteractionHelper::OnPointerReleasedBase()
{
    UpdateVisualState(true);
}

bool InteractionHelper::AllowPointerEnter(RoutedEventArgs^ e)
{
    if (nullptr == e)
    {
        throw ref new InvalidArgumentException("e");
    }

    bool enabled = m_control->IsEnabled;
    if (enabled)
    {
        m_isPointerOver = true;
    }
    return enabled;
}

bool InteractionHelper::AllowPointerExit(RoutedEventArgs^ e)
{
    if (nullptr == e)
    {
        throw ref new InvalidArgumentException("e");
    }

    bool enabled = m_control->IsEnabled;
    if (enabled)
    {
        m_isPointerOver = false;
        m_isPointerPressed = false;
        m_isPointerExited = true;
    }
    return enabled;
}

bool InteractionHelper::AllowKeyDown(KeyRoutedEventArgs^ e)
{
    if (nullptr == e)
    {
        throw ref new InvalidArgumentException("e");
    }

    return m_control->IsEnabled;
}

VirtualKey InteractionHelper::GetLogicalKey(FlowDirection flowDirection, VirtualKey originalKey)
{
    VirtualKey result = originalKey;
    if (flowDirection == FlowDirection::RightToLeft)
    {
        switch (originalKey)
        {
        case VirtualKey::Left:
            result = VirtualKey::Right;
            break;
        case VirtualKey::Right:
            result = VirtualKey::Left;
            break;
        }
    }
    return result;
}

void InteractionHelper::UpdateVisualStateBase(bool useTransitions)
{
    // Handle the Common states
    vector<Platform::String^> states;
    if (!m_control->IsEnabled)
    {
        states.push_back(StateDisabled);
        states.push_back(StateNormal);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
    else if (m_isReadOnly)
    {
        states.push_back(StateReadOnly);
        states.push_back(StateNormal);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
    else if (m_isPointerPressed)
    {
        states.push_back(StatePointerPressed);
        states.push_back(StatePointerOver);
        states.push_back(StateNormal);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
    else if (m_isPointerOver)
    {
        states.push_back(StatePointerOver);
        states.push_back(StateNormal);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
    else
    {
        states.push_back(StateNormal);
        VisualStates::GoToState(m_control, useTransitions, states);
    }

    // Handle the Focused states
    if (m_isFocused)
    {
        states.push_back(StateFocused);
        states.push_back(StateUnfocused);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
    else
    {
        states.push_back(StateUnfocused);
        VisualStates::GoToState(m_control, useTransitions, states);
    }
}