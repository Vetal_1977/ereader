#include "pch.h"
#include "MenuItemBase.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;

MenuItemBase::MenuItemBase()
{
}

// Dependency Properties
static DependencyProperty^ _menuTextMarginProperty =
    DependencyProperty::Register("MenuTextMargin", 
    TypeName(Thickness::typeid), 
    TypeName(MenuItemBase::typeid), 
    nullptr);

DependencyProperty^ MenuItemBase::MenuTextMarginProperty::get()
{
    return _menuTextMarginProperty;
}

Thickness MenuItemBase::MenuTextMargin::get()
{
    return static_cast<Thickness>(GetValue(MenuTextMarginProperty));
}

void MenuItemBase::MenuTextMargin::set(Thickness value)
{
    SetValue(MenuTextMarginProperty, value);
}