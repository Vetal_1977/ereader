#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            public interface class IUpdateVisualState
            {
                void UpdateVisualState(bool useTransitions);
            };
        }
    }
}