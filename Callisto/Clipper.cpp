#include "pch.h"
#include "Clipper.h"

using namespace Callisto::Controls::Primitives;
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;

Clipper::Clipper()
{
    m_sizedToken = SizeChanged += ref new SizeChangedEventHandler(this, &Clipper::OnSizedChanged); 
}

static DependencyProperty^ m_ratioVisibleProperty =
    DependencyProperty::Register("RatioVisible", 
    TypeName(float::typeid), 
    TypeName(Clipper::typeid), 
    ref new PropertyMetadata(1.0f, ref new PropertyChangedCallback(&Clipper::OnRatioVisiblePropertyChanged)));

DependencyProperty^ Clipper::RatioVisibleProperty::get()
{
    return m_ratioVisibleProperty;
}

float Clipper::RatioVisible::get()
{
    return static_cast<float>(GetValue(RatioVisibleProperty));
}

void Clipper::RatioVisible::set(float value)
{
    SetValue(RatioVisibleProperty, value);
}

void Clipper::OnRatioVisiblePropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e)
{
    auto source = static_cast<Clipper^>(d);
    float oldValue = static_cast<float>(e->OldValue);
    float newValue = static_cast<float>(e->NewValue);
    source->OnRatioVisibleChanged(oldValue, newValue);
}

void Clipper::OnRatioVisibleChanged(float oldValue, float newValue)
{
    if (newValue >= 0.0f && newValue <= 1.0f)
    {
        ClipContent();
    }
    else
    {
        if (newValue < 0.0)
        {
            RatioVisible = 0.0f;
        }
        else if (newValue > 1.0)
        {
            RatioVisible = 1.0f;
        }
    }
}

void Clipper::ClipContent()
{
}

void Clipper::OnSizedChanged(Object^ sender, SizeChangedEventArgs^ e)
{
    ClipContent();
}