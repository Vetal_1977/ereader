#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class NumericUpDown sealed : public Windows::UI::Xaml::Controls::TextBox
        {
        public:
            NumericUpDown();
            virtual ~NumericUpDown();

            property int Delay
            {
                int get();
                void set(int value);
            }

            property int Interval
            {
                int get();
                void set(int value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ MaximumProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Maximum
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ MinimumProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Minimum
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ ValueProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Value
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ DecimalPlacesProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property int DecimalPlaces
            {
                int get();
                void set(int value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ IncrementProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double Increment
            {
                double get();
                void set(double value);
            }

        protected:
            virtual void OnKeyDown(Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e) override;
            virtual void OnApplyTemplate() override;

        internal:
            static void OnMaximumPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnMinimumPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnValuePropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnDecimalPlacesPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void OnIncrementPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        private:
            Windows::UI::Xaml::Controls::Primitives::RepeatButton^ m_incrementButton;
            Windows::UI::Xaml::Controls::Primitives::RepeatButton^ m_decrementButton;
            Platform::String^ m_text;
            bool m_canIncrement;
            bool m_canDecrement;
            int m_delay;
            int m_interval;
            int m_levelsFromRootCall;
            double m_requestedMax;
            double m_requestedMin;
            double m_requestedVal;
            double m_initialMax;
            double m_initialMin;
            double m_initialVal;
            double m_requestedInc;
            double m_initialInc;

            Windows::Foundation::EventRegistrationToken m_lostFocusEvent;
            Windows::Foundation::EventRegistrationToken m_gotFocusEvent;
            Windows::Foundation::EventRegistrationToken m_textChangedEvent;
            Windows::Foundation::EventRegistrationToken m_incrementButtonSizeChangedEvent;
            Windows::Foundation::EventRegistrationToken m_incrementClickedEvent;
            Windows::Foundation::EventRegistrationToken m_decrementButtonSizeChangedEvent;
            Windows::Foundation::EventRegistrationToken m_decrementClickedEvent;

            void OnLostFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnGotFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnTextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
            void OnPartButtonSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);
            void OnIncrementClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnDecrementClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

            void OnDecimalPlacesChanged(int oldValue, int newValue);
            void OnValueChanged();
            void OnMaximumChanged(double oldValue, double newValue);
            void OnMinimumChanged(double oldValue, double newValue);
            void OnIncrementChanged(double oldValue, double newValue);

            void ProcessUserInput();
            void ApplyValue(Platform::String^ text);
            void SetTextBoxText();
            void SetValidIncrementDirection();
            Platform::String^ FormatValue();
            static void ValidateDoubleValue(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            static void ValidateDoubleValue(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyProperty^ property, Platform::Object^ oldValue, Platform::Object^ newValue);
            static bool IsValidDoubleValue(Platform::Object^ value, double* number);
            static void ValidateIncrementValue(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
            void CoerceMaximum();
            void CoerceValue();
            void DoIncrement();
            void DoDecrement();
        };
    }
}
