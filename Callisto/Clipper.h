#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Primitives
        {
            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class Clipper : Windows::UI::Xaml::Controls::ContentControl
            {
            public:
                // dependency properties
                static property Windows::UI::Xaml::DependencyProperty^ RatioVisibleProperty
                {
                    Windows::UI::Xaml::DependencyProperty^ get();
                };

                property float RatioVisible
                {
                    float get();
                    void set(float value);
                }

            internal:
                static void OnRatioVisiblePropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

            internal:
                Clipper();

            protected:
                virtual void OnRatioVisibleChanged(float oldValue, float newValue);
                virtual void ClipContent();

            private:
                Windows::Foundation::EventRegistrationToken m_sizedToken;

                void OnSizedChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);
            };
        }
    }
}