#include "pch.h"
#include "ColorContrastConverter.h"

using namespace Callisto::Converters;
using namespace Platform;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI;

Object^ ColorContrastConverter::Convert(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    auto brush = dynamic_cast<SolidColorBrush^>(value);
    auto yiq = ((brush->Color.R * 299) + (brush->Color.G * 587) + (brush->Color.B * 114)) / 1000;
    Color contrastColor;
    auto boxedBool = dynamic_cast<Box<bool>^>(parameter);
    auto boolValue = (boxedBool != nullptr && boxedBool->Value);
    bool invert = (parameter != nullptr) && boolValue;

    // check to see if we actually need to invert
    contrastColor = invert
        ? ((yiq >= 128) ? Colors::White : Colors::Black)
        : ((yiq >= 128) ? Colors::Black : Colors::White);

    return ref new SolidColorBrush(contrastColor);
}

Object^ ColorContrastConverter::ConvertBack(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    return value;
}
