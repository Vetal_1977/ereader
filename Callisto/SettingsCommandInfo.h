#pragma once

#include "ISettingsCommandInfo.h"

namespace Callisto
{
    namespace Controls
    {
        namespace SettingsManagement
        {
            ref class SettingsCommandInfo sealed : public ISettingsCommandInfo
            {
            public:
                SettingsCommandInfo(Platform::String^ headerText, double width, Windows::UI::Xaml::Controls::UserControl^ instance);

                property Platform::String^ HeaderText
                {
                    virtual Platform::String^ get();
                }

                property Windows::UI::Xaml::Controls::UserControl^ Instance
                {
                    virtual Windows::UI::Xaml::Controls::UserControl^ get();
                }

                property double Width
                {
                    virtual double get();
                }

            private:
                double m_width;
                Platform::String^ m_headerText;
                Windows::UI::Xaml::Controls::UserControl^ m_instance;
            };
        }
    }
}
