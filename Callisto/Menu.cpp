#include "pch.h"
#include "Menu.h"
#include "MenuItem.h"
#include "Flyout.h"

using namespace Callisto::Controls;
using namespace Platform::Collections;
using namespace Platform;
using namespace Windows::Foundation::Collections;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml;

Menu::Menu()
{
    DefaultStyleKey = Menu::typeid->FullName;

    m_loadedToken = Loaded += ref new RoutedEventHandler(this, &Menu::OnLoaded);

    auto handler = ref new KeyEventHandler(this, &Menu::OnMenuKeyDown);
    m_keyDownToken = this->KeyDown += handler;
    // not sure why this doesn't work
    //AddHandler(KeyDownEvent, handler, true);

    m_items = ref new Vector<Control^>();
    m_itemChangeToken = m_items->VectorChanged += ref new VectorChangedEventHandler<Control^>(this, &Menu::OnItemsCollectionChanged);
}

Menu::~Menu()
{
    m_items->VectorChanged -= m_itemChangeToken;
    Loaded -= m_loadedToken;
    this->KeyDown -= m_keyDownToken;

    for (auto itemEvent : m_itemTokens)
    {
        itemEvent.MenuItem->Tapped -= itemEvent.Token;
    }
}

IObservableVector<Control^>^ Menu::Items::get()
{
    return m_items;
}

// Event Handlers
void Menu::OnApplyTemplate()
{
    Control::OnApplyTemplate();

    m_itemContainerList = dynamic_cast<ItemsControl^>(GetTemplateChild("ItemList"));
    m_itemContainerList->ItemsSource = m_items;
}

void Menu::OnItemsCollectionChanged(IObservableVector<Control^>^ sender, IVectorChangedEventArgs^ e)
{
    if (e->CollectionChange == CollectionChange::ItemRemoved)
    {
        MenuItemEvent itemEvent = m_itemTokens.at(e->Index);
        itemEvent.MenuItem->Tapped -= itemEvent.Token;
        m_itemTokens.erase(begin(m_itemTokens) + e->Index);
    }
    else if (e->CollectionChange == CollectionChange::ItemInserted)
    {
        auto item = dynamic_cast<MenuItem^>(m_items->GetAt(e->Index));
        if (nullptr != item)
        {
            EventRegistrationToken token = item->Tapped += ref new TappedEventHandler(this, &Menu::OnMenuItemSelected);
            MenuItemEvent itemEvent;
            itemEvent.Token = token;
            itemEvent.MenuItem = item;
            m_itemTokens.push_back(itemEvent);
        }
    }
    else if (e->CollectionChange == CollectionChange::Reset)
    {

    }
}

void Menu::OnLoaded(Object^ sender, RoutedEventArgs^ e)
{
    auto menu = static_cast<Menu^>(sender);
    menu->IsHitTestVisible = true;
}

void Menu::OnMenuKeyDown(Object^ sender, KeyRoutedEventArgs^ args)
{
    auto parent = static_cast<Flyout^>(Parent);
    switch (args->Key)
    {
    case Windows::System::VirtualKey::Escape:
        parent->IsOpen = false;
        break;
    case Windows::System::VirtualKey::Up:
        ChangeFocusedItem(false);
        break;
    case Windows::System::VirtualKey::Down:
        ChangeFocusedItem(true);
        break;
    case Windows::System::VirtualKey::Home:
    case Windows::System::VirtualKey::PageUp:
        PageFocusedItem(true);
        break;
    case Windows::System::VirtualKey::End:
    case Windows::System::VirtualKey::PageDown:
        PageFocusedItem(false);
        break;
    default:
        break;
    }
    //Control::OnKeyDown(args);
}

void Menu::OnMenuItemSelected(Object^ sender, TappedRoutedEventArgs^ args)
{
    auto item = static_cast<MenuItem^>(sender);

    if (nullptr == item)
    {
        return;
    }

    auto flyout = static_cast<Flyout^>(Parent);
    flyout->IsOpen = false;

    if (nullptr != item->Command)
    {
        item->Command->Execute(item->CommandParameter);
    }
}

unsigned int Menu::GetNextItemIndex(int startIndex, bool ascending)
{
    int index = startIndex + (ascending ? 1 : -1);
    int size = m_items->Size;
    if (index >= size)
    {
        // if after end of collection, go to start
        index = 0;
    }
    else if (index < 0)
    {
        // if before start of collection, go to end
        index = size - 1;
    }

    return index;
}

void Menu::ChangeFocusedItem(bool ascendIndex)
{
    auto focusedElement = FocusManager::GetFocusedElement();
    auto focusedElementType = focusedElement->GetType();
    auto item = dynamic_cast<MenuItemInternal^>(focusedElement);
    unsigned int startIndex;
    if (focusedElementType == m_itemContainerList->GetType() && m_items->Size > 0)
    {
        // focused item is the item container list, so try to set focus to an initial item
        startIndex = GetNextItemIndex(-1, ascendIndex);
    }
    else if (nullptr == item)
    {
        // focused item isn't the item container list or a menu item, so ignore
        return;
    }
    else
    {
        // focused item is already a menu item, so try to set focus to next
        unsigned int idx;
        bool found = m_items->IndexOf(item, &idx);
        if (found)
        {
            startIndex = GetNextItemIndex(idx, ascendIndex);
        }
        else
        {
            startIndex = GetNextItemIndex(-1, ascendIndex);
        }
    }

    unsigned int index = startIndex;

    auto nextItem = dynamic_cast<MenuItemInternal^>(m_items->GetAt(index));

    // focus next item, if it's a menu item
    if (nullptr != nextItem)
    {
        nextItem->Focus(Windows::UI::Xaml::FocusState::Programmatic);
    }
    else
    {
        // next element wasn't a MenuItem, so loop through once trying to find the next item
        index = GetNextItemIndex(index, ascendIndex);
        nextItem = dynamic_cast<MenuItemInternal^>(m_items->GetAt(index));
        while (nullptr == nextItem && index != startIndex)
        {
            index = GetNextItemIndex(index, ascendIndex);
            nextItem = dynamic_cast<MenuItemInternal^>(m_items->GetAt(index));
        }
        if (nullptr != nextItem)
        {
            nextItem->Focus(Windows::UI::Xaml::FocusState::Programmatic);
        }
    }
}

void Menu::PageFocusedItem(bool top)
{
    int itemNumber = top ? 0 : m_items->Size - 1;

    Control^ item = m_items->GetAt(itemNumber);
    item->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}