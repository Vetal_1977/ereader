﻿//
// pch.h
// Header for standard system include files.
//

#pragma once
#include <algorithm>
#include <array>
#include <assert.h>
#include <chrono>
#include <collection.h>
#include <iomanip> 
#include <limits>
#include <map>
#include <ppltasks.h>
#include <random>
#include <sstream>
#include <vector>

#include "BrushToColorConverter.h"
#include "ColorBrightnessConverter.h"
#include "ColorContrastConverter.h"
#include "CustomDialog.h"
#include "DropDownButton.h"
#include "DynamicTextBlock.h"
#include "FlipViewIndicator.h"
#include "Flyout.h"
#include "LinearClipper.h"
#include "LiveTile.h"
#include "Menu.h"
#include "MenuItemSeparator.h"
#include "NumericUpDown.h"
#include "RatingItem.h"
#include "Rating.h"
#include "ToggleMenuItem.h"
#include "VisualElement.h"
#include "WatermarkTextBox.h"
