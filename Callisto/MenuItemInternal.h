#pragma once
#include "MenuItemBase.h"

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class MenuItemInternal : public MenuItemBase
        {
        internal:
            MenuItemInternal();

        public:
            static property Windows::UI::Xaml::DependencyProperty^ TextProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::String^ Text
            {
                Platform::String^ get();
                void set(Platform::String^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ CommandProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Input::ICommand^ Command
            {
                Windows::UI::Xaml::Input::ICommand^ get();
                void set(Windows::UI::Xaml::Input::ICommand^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ CommandParameterProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::Object^ CommandParameter
            {
                Platform::Object^ get();
                void set(Platform::Object^ value);
            }

        protected:
            virtual void OnPointerEntered(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerExited(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerMoved(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerReleased(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnPointerPressed(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e) override;
            virtual void OnGotFocus(Windows::UI::Xaml::RoutedEventArgs^ e) override;
            virtual void OnLostFocus(Windows::UI::Xaml::RoutedEventArgs^ e) override;
            virtual void OnKeyDown(Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e) override;

        private:
            bool m_isActive;

            void UpdateState(bool useTransitions);
        };
    }
}
