#pragma once
namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            [Windows::UI::Xaml::Data::Bindable]
            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class VisualElement sealed
            {
            public:
                property Platform::String^ DisplayName
                {
                    Platform::String^ get();
                    void set(Platform::String^ value);
                }

                property Platform::String^ Description
                {
                    Platform::String^ get();
                    void set(Platform::String^ value);
                }

                property Windows::Foundation::Uri^ LogoUri 
                { 
                    Windows::Foundation::Uri^ get(); 
                    void set(Windows::Foundation::Uri^ value); 
                }

                property Windows::Foundation::Uri^ SmallLogoUri 
                { 
                    Windows::Foundation::Uri^ get(); 
                    void set(Windows::Foundation::Uri^ value); 
                }

                property Platform::String^ BackgroundColorAsString
                {
                    Platform::String^ get();
                    void set(Platform::String^ value);
                }

                property Windows::UI::Color BackgroundColor 
                { 
                    Windows::UI::Color get(); 
                }

            private:
                Platform::String^ m_displayName;
                Platform::String^ m_description;
                Windows::Foundation::Uri^ m_logoUri;
                Windows::Foundation::Uri^ m_smallLogoUri;
                Platform::String^ m_backgroundColorAsString;

                byte ParseHexValue(std::wstring hexValue);
            };
        }
    }
}

