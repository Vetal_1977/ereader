#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::UI::Xaml::TemplatePartAttribute(Name = L"PART_Watermark", Type = Windows::UI::Xaml::Controls::ContentPresenter::typeid)]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class WatermarkTextBox sealed : public Windows::UI::Xaml::Controls::TextBox
        {
        public:
            WatermarkTextBox();
            virtual ~WatermarkTextBox();

            static property Windows::UI::Xaml::DependencyProperty^ WatermarkProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::Object^ Watermark
            {
                Platform::Object^ get();
                void set(Platform::Object^ value);
            }

        protected:
            virtual void OnApplyTemplate() override;

        internal:
            static void OnWatermarkPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        private:
            Windows::UI::Xaml::Controls::TextBlock^ m_elementContent;
            bool m_isHovered;
            bool m_hasFocusInternal;
            Windows::ApplicationModel::Resources::ResourceLoader^ m_resources;
            Windows::Foundation::EventRegistrationToken m_gotFoucusToken;
            Windows::Foundation::EventRegistrationToken m_lostFoucusToken;
            Windows::Foundation::EventRegistrationToken m_pointerEnteredToken;
            Windows::Foundation::EventRegistrationToken m_pointerExitedToken;
            Windows::Foundation::EventRegistrationToken m_textChangedToken;
            Windows::Foundation::EventRegistrationToken m_loadedToken;

            void OnWatermarkChanged();
            void OnGotFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnLostFocus(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnPointerEntered(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
            void OnPointerExited(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
            void OnTextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
            void OnLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

            void ChangeVisualState();
            void ChangeVisualState(bool useTransitions);
        };
    }
}
