#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::UI::Xaml::TemplatePartAttribute(Name = L"PART_BackButton", Type = Windows::UI::Xaml::Controls::Button::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = L"PART_RootBorder", Type = Windows::UI::Xaml::Controls::Border::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = L"PART_RootGrid", Type = Windows::UI::Xaml::Controls::Grid::typeid)]
        [Windows::UI::Xaml::TemplatePartAttribute(Name = L"PART_Content", Type = Windows::UI::Xaml::Controls::ContentPresenter::typeid)]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class CustomDialog sealed : public Windows::UI::Xaml::Controls::ContentControl
        {
        public:
            CustomDialog();
            virtual ~CustomDialog();
            
            event Windows::UI::Xaml::RoutedEventHandler^ BackButtonClicked {
                Windows::Foundation::EventRegistrationToken add(Windows::UI::Xaml::RoutedEventHandler^ e) {
                    m_backButtonClickedObserved = true;
                    return m_backButtonClickedEvent += e;
                }

                void remove(Windows::Foundation::EventRegistrationToken t) {
                    m_backButtonClickedObserved = false;
                    m_backButtonClickedEvent -= t;
                }

                void raise(Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e) {
                    m_backButtonClickedEvent(sender, e);
                }
            };

            static property Windows::UI::Xaml::DependencyProperty^ IsOpenProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property bool IsOpen
            {
                bool get();
                void set(bool value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ BackButtonVisibilityProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Visibility BackButtonVisibility
            {
                Windows::UI::Xaml::Visibility get();
                void set(Windows::UI::Xaml::Visibility value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ TitleProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::String^ Title
            {
                Platform::String^ get();
                void set(Platform::String^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ BackButtonCommandProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Input::ICommand^ BackButtonCommand
            {
                Windows::UI::Xaml::Input::ICommand^ get();
                void set(Windows::UI::Xaml::Input::ICommand^ value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ BackButtonCommandParameterProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Platform::Object^ BackButtonCommandParameter
            {
                Platform::Object^ get();
                void set(Platform::Object^ value);
            }

        protected:
            virtual void OnApplyTemplate() override;

        internal:
            static void OnIsOpenPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);
        
        private:
            Windows::UI::Xaml::Controls::Grid^ m_rootGrid;
            Windows::UI::Xaml::Controls::Border^ m_rootBorder;
            Windows::UI::Xaml::Controls::Button^ m_backButton;
            Windows::Foundation::EventRegistrationToken m_windowSizeChangedToken;
            Windows::Foundation::EventRegistrationToken m_backButtonClickedToken;
            event Windows::UI::Xaml::RoutedEventHandler^ m_backButtonClickedEvent;
            bool m_backButtonClickedObserved;

            void OnWindowSizeChanged(Platform::Object^ sender, Windows::UI::Core::WindowSizeChangedEventArgs^ e);
            void OnBackButtonClicked(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

            void ResizeContainers();
            void RemoveBackButtonClickHandler();
        };
    }
}
