#include "pch.h"
#include "DropDownButton.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;

String^ const PartArrowGlyph = L"ArrowGlyph";
double const FontSizeThreshold = 36;
String^ const ArrowGlyphSmall = L"\uE099";

DropDownButton::DropDownButton() 
{
    DefaultStyleKey = DropDownButton::typeid->FullName;
}

DropDownButton::~DropDownButton()
{
}

void DropDownButton::OnApplyTemplate()
{
    m_arrowGlyph = dynamic_cast<TextBlock^>(GetTemplateChild(PartArrowGlyph));

    // TODO: if the FontSize property changes we need to run this logic again
    if (m_arrowGlyph != nullptr)
    {
        if (FontSize < FontSizeThreshold)
        {
            m_arrowGlyph->Text = ArrowGlyphSmall;
        }
    }
}