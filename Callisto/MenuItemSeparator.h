#pragma once
#include "MenuItemBase.h"

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class MenuItemSeparator sealed : public MenuItemBase
        {
        public:
            MenuItemSeparator();
        };
    }
}

