#include "pch.h"
#include "BrushToColorConverter.h"

using namespace Callisto::Converters;
using namespace Platform;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;

Object^ BrushToColorConverter::Convert(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    auto brush = dynamic_cast<SolidColorBrush^>(value);
    if (nullptr != brush)
    {
        return brush->Color;
    }
    else
    {
        return value;
    }
}

Object^ BrushToColorConverter::ConvertBack(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
    return value;
}
