#pragma once
namespace Callisto
{
    namespace Controls
    {
        namespace SettingsManagement
        {
            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class SettingChangedEventArgs sealed
            {
            public:
                SettingChangedEventArgs(Platform::String^ settingsName);
                
                property Platform::String^ SettingsName
                {
                    Platform::String^ get();
                    void set(Platform::String^ value);
                }

            private:
                Platform::String^ m_settingsName;
            };
        }
    }
}
