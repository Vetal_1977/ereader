﻿#include "pch.h"
#include "Flyout.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Input;
using namespace Windows::UI::ViewManagement;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media::Animation;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml;
using namespace std;

// This is the edge gutter when positioned against left/right/top/bottom or element edges 
// should be 6px, but that doesn't take into account focus rects
// TODO: Revisit gutter constant if needs to be less due to focus rects/touch targets
const double GutterBuffer = 5.0;
const double GutterBufferAdjusted = 4.0; // for element gutter due to focus rects

Flyout::Flyout() : m_ihmFocusMoved(false), m_ihmOccludeHeight(0.0)
{
    DefaultStyleKey = Flyout::typeid->FullName;

    m_activatedToken = Window::Current->Activated += ref new WindowActivatedEventHandler(this, &Flyout::OnWindowActivated);

    Placement = PlacementMode::Top;

    m_windowBounds = Window::Current->Bounds;
    m_rootVisual = Window::Current->Content;

    Loaded += ref new RoutedEventHandler(this, &Flyout::OnLoaded);

    m_hostPopup = ref new Popup();
    m_hostPopup->IsHitTestVisible = false;
    m_hostPopup->Opacity = 0;
    m_hostPopup->Closed += ref new EventHandler<Object^>(this, &Flyout::OnHostPopupClosed);
    m_hostPopup->Opened += ref new EventHandler<Object^>(this, &Flyout::OnHostPopupOpened);
    m_hostPopup->IsLightDismissEnabled = true;
    m_hostPopup->Child = this;
}

Flyout::~Flyout()
{
    if (m_hostPopup != nullptr)
    {
        m_hostPopup->Child = nullptr;
        m_hostPopup->ChildTransitions = nullptr;

        Content = nullptr;
    }

    SizeChanged -= m_sizeChangedToken;
    Window::Current->Activated -= m_activatedToken;
}

// Properties

Popup^ Flyout::HostPopup::get()
{
    return m_hostPopup;
}

// Dependency Properties

static DependencyProperty^ _isOpenProperty =
    DependencyProperty::Register("IsOpen", 
    TypeName(bool::typeid), 
    TypeName(Flyout::typeid), 
    ref new PropertyMetadata(false, ref new PropertyChangedCallback(&Flyout::OnIsOpenPropertyChanged)));

DependencyProperty^ Flyout::IsOpenProperty::get()
{
    return _isOpenProperty;
}

bool Flyout::IsOpen::get()
{
    return static_cast<bool>(GetValue(IsOpenProperty));
}

void Flyout::IsOpen::set(bool value)
{
    SetValue(IsOpenProperty, value);
}

static DependencyProperty^ m_hostMarginProperty =
    DependencyProperty::Register("HostMargin", 
    TypeName(Thickness::typeid), 
    TypeName(Flyout::typeid), 
    nullptr);

DependencyProperty^ Flyout::HostMarginProperty::get()
{
    return m_hostMarginProperty;
}

Thickness Flyout::HostMargin::get()
{
    return static_cast<Thickness>(GetValue(HostMarginProperty));
}

void Flyout::HostMargin::set(Thickness value)
{
    SetValue(HostMarginProperty, value);
}

static DependencyProperty^ m_placementProperty =
    DependencyProperty::Register("Placement", 
    TypeName(PlacementMode::typeid), 
    TypeName(Flyout::typeid), 
    nullptr);

DependencyProperty^ Flyout::PlacementProperty::get()
{
    return m_placementProperty;
}

PlacementMode Flyout::Placement::get()
{
    return static_cast<PlacementMode>(GetValue(PlacementProperty));
}

void Flyout::Placement::set(PlacementMode value)
{
    SetValue(PlacementProperty, value);
}

static DependencyProperty^ m_horizontalOffsetProperty =
    DependencyProperty::Register("HorizontalOffset", 
    TypeName(double::typeid), 
    TypeName(Flyout::typeid), 
    ref new PropertyMetadata(0.0));

DependencyProperty^ Flyout::HorizontalOffsetProperty::get()
{
    return m_horizontalOffsetProperty;
}

double Flyout::HorizontalOffset::get()
{
    return static_cast<double>(GetValue(HorizontalOffsetProperty));
}

void Flyout::HorizontalOffset::set(double value)
{
    SetValue(HorizontalOffsetProperty, value);
}

static DependencyProperty^ m_verticalOffsetProperty =
    DependencyProperty::Register("VerticalOffset", 
    TypeName(double::typeid), 
    TypeName(Flyout::typeid), 
    ref new PropertyMetadata(0.0));

DependencyProperty^ Flyout::VerticalOffsetProperty::get()
{
    return m_horizontalOffsetProperty;
}

double Flyout::VerticalOffset::get()
{
    return static_cast<double>(GetValue(VerticalOffsetProperty));
}

void Flyout::VerticalOffset::set(double value)
{
    SetValue(VerticalOffsetProperty, value);
}

static DependencyProperty^ m_placementTargetProperty =
    DependencyProperty::Register("PlacementTarget", 
    TypeName(UIElement::typeid), 
    TypeName(Flyout::typeid), 
    nullptr);

DependencyProperty^ Flyout::PlacementTargetProperty::get()
{
    return m_placementTargetProperty;
}

UIElement^ Flyout::PlacementTarget::get()
{
    return static_cast<UIElement^>(GetValue(PlacementTargetProperty));
}

void Flyout::PlacementTarget::set(UIElement^ value)
{
    SetValue(PlacementTargetProperty, value);
}

// Event Handlers

void Flyout::OnIsOpenPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto flyout = dynamic_cast<Flyout^>(sender);
    if (nullptr != flyout)
    {
        flyout->HostPopup->IsOpen = static_cast<bool>(e->NewValue);
    }
}

void Flyout::OnWindowActivated(Platform::Object^ sender, Windows::UI::Core::WindowActivatedEventArgs^ e)
{
    if (e->WindowActivationState == Windows::UI::Core::CoreWindowActivationState::Deactivated)
    {
        IsOpen = false;
    }
}

void Flyout::OnLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
    auto flyout = static_cast<Flyout^>(sender);
    flyout->IsHitTestVisible = true;
}

void Flyout::OnHostPopupClosed(Platform::Object^ sender, Platform::Object^ e)
{
    // m_hostPopup->Child = nullptr;
    // m_hostPopup->ChildTransitions = nullptr;

    // important to remove this or else there will be a leak
    Window::Current->Activated -= m_activatedToken;
    InputPane::GetForCurrentView()->Showing -= m_inputPaneShowingToken;
    InputPane::GetForCurrentView()->Hiding -= m_inputPaneHidingToken;

    Content = nullptr;

    Closed(this, e);

    IsOpen = false;
}

void Flyout::OnHostPopupOpened(Platform::Object^ sender, Platform::Object^ e)
{
    // there seems to be some issue where the measure is happening too fast when the size
    // of the flyout is 0,0.  This attempts to solve this. 
    // credit to avidgator/jvlppm for suggestions on github project
    if (m_hostPopup->ActualHeight == 0 || m_hostPopup->ActualWidth == 0)
    {
        m_sizeChangedToken = SizeChanged += ref new SizeChangedEventHandler([this](Object^ sender, SizeChangedEventArgs^ e) 
        {
            if (e->NewSize.Width != 0 && e->NewSize.Height != 0)
            {
                OnHostPopupOpened(sender, e);
                SizeChanged -= m_sizeChangedToken;
            }
        });		
    }

    m_hostPopup->HorizontalOffset = HorizontalOffset;
    m_hostPopup->VerticalOffset = VerticalOffset;

    auto infinity = numeric_limits<float>::infinity();
    Measure(Size(infinity, infinity));

    PerformPlacement(HorizontalOffset, VerticalOffset);

    // handling the case where it isn't parented to the visual tree
    // inspect the visual root and adjust.
    if (m_hostPopup->Parent == nullptr)
    {
        m_inputPaneShowingToken = InputPane::GetForCurrentView()->Showing += ref new TypedEventHandler<InputPane^, InputPaneVisibilityEventArgs^>(this, &Flyout::OnInputPaneShowing);
        m_inputPaneHidingToken = InputPane::GetForCurrentView()->Hiding += ref new TypedEventHandler<InputPane^, InputPaneVisibilityEventArgs^>(this, &Flyout::OnInputPaneHiding);
    }

    auto content = dynamic_cast<Control^>(Content);
    if (content != nullptr)
        content->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void Flyout::OnInputPaneShowing(InputPane^ sender, InputPaneVisibilityEventArgs^ e)
{
    //_hostPopup.VerticalOffset -= (int)args.OccludedRect.Height;
    FrameworkElement^ focusedItem = dynamic_cast<FrameworkElement^>(Windows::UI::Xaml::Input::FocusManager::GetFocusedElement());

    if (focusedItem != nullptr)
    {
        // if the focused item is within height - occludedrect height - buffer(50)
        // then it doesn't need to be changed
        GeneralTransform^ gt = focusedItem->TransformToVisual(Window::Current->Content);
        Point focusedPoint = gt->TransformPoint(Point(0.0, 0.0));

        if (focusedPoint.Y > (m_windowBounds.Height - e->OccludedRect.Height - 50))
        {
            m_ihmFocusMoved = true;
            m_ihmOccludeHeight = e->OccludedRect.Height;
            m_hostPopup->VerticalOffset -= static_cast<int>(m_ihmOccludeHeight);
        }
    }
}

void Flyout::OnInputPaneHiding(InputPane^ sender, InputPaneVisibilityEventArgs^ e)
{
    // if the ihm occluded something and we had to move, we need to adjust back
    if (m_ihmFocusMoved)
    {
        m_hostPopup->VerticalOffset += m_ihmOccludeHeight; // ensure defaults back to normal
        m_ihmFocusMoved = false;
    }
}

// Private methods
void Flyout::PerformPlacement(double horizontalOffset, double verticalOffset)
{
    double x = 0.0;
    double y = 0.0;
    PlacementMode placement = Placement;
    FrameworkElement^ element = dynamic_cast<FrameworkElement^>(PlacementTarget);
    bool isRTL = (element != nullptr) ? (element->FlowDirection == Windows::UI::Xaml::FlowDirection::RightToLeft) : false;

    if ((element != nullptr) && !element->IsHitTestVisible)
    {
        return;
    }

    switch (placement)
    {
    case PlacementMode::Bottom:
    case PlacementMode::Left:
    case PlacementMode::Right:
    case PlacementMode::Top:
        {
            array<Point, 4> target = GetTransformedPoints(element, isRTL, element);
            array<Point, 4> menu = GetTransformedPoints(static_cast<FrameworkElement^>(m_hostPopup->Child), isRTL, element);
            if (menu[0].X > menu[1].X)
            {
                return;
            }
            Point p2 = PlacePopup(m_windowBounds, target, menu, placement);
            x = p2.X;
            if (isRTL)
            {
                // TODO: Handle RTL - PerformPlacement
                //x = _windowBounds.Width - x;
                //this._hostPopup.VerticalOffset = y;
                //this._hostPopup.HorizontalOffset = x;
                //return;
            }
            y = p2.Y;
        }
        break;
    case PlacementMode::Mouse:
        throw ref new NotImplementedException("Mouse PlacementMode is not implemented.");
    }

    if (x < 0.0) x = 0.0;

    if (y < 0.0) y = 0.0;

    auto childElement = static_cast<FrameworkElement^>(m_hostPopup->Child);
    auto calcH = CalculateHorizontalCenterOffset(x, childElement->ActualWidth, element->ActualWidth);
    auto calcY = CalculateVerticalCenterOffset(y, childElement->ActualHeight, element->ActualHeight);

    if (calcH < 0)
    {
        calcH = GutterBuffer;
    }
    else
    {
        // TODO: Correct right nudge positioning as it is incorrect
        if ((calcH > m_windowBounds.Width) || (calcH + childElement->ActualWidth) > m_windowBounds.Width)
        {
            calcH = m_windowBounds.Width - childElement->ActualWidth - GutterBuffer;
        }
    }

    UIElement^ parent = dynamic_cast<UIElement^>(m_hostPopup->Parent);
    if (parent != nullptr)
    {
        auto transform = parent->TransformToVisual(Window::Current->Content);
        auto offsetAdjustment = transform->TransformPoint(Point(0, 0));
        calcH -= offsetAdjustment.X;
        calcY -= offsetAdjustment.Y;
    }

    m_hostPopup->HorizontalOffset = calcH;
    m_hostPopup->VerticalOffset = calcY;
    m_hostPopup->IsHitTestVisible = true;
    m_hostPopup->Opacity = 1;

    // for entrance animation
    // UX guidelines show a PopIn animation
    Storyboard^ inAnimation = ref new Storyboard();
    PopInThemeAnimation^ popin = ref new PopInThemeAnimation();

    // TODO: Switch statement begs of refactoring
    switch (Placement)
    {
    case PlacementMode::Bottom:
        popin->FromVerticalOffset = -10;
        popin->FromHorizontalOffset = 0;
        break;
    case PlacementMode::Left:
        popin->FromVerticalOffset = 0;
        popin->FromHorizontalOffset = 10;
        break;
    case PlacementMode::Right:
        popin->FromVerticalOffset = 0;
        popin->FromHorizontalOffset = -10;
        break;
    case PlacementMode::Top:
        popin->FromVerticalOffset = 10;
        popin->FromHorizontalOffset = 0;
        break;
    }
    Storyboard::SetTarget(popin, m_hostPopup);
    inAnimation->Children->Append(popin);
    inAnimation->Begin();
}

std::array<Point, 4> Flyout::GetTransformedPoints(FrameworkElement^ element, bool isRTL, FrameworkElement^ relativeTo)
{
    std::array<Point, 4> pointArray;
    if ((element != nullptr) && (relativeTo != nullptr))
    {
        float actualHeight = static_cast<float>(element->ActualHeight);
        float actualWidth = static_cast<float>(element->ActualWidth);
        GeneralTransform^ gt = relativeTo->TransformToVisual(m_rootVisual);
        pointArray[0] = gt->TransformPoint(Point(0.0, 0.0));
        pointArray[1] = gt->TransformPoint(Point(actualWidth, 0.0));
        pointArray[2] = gt->TransformPoint(Point(0.0, actualHeight));
        pointArray[3] = gt->TransformPoint(Point(actualWidth, actualHeight));

        FrameworkElement^ _el = dynamic_cast<FrameworkElement^>(m_rootVisual);
        bool flag = (_el != nullptr) ? (_el->FlowDirection == Windows::UI::Xaml::FlowDirection::RightToLeft) : false;
        if (isRTL != flag)
        {
            // TODO: Handle RTL - GetTransformedPoints
            //for (int i = 0; i < pointArray.size; i++)
            //{
            //    pointArray[i].X = _windowBounds->Width - pointArray[i].X;
            //}
        }
    }
    return pointArray;
}

Rect Flyout::GetBounds(const array<Point, 4>& interestPoints)
{
    float num2;
    float num4;
    float x = num2 = interestPoints[0].X;
    float y = num4 = interestPoints[0].Y;
    for (unsigned int idx = 1; idx < interestPoints.size(); idx++)
    {
        float num6 = interestPoints[idx].X;
        float num7 = interestPoints[idx].Y;
        if (num6 < x)
        {
            x = num6;
        }
        if (num6 > num2)
        {
            num2 = num6;
        }
        if (num7 < y)
        {
            y = num7;
        }
        if (num7 > num4)
        {
            num4 = num7;
        }
    }
    return Rect(x, y, static_cast<float>((num2 - x) + 1.0), static_cast<float>((num4 - y) + 1.0));
}

Point Flyout::PlacePopup(Rect window, const array<Point, 4>& target, const array<Point, 4>& flyout, PlacementMode placement)
{
    vector<Point> pointArray;
    float y = 0.0;
    float x = 0.0;
    auto bounds = GetBounds(target);
    auto rect2 = GetBounds(flyout);
    float width = rect2.Width;
    float height = rect2.Height;
    if (placement == PlacementMode::Right)
    {
        float num5 = max<float>(0.0, target[0].X);
        float num6 = window.Width - min<float>(window.Width, target[1].X + 1.0f);
        if ((num6 < width) && (num6 < num5))
        {
            placement = PlacementMode::Left;
        }
    }
    else if (placement == PlacementMode::Left)
    {
        float num7 = window.Width - min<float>(window.Width, target[1].X + 1.0f);
        float num8 = max<float>(0.0f, target[0].X);
        if ((num8 < width) && (num8 < num7))
        {
            placement = PlacementMode::Right;
        }
    }
    else if (placement == PlacementMode::Top)
    {
        float num9 = std::max<float>(0.0f, target[0].Y);
        float num10 = window.Height - std::min<float>(window.Height, target[2].Y + 1.0f);
        if ((num9 < height) && (num9 < num10))
        {
            placement = PlacementMode::Bottom;
        }
    }
    else if (placement == PlacementMode::Bottom)
    {
        float num11 = std::max<float>(0.0, target[0].Y);
        float num12 = window.Height - std::min<float>(window.Height, target[2].Y + 1.0f);
        if ((num12 < height) && (num12 < num11))
        {
            placement = PlacementMode::Top;
        }
    }
    switch (placement)
    {
    case PlacementMode::Bottom:
        {
            std::array<Point, 4> temp = { 
                Point(target[2].X, max<float>(0.0f, (target[2].Y + 1.0f))), 
                Point((target[3].X - width) + 1.0f, max<float>(0.0f, (float)(target[2].Y + 1.0f))), 
                Point(0.0f, max<float>(0.0f, (target[2].Y + 1.0f))) 
            };
            pointArray.insert(begin(pointArray), begin(temp), end(temp));
        }
        break;

    case PlacementMode::Right:
        {
            std::array<Point, 3> temp = { 
                Point(max<float>(0.0f, (target[1].X + 1.0f)), target[1].Y), 
                Point(max<float>(0.0f, (target[3].X + 1.0f)), (target[3].Y - height) + 1.0f), 
                Point(max<float>(0.0f, (target[1].X + 1.0f)), 0.0f) };
            pointArray.insert(begin(pointArray), begin(temp), end(temp));
        }
        break;

    case PlacementMode::Left:
        {
            array<Point, 3> temp = { 
                Point(min<float>(window.Width, target[0].X) - width, target[1].Y), 
                Point(min<float>(window.Width, target[2].X) - width, (target[3].Y - height) + 1.0f), 
                Point(min<float>(window.Width, target[0].X) - width, 0.0f) };
            pointArray.insert(begin(pointArray), begin(temp), end(temp));
        }
        break;

    case PlacementMode::Top:
        {
            array<Point, 3> temp = { 
                Point(target[0].X, min<float>(target[0].Y, window.Height) - height), 
                Point((target[1].X - width) + 1.0f, min<float>(target[0].Y, window.Height) - height), 
                Point(0.0f, min<float>(target[0].Y, window.Height) - height) };
            pointArray.insert(begin(pointArray), begin(temp), end(temp));
        }
        break;

    default:
        {
            array<Point, 1> temp = { Point(0.0f, 0.0f) };
            pointArray.insert(begin(pointArray), begin(temp), end(temp));
        }
        break;
    }
    float num13 = width * height;
    unsigned int index = 0;
    float num15 = 0.0f;
    for (unsigned int i = 0; i < pointArray.size(); i++)
    {
        Rect rect3(pointArray[i].X, pointArray[i].Y, width, height);
        rect3.Intersect(window);
        float d = rect3.Width * rect3.Height;
        if (d == numeric_limits<float>::infinity())
        {
            index = pointArray.size() - 1;
            break;
        }
        if (d > num15)
        {
            index = i;
            num15 = d;
        }
        if (d == num13)
        {
            index = i;
            break;
        }
    }
    // x = pointArray[index].X;
    x = pointArray[0].X; // TODO: taking this solves my horizontal nudging, but is a hack...keeping it though until a better solution
    y = pointArray[index].Y;
    if (index > 1)
    {
        if ((placement == PlacementMode::Left) || (placement == PlacementMode::Right))
        {
            if (((y != target[0].Y) && (y != target[1].Y)) && (((y + height) != target[0].Y) && ((y + height) != target[1].Y)))
            {
                float num18 = bounds.Top + (bounds.Height / 2.0f);
                if ((num18 > 0.0) && ((num18 - 0.0f) > (window.Height - num18)))
                {
                    y = window.Height - height;
                }
                else
                {
                    y = 0.0f;
                }
            }
        }
        else if (((placement == PlacementMode::Top) || (placement == PlacementMode::Bottom)) && (((x != target[0].X) && (x != target[1].X)) && (((x + width) != target[0].X) && ((x + width) != target[1].X))))
        {
            float num19 = bounds.Left + (bounds.Width / 2.0f);
            if ((num19 > 0.0f) && ((num19 - 0.0f) > (window.Width - num19)))
            {
                x = window.Width - width;
            }
            else
            {
                x = 0.0f;
            }
        }
    }

    m_realizedPlacement = placement;
    auto p = Point(x, y);
    return p;
}

double Flyout::CalculateGutter(double offset)
{
    double trueOffset = offset;

    // TODO: Need to fix Left/Right gutter adjustments
    switch (m_realizedPlacement)
    {
    case PlacementMode::Bottom:
        trueOffset = offset + GutterBufferAdjusted;
        break;
    case PlacementMode::Left:
        trueOffset = offset - GutterBufferAdjusted;
        break;
    case PlacementMode::Right:
        trueOffset = offset + GutterBufferAdjusted;
        break;
    case PlacementMode::Top:
        trueOffset = offset - GutterBufferAdjusted;
        break;
    default:
        break;
    }

    return trueOffset;
}

double Flyout::CalculateHorizontalCenterOffset(double initialOffset, double flyoutWidth, double elementWidth)
{
    double newX = 0.0;

    if (m_realizedPlacement == PlacementMode::Top || m_realizedPlacement == PlacementMode::Bottom)
    {
        newX = HorizontalOffset + initialOffset - ((flyoutWidth / 2) - (elementWidth / 2));
    }
    else
    {
        newX = HorizontalOffset + initialOffset;
    }
    return newX;
}

double Flyout::CalculateVerticalCenterOffset(double initialOffset, double flyoutHeight, double elementHeight)
{
    double newY = 0.0;

    if (m_realizedPlacement == PlacementMode::Top || m_realizedPlacement == PlacementMode::Bottom)
    {
        newY = VerticalOffset + initialOffset;
    }
    else
    {
        newY = VerticalOffset + initialOffset - (flyoutHeight / 2) + (elementHeight / 2);
    }
    return CalculateGutter(newY);
}
