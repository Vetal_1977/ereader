#include "pch.h"
#include "WatermarkTextBox.h"
#include "VisualStates.h"

using namespace Callisto::Controls::Common;
using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::ApplicationModel::Resources;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;
using namespace std;

WatermarkTextBox::WatermarkTextBox() 
{
    DefaultStyleKey = WatermarkTextBox::typeid->FullName;
    // "Callisto/Resources"
    m_resources = ResourceLoader::GetForCurrentView("Callisto/Resources");
    // set some default text
    // TODO: Localize this string.
    String^ text = m_resources->GetString("WatermarkTextBoxDefault");
    Watermark = text;

    m_gotFoucusToken = GotFocus += ref new RoutedEventHandler(this, &WatermarkTextBox::OnGotFocus);
    m_lostFoucusToken = LostFocus += ref new RoutedEventHandler(this, &WatermarkTextBox::OnLostFocus);
    m_pointerEnteredToken = PointerEntered += ref new PointerEventHandler(this, &WatermarkTextBox::OnPointerEntered);
    m_pointerExitedToken = PointerExited += ref new PointerEventHandler(this, &WatermarkTextBox::OnPointerExited);
    m_textChangedToken = TextChanged += ref new TextChangedEventHandler(this, &WatermarkTextBox::OnTextChanged);
    m_loadedToken = Loaded += ref new RoutedEventHandler(this, &WatermarkTextBox::OnLoaded);
}

WatermarkTextBox::~WatermarkTextBox()
{
    GotFocus -= m_gotFoucusToken;
    LostFocus -= m_lostFoucusToken;
    PointerEntered -= m_pointerEnteredToken;
    PointerExited -= m_pointerExitedToken;
    TextChanged -= m_textChangedToken;
}

// Dependency Properties

static DependencyProperty^ _watermarkProperty =
    DependencyProperty::Register("Watermark", 
    TypeName(String::typeid),
    TypeName(WatermarkTextBox::typeid), 
    ref new PropertyMetadata(L"", ref new PropertyChangedCallback(&WatermarkTextBox::OnWatermarkPropertyChanged)));

DependencyProperty^ WatermarkTextBox::WatermarkProperty::get()
{
    return _watermarkProperty;
}

Object^ WatermarkTextBox::Watermark::get()
{
    return dynamic_cast<Object^>(GetValue(WatermarkProperty));
}

void WatermarkTextBox::Watermark::set(Object^ value)
{
    SetValue(WatermarkProperty, value);
}

// Overrides

void WatermarkTextBox::OnApplyTemplate()
{
    TextBox::OnApplyTemplate();

    m_elementContent = dynamic_cast<TextBlock^>(GetTemplateChild("PART_Watermark"));

    OnWatermarkChanged();

    ChangeVisualState(false);
}

// Event Handlers

void WatermarkTextBox::OnWatermarkPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto wmt = dynamic_cast<WatermarkTextBox^>(sender);

    if (wmt != nullptr)
    {
        Control^ watermarkControl = dynamic_cast<Control^>(wmt->Watermark);
        if (watermarkControl != nullptr)
        {
            watermarkControl->IsTabStop = true;
            watermarkControl->IsHitTestVisible = true;
        }
    }
}

void WatermarkTextBox::OnWatermarkChanged()
{
    if (m_elementContent != nullptr)
    {
        auto watermarkControl = dynamic_cast<Control^>(Watermark);
        if (watermarkControl != nullptr)
        {
            watermarkControl->IsTabStop = false;
            watermarkControl->IsHitTestVisible = false;
        }
    }
}

void WatermarkTextBox::OnGotFocus(Object^ sender, RoutedEventArgs^ e)
{
    if (IsEnabled)
    {
        m_hasFocusInternal = true;

        ChangeVisualState();
    }
}

void WatermarkTextBox::OnLostFocus(Object^ sender, RoutedEventArgs^ e)
{
    m_hasFocusInternal = false;
    ChangeVisualState();
}


void WatermarkTextBox::OnPointerEntered(Object^ sender, PointerRoutedEventArgs^ e)
{
    m_isHovered = true;

    if (!m_hasFocusInternal)
    {
        ChangeVisualState();
    }
}

void WatermarkTextBox::OnPointerExited(Object^ sender, PointerRoutedEventArgs^ e)
{
    m_isHovered = false;

    if (!m_hasFocusInternal)
    {
        ChangeVisualState();
    }
}

void WatermarkTextBox::OnTextChanged(Object^ sender, TextChangedEventArgs^ e)
{
    ChangeVisualState();
}

void WatermarkTextBox::OnLoaded(Object^ sender, RoutedEventArgs^ e)
{
    ApplyTemplate();
    ChangeVisualState(false);
}

// Methods

void WatermarkTextBox::ChangeVisualState()
{
    ChangeVisualState(true);
}

void WatermarkTextBox::ChangeVisualState(bool useTransitions)
{
    // Update the CommonStates group
    vector<Platform::String^> states;
    if (!IsEnabled)
    {
        states.push_back(StateDisabled);
        states.push_back(StateNormal);
        VisualStates::GoToState(this, useTransitions, states);
    }
    else if (m_isHovered)
    {
        states.push_back(StatePointerOver);
        states.push_back(StateNormal);
        VisualStates::GoToState(this, useTransitions, states);
    }
    else
    {
        states.push_back(StateNormal);
        VisualStates::GoToState(this, useTransitions, states);
    }

    // Update the FocusStates group
    states.clear();
    if (m_hasFocusInternal && IsEnabled)
    {
        states.push_back(StateFocused);
        states.push_back(StateUnfocused);
        VisualStates::GoToState(this, useTransitions, states);
    }
    else
    {
        states.push_back(StateUnfocused);
        VisualStates::GoToState(this, useTransitions, states);
    }

    // Update the WatermarkStates group
    states.clear();
    if (Watermark != nullptr && (Text == "" || Text == nullptr) && !m_hasFocusInternal)
    {
        states.push_back(StateWatermarked);
        states.push_back(StateUnwatermarked);
        VisualStates::GoToState(this, useTransitions, states);
    }
    else
    {
        states.push_back(StateUnwatermarked);
        VisualStates::GoToState(this, useTransitions, states);
    }
}

