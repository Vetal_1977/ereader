#pragma once

#include "Clipper.h"
#include "ExpandDirection.h"

namespace Callisto
{
    namespace Controls
    {
        namespace Primitives
        {
            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class LinearClipper sealed : public Clipper
            {
            public:
                // Dependency Properties
                static property Windows::UI::Xaml::DependencyProperty^ ExpandDirectionProperty
                {
                    Windows::UI::Xaml::DependencyProperty^ get();
                };

                property Callisto::Controls::ExpandDirection ExpandDirection
                {
                    Callisto::Controls::ExpandDirection get();
                    void set(Callisto::Controls::ExpandDirection value);
                }

            internal:
                static void OnExpandDirectionPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

            protected:
                virtual void OnExpandDirectionChanged(Callisto::Controls::ExpandDirection oldValue, Callisto::Controls::ExpandDirection newValue);
                virtual void ClipContent() override;
            };
        }
    }
}

