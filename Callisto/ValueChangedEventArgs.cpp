#include "pch.h"
#include "ValueChangedEventArgs.h"

using namespace Callisto::Controls;

ValueChangedEventArgs::ValueChangedEventArgs(double oldValue, double newValue) : m_oldValue(oldValue), m_newValue(newValue)
{
}

double ValueChangedEventArgs::OldValue::get()
{
    return m_oldValue;
}

double ValueChangedEventArgs::NewValue::get()
{
    return m_newValue;
}