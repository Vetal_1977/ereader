#include "pch.h"
#include "ToggleMenuItem.h"

using namespace Callisto::Controls;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Interop;

ToggleMenuItem::ToggleMenuItem()
{
    DefaultStyleKey = ToggleMenuItem::typeid->FullName;
}

// Dependency Properties
static DependencyProperty^ _isCheckedProperty =
    DependencyProperty::Register("IsChecked", 
    TypeName(bool::typeid), 
    TypeName(ToggleMenuItem::typeid), 
    nullptr);

DependencyProperty^ ToggleMenuItem::IsCheckedProperty::get()
{
    return _isCheckedProperty;
}

bool ToggleMenuItem::IsChecked::get()
{
    return static_cast<bool>(GetValue(IsCheckedProperty));
}

void ToggleMenuItem::IsChecked::set(bool value)
{
    SetValue(IsCheckedProperty, value);
}