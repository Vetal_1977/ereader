#include "pch.h"
#include "ItemsControlExtension.h"

using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media;

ItemsControlExtension::ItemsControlExtension(ItemsControl^ control) : m_itemsControl(control)
{
}

Panel^ ItemsControlExtension::GetItemsHost()
{
    // Lookup the ItemsHost if we haven't already cached it.
    if (m_itemsHost == nullptr && m_itemsControl != nullptr && m_itemsControl->ItemContainerGenerator != nullptr)
    {
        // Get any live container
        DependencyObject^ container = m_itemsControl->ContainerFromIndex(0);
        if (container != nullptr)
        {
            // Get the parent of the container
            m_itemsHost = dynamic_cast<Panel^>(VisualTreeHelper::GetParent(container));
        }
    }

    return m_itemsHost;
}

ScrollViewer^ ItemsControlExtension::GetScrollHost()
{
    if (m_scrollHost == nullptr)
    {
        Panel^ itemsHost = m_itemsHost;
        if (itemsHost != nullptr)
        {
            for (DependencyObject^ obj = itemsHost; obj != m_itemsControl && obj != nullptr; obj = VisualTreeHelper::GetParent(obj))
            {
                ScrollViewer^ viewer = dynamic_cast<ScrollViewer^>(obj);
                if (viewer != nullptr)
                {
                    m_scrollHost = viewer;
                    break;
                }
            }
        }
    }
    return m_scrollHost;
}

void ItemsControlExtension::PrepareContainerForItemOverride(Windows::UI::Xaml::DependencyObject^ element, Windows::UI::Xaml::Style^ parentItemContainerStyle)
{
    Control^ control = dynamic_cast<Control^>(element);
    if (parentItemContainerStyle != nullptr && control != nullptr && control->Style == nullptr)
    {
        control->SetValue(Control::StyleProperty, parentItemContainerStyle);
    }
}

void ItemsControlExtension::OnApplyTemplate()
{
    m_itemsHost = nullptr;
    m_scrollHost = nullptr;
}

void ItemsControlExtension::UpdateItemContainerStyle(Style^ itemContainerStyle)
{
    if (itemContainerStyle == nullptr)
    {
        return;
    }

    Panel^ itemsHost = m_itemsHost;
    if (itemsHost == nullptr || itemsHost->Children == nullptr)
    {
        return;
    }

    IVector<UIElement^>^ vec = itemsHost->Children;
    for (auto element : vec)
    {
        UIElement^ uiElement = element;
        auto obj = dynamic_cast<FrameworkElement^>(uiElement);
        if (obj->Style == nullptr)
        {
            obj->Style = itemContainerStyle;
        }
    }
}

void ItemsControlExtension::ScrollIntoView(Windows::UI::Xaml::FrameworkElement^ element)
{
    // Get the ScrollHost
    ScrollViewer^ scrollHost = m_scrollHost;
    if (scrollHost == nullptr)
    {
        return;
    }

    // Get the position of the element relative to the ScrollHost
    GeneralTransform^ transform = nullptr;
    try
    {
        transform = element->TransformToVisual(scrollHost);
    }
    catch (Exception^)
    {
        // Ignore failures when not in the visual tree
        return;
    }
    Rect itemRect = Rect(
        transform->TransformPoint(Point()),
        transform->TransformPoint(Point(static_cast<float>(element->ActualWidth), static_cast<float>(element->ActualHeight))));

    // Scroll vertically
    double verticalOffset = scrollHost->VerticalOffset;
    double verticalDelta = 0;
    double hostBottom = scrollHost->ViewportHeight;
    double itemBottom = itemRect.Bottom;
    if (hostBottom < itemBottom)
    {
        verticalDelta = itemBottom - hostBottom;
        verticalOffset += verticalDelta;
    }
    double itemTop = itemRect.Top;
    if (itemTop - verticalDelta < 0)
    {
        verticalOffset -= verticalDelta - itemTop;
    }
    //scrollHost->ScrollToVerticalOffset(verticalOffset);
	//scrollHost->ChangeView(0.0, (double)verticalOffset, 1.0f);

    // Scroll horizontally
    double horizontalOffset = scrollHost->HorizontalOffset;
    double horizontalDelta = 0;
    double hostRight = scrollHost->ViewportWidth;
    double itemRight = itemRect.Right;
    if (hostRight < itemRight)
    {
        horizontalDelta = itemRight - hostRight;
        horizontalOffset += horizontalDelta;
    }
    double itemLeft = itemRect.Left;
    if (itemLeft - horizontalDelta < 0)
    {
        horizontalOffset -= horizontalDelta - itemLeft;
    }
    //scrollHost->ScrollToHorizontalOffset(horizontalOffset);
	
	scrollHost->ChangeView((double)horizontalOffset, (double)verticalOffset, 1.0f, true);
}