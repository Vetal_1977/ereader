#pragma once

#include "INotifySettingChanged.h"
#include "SettingChangedEventArgs.h"

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            ref class VisualElement;
        }

		public ref class SettingsFlyoutWidth sealed
		{
		public:
			static property double Narrow { double get() { return 346.0; } };
			static property double Wide { double get() { return 646.0; } };
		};

        namespace SettingsManagement
        {
            interface class ISettingsCommandInfo;

            [Windows::Foundation::Metadata::WebHostHidden]
            public ref class AppSettings sealed : public INotifySettingChanged
            {
            public:
                AppSettings();
                virtual ~AppSettings();

                virtual event Windows::Foundation::EventHandler<SettingChangedEventArgs^>^ SettingChanged
                {
                    Windows::Foundation::EventRegistrationToken add(Windows::Foundation::EventHandler<SettingChangedEventArgs^>^ e);
                    void remove(Windows::Foundation::EventRegistrationToken t);
                }

				void AddCommand(Windows::UI::Xaml::Controls::SettingsFlyout^ settingsFlyout, Platform::String^ headerKey);

                void SignalSettingChanged(Platform::String^ settingName);

            private:
				std::map<Platform::String^, Windows::UI::Xaml::Controls::SettingsFlyout^> m_commands;
                Callisto::Controls::Common::VisualElement^ m_visualElement;
                Windows::Foundation::EventRegistrationToken m_commandsRequestedToken;
                bool m_settingChangedObserved;

                event Windows::Foundation::EventHandler<SettingChangedEventArgs^>^ m_settingChangedEvent;

                void OnCommandsRequested(Windows::UI::ApplicationSettings::SettingsPane^ sender, Windows::UI::ApplicationSettings::SettingsPaneCommandsRequestedEventArgs^ e);
                void OnSettingsCommandInvoked(Windows::UI::Popups::IUICommand^ command);
            };
        }
    }
}