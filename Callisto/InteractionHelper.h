#pragma once

namespace Callisto
{
    namespace Controls
    {
        namespace Common
        {
            interface class IUpdateVisualState;

            class InteractionHelper
            {
            public:
                InteractionHelper(Windows::UI::Xaml::Controls::Control^ control);
                ~InteractionHelper();

                // Properties
                Windows::UI::Xaml::Controls::Control^ GetControl();
                bool IsPointerPressed();
                bool IsPointerOver();
                bool IsFocused();
                unsigned int GetClickCount();
                Windows::System::VirtualKey GetLogicalKey(Windows::UI::Xaml::FlowDirection flowDirection, Windows::System::VirtualKey originalKey);

                bool AllowPointerPressed(Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
                void OnPointerPressedBase();
                void OnPointerReleasedBase();
                bool AllowPointerEnter(Windows::UI::Xaml::RoutedEventArgs^ e);
                bool AllowPointerExit(Windows::UI::Xaml::RoutedEventArgs^ e);
                
                bool AllowKeyDown(Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e);
                
                void UpdateVisualStateBase(bool useTransitions);

            private:
                // TODO: See if I can make these WeakReferences
                IUpdateVisualState^ m_updateVisualState;
                Windows::UI::Xaml::Controls::Control^ m_control;
                bool m_isPointerPressed;
                bool m_isPointerOver;
                bool m_isFocused;
                bool m_isReadOnly;
                bool m_isPointerExited;
                Windows::Foundation::DateTime m_lastClickTime;
                Windows::Foundation::Point m_lastClickPosition;
                unsigned int m_clickCount;

                // Event Tokens
                Windows::Foundation::EventRegistrationToken m_controlLoadedToken;
                Windows::Foundation::EventRegistrationToken m_controlIsEnabledChangedToken;

                void UpdateVisualState(bool useTransitions);
            };
        }
    }
}