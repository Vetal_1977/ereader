#pragma once


namespace Callisto
{
    namespace Controls
    {
        public enum class RatingSelectionMode
        {
            Continuous,
            Individual
        };
    }
}