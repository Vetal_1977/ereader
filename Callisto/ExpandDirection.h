#pragma once

namespace Callisto
{
    namespace Controls
    {
        public enum class ExpandDirection
        {
            Down,
            Up,
            Left,
            Right
        };
    }
}