﻿#pragma once

namespace Callisto
{
    namespace Controls
    {
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class Flyout sealed : public Windows::UI::Xaml::Controls::ContentControl
        {
        public:
            Flyout();
            virtual ~Flyout();

            event Windows::Foundation::EventHandler<Platform::Object^>^ Closed;

            property Windows::UI::Xaml::Controls::Primitives::Popup^ HostPopup
            {
                Windows::UI::Xaml::Controls::Primitives::Popup^ get();
            }

            static property Windows::UI::Xaml::DependencyProperty^ IsOpenProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property bool IsOpen
            {
                bool get();
                void set(bool value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ HostMarginProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Thickness HostMargin
            {
                Windows::UI::Xaml::Thickness get();
                void set(Windows::UI::Xaml::Thickness value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ PlacementProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::Controls::Primitives::PlacementMode Placement
            {
                Windows::UI::Xaml::Controls::Primitives::PlacementMode get();
                void set(Windows::UI::Xaml::Controls::Primitives::PlacementMode value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ HorizontalOffsetProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double HorizontalOffset
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ VerticalOffsetProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property double VerticalOffset
            {
                double get();
                void set(double value);
            }

            static property Windows::UI::Xaml::DependencyProperty^ PlacementTargetProperty
            {
                Windows::UI::Xaml::DependencyProperty^ get();
            };

            property Windows::UI::Xaml::UIElement^ PlacementTarget
            {
                Windows::UI::Xaml::UIElement^ get();
                void set(Windows::UI::Xaml::UIElement^ value);
            }

        internal:
            static void OnIsOpenPropertyChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

        private:
            Windows::UI::Xaml::Controls::Primitives::Popup^ m_hostPopup;
            Windows::Foundation::EventRegistrationToken m_activatedToken;
            Windows::Foundation::EventRegistrationToken m_sizeChangedToken;
            Windows::Foundation::EventRegistrationToken m_inputPaneShowingToken;
            Windows::Foundation::EventRegistrationToken m_inputPaneHidingToken;
            Windows::Foundation::Rect m_windowBounds;
            Windows::UI::Xaml::UIElement^ m_rootVisual;
            Windows::UI::Xaml::Controls::Primitives::PlacementMode m_realizedPlacement;
            bool m_ihmFocusMoved;
            double m_ihmOccludeHeight;

            void OnWindowActivated(Platform::Object^ sender, Windows::UI::Core::WindowActivatedEventArgs^ e);
            void OnLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
            void OnHostPopupClosed(Platform::Object^ sender, Platform::Object^ e);
            void OnHostPopupOpened(Platform::Object^ sender, Platform::Object^ e);
            void OnInputPaneShowing(Windows::UI::ViewManagement::InputPane^ sender, Windows::UI::ViewManagement::InputPaneVisibilityEventArgs^ e);
            void OnInputPaneHiding(Windows::UI::ViewManagement::InputPane^ sender, Windows::UI::ViewManagement::InputPaneVisibilityEventArgs^ e);

            void PerformPlacement(double horizontalOffset, double verticalOffset);
            std::array<Windows::Foundation::Point, 4> GetTransformedPoints(Windows::UI::Xaml::FrameworkElement^ element, bool isRTL, Windows::UI::Xaml::FrameworkElement^ relativeTo);
            Windows::Foundation::Rect GetBounds(const std::array<Windows::Foundation::Point, 4>& interestPoints);
            Windows::Foundation::Point PlacePopup(Windows::Foundation::Rect window, const std::array<Windows::Foundation::Point, 4>& target, const std::array<Windows::Foundation::Point, 4>& flyout, Windows::UI::Xaml::Controls::Primitives::PlacementMode placement);
            double CalculateGutter(double offset);
            double CalculateHorizontalCenterOffset(double initialOffset, double flyoutWidth, double elementWidth);
            double CalculateVerticalCenterOffset(double initialOffset, double flyoutHeight, double elementHeight);
        };
    }
}