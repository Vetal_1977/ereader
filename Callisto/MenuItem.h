#pragma once
#include "MenuItemInternal.h"


namespace Callisto
{
    namespace Controls
    {
        [Windows::UI::Xaml::TemplateVisualState(Name = "Base", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Hover", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Pressed", GroupName = "Common")]
        [Windows::UI::Xaml::TemplateVisualState(Name = "Disabled", GroupName = "Common")]
        [Windows::Foundation::Metadata::WebHostHidden]
        public ref class MenuItem sealed : public MenuItemInternal
        {
        public:
            MenuItem();

        private:
            bool m_isActive;
        };
    }
}

