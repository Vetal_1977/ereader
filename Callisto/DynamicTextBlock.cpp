#include "pch.h"
#include "DynamicTextBlock.h"

using namespace Callisto::Controls;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Input;
using namespace Windows::UI::ViewManagement;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media::Animation;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml;
using namespace std;

DynamicTextBlock::DynamicTextBlock() 
{
    m_textBlock = ref new TextBlock();
    Content = m_textBlock;
}

DynamicTextBlock::~DynamicTextBlock()
{
}

// Dependency Properties

static DependencyProperty^ _textProperty =
    DependencyProperty::Register("Text", 
    TypeName(String::typeid),
    TypeName(DynamicTextBlock::typeid), 
    ref new PropertyMetadata(L"", ref new PropertyChangedCallback(&DynamicTextBlock::OnTextPropertyChanged)));

DependencyProperty^ DynamicTextBlock::TextProperty::get()
{
    return _textProperty;
}

String^ DynamicTextBlock::Text::get()
{
    return dynamic_cast<String^>(GetValue(TextProperty));
}

void DynamicTextBlock::Text::set(String^ value)
{
     SetValue(TextProperty, value);
}

static DependencyProperty^ _textWrappingProperty =
    DependencyProperty::Register("TextWrapping", 
    TypeName(TextWrapping::typeid),
    TypeName(DynamicTextBlock::typeid), 
    ref new PropertyMetadata(TextWrapping::NoWrap, ref new PropertyChangedCallback(&DynamicTextBlock::OnTextWrappingPropertyChanged)));

DependencyProperty^ DynamicTextBlock::TextWrappingProperty::get()
{
    return _textWrappingProperty;
}

Windows::UI::Xaml::TextWrapping DynamicTextBlock::TextWrapping::get()
{
    return static_cast<Windows::UI::Xaml::TextWrapping>(GetValue(TextWrappingProperty));
}

void DynamicTextBlock::TextWrapping::set(Windows::UI::Xaml::TextWrapping value)
{
    SetValue(TextWrappingProperty, value);
}

static DependencyProperty^ _lineHeightProperty =
    DependencyProperty::Register("LineHeight", 
    TypeName(double::typeid),
    TypeName(DynamicTextBlock::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&DynamicTextBlock::OnLineHeightPropertyChanged)));

DependencyProperty^ DynamicTextBlock::LineHeightProperty::get()
{
    return _lineHeightProperty;
}

double DynamicTextBlock::LineHeight::get()
{
    return static_cast<double>(GetValue(LineHeightProperty));
}

void DynamicTextBlock::LineHeight::set(double value)
{
    SetValue(LineHeightProperty, value);
}

static DependencyProperty^ _lineStackingStrategyProperty =
    DependencyProperty::Register("LineStackingStrategy", 
    TypeName(LineStackingStrategy::typeid),
    TypeName(DynamicTextBlock::typeid), 
    ref new PropertyMetadata(LineStackingStrategy::BlockLineHeight, ref new PropertyChangedCallback(&DynamicTextBlock::OnLineStackingStrategyPropertyChanged)));

DependencyProperty^ DynamicTextBlock::LineStackingStrategyProperty::get()
{
    return _lineStackingStrategyProperty;
}

Windows::UI::Xaml::LineStackingStrategy DynamicTextBlock::LineStackingStrategy::get()
{
    return static_cast<Windows::UI::Xaml::LineStackingStrategy>(GetValue(LineStackingStrategyProperty));
}

void DynamicTextBlock::LineStackingStrategy::set(Windows::UI::Xaml::LineStackingStrategy value)
{
    SetValue(LineStackingStrategyProperty, value);
}

Size DynamicTextBlock::MeasureOverride(Size availableSize)
{
    // just to make the code easier to read
    bool wrapping = (this->TextWrapping == Windows::UI::Xaml::TextWrapping::Wrap);

    Size unboundSize = wrapping ? Size(availableSize.Width, numeric_limits<float>::infinity()) : Size(numeric_limits<float>::infinity(), availableSize.Height);
    auto reducedText = this->Text;

    // set the text and measure it to see if it fits without alteration
    if (reducedText == nullptr || reducedText->IsEmpty()) 
    {
            reducedText = L"";
    }
    m_textBlock->Text = reducedText;
    Size textSize = ContentControl::MeasureOverride(unboundSize);

    while (wrapping ? textSize.Height > availableSize.Height : textSize.Width > availableSize.Width)
    {
        unsigned int length = reducedText->Length();
        unsigned int prevLength = reducedText->Length();

        if (length > 0)
        {
            reducedText = ReduceText(reducedText);
        }

        if (length == prevLength)
        {
            break;
        }

        m_textBlock->Text = reducedText + "...";
        textSize = ContentControl::MeasureOverride(unboundSize);
    }

    return ContentControl::MeasureOverride(availableSize);
}

Platform::String^ DynamicTextBlock::ReduceText(Platform::String^ text)
{
    wstring temp = text->Data();
    wstring sub = temp.substr(0, temp.length() - 1);
    return ref new String(sub.c_str());
}
// Event Handlers

void DynamicTextBlock::OnTextPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto dynamicTextBlock = dynamic_cast<DynamicTextBlock^>(sender);
    dynamicTextBlock->OnTextChanged(e);
}

void DynamicTextBlock::OnTextChanged(DependencyPropertyChangedEventArgs^ e)
{
    InvalidateMeasure();
}

void DynamicTextBlock::OnTextWrappingPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto dynamicTextBlock = dynamic_cast<DynamicTextBlock^>(sender);
    dynamicTextBlock->OnTextWrappingChanged(e);
}

void DynamicTextBlock::OnTextWrappingChanged(DependencyPropertyChangedEventArgs^ e)
{
    m_textBlock->TextWrapping = static_cast<Windows::UI::Xaml::TextWrapping>(e->NewValue);
    InvalidateMeasure();
}

void DynamicTextBlock::OnLineHeightPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto dynamicTextBlock = dynamic_cast<DynamicTextBlock^>(sender);
    dynamicTextBlock->OnLineHeightChanged(e);
}

void DynamicTextBlock::OnLineHeightChanged(DependencyPropertyChangedEventArgs^ e)
{
    m_textBlock->LineHeight = static_cast<double>(e->NewValue);
    InvalidateMeasure();
}

void DynamicTextBlock::OnLineStackingStrategyPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto dynamicTextBlock = dynamic_cast<DynamicTextBlock^>(sender);
    dynamicTextBlock->OnTextWrappingChanged(e);
}

void DynamicTextBlock::OnLineStackingStrategyChanged(DependencyPropertyChangedEventArgs^ e)
{
    m_textBlock->LineStackingStrategy = static_cast<Windows::UI::Xaml::LineStackingStrategy>(e->NewValue);
    InvalidateMeasure();
}