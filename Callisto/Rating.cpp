#include "pch.h"
#include "Rating.h"
#include "RatingItem.h"
#include "ItemsControlExtension.h"
#include "InteractionHelper.h"
#include "VisualStates.h"

using namespace Callisto::Controls;
using namespace Callisto::Controls::Common;
using namespace Platform;
using namespace Windows::System;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Input;
using namespace std;

static DependencyProperty^ m_displayValueProperty =
    DependencyProperty::Register("DisplayValue", 
    TypeName(double::typeid), 
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&Rating::OnDisplayValuePropertyChanged)));

DependencyProperty^ Rating::DisplayValueProperty::get()
{
    return m_displayValueProperty;
}

double Rating::DisplayValue::get()
{
    return static_cast<double>(GetValue(DisplayValueProperty));
}

void Rating::DisplayValue::set(double value)
{
    SetValue(DisplayValueProperty, value);
}

static DependencyProperty^ _selectionModeProperty =
    DependencyProperty::Register("SelectionMode", 
    TypeName(RatingSelectionMode::typeid), 
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(RatingSelectionMode::Continuous, ref new PropertyChangedCallback(&Rating::OnSelectionModePropertyChanged)));

DependencyProperty^ Rating::SelectionModeProperty::get()
{
    return _selectionModeProperty;
}

RatingSelectionMode Rating::SelectionMode::get()
{
    return static_cast<RatingSelectionMode>(GetValue(SelectionModeProperty));
}

void Rating::SelectionMode::set(RatingSelectionMode value)
{
    SetValue(ItemCountProperty, value);
}

static DependencyProperty^ _itemCountProperty =
    DependencyProperty::Register("ItemCount", 
    TypeName(uint32_t::typeid), 
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&Rating::OnItemCountPropertyChanged)));

DependencyProperty^ Rating::ItemCountProperty::get()
{
    return _itemCountProperty;
}

unsigned int Rating::ItemCount::get()
{
    return static_cast<unsigned int>(GetValue(ItemCountProperty));
}

void Rating::ItemCount::set(unsigned int value)
{
    SetValue(ItemCountProperty, value);
}

static DependencyProperty^ _valueProperty =
    DependencyProperty::Register("Value", 
    TypeName(double::typeid), 
    TypeName(Rating::typeid), 
    ref new PropertyMetadata(0.0, ref new PropertyChangedCallback(&Rating::OnValuePropertyChanged)));

DependencyProperty^ Rating::ValueProperty::get()
{
    return _valueProperty;
}

double Rating::Value::get()
{
    return static_cast<double>(GetValue(ValueProperty));
}

void Rating::Value::set(double value)
{
    SetValue(ValueProperty, value);
}

double Rating::WeightedValue::get()
{
    return m_weightedValue;
}

static DependencyProperty^ _pointerOverFillProperty =
    DependencyProperty::Register("PointerOverFill", 
    TypeName(SolidColorBrush::typeid), 
    TypeName(Rating::typeid), 
    nullptr);

DependencyProperty^ Rating::PointerOverFillProperty::get()
{
    return _pointerOverFillProperty;
}

SolidColorBrush^ Rating::PointerOverFill::get()
{
    return static_cast<SolidColorBrush^>(GetValue(PointerOverFillProperty));
}

void Rating::PointerOverFill::set(SolidColorBrush^ value)
{
    SetValue(PointerOverFillProperty, value);
}

static DependencyProperty^ _pointerPressedFillProperty =
    DependencyProperty::Register("PointerPressedFill", 
    TypeName(SolidColorBrush::typeid), 
    TypeName(Rating::typeid), 
    nullptr);

DependencyProperty^ Rating::PointerPressedFillProperty::get()
{
    return _pointerPressedFillProperty;
}

SolidColorBrush^ Rating::PointerPressedFill::get()
{
    return static_cast<SolidColorBrush^>(GetValue(PointerPressedFillProperty));
}

void Rating::PointerPressedFill::set(SolidColorBrush^ value)
{
    SetValue(PointerPressedFillProperty, value);
}

// Event Handlers

void Rating::OnDisplayValuePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto source = dynamic_cast<Rating^>(sender);
    source->OnDisplayValueChanged();
}

void Rating::OnDisplayValueChanged()
{
    UpdateDisplayValues();
}

void Rating::OnSelectionModePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto source = static_cast<Rating^>(sender);
    auto oldValue = static_cast<RatingSelectionMode>(e->OldValue);
    auto newValue = static_cast<RatingSelectionMode>(e->NewValue);
    source->OnSelectionModeChanged(oldValue, newValue);
}

void Rating::OnSelectionModeChanged(RatingSelectionMode oldValue, RatingSelectionMode newValue)
{
    UpdateDisplayValues();
}

void Rating::OnItemContainerStylePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    Rating^ source = static_cast<Rating^>(sender);
    Windows::UI::Xaml::Style^ newValue = static_cast<Windows::UI::Xaml::Style^>(e->NewValue);
    source->OnItemContainerStyleChanged(newValue);
}

void Rating::OnItemContainerStyleChanged(Windows::UI::Xaml::Style^ newValue)
{
    m_itemsControlHelper->UpdateItemContainerStyle(newValue);
}

void Rating::OnItemCountPropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto source = dynamic_cast<Rating^>(sender);
    unsigned int value = static_cast<unsigned int>(e->NewValue);
    source->OnItemCountChanged(value);
}

void Rating::OnItemCountChanged(unsigned int newValue)
{
    unsigned int amountToAdd = newValue - Items->Size;
    if (amountToAdd > 0)
    {
        for (unsigned int cnt = 0; cnt < amountToAdd; cnt++)
        {
            Items->Append(ref new RatingItem());
        }
    }
    else if (amountToAdd < 0)
    {
        for (unsigned int cnt = 0; cnt < static_cast<unsigned int>(abs(static_cast<int>(amountToAdd))); cnt++)
        {
            Items->RemoveAt(Items->Size - 1);
        }
    }
}

void Rating::OnValuePropertyChanged(DependencyObject^ sender, DependencyPropertyChangedEventArgs^ e)
{
    auto source = static_cast<Rating^>(sender);
    double oldValue = static_cast<double>(e->OldValue);
    double newValue = static_cast<double>(e->NewValue);
    source->OnValueChanged(oldValue, newValue);
}

void Rating::OnValueChanged(double oldValue, double newValue)
{
    m_weightedValue = newValue / ItemCount;

    UpdateValues();

    /*ValueChangedEventHandler<double> handler = ValueChanged;
    if (handler != null)
    {
    handler(this, new ValueChangedEventArgs<double>(oldValue, newValue));
    }*/
}

void Rating::OnItemsChanged(Object^ e)
{
    ItemCount = Items->Size;

    ItemsControl::OnItemsChanged(e);
}

void Rating::OnApplyTemplate()
{
    m_itemsControlHelper->OnApplyTemplate();
    ItemsControl::OnApplyTemplate();
}

void Rating::OnPointerEntered(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerEnter(e))
    {
        m_interactionHelper->UpdateVisualStateBase(true);
    }
    ItemsControl::OnPointerEntered(e);
}

void Rating::OnPointerExited(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerExit(e))
    {
        m_interactionHelper->UpdateVisualStateBase(true);
    }
    ItemsControl::OnPointerExited(e);
}

void Rating::OnPointerPressed(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerPressed(e))
    {
        m_interactionHelper->OnPointerPressedBase();
    }
    ItemsControl::OnPointerPressed(e);
}

void Rating::OnPointerReleased(PointerRoutedEventArgs^ e)
{
    if (m_interactionHelper->AllowPointerPressed(e))
    {
        m_interactionHelper->OnPointerReleasedBase();
    }
    ItemsControl::OnPointerReleased(e);
}

void Rating::OnRatingItemValueSelected(RatingItem^ ratingItem, double newValue)
{
    vector<RatingItem^> ratingItems = GetRatingItems();
    double total = ratingItems.size();
    vector<RatingItem^>::iterator it = find(ratingItems.begin(), ratingItems.end(), ratingItem);
    int pos = it - ratingItems.begin();
    Value = pos + 1;
}

void Rating::OnKeyDown(KeyRoutedEventArgs^ e)
{
    if (!m_interactionHelper->AllowKeyDown(e))
    {
        return;
    }

    ItemsControl::OnKeyDown(e);

    if (e->Handled)
    {
        return;
    }

    // Some keys (e.g. Left/Right) need to be translated in RightToLeft mode
    VirtualKey invariantKey = m_interactionHelper->GetLogicalKey(FlowDirection, e->Key);

    switch (invariantKey)
    {
    case VirtualKey::Left:
        {
            auto ratingItem = dynamic_cast<RatingItem^>(FocusManager::GetFocusedElement());

            if (ratingItem != nullptr)
            {
                ratingItem = GetRatingItemAtOffsetFrom(ratingItem, -1);
            }
            else
            {
                auto items = GetRatingItems();
                if (items.size() > 0)
                {
                    ratingItem = items[0];
                }
            }
            if (ratingItem != nullptr)
            {
                if (ratingItem->Focus(Windows::UI::Xaml::FocusState::Keyboard))
                {
                    e->Handled = true;
                }
            }
        }
        break;
    case VirtualKey::Right:
        {
            auto ratingItem =  dynamic_cast<RatingItem^>(FocusManager::GetFocusedElement());

            if (ratingItem != nullptr)
            {
                ratingItem = GetRatingItemAtOffsetFrom(ratingItem, 1);
            }
            else
            {
                auto items = GetRatingItems();
                if (items.size() > 0)
                {
                    ratingItem = items[0];
                }
            }
            if (ratingItem != nullptr)
            {
                if (ratingItem->Focus(Windows::UI::Xaml::FocusState::Keyboard))
                {
                    e->Handled = true;
                }
            }
        }
        break;
    case VirtualKey::Add:
        {
            if (IsEnabled)
            {
                auto ratingItem = GetSelectedRatingItem();
                if (ratingItem != nullptr)
                {
                    ratingItem = GetRatingItemAtOffsetFrom(ratingItem, 1);
                }
                else
                {
                    auto items = GetRatingItems();
                    if (items.size() > 0)
                    {
                        ratingItem = items[0];
                    }
                }
                if (ratingItem != nullptr)
                {
                    ratingItem->SelectValue();
                    e->Handled = true;
                }
            }
        }
        break;
    case VirtualKey::Subtract:
        {
            if (IsEnabled)
            {
                auto ratingItem = GetSelectedRatingItem();
                if (ratingItem != nullptr)
                {
                    ratingItem = GetRatingItemAtOffsetFrom(ratingItem, -1);
                }
                if (ratingItem != nullptr)
                {
                    ratingItem->SelectValue();
                    e->Handled = true;
                }
            }
        }
        break;
    }
}

bool Rating::IsItemItsOwnContainerOverride(Object^ item)
{
    return (dynamic_cast<RatingItem^>(item) != nullptr);
}

void Rating::PrepareContainerForItemOverride(DependencyObject^ element, Object^ item)
{
    auto ratingItem = dynamic_cast<RatingItem^>(element);
	
	auto index = this->IndexFromContainer(element);

    if (index > -1)
    {
        wstringstream data;
        data << index + 1;

        ToolTipService::SetToolTip(ratingItem, ref new String(data.str().c_str()));
    }
    
    auto binding = ref new Binding();
    binding->Path = ref new PropertyPath("Foreground");
    binding->Source = this;
    ratingItem->SetBinding(Control::ForegroundProperty, binding);
    binding = nullptr;

    binding = ref new Binding();
    binding->Path = ref new PropertyPath("PointerOverFill");
    binding->Source = this;
    ratingItem->SetBinding(RatingItem::PointerOverFillProperty, binding);
    binding = nullptr;

    binding = ref new Binding();
    binding->Path = ref new PropertyPath("PointerPressedFill");
    binding->Source = this;
    ratingItem->SetBinding(RatingItem::PointerPressedFillProperty, binding);
    binding = nullptr;

    binding = ref new Binding();
    binding->Path = ref new PropertyPath("FontSize");
    binding->Source = this;
    ratingItem->SetBinding(Control::FontSizeProperty, binding);
    binding = nullptr;

    binding = ref new Binding();
    binding->Path = ref new PropertyPath("Tag");
    binding->Source = this;
    ratingItem->SetBinding(Control::TagProperty, binding);
    binding = nullptr;

    binding = ref new Binding();
    binding->Path = ref new PropertyPath("Tag");
    binding->Source = this;
    ratingItem->SetBinding(Control::BackgroundProperty, binding);
    binding = nullptr;

    ratingItem->IsEnabled = IsEnabled;
    if (ratingItem->Style == nullptr)
    {
        ratingItem->Style = ItemContainerStyle;
    }
    
    m_ratingItemTappedToken = ratingItem->Click += ref new RoutedEventHandler(this, &Rating::OnRatingItemTapped);
    m_ratingItemPointerEnterToken = ratingItem->PointerEntered += ref new PointerEventHandler(this, &Rating::OnRatingItemPointerEnter);
    m_ratingItemPointerExitedToken = ratingItem->PointerExited += ref new PointerEventHandler(this, &Rating::OnRatingItemPointerExited);

    ratingItem->ParentRating = this;
    ItemsControl::PrepareContainerForItemOverride(element, item);
}

void Rating::OnRatingItemPointerEnter(Object^ sender, PointerRoutedEventArgs^ e)
{
    m_hoveredRatingItem = dynamic_cast<RatingItem^>(sender);
    UpdateHoverStates();
}

void Rating::OnRatingItemPointerExited(Object^ sender, PointerRoutedEventArgs^ e)
{
    m_hoveredRatingItem = nullptr;
    UpdateDisplayValues();
    UpdateHoverStates();
}

void Rating::OnRatingItemTapped(Object^ sender, RoutedEventArgs^ e)
{
    if (IsEnabled)
    {
        auto item = dynamic_cast<RatingItem^>(sender);
        OnRatingItemValueSelected(item, 1.0);
    }
}

void Rating::ClearContainerForItemOverride(DependencyObject^ element, Object^ item)
{
    auto ratingItem = dynamic_cast<RatingItem^>(element);
    
    ratingItem->Click -= m_ratingItemTappedToken;
    ratingItem->PointerEntered -= m_ratingItemPointerEnterToken;
    ratingItem->PointerExited -= m_ratingItemPointerExitedToken;
    ratingItem->ParentRating = nullptr;

    if (ratingItem == m_hoveredRatingItem)
    {
        m_hoveredRatingItem = nullptr;
        UpdateDisplayValues();
        UpdateHoverStates();
    }

    ItemsControl::ClearContainerForItemOverride(element, item);
}

// Methods

DependencyObject^ Rating::GetContainerForItemOverride()
{
    return ref new RatingItem();
}

std::vector<RatingItem^> Rating::GetRatingItems()
{
    std::vector<RatingItem^> items;
    for (unsigned int index = 0; index < Items->Size; index++)
    {
        auto ratingItem = dynamic_cast<RatingItem^>(this->ContainerFromIndex(index));
        if (ratingItem != nullptr) {
            items.push_back(ratingItem);
        }
    }
    return items;
}

void Rating::UpdateDisplayValues()
{

}

void Rating::UpdateValues()
{

}

void Rating::UpdateVisualState(bool useTransitions)
{
    m_interactionHelper->UpdateVisualStateBase(useTransitions);
}

void Rating::UpdateHoverStates()
{
    if (m_hoveredRatingItem != nullptr && IsEnabled)
    {
        vector<RatingItem^> ratingItems = GetRatingItems();
        vector<RatingItem^>::iterator it = find(ratingItems.begin(), ratingItems.end(), m_hoveredRatingItem);
        unsigned int indexOfItem = it - ratingItems.begin();

        double total = ratingItems.size();
        double filled = indexOfItem + 1;

        DisplayValue = filled / total;

        for (unsigned int idx = 0; idx < ratingItems.size(); idx++)
        {
            auto ratingItem = ratingItems[idx];
            if (idx <= indexOfItem && SelectionMode == RatingSelectionMode::Continuous)
            {
                vector<Platform::String^> states;
                states.push_back(StatePointerOver);
                VisualStates::GoToState(ratingItem, true, states);
            }
            else
            {
                IUpdateVisualState^ updateVisualState = dynamic_cast<IUpdateVisualState^>(ratingItem);
                updateVisualState->UpdateVisualState(true);
            }
        }
    }
    else
    {
        DisplayValue = Value;
        vector<RatingItem^> items = GetRatingItems();
        for (auto item : items)
        {
            IUpdateVisualState^ updateVisualState = dynamic_cast<IUpdateVisualState^>(item);
            if (updateVisualState != nullptr)
            {
                updateVisualState->UpdateVisualState(true);
            }
        }
    }
}

bool IsGreaterThanZero (RatingItem^ item) {
    return item->Value > 0.0;
}

RatingItem^ Rating::GetSelectedRatingItem()
{
    vector<RatingItem^> items = GetRatingItems();

    vector<RatingItem^>::reverse_iterator item = find_if(items.rbegin(), items.rend(), IsGreaterThanZero);
    if (*item != nullptr)
    {
        return *item;
    }
    return nullptr;
}

RatingItem^ Rating::GetRatingItemAtOffsetFrom(RatingItem^ ratingItem, int offset)
{
    vector<RatingItem^> ratingItems = GetRatingItems();
    vector<RatingItem^>::iterator it = find(ratingItems.begin(), ratingItems.end(), m_hoveredRatingItem);
    if (*it == nullptr)
    {
        return nullptr;
    }
    unsigned int index = it - ratingItems.begin();
    index += offset;
    if (index >= 0 && index < ratingItems.size())
    {
        ratingItem = ratingItems[index];
    }
    else
    {
        ratingItem = nullptr;
    }
    return ratingItem;
}
