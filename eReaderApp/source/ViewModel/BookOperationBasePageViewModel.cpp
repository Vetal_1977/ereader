﻿/*
* @file BookOperationBasePageViewModel.cpp
* @brief Base view model for pages that operation on books (e.g. StartPage, LibraryPage view models)
*
* @date 2013-03-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include <ATLComTime.h>
#include "DelegateCommand.h"
#include "BookOperationBasePageViewModel.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "ExceptionHandling\TaskExceptionsExtensions.h"
#include "View\BookPage.xaml.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::Globalization::DateTimeFormatting;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::UI::Popups;

namespace eReader
{

BookOperationBasePageViewModel::BookOperationBasePageViewModel(BookReader^ bookReader) :
	_bookReader(bookReader),
	_hasStatusToDisplay(false),
	_statusString(L""),
	_isOperationInProgress(false)
{
	// action commands
	_importBookFromFileCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookOperationBasePageViewModel::_importBookFromFile),
		nullptr);
	_deleteBooksCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookOperationBasePageViewModel::_deleteBooks),
		ref new CanExecuteDelegate(this, &BookOperationBasePageViewModel::_canDeleteBooks));
}

IObservableVector<BookSet^>^ BookOperationBasePageViewModel::BookSets::get()
{ 
	return _getBookSets();
}

Windows::UI::Xaml::Input::ICommand^ BookOperationBasePageViewModel::ImportBookFromFileCommand::get()
{
	return _importBookFromFileCommand;
}

Windows::UI::Xaml::Input::ICommand^ BookOperationBasePageViewModel::DeleteBooksCommand::get()
{
	return _deleteBooksCommand;
}

bool BookOperationBasePageViewModel::HasStatusToDisplay::get()
{
	return _hasStatusToDisplay;
}

String^ BookOperationBasePageViewModel::StatusString::get()
{
	return _statusString;
}

bool BookOperationBasePageViewModel::IsOperationInProgress::get()
{
	return _isOperationInProgress;
}

void BookOperationBasePageViewModel::reEvaluateCommands()
{
	_deleteBooksCommand->RaiseCanExecuteChanged();
}

void BookOperationBasePageViewModel::OnBookSelected(Platform::Object^ selectedBook)
{
	auto selBookContainer = safe_cast<BookContainer^>(selectedBook);

	try
	{
		_bookReader->updateLastOpenedDate(selBookContainer->InternalBookData->BookId);
	}
	catch(ReaderException& e)
	{
		ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
	}
	
	NavigateToPage(TypeName(BookPage::typeid), selBookContainer->FullPath);
}

IObservableVector<BookSet^>^ BookOperationBasePageViewModel::_getBookSets()
{
	throw ref new NotImplementedException();
}

void BookOperationBasePageViewModel::_updateAllBookSets()
{
	throw ref new NotImplementedException();
}

void BookOperationBasePageViewModel::_importBookFromFile(_In_ Platform::Object^ parameter)
{
	auto picker = ref new FileOpenPicker();

	picker->SuggestedStartLocation = PickerLocationId::Desktop;
	picker->ViewMode = PickerViewMode::List;
	picker->FileTypeFilter->Append(ref new String(L".fb2"));
	picker->FileTypeFilter->Append(ref new String(L".zip"));
	picker->FileTypeFilter->Append(ref new String(L".epub"));
	picker->FileTypeFilter->Append(ref new String(L".mobi"));
	picker->FileTypeFilter->Append(ref new String(L".txt"));

	task<IVectorView<StorageFile^>^> pickFiles(picker->PickMultipleFilesAsync());
	pickFiles.then([=](IVectorView<StorageFile^>^ files)
	{
		if (files->Size > 0)
		{
			_updateStatus(true, ResLoader::getString(L"AddBooksStatus"));
			_activateOperationInProgress(true);

			static size_t addBookCount = 0;
			static bool anyImportSucceeded = false;
			static bool allImportsSucceeded = true;
			const size_t noOfBooksToAdd = files->Size;
			addBookCount = 0;
			anyImportSucceeded = false;
			allImportsSucceeded = true;
			for each(StorageFile^ file in files)
			{
				BookData^ bookData = ref new BookData();
				auto importBookRunTask = create_task(_bookReader->importBookFromFile(file, bookData));
				importBookRunTask.then([=](task<bool> importBookTask)
				{
					bool succeeded = false;
					try
					{
						succeeded = importBookTask.get();
					}
					catch (ReaderException& ex)
					{
						ExceptionPolicyFactory::getCurrentPolicy()->handleException(ex);
						succeeded = false;
					}
					anyImportSucceeded = anyImportSucceeded || succeeded;
					allImportsSucceeded = allImportsSucceeded && succeeded;
					if (++addBookCount == noOfBooksToAdd)
					{
						if (anyImportSucceeded)
						{
							// update the book sets after all book are added
							_updateAllBookSets();
						}
						_updateStatus(false, L"");
						_activateOperationInProgress(false);

						if (!allImportsSucceeded)
						{
							MessageDialog^ msgDialog = ref new MessageDialog(Utils::ResLoader::getString("CannotAddSomeBooks"));
							msgDialog->ShowAsync();
						}
					}
				});
				//}).then(ObserveException<void>(ExceptionPolicyFactory::getCurrentPolicy(), false)).then([=, &allImportsSucceeded]()
			}
		}
	});
}

void BookOperationBasePageViewModel::_deleteBooks(_In_ Platform::Object^ books)
{
	auto confirmationDialog = ref new MessageDialog(ResLoader::getString("DeleteBooksConfirm"));

	auto yesCommand = ref new UICommand(ResLoader::getString("Yes"), nullptr);
	yesCommand->Id = safe_cast<Object^>((int)CommandId::Yes);
	confirmationDialog->Commands->Append(yesCommand);

	auto noCmd = ref new UICommand(ResLoader::getString("No"), nullptr);
	noCmd->Id = safe_cast<Object^>((int)CommandId::No);
	confirmationDialog->Commands->Append(noCmd);
	confirmationDialog->DefaultCommandIndex = 1;

	task<IUICommand^> showDialogTask(confirmationDialog->ShowAsync());
	showDialogTask.then([=](IUICommand^ cmd)
	{
		if ((int)cmd->Id == CommandId::Yes)
		{
			auto booksLocal = safe_cast<IVector<Object^>^>(books);
			assert(booksLocal != nullptr);
			assert(booksLocal->Size > 0);

			_updateStatus(true, ResLoader::getString(L"DeleteBooksStatus"));
			_activateOperationInProgress(true);

			// delete books asynchronously
			static size_t deleteBookCount = 0;
			const size_t noOfBooksToDelete = booksLocal->Size;
			deleteBookCount = 0;
			for each (Object^ bookContainer in booksLocal)
			{
				task<void> deleteBookRunTask = create_task(_bookReader->deleteBook(safe_cast<BookContainer^>(bookContainer)->InternalBookData));
				deleteBookRunTask.then([=](task<void> deleteBookTask)
				{
					try
					{
						deleteBookTask.get();
					}
					catch (ReaderException& ex)
					{
						ExceptionPolicyFactory::getCurrentPolicy()->handleException(ex);
					}
				
					if (++deleteBookCount == noOfBooksToDelete)
					{
						// update the book sets after all books are deleted
						_updateAllBookSets();
						_updateStatus(false, L"");
						_activateOperationInProgress(false);
					}
				});
			}
		}
	});
}

bool BookOperationBasePageViewModel::_canDeleteBooks(_In_ Platform::Object^ books)
{
	return _containBooks(books);
}

bool BookOperationBasePageViewModel::_containBooks(_In_ Platform::Object^ books)
{
	auto booksLocal = safe_cast<IVector<Object^>^>(books);
	if (booksLocal == nullptr || booksLocal->Size == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void BookOperationBasePageViewModel::_updateStatus(bool displayStatus, String^ statusString)
{
	_hasStatusToDisplay = displayStatus;
	_statusString = statusString;
	OnPropertyChanged("StatusString");
	OnPropertyChanged("HasStatusToDisplay");
}

void BookOperationBasePageViewModel::_activateOperationInProgress(bool isActive)
{
	_isOperationInProgress = isActive;
	OnPropertyChanged("IsOperationInProgress");
}

}
