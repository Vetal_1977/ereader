/*
* @file CRDocViewCallback.h
* @brief WinRT abstract callback class that mirror CR::LVDocViewCallback
*
* This callback should be implemented by a WinRT class and will be called
* from the callback transformer as a response to a call of a corresponding
* CR::LVDocViewCallback method
*
* @date 2013-02-02
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "lvtinydom.h"

namespace eReader
{

	ref class CRDocViewCallback abstract
	{
	internal:
		/// on starting file loading
		virtual void OnLoadFileStart(_In_ Platform::String^ filename) = 0;
		/// format detection finished
		virtual void OnLoadFileFormatDetected(_In_ doc_format_t fileFormat) = 0;
		/// file loading is finished successfully - drawCoveTo() may be called there
		virtual void OnLoadFileEnd() = 0;
		/// first page is loaded from file an can be formatted for preview
		virtual void OnLoadFileFirstPagesReady() = 0;
		/// file progress indicator, called with values 0..100
		virtual void OnLoadFileProgress(_In_ int percent) = 0;
		/// document formatting started
		virtual void OnFormatStart() = 0;
		/// document formatting finished
		virtual void OnFormatEnd() = 0;
		/// format progress, called with values 0..100
		virtual void OnFormatProgress(_In_ int percent) = 0;
		/// format progress, called with values 0..100
		virtual void OnExportProgress(_In_ int percent) = 0;
		/// file load finished with error
		virtual void OnLoadFileError(_In_ Platform::String^ message) = 0;
		/// Override to handle external links
		virtual void OnExternalLink(_In_ Platform::String^ url, _In_ ldomNode* node) = 0;
		/// Called when page images should be invalidated (clearImageCache() called in LVDocView)
		virtual void OnImageCacheClear() = 0;
		/// return true if reload will be processed by external code, false to let internal code process it
		virtual bool OnRequestReload() = 0;
	};

}
