/** 
 * @file 	SearchTextObject.h
 *
 * @brief 	<brief description>
 *
 * Help class represented a search text object. Stores the current search text and navigates to the found place in the book text
 *
 * @author 	Copyright 2013 Vitaly Bezgachev; vitaly.bezgachev@gmail.com. All rights reserved.
 * @date 	2013-12-01
 *
 * Change history: 
 *
 * - <date, author, description> 
 *
 */

#include "pch.h"
#include "SearchTextObject.h"

#define MAX_FOUND_WORD_COUNT	200

namespace eReader
{
namespace ViewModel
{

SearchTextObject::SearchTextObject(LVDocView* lvDocView) :
	_lvDocView(lvDocView)
{
	reset();
}


SearchTextObject::~SearchTextObject(void)
{
}

const bool SearchTextObject::findAndMoveToText(Platform::String^ pattern, const TextSearchDirection searchDirection, const bool caseInsensitive)
{
	assert(_lvDocView != nullptr);

	// check whether any search parameter has been change last time
	bool searchParamsChanged = false;
	if (caseInsensitive)
	{
		searchParamsChanged = (_wcsicmp(pattern->Data(), _pattern.data()) != 0);
	}
	else
	{
		searchParamsChanged = (wcscmp(pattern->Data(), _pattern.data()) != 0);
	}

	if (searchParamsChanged)
	{
		_pattern = pattern->Data();
	}

	if (_pattern.empty())
	{
		reset();
		return false;
	}

	if (_searchDirection != searchDirection)
	{
		_searchDirection = searchDirection;
	}

	if (_caseInsensitive != caseInsensitive)
	{
		searchParamsChanged = true;
		_caseInsensitive = caseInsensitive;
	}

	// if any changes was made then search in the text again or we reached the end of the array then search again
	ldomDocument* document = _lvDocView->getDocument();
	assert(document != nullptr);

	// determine parameters for CR engine searching
	lvRect rc;
	_lvDocView->GetPos(rc);

	// see <...>\crengine\android\jni\docview.cpp for conditions
	int origin = 0;
	bool reverse = false;
	int start = -1;
	int end = -1;
	switch(_searchDirection)
	{
	case TextSearchDirection::CurrentToFirst:
		{
			reverse = true;
			if (searchParamsChanged)
			{
				origin = 0;
				end = rc.bottom;
			}
			else
			{
				origin = 1;
				end = _lastFoundPos;
			}
		}
		break;

	case TextSearchDirection::CurrentToLast:
		{
			reverse = false;
			if (searchParamsChanged)
			{
				origin = 0;
				start = rc.top;
			}
			else
			{
				origin = 1;
				start = _lastFoundPos;
			}
		}
		break;

	case TextSearchDirection::FirstToCurrent:
		origin = -1;
		reverse = false;
		end = rc.top;
		break;

	case TextSearchDirection::LastToCurrent:
		origin = -1;
		reverse = true;
		start = rc.bottom;
		break;
	}

	LVArray<ldomWord> words;
	if (document->findText(_pattern, _caseInsensitive, reverse, start, end, words, MAX_FOUND_WORD_COUNT, rc.height()))
	{
		// select found words in the document
		_lvDocView->clearSelection();
		_lvDocView->selectWords(words);
	}
	else
	{
		return false;
	}

	// go to the next found word
	ldomMarkedRangeList* ranges = _lvDocView->getMarkedRanges();
	if (ranges)
	{
		if (ranges->length() > 0)
		{
			_lastFoundPos = ranges->get(0)->start.y;
			_lvDocView->SetPos(_lastFoundPos);

			// ensures that the last found position is skipped by next search
			if (reverse)
			{
				_lastFoundPos -= _lvDocView->getFontSize() * 3 / 2;
			}
			else
			{
				_lastFoundPos += _lvDocView->getFontSize() * 3 / 2;
			}
			return true;
		}
	}

	return false;
}

void SearchTextObject::reset()
{
	_pattern.clear();
	_searchDirection = TextSearchDirection::CurrentToLast;
	_caseInsensitive = true;
	_lastFoundPos = 0;

	_lvDocView->clearSelection();
}

} // ViewModel
} // eReader