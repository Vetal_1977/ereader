﻿/*
* @file BookPageViewModel.cpp
* @brief Represents data of a single book page
*
* @date 2013-01-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/
#include "pch.h"
#include "BookPageViewModel.h"
#include "Presentation\CRPageDrawBuffer.h"
#include "Utils\Helper.h"

using namespace Platform;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Data;
using namespace eReader::Presentation;
using namespace eReader::Model;
using namespace eReader::ViewModel;

namespace eReader
{

BookPageViewModel::BookPageViewModel(DXRenderer^ renderer, BookReader^ bookReader)
{
	_drawBuffer = ref new CRPageDrawBuffer(renderer, bookReader);
	_content = new ContentImageSource(renderer, _drawBuffer);
}

ImageSource^ BookPageViewModel::Content::get()
{
	return _content->getImageSource();
}

int BookPageViewModel::DisplayPageDelta::get()
{
	assert(_drawBuffer != nullptr);
	return _drawBuffer->DisplayPageDelta;
}	

void BookPageViewModel::updateDisplayPageDelta(int newNumber, const bool updateBuffer)
{
	assert(_drawBuffer != nullptr);
	_drawBuffer->updateDisplayPageDelta(newNumber, updateBuffer);
	_content->updateContent(_drawBuffer);
	PropertyChanged(this, ref new PropertyChangedEventArgs("Content"));
}

}
