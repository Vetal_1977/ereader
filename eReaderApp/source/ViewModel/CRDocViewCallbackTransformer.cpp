/*
* @file CRDocViewCallbackTransformer.cpp
* @brief The class implements native callback interface and calls appropriate WinRT callback interface
*
* The class implements CR::LVDocViewCallback and calls appropriate methods of
* ILVDocViewCallbackWinRT callback class
*
* @date 2013-01-04
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "CRDocViewCallbackTransformer.h"

using namespace Platform;

namespace eReader
{

CRDocViewCallbackTransformer::CRDocViewCallbackTransformer(CRDocViewCallback^ winRTCallback) :
	_winRTCallback(winRTCallback)
{
}

void CRDocViewCallbackTransformer::OnLoadFileStart(lString16 filename)
{
	String^ filenameWinRT = ref new String(filename.c_str());
	_winRTCallback->OnLoadFileStart(filenameWinRT);
}

void CRDocViewCallbackTransformer::OnLoadFileFormatDetected(doc_format_t fileFormat)
{
	_winRTCallback->OnLoadFileFormatDetected(fileFormat);
}

void CRDocViewCallbackTransformer::OnLoadFileEnd()
{
	_winRTCallback->OnLoadFileEnd();
}

void CRDocViewCallbackTransformer::OnLoadFileFirstPagesReady()
{
	_winRTCallback->OnLoadFileFirstPagesReady();
}

void CRDocViewCallbackTransformer::OnLoadFileProgress(int percent)
{
	_winRTCallback->OnLoadFileProgress(percent);
}

void CRDocViewCallbackTransformer::OnFormatStart()
{
	_winRTCallback->OnFormatStart();
}

void CRDocViewCallbackTransformer::OnFormatEnd()
{
	_winRTCallback->OnFormatEnd();
}

void CRDocViewCallbackTransformer::OnFormatProgress(int percent)
{
	_winRTCallback->OnFormatProgress(percent);
}

void CRDocViewCallbackTransformer::OnExportProgress(int percent)
{
	_winRTCallback->OnExportProgress(percent);
}

void CRDocViewCallbackTransformer::OnLoadFileError(lString16 message)
{
	String^  messageWinRT = ref new String(message.c_str());
	_winRTCallback->OnLoadFileError(messageWinRT);
}

void CRDocViewCallbackTransformer::OnExternalLink(lString16 url, ldomNode* node)
{
	String^ urlWinRT = ref new String(url.c_str());
	_winRTCallback->OnExternalLink(urlWinRT, node);
}

void CRDocViewCallbackTransformer::OnImageCacheClear()
{
	_winRTCallback->OnImageCacheClear();
}

bool CRDocViewCallbackTransformer::OnRequestReload()
{
	return _winRTCallback->OnRequestReload();
}

}
