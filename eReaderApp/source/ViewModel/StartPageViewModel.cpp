﻿/*
* @file StartPageViewModel.cpp
* @brief View model of a StartPage
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "StartPageViewModel.h"
#include "View\BookPage.xaml.h"
#include "View\LibraryPage.xaml.h"
#include "View\LastReadPage.xaml.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "DelegateCommand.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::UI::Xaml::Input;

#define MAX_NO_LAST_READ_BOOKS 4
#define MAX_NO_LAST_ADDED_BOOKS 6

namespace eReader
{

StartPageViewModel::StartPageViewModel(BookReader^ bookReader) : BookOperationBasePageViewModel(bookReader),
	_startPageBookSets(ref new Vector<BookSet^>())
{
}

void StartPageViewModel::LoadState(IMap<String^, Object^>^ /*stateMap*/)
{
	_updateAllBookSets();
}

void StartPageViewModel::SaveState(IMap<String^, Object^>^ /*stateMap*/)
{
}

void StartPageViewModel::OnBookSetSelected(Platform::Object^ selectedBookSet)
{
	auto selBookSet = safe_cast<BookSet^>(selectedBookSet);
	switch (selBookSet->getSetType())
	{
	case BookSetType::Library:
		NavigateToPage(TypeName(LibraryPage::typeid), nullptr);
		break;

	case BookSetType::LastRead:
		NavigateToPage(TypeName(LastReadPage::typeid), nullptr);
		break;

	default:
		break;
	}
}

IObservableVector<BookSet^>^ StartPageViewModel::_getBookSets()
{
	return _startPageBookSets;
}

void StartPageViewModel::_updateAllBookSets()
{
	assert(_startPageBookSets != nullptr);

	_startPageBookSets->Clear();

	// create 2 book sets for a start page: "last read" and "currently added"
	// each set contains max. 6 books
	{
		BookSet^ currentlyReadBookSet = ref new BookSet(BookSetType::LastRead);
		currentlyReadBookSet->Name = ResLoader::getString(L"CurrentlyRead");
	
		IVector<BookData^>^ lastReadBooks = _bookReader->getLastReadBooks(MAX_NO_LAST_READ_BOOKS);
		for each (BookData^ bookData in lastReadBooks)
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentlyReadBookSet->Books->Append(bookContainer);
		}

		_startPageBookSets->InsertAt(BookSetIdx::LastRead, currentlyReadBookSet);
	}

	{
		BookSet^ lastAddedBookSet = ref new BookSet(BookSetType::Library);
		lastAddedBookSet->Name = ResLoader::getString(L"Library_LastAdded");
	
		IVector<BookData^>^ lastAddedBooks = _bookReader->getLastAddedBooks(MAX_NO_LAST_ADDED_BOOKS);
		for each (BookData^ bookData in lastAddedBooks)
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			lastAddedBookSet->Books->Append(bookContainer);
		}
		_startPageBookSets->InsertAt(BookSetIdx::Library, lastAddedBookSet);
	}

	OnPropertyChanged("BookSets");
}

}
