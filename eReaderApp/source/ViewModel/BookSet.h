/*
* @file BookSet.h
* @brief Book set class
*
* Class contains a vector of books and has a name. 
* May be used directly or by library subsets. 
* My be bound by GUI elements
*
* @date 2013-01-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "Common\BindableBase.h"
#include "BookContainer.h"

namespace eReader
{
	namespace ViewModel
	{
		public enum class BookSetType
		{
			// Displayed on the StartPage
			LastRead = 1,
			Library,

			// Displayed on the LibraryPage
			Authors,
			Titles,
			LastAdded,
			Collections
		};

		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class BookSet sealed : eReader::Common::BindableBase
		{
		internal:
			BookSet(BookSetType bookSetType);

		public:
			property Windows::Foundation::Collections::IObservableVector<BookContainer^>^ Books 
			{ 
				Windows::Foundation::Collections::IObservableVector<BookContainer^>^ get();
			}
			property Platform::String^ Name { Platform::String^ get(); void set(Platform::String^ value); }
			 
		internal:
			// Returns a type of this book set
			// @return Well-known book set type
			const BookSetType getSetType() const;

		private:
			Platform::String^ _name;
			Platform::Collections::Vector<BookContainer^>^ _books;
			BookSetType _bookSetType;
		};
	} // Model
}

