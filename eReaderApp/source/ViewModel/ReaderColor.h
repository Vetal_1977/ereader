/*
* @file ReaderColor.h
* @brief The class represents reader colors (background texture file and font color)
*
* @date 2013-01-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
	//
	// Reader color type combination
	//
	enum class ColorCombinationType
	{
		Texture_Foreground = 1,
		Background_Foreground
	};

	//
	// Represent a single color combination for reader page
	//
	ref class ReaderColor
	{
	internal:
		// Constructs this object
		// @param textureFilename Name of a texture file
		// @param fontColor Color of the font that must be visible for the specified texture
		// @param activeBookmarkFilename File name containing an active bookmark icon
		// @param activeBookmarkFilename File name containing an inactive bookmark icon
		ReaderColor(Platform::String^ textureFilename, const Windows::UI::Color& fontColor,
			Platform::String^ activeBookmarkFilename, Platform::String^ inactiveBookmarkFilename);

		// Constructs this object
		// @param backgroundColor Color of the font that must be visible for the specified texture
		// @param fontColor Color of the font that must be visible for the specified texture
		// @param activeBookmarkFilename File name containing an active bookmark icon
		// @param activeBookmarkFilename File name containing an inactive bookmark icon
		ReaderColor(const Windows::UI::Color& backgroundColor, const Windows::UI::Color& fontColor,
			Platform::String^ activeBookmarkFilename, Platform::String^ inactiveBookmarkFilename);

		property Platform::String^ TextureFilename { Platform::String^ get(); }
		property const Windows::UI::Color FontColor { const Windows::UI::Color get(); }
		property const Windows::UI::Color BackgroundColor { const Windows::UI::Color get(); }
		property const ColorCombinationType ColorCombination { const ColorCombinationType get(); }
		property Platform::String^ ActiveBookmarkFilename { Platform::String^ get(); }
		property Platform::String^ InactiveBookmarkFilename { Platform::String^ get(); }

	private:
		Platform::String^ _textureFilename;
		Windows::UI::Color _backgroundColor;
		Windows::UI::Color _fontColor;
		ColorCombinationType _colorCombinationType;
		Platform::String^ _activeBookmarkFilename;
		Platform::String^ _inactiveBookmarkFilename;
	};

}
