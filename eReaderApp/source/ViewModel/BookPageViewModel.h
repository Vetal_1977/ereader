﻿/*
* @file BookPageViewModel.h
* @brief Represents data of a single book page
*
* @date 2013-01-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "ViewModelBase.h"
#include "Presentation\ContentImageSource.h"
#include "lvdocview.h"

namespace eReader
{
	//
	// The BookPageViewModel class contains the presentation logic of a single page
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class BookPageViewModel sealed : public Windows::UI::Xaml::Data::INotifyPropertyChanged
	{
	internal:
		BookPageViewModel(Presentation::DXRenderer^ renderer, ViewModel::BookReader^ bookReader);

	public:
		virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;
		property Windows::UI::Xaml::Media::ImageSource^ Content { Windows::UI::Xaml::Media::ImageSource^ get(); }

	internal:
		property int DisplayPageDelta { int get(); }

		// Invoked to update a page view according to new numbering
		// @param newNumber Number of a new page that becoming a current page
		// @param updateBuffer true if a draw buffer of the page need to be updated
		void updateDisplayPageDelta(_In_ int newNumber, _In_ const bool updateBuffer);

	private:
		Microsoft::WRL::ComPtr<Presentation::ContentImageSource> _content;
		Presentation::CRPageDrawBuffer^ _drawBuffer;
	};
}
