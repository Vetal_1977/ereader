/** 
 * @file 	SearchTextObject.h
 *
 * @brief 	<brief description>
 *
 * Help class represented a search text object. Stores the current search text and navigates to te found place in the book text
 *
 * @author 	Copyright 2013 Vitaly Bezgachev; vitaly.bezgachev@gmail.com. All rights reserved.
 * @date 	2013-12-01
 *
 * Change history: 
 *
 * - <date, author, description> 
 *
 */

#pragma once

namespace eReader
{
	namespace ViewModel
	{
		//
		// Text search direction, used in the search in text method
		//
		enum TextSearchDirection
		{
			FirstToCurrent = 0,
			CurrentToFirst,
			CurrentToLast,
			LastToCurrent
		};

		//
		// Search text object class
		//
		class SearchTextObject 
		{
		public:
			SearchTextObject(LVDocView* _lvDocView);
			virtual ~SearchTextObject();

			// Searches for the specified text pattern and moves to the found position
			// @param pattern Text pattern
			// @param searchDirection Text search direction (from first page to current, current to first page, etc.) 
			// @param caseInsensitive True if a case-insensitive search
			// @return true if the text was found and the navigation occurred
			const bool findAndMoveToText(_In_ Platform::String^ pattern, _In_ const TextSearchDirection searchDirection, _In_ const bool caseInsensitive);

			// Resets the search parameters. To be called if the user hide/cancel the current search
			void reset();

		private:
			LVDocView* _lvDocView;
			lString16 _pattern;
			TextSearchDirection _searchDirection;
			bool _caseInsensitive;
			int _lastFoundPos;
		};
	} // ViewModel
} // eReader
