/*
* @file FilteredLibraryPageViewModel.cpp
* @brief View model of a FilteredLibraryPage
*
* @date 2013-06-11
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DelegateCommand.h"
#include "FilteredLibraryPageViewModel.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "View\FilteredLibraryPage.xaml.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::UI::Popups;

namespace eReader
{

FilteredLibraryPageViewModel::FilteredLibraryPageViewModel(BookReader^ bookReader) :	BookOperationBasePageViewModel(bookReader)
{
}

Object^ FilteredLibraryPageViewModel::DataSource::get()
{
	Vector<ViewModel::BookSet^>^ bookSets = ref new Vector<ViewModel::BookSet^>();
	bookSets->Append(safe_cast<ViewModel::BookSet^>(_bookReader->SelectedLibraryBookSet));
	return bookSets;
}

IObservableVector<BookSet^>^ FilteredLibraryPageViewModel::_getBookSets()
{
	// no internal book sets - return empty
	return ref new Vector<ViewModel::BookSet^>();
}

void FilteredLibraryPageViewModel::_updateAllBookSets()
{
	// nothing to update
}

}
