﻿/*
* @file LibraryPageViewModel.cpp
* @brief View model of a StartPage
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DelegateCommand.h"
#include "LibraryPageViewModel.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "View\FilteredLibraryPage.xaml.h"

#define MAX_NO_LAST_ADDED_LIBRARY_BOOKS 16 

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::UI::Popups;

namespace eReader
{

String^ const LibraryPageViewModel::_currentBookSetIdxKey = "currentBookSetIdx";

LibraryPageViewModel::LibraryPageViewModel(BookReader^ bookReader) :	BookOperationBasePageViewModel(bookReader),
	_authorBookSets(ref new Vector<BookSet^>()),
	_titleBookSets(ref new Vector<BookSet^>()),
	_lastAddedBookSets(ref new Vector<BookSet^>()),
	_collectionBookSets(ref new Vector<BookSet^>()),
	_bookCollectionNames(ref new Vector<String^>),
	_bookCollections(nullptr),
	_currentBookSetIdx(BookSetIdx::LastAdded)
{
	// commands for book ordering
	_authorsCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_orderByAuthor),
		nullptr);
	_titlesCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_orderByTitle),
		nullptr);
	_lastAddedCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_orderByLastAddedDate),
		nullptr);
	_collectionsCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_orderByCollection),
		nullptr);

	// action command
	_deleteCollectionCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_deleteCollection),
		nullptr);
	_addBookToCollectionCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_addToCollection),
		nullptr);
	_removeBooksFromCollectionCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LibraryPageViewModel::_removeFromCollection),
		ref new CanExecuteDelegate(this, &LibraryPageViewModel::_canRemoveFromCollection));
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::AuthorsCommand::get()
{
	return _authorsCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::TitlesCommand::get()
{
	return _titlesCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::LastAddedCommand::get()
{
	return _lastAddedCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::CollectionsCommand::get()
{
	return _collectionsCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::DeleteCollectionCommand::get()
{
	return _deleteCollectionCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::AddBookToCollectionCommand::get()
{
	return _addBookToCollectionCommand;
}

Windows::UI::Xaml::Input::ICommand^ LibraryPageViewModel::RemoveBookFromCollectionCommand::get()
{
	return _removeBooksFromCollectionCommand;
}

IObservableVector<String^>^ LibraryPageViewModel::BookCollectionNames::get()
{
	return _bookCollectionNames;
}

String^ LibraryPageViewModel::SortByString::get()
{
	String^ format = Utils::ResLoader::getString("SortByFormat");
	String^ entity;

	switch (_currentBookSetIdx)
	{
	case BookSetIdx::Authors:
		entity = Utils::ResLoader::getString("Author");
		break;

	case BookSetIdx::Titles:
		entity = Utils::ResLoader::getString("Title");
		break;

	case BookSetIdx::Collections:
		entity = Utils::ResLoader::getString("Collection");
		break;

	default:
		assert(false);
	case BookSetIdx::LastAdded:
		entity = Utils::ResLoader::getString("Date");
		break;
	}

	static wchar_t sortByString[256];
	swprintf_s(sortByString, format->Data(), entity->Data());

	return ref new String(sortByString);
}

void LibraryPageViewModel::reEvaluateCommands()
{
	BookOperationBasePageViewModel::reEvaluateCommands();
	_deleteCollectionCommand->RaiseCanExecuteChanged();
	_removeBooksFromCollectionCommand->RaiseCanExecuteChanged();
}

void LibraryPageViewModel::LoadState(IMap<String^, Object^>^ stateMap)
{
	if (stateMap != nullptr)
	{
		if (stateMap->HasKey(_currentBookSetIdxKey))
		{
			_currentBookSetIdx = static_cast<BookSetIdx>(static_cast<uint16>(stateMap->Lookup(_currentBookSetIdxKey)));
			OnPropertyChanged("SortByString");
		}
	}
	_updateAllBookSets();
}

void LibraryPageViewModel::SaveState(IMap<String^, Object^>^ stateMap)
{
	stateMap->Insert(_currentBookSetIdxKey, static_cast<uint16>(_currentBookSetIdx));
}

void LibraryPageViewModel::OnBookSetSelected(Platform::Object^ selectedBookSet)
{
	// the book set can not be passed as a parameter to the navigation function:
	// it is a restriction of Frame.GetNavigationState (called in SuspensionManager.SaveAsync)
	// for complex objects
	_bookReader->SelectedLibraryBookSet = safe_cast<BookSet^>(selectedBookSet);
	NavigateToPage(TypeName(FilteredLibraryPage::typeid), nullptr);
}

IObservableVector<BookSet^>^ LibraryPageViewModel::_getBookSets()
{
	IObservableVector<BookSet^>^ retBookSets = nullptr;

	switch (_currentBookSetIdx)
	{
	case BookSetIdx::Authors:
		retBookSets = _authorBookSets;
		break;

	case BookSetIdx::Titles:
		retBookSets = _titleBookSets;
		break;

	case BookSetIdx::LastAdded:
		retBookSets = _lastAddedBookSets;
		break;

	case BookSetIdx::Collections:
		retBookSets = _collectionBookSets;
		break;

	default:
		assert(false);
		break;
	}

	return retBookSets;
}

void LibraryPageViewModel::_updateAllBookSets()
{
	_updateAuthorBookSets();
	_updateTitleBookSets();
	_updateCollectionBookSets();
	_updateLastAddedBookSets();

	OnPropertyChanged("BookSets");
}

void LibraryPageViewModel::_deleteCollection(_In_ Platform::Object^ collectionName)
{
	auto confirmationDialog = ref new MessageDialog(ResLoader::getString("DeleteCollectionConfirm"));

	auto yesCommand = ref new UICommand(ResLoader::getString("Yes"), nullptr);
	yesCommand->Id = safe_cast<Object^>((int)CommandId::Yes);
	confirmationDialog->Commands->Append(yesCommand);

	auto noCmd = ref new UICommand(ResLoader::getString("No"), nullptr);
	noCmd->Id = safe_cast<Object^>((int)CommandId::No);
	confirmationDialog->Commands->Append(noCmd);
	confirmationDialog->DefaultCommandIndex = 1;

	task<IUICommand^> showDialogTask(confirmationDialog->ShowAsync());
	showDialogTask.then([=](IUICommand^ cmd)
	{
		if ((int)cmd->Id == CommandId::Yes)
		{
			BookCollection^ targetCollection = nullptr;
			String^ collectionNameLocal = safe_cast<String^>(collectionName);
			
			assert(_bookCollections != nullptr);
			if (_bookCollections != nullptr)
			{
				for each(BookCollection^ collection in _bookCollections)
				{
					if (0 == _wcsicmp(collection->CollectionName->Data(), collectionNameLocal->Data()))
					{
						targetCollection = collection;
						break;
					}
				}
			}
	
			try
			{
				assert(targetCollection != nullptr);
				if (targetCollection != nullptr)
				{
					_bookReader->deleteCollection(targetCollection);
				}
			}
			catch(ReaderException& e)
			{
				ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
			}

			_updateAllBookSets();
		}
	});
}

void LibraryPageViewModel::_addToCollection(_In_ Platform::Object^ bookSet)
{
	BookSet^ bookSetLocal = safe_cast<BookSet^>(bookSet);

	// check the name of the collection - if such collection already exists,
	// add books into it, if not - create a new one
	BookCollection^ targetCollection = nullptr;

	assert(_bookCollections != nullptr);
	if (_bookCollections != nullptr)
	{
		if (_bookCollections != nullptr && _bookCollections->Size > 0)
		{
			for each(BookCollection^ collection in _bookCollections)
			{
				if (0 == _wcsicmp(collection->CollectionName->Data(), bookSetLocal->Name->Data()))
				{
					targetCollection = collection;
					break;
				}
			}
		}
	}

	try
	{
		if (targetCollection == nullptr)
		{
			// create new collection
			targetCollection = ref new BookCollection();
			targetCollection->CollectionName = ref new String(bookSetLocal->Name->Data());
			targetCollection->Description = ref new String(L"");
			_bookReader->addCollection(targetCollection);
		}
	
		// add books into collection
		for each(BookContainer^ book in bookSetLocal->Books)
		{
			book->CollectionId = targetCollection->CollectionId;
			_bookReader->addBookToCollection(book->InternalBookData->BookId, book->CollectionId);
		}
	}
	catch(ReaderException& e)
	{
		ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
	}

	_updateAllBookSets();
}

void LibraryPageViewModel::_removeFromCollection(_In_ Platform::Object^ books)
{
	auto booksLocal = safe_cast<IVector<Object^>^>(books);
	assert(booksLocal != nullptr);
	assert(booksLocal->Size > 0);

	try
	{
		for each (Object^ bookContainer in booksLocal)
		{
			auto bookContainerLocal = safe_cast<BookContainer^>(bookContainer);
			_bookReader->removeBookFromCollection(bookContainerLocal->InternalBookData->BookId, bookContainerLocal->CollectionId);
		}
	}
	catch(ReaderException& e)
	{
		ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
	}

	_updateAllBookSets();
}

bool LibraryPageViewModel::_canRemoveFromCollection(_In_ Platform::Object^ books)
{
	return (_containBooks(books) && _currentBookSetIdx == BookSetIdx::Collections);
}

void LibraryPageViewModel::_orderByAuthor(_In_ Platform::Object^ parameter)
{
	_currentBookSetIdx = BookSetIdx::Authors;
	OnPropertyChanged("BookSets");
	OnPropertyChanged("SortByString");
}

void LibraryPageViewModel::_orderByTitle(_In_ Platform::Object^ parameter)
{
	_currentBookSetIdx = BookSetIdx::Titles;
	OnPropertyChanged("BookSets");
	OnPropertyChanged("SortByString");
}

void LibraryPageViewModel::_orderByLastAddedDate(_In_ Platform::Object^ parameter)
{
	_currentBookSetIdx = BookSetIdx::LastAdded;
	OnPropertyChanged("BookSets");
	OnPropertyChanged("SortByString");
}

void LibraryPageViewModel::_orderByCollection(_In_ Platform::Object^ parameter)
{
	_currentBookSetIdx = BookSetIdx::Collections;
	OnPropertyChanged("BookSets");
	OnPropertyChanged("SortByString");
}

void LibraryPageViewModel::_updateAuthorBookSets()
{
	assert(_authorBookSets != nullptr);

	_authorBookSets->Clear();

	// get the list of authors from the database
	auto authors = _bookReader->getAuthors();

	// get books of each author from the database
	// create one book set per author
	for each(String^ author in authors)
	{
		BookSet^ authorBookSet = ref new BookSet(BookSetType::Authors);
		authorBookSet->Name = author;
	
		IVector<BookData^>^ authorBooks = _bookReader->getBooksByAuthor(author);
		assert(authorBooks->Size > 0);
		for each (BookData^ bookData in authorBooks)
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			authorBookSet->Books->Append(bookContainer);
		}

		_authorBookSets->Append(authorBookSet);
	}
}

void LibraryPageViewModel::_updateTitleBookSets()
{
	assert(_titleBookSets!= nullptr);

	_titleBookSets->Clear();

	// get books from the database ordered by title
	auto books = _bookReader->getBooksByTitle();

	// create one book set per first character of the title
	wchar_t currentFirstCharacter = L'';
	BookSet^ currentTitleBookSet = nullptr;
	for each(BookData^ bookData in books)
	{
		if (currentFirstCharacter != *bookData->Title->Begin())
		{
			currentFirstCharacter = *bookData->Title->Begin();

			wchar_t bookSetTitle[] = {currentFirstCharacter, '\x0'};
			currentTitleBookSet = ref new BookSet(BookSetType::Titles);
			currentTitleBookSet->Name = ref new String(bookSetTitle);
		
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentTitleBookSet->Books->Append(bookContainer);

			_titleBookSets->Append(currentTitleBookSet);
		}
		else
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentTitleBookSet->Books->Append(bookContainer);
		}
	}
}

void LibraryPageViewModel::_updateLastAddedBookSets()
{
	assert(_lastAddedBookSets!= nullptr);

	_lastAddedBookSets->Clear();

	// get books from the database ordered by title
	auto books = _bookReader->getLastAddedBooks(MAX_NO_LAST_ADDED_LIBRARY_BOOKS);

	// create one book set per first character of the title
	const wchar_t *storeDate = nullptr;
	BookSet^ currentLastAddedBookSet = nullptr;
	for each(BookData^ bookData in books)
	{
		if (storeDate == nullptr || 0 != _wcsicmp(storeDate, bookData->StoreDateOnly->Data()))
		{
			storeDate = bookData->StoreDateOnly->Data();

			currentLastAddedBookSet = ref new BookSet(BookSetType::LastAdded);
			currentLastAddedBookSet->Name = BookReader::getDateInCurrentLocale(storeDate);
		
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentLastAddedBookSet->Books->Append(bookContainer);

			_lastAddedBookSets->Append(currentLastAddedBookSet);
		}
		else
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentLastAddedBookSet->Books->Append(bookContainer);
		}
	}
}

void LibraryPageViewModel::_updateCollectionBookSets()
{
	assert(_collectionBookSets!= nullptr);

	_collectionBookSets->Clear();
	_bookCollectionNames->Clear();
	_bookCollections = nullptr;

	// get the list of collections from the database
	_bookCollections = _bookReader->getCollections();

	// get books of each author from the database
	// create one book set per author
	for each(BookCollection^ collection in _bookCollections)
	{
		_bookCollectionNames->Append(ref new String(collection->CollectionName->Data()));

		IVector<BookData^>^ collectionBooks = _bookReader->getBooksByCollection(collection);
		if (collectionBooks->Size > 0) // add a collection only it contains books
		{
			BookSet^ collectionBookSet = ref new BookSet(BookSetType::Collections);
			collectionBookSet->Name = collection->CollectionName;
	
			for each (BookData^ bookData in collectionBooks)
			{
				BookContainer^ bookContainer = ref new BookContainer(bookData);
				bookContainer->CollectionId = collection->CollectionId;
				collectionBookSet->Books->Append(bookContainer);
			}

			_collectionBookSets->Append(collectionBookSet);
		}
	}
}

}
