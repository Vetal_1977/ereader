/*
* @file TableOfContent.h
* @brief Table of content class and TOC item class
*
* The classes should be used by corresponding view and
* view model to display a table of content of a book
*
* @date 2013-03-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "lvtinydom.h"

namespace eReader
{
	namespace ViewModel
	{
		//
		// Table of content item wrapper
		//
		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class TOCItemWrapper sealed
		{
		internal:
			// Constructs the class instance
			// @param tocItem CR TOC item to wrap
			TOCItemWrapper(_In_ LVTocItem& tocItem);

		public:
			property Platform::String^ ItemName
			{
				Platform::String^ get();
			}

			property int PageNumber
			{
				int get();
			}

			property Windows::UI::Xaml::Thickness LevelMargin
			{
				Windows::UI::Xaml::Thickness get();
			}

			property int ItemWidth
			{
				int get();
			}

		private:
			Platform::String^ _name;
			int _pageNumber;
			Windows::UI::Xaml::Thickness _level;
		};

		//
		// Table of content wrapper. Remember a root TOC item and create wrappers
		// for TOC items
		//
		ref class TableOfContent
		{
		internal:
			// Constructs the class instance
			// @param tocRoot CR root item of the table of content
			TableOfContent(_In_ LVTocItem& tocRoot);

		public:
			Windows::Foundation::Collections::IObservableVector<TOCItemWrapper^>^ getItems();

		internal:
			static const int getTOCMenuMaxWidth();

		private:
			// Append child items of the specified item into the table of content. Recursive method!
			// @param tocItem CR engine TOC item to append children into the table of content
			void _appendChildItemsRecursive(_In_ LVTocItem& tocItem);

		private:
			Platform::Collections::Vector<TOCItemWrapper^>^ _TOC;
		};
	} // ViewModel
}
