﻿/*
* @file LastReadPageViewModel.cpp
* @brief View model of a LastAddedPage
*
* @date 2013-03-10
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DelegateCommand.h"
#include "LastReadPageViewModel.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"

#define MAX_NO_LAST_READ_LIBRARY_BOOKS 16 

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::UI::Popups;

namespace eReader
{

LastReadPageViewModel::LastReadPageViewModel(BookReader^ bookReader) :	BookOperationBasePageViewModel(bookReader),
	_lastReadBookSets(ref new Vector<BookSet^>())
{
	// action command
	_clearHistoryCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LastReadPageViewModel::_clearHistory),
		nullptr);
	_removeBooksFromHistoryCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &LastReadPageViewModel::_removeFromHistory),
		ref new CanExecuteDelegate(this, &LastReadPageViewModel::_canRemoveFromHistory));
}

Windows::UI::Xaml::Input::ICommand^ LastReadPageViewModel::RemoveBooksFromHistoryCommand::get()
{
	return _removeBooksFromHistoryCommand;
}

Windows::UI::Xaml::Input::ICommand^ LastReadPageViewModel::ClearHistoryCommand::get()
{
	return _clearHistoryCommand;
}

void LastReadPageViewModel::reEvaluateCommands()
{
	BookOperationBasePageViewModel::reEvaluateCommands();
	_removeBooksFromHistoryCommand->RaiseCanExecuteChanged();
	_clearHistoryCommand->RaiseCanExecuteChanged();
}

void LastReadPageViewModel::LoadState(IMap<String^, Object^>^ /*stateMap*/)
{
	_updateAllBookSets();
}

IObservableVector<BookSet^>^ LastReadPageViewModel::_getBookSets()
{
	return _lastReadBookSets;
}

void LastReadPageViewModel::_updateAllBookSets()
{
	assert(_lastReadBookSets!= nullptr);

	_lastReadBookSets->Clear();

	// get books from the database ordered by title
	auto books = _bookReader->getLastReadBooks(MAX_NO_LAST_READ_LIBRARY_BOOKS);

	// create one book set per first character of the title
	const wchar_t *lastOpenedDate = nullptr;
	BookSet^ currentLastAddedBookSet = nullptr;
	for each(BookData^ bookData in books)
	{
		if (lastOpenedDate == nullptr || 0 != _wcsicmp(lastOpenedDate, bookData->LastOpenedDateOnly->Data()))
		{
			lastOpenedDate = bookData->LastOpenedDateOnly->Data();

			currentLastAddedBookSet = ref new BookSet(BookSetType::LastAdded);
			currentLastAddedBookSet->Name = BookReader::getDateInCurrentLocale(lastOpenedDate);
		
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentLastAddedBookSet->Books->Append(bookContainer);

			_lastReadBookSets->Append(currentLastAddedBookSet);
		}
		else
		{
			BookContainer^ bookContainer = ref new BookContainer(bookData);
			currentLastAddedBookSet->Books->Append(bookContainer);
		}
	}

	OnPropertyChanged("BookSets");
}

void LastReadPageViewModel::_clearHistory(Platform::Object^ parameter)
{
	_updateStatus(true, Utils::ResLoader::getString("ClearingHistory"));
	_activateOperationInProgress(true);

	try
	{
		_bookReader->clearLastOpenedDateForAllBooks();
	}
	catch(ReaderException& e)
	{
		ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
	}
	_updateAllBookSets();
	OnPropertyChanged("BookSets");

	_updateStatus(false, L"");
	_activateOperationInProgress(false);
}

void LastReadPageViewModel::_removeFromHistory(_In_ Platform::Object^ books)
{
	_updateStatus(true, Utils::ResLoader::getString("ClearingHistory"));
	_activateOperationInProgress(true);

	auto booksLocal = safe_cast<IVector<Object^>^>(books);
	assert(booksLocal != nullptr);
	assert(booksLocal->Size > 0);

	try
	{
		for each (Object^ bookContainer in booksLocal)
		{
			auto bookContainerLocal = safe_cast<BookContainer^>(bookContainer);
			_bookReader->clearLastOpenedDate(bookContainerLocal->InternalBookData->BookId);
		}
	}
	catch(ReaderException& e)
	{
		ExceptionPolicyFactory::getCurrentPolicy()->handleException(e);
	}
	_updateAllBookSets();
	OnPropertyChanged("BookSets");

	_updateStatus(false, L"");
	_activateOperationInProgress(false);
}

bool LastReadPageViewModel::_canRemoveFromHistory(_In_ Platform::Object^ books)
{
	return _containBooks(books);
}

}
