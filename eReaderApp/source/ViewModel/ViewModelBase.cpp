﻿/*
* @file ViewModelBase.cpp
* @brief Base class for view models
*
* All view models must be inherited from this class because of MVVW pattern
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ViewModelBase.h"

using namespace Platform;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;

namespace eReader
{

ViewModelBase::ViewModelBase()
{
}

void ViewModelBase::GoBack()
{
    NavigateBack();
}

void ViewModelBase::GoHome()
{
    NavigateHome();
}

void ViewModelBase::GoToPage(TypeName page, Object^ parameter)
{
    NavigateToPage(page, parameter);
}

}
