﻿/*
* @file NoBooksPageViewModel.cpp
* @brief View model of a NoBooksPage
*
* @date 2013-02-05
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "NoBooksPageViewModel.h"
#include "View\StartPage.xaml.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "DelegateCommand.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace eReader::Model;
using namespace eReader::ViewModel;
using namespace eReader::Utils;
using namespace concurrency;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Popups;

namespace eReader
{

NoBooksPageViewModel::NoBooksPageViewModel(BookReader^ bookReader) :
	_bookReader(bookReader),
	_hasStatusToDisplay(false),
	_statusString(L""),
	_isOperationInProgress(false)
{
	_addBookCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &NoBooksPageViewModel::_importBookFromFile),
		nullptr);
}

ICommand^ NoBooksPageViewModel::AddBookCommand::get()
{
	return _addBookCommand;
}

bool NoBooksPageViewModel::HasStatusToDisplay::get()
{
	return _hasStatusToDisplay;
}

String^ NoBooksPageViewModel::StatusString::get()
{
	return _statusString;
}

bool NoBooksPageViewModel::IsOperationInProgress::get()
{
	return _isOperationInProgress;
}

void NoBooksPageViewModel::IsOperationInProgress::set(bool value)
{
	_isOperationInProgress = value;
	OnPropertyChanged("IsOperationInProgress");
}

void NoBooksPageViewModel::_importBookFromFile(Object^ /*parameter*/)
{
	auto picker = ref new FileOpenPicker();

	picker->SuggestedStartLocation = PickerLocationId::Desktop;
	picker->ViewMode = PickerViewMode::List;
	picker->FileTypeFilter->Append(ref new String(L".fb2"));
	picker->FileTypeFilter->Append(ref new String(L".zip"));
	picker->FileTypeFilter->Append(ref new String(L".epub"));
	picker->FileTypeFilter->Append(ref new String(L".zip"));
	picker->FileTypeFilter->Append(ref new String(L".mobi"));
	picker->FileTypeFilter->Append(ref new String(L".txt"));

	task<IVectorView<StorageFile^>^> pickFiles(picker->PickMultipleFilesAsync());
	pickFiles.then([=](IVectorView<StorageFile^>^ files)
	{
		if (files->Size > 0)
		{
			_updateStatus(true, ResLoader::getString(L"AddBooksStatus"));
			IsOperationInProgress = true;
		
			static size_t addBookCount = 0;
			static bool allImportsSucceeded = true;
			const size_t noOfBooksToAdd = files->Size;
			addBookCount = 0;
			allImportsSucceeded = true;
			for each(StorageFile^ file in files)
			{
				BookData^ bookData = ref new BookData();
				auto importBookRunTask = create_task(_bookReader->importBookFromFile(file, bookData));
				importBookRunTask.then([=](task<bool> importBookTask)
				{
					bool succeeded = false;
					try
					{
						succeeded = importBookTask.get();
					}
					catch (ReaderException& ex)
					{
						ExceptionPolicyFactory::getCurrentPolicy()->handleException(ex);
						succeeded = false;
					}

					allImportsSucceeded = allImportsSucceeded && succeeded;
					if (++addBookCount == noOfBooksToAdd)
					{
						// update the book sets after all book are added
						_updateStatus(false, L"");
						IsOperationInProgress = false;
						
						if (!_bookReader->isLibraryEmpty())
						{
							ViewModelBase::GoToPage(TypeName(StartPage::typeid), nullptr); 
						}

						if (!allImportsSucceeded)
						{
							MessageDialog^ msgDialog = ref new MessageDialog(Utils::ResLoader::getString("CannotAddSomeBooks"));
							msgDialog->ShowAsync();
						}
					}
				});
			}
		}
	});
}

void NoBooksPageViewModel::_updateStatus(bool displayStatus, String^ statusString)
{
	_hasStatusToDisplay = displayStatus;
	_statusString = statusString;
	OnPropertyChanged("StatusString");
	OnPropertyChanged("HasStatusToDisplay");
}

}
