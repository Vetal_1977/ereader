/*
* @file BookContainer.h
* @brief Book container class
*
* Contains book data from the database and additional data from the file storage.
* Provides access for GUI elements only to those fields, which can be displayed
*
* @date 2013-02-01
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "Common\BindableBase.h"
#include "Model\BookData.h"

namespace eReader
{
	namespace ViewModel
	{
		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class BookContainer sealed : eReader::Common::BindableBase
		{
		internal:
			BookContainer(Model::BookData^ bookData);

		public:
			property Platform::String^ Title { Platform::String^ get(); }
			property Platform::String^ Authors { Platform::String^ get(); }
			property Windows::UI::Xaml::Media::ImageSource^ Image { Windows::UI::Xaml::Media::ImageSource^ get(); }

		internal:
			property Platform::String^ FullPath { Platform::String^ get(); };
			property Model::BookData^ InternalBookData { Model::BookData^ get(); }
			property int CollectionId { int get(); void set(int value); }
			 
		private:
			void _openCoverImageFileAsync();

		private:
			Model::BookData^ _bookData;
			Windows::UI::Xaml::Media::Imaging::BitmapSource^ _imageSource;
			int _collectionID; // <! have a sense if the book container belong to a collection book set; for other book set types may be ignore
		};
	} // Model
}

