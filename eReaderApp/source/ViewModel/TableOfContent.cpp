/*
* @file TableOfContent.cpp
* @brief Table of content class and TOC item class
*
* The classes should be used by corresponding view and
* view model to display a table of content of a book
*
* @date 2013-03-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "TableOfContent.h"

#define TOC_MENU_MAX_WIDTH 300 // pixels

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;

namespace eReader
{
namespace ViewModel
{
//
// Table of content item wrapper
//
TOCItemWrapper::TOCItemWrapper(LVTocItem& tocItem) :
	_name(ref new String(tocItem.getName().data())),
	_pageNumber((tocItem.getPage() + 1)), // CR pages start from 0, we need to start it from 1
	_level(tocItem.getLevel() * 2, 0, 0, 0)
{
}

String^ TOCItemWrapper::ItemName::get()
{
	return _name;
}

int TOCItemWrapper::PageNumber::get()
{
	return _pageNumber;
}

Thickness TOCItemWrapper::LevelMargin::get()
{
	return _level;
}

int TOCItemWrapper::ItemWidth::get()
{
	return TableOfContent::getTOCMenuMaxWidth();
}

//
// Table of content class
//
TableOfContent::TableOfContent(LVTocItem& tocRoot)
{
	_TOC = ref new Vector<TOCItemWrapper^>();
	_appendChildItemsRecursive(tocRoot);
}

IObservableVector<TOCItemWrapper^>^ TableOfContent::getItems()
{
	return _TOC;
}

const int TableOfContent::getTOCMenuMaxWidth()
{
	return static_cast<int>(__min(TOC_MENU_MAX_WIDTH, Window::Current->Bounds.Width));
}

void TableOfContent::_appendChildItemsRecursive(LVTocItem& tocItem)
{
	for (int idx=0; idx < tocItem.getChildCount(); idx++ )
	{
		LVTocItem* childItem = tocItem.getChild(idx);
		auto tocItem = ref new TOCItemWrapper(*childItem);
		_TOC->Append(tocItem);
		_appendChildItemsRecursive(*childItem);
	}
}

} // ViewModel
}
