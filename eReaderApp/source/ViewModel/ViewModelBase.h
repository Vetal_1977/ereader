﻿/*
* @file ViewModelBase.h
* @brief Base class for view models
*
* All view models must be inherited from this class because of MVVW pattern
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "Common\BindableBase.h"

namespace eReader
{
	public delegate void NavigateEventHandler();
	public delegate void PageNavigateEventHandler(Windows::UI::Xaml::Interop::TypeName pageType, Platform::Object^ parameter);

	//
	// The ViewModelBase class contains the common presentation logic used by all view models
	//
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ViewModelBase : public Common::BindableBase
	{
	internal:
		ViewModelBase();

		event NavigateEventHandler^ NavigateBack;
		event NavigateEventHandler^ NavigateHome;
		event PageNavigateEventHandler^ NavigateToPage;

		virtual void LoadState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) {};
		virtual void SaveState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) {};
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) {};
		virtual void OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) {};

	protected private:
		virtual void GoBack();
		virtual void GoHome();
		virtual void GoToPage(Windows::UI::Xaml::Interop::TypeName pageType, Platform::Object^ parameter);
	};
}
