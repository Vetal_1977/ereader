/*
* @file ViewModelLocator.cpp
* @brief Locator of known view models
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ViewModelLocator.h"

using namespace Platform::Collections;
using namespace Platform;
using namespace Windows::ApplicationModel::Resources;
using namespace Windows::UI::Xaml;

namespace eReader
{

ViewModelLocator::ViewModelLocator()
{
}

ViewModelLocator::~ViewModelLocator()
{
}

StartPageViewModel^ ViewModelLocator::StartPageVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new StartPageViewModel(currentApp->eBookReader);
}

NoBooksPageViewModel^ ViewModelLocator::NoBooksPageVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new NoBooksPageViewModel(currentApp->eBookReader);
}

BookViewModel^ ViewModelLocator::BookVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new BookViewModel(currentApp->eBookReader);
}

LibraryPageViewModel^ ViewModelLocator::LibraryPageVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new LibraryPageViewModel(currentApp->eBookReader);
}

LastReadPageViewModel^ ViewModelLocator::LastReadPageVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new LastReadPageViewModel(currentApp->eBookReader);
}

FilteredLibraryPageViewModel^ ViewModelLocator::FilteredLibraryPageVM::get()
{
	App^ currentApp = safe_cast<App^>(Application::Current);
	return ref new FilteredLibraryPageViewModel(currentApp->eBookReader);
}

}
