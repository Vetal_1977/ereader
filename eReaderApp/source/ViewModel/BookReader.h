/*
* @file BookReader.h
* @brief Book reader class
*
* Data reader reads, writes books and their info and collections from/into the storage;
* it must be initialized by a program start and provides methods to
* access books and collections.
* It contains also instances of classes to provide access to a cool reader engine
*
* @date 2012-12-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include <collection.h>
#include "Model\DatabaseStorage.h"
#include "Model\FileStorage.h"
#include "BookSet.h"
#include "ReaderColor.h"
#include "CRDocViewCallbackTransformer.h"
#include "SearchTextObject.h"

namespace eReader
{
	namespace ViewModel
	{
		ref class TableOfContent;
		ref class BookmarkTable;

		//
		// Class for accessing a book data source
		//
		ref class BookReader sealed : CRDocViewCallback
		{
		internal:
			BookReader();

		internal:
			//
			// CRDocViewCallback wrapper methods
			//
			/// on starting file loading
			virtual void OnLoadFileStart(_In_ Platform::String^ /*filename*/) override { }
			/// format detection finished
			virtual void OnLoadFileFormatDetected(_In_ doc_format_t /*fileFormat*/) override;
			/// file loading is finished successfully - drawCoveTo() may be called there
			virtual void OnLoadFileEnd() override { }
			/// first page is loaded from file an can be formatted for preview
			virtual void OnLoadFileFirstPagesReady() override { }
			/// file progress indicator, called with values 0..100
			virtual void OnLoadFileProgress(_In_ int /*percent*/) override { }
			/// document formatting started
			virtual void OnFormatStart() override { }
			/// document formatting finished
			virtual void OnFormatEnd() override { }
			/// format progress, called with values 0..100
			virtual void OnFormatProgress(_In_ int /*percent*/) override { }
			/// format progress, called with values 0..100
			virtual void OnExportProgress(_In_ int /*percent*/) override { }
			/// file load finished with error
			virtual void OnLoadFileError(_In_ Platform::String^ /*message*/) override { }
			/// Override to handle external links
			virtual void OnExternalLink(_In_ Platform::String^ /*url*/, _In_ ldomNode * /*node*/) override { }
			/// Called when page images should be invalidated (clearImageCache() called in LVDocView)
			virtual void OnImageCacheClear() override { }
			/// return true if reload will be processed by external code, false to let internal code process it
			virtual bool OnRequestReload() override { return false; }

		internal:
			property BookSet^ SelectedLibraryBookSet
			{
				BookSet^ get();
				void set(BookSet^ value);
			}

			// Initialize book reader stuff asynchronously
			// This method should be called by the app start
			concurrency::task<void> initAsync();

			// Resumes the book reader. This method must be called during resuming of the application
			// The following initialization steps are done:
			// - opened the database
			// - Initialize CR font manager
			// - CR engine is created and initialized
			// - CR engine state is restored (including last opened book)
			// @return Task to resume the book reader
			concurrency::task<void> resumeAsync();

			// Suspends the book reader. This method must be called during suspending of the application
			// The following de-initialization steps are done:
			// - close the database
			// - CR engine state is saved
			// - CR engine is closed (including book close) and freed
			// - Shutdown CR font manager
			void suspend();

			// Checks whether the library contains any book
			// @return true if the library contains book(s), false - otherwise
			const bool isLibraryEmpty();

			// Returns the string representation of the specified date using current locale
			// @param dateInDBFormat Date in a format YYYY-MM-DD
			// @return Formatted date using the current locale
			static Platform::String^ getDateInCurrentLocale(const wchar_t* dateInDBFormat);

#pragma region Methods to access a data storage
			// Imports a book from the specified file
			// @param bookStorageFile Storage file contained a new book to add to the library
			// @param bookData In/Out book data to fill. Must not be nullptr
			// @return Task of importing of the book. Return value of type 'bool' indicates success/failure
			concurrency::task<bool> importBookFromFile(_In_ Windows::Storage::StorageFile^ bookStorageFile, _In_ _Out_ Model::BookData^ bookData);

			// Deletes specified book
			// @param bookToDelete Data of the book to delete. Is an instance of Model::BookData
			// @return Task of a book deletion
			concurrency::task<void> deleteBook(_In_ Platform::Object^ bookToDelete);

			// Reads last read books from the database
			// @param maxNoOfBooks Maximum number of books to return by this method
			// @return Data of last read books from the database
			Windows::Foundation::Collections::IVector<Model::BookData^>^ getLastReadBooks(_In_ const uint16 maxNoOfBooks);

			// Reads last added books from the database
			// @param maxNoOfBooks Maximum number of books to return by this method
			// @return Data of last added books from the database
			Windows::Foundation::Collections::IVector<Model::BookData^>^ getLastAddedBooks(_In_ const uint16 maxNoOfBooks);

			// Reads all books in the database for the specified date
			// @param date Date when a book is saved into the storage
			// @return Vector of books for the specified date
			//Windows::Foundation::Collections::IVector<Model::BookData^>^ getBooksByDate(_In_ Platform::String^ date);

			// Reads book authors from the database. 
			// @return String vector of found authors. Each author exists only once in the vector
			Windows::Foundation::Collections::IVector<Platform::String^>^ getAuthors();

			// Reads books of a specified author
			// @param author Author name
			// @return Data of author books from the database
			Windows::Foundation::Collections::IVector<Model::BookData^>^ getBooksByAuthor(_In_ Platform::String^ author);

			// Reads all books in the database ordered by title
			// @return Vector of books ordered by title
			Windows::Foundation::Collections::IVector<Model::BookData^>^ getBooksByTitle();

			// Reads all books in the database that title starts with a specified character
			// @param startChar Title start character
			// @return Vector of books that title title start with a specified character
			//Windows::Foundation::Collections::IVector<Model::BookData^>^ getBooksByTitle(_In_ const wchar_t startChar);

			// Reads collections from the database. 
			// @return Vector of stored collections
			Windows::Foundation::Collections::IVector<Model::BookCollection^>^ getCollections();

			// Reads books of a specified collection
			// @param collection Collection object
			// @return Data of collection books from the database
			Windows::Foundation::Collections::IVector<Model::BookData^>^ getBooksByCollection(_In_ Model::BookCollection^ collection);

			// Adds a book into the collection. If the book is already there, nothing occurs. 
			// @param bookId Book ID to add into the collection
			// @param collectionId Collection ID to add the book into
			// @exception ReaderException Thrown if book-collection reference cannot be added
			void addBookToCollection(_In_ const int bookId, _In_ const int collectionId);

			// Adds a collection into the database
			// @param collection Collection to add. The CollectionId must not be specified, it is saved into the object after adding into the database
			// @exception ReaderException Thrown if the collection cannot be added
			void addCollection(_In_ _Out_ Model::BookCollection^ collection);

			// Removes a book from the collection. If the book is not in the collection, nothing occurs
			// @param bookId Book ID  to remove from the collection
			// @param collectionId Collection ID to remove the book from
			// @exception ReaderException Thrown if book-collection reference cannot be deleted
			void removeBookFromCollection(_In_ const int bookId, _In_ const int collectionId);

			// Deletes a collection from the database
			// @param collection Collection to delete
			// @exception ReaderException Thrown if the collection cannot be deleted
			void deleteCollection(_In_ Model::BookCollection^ collection);

			// Updates last opened date field for the book. The current date is set as a last opened date for the book
			// @param bookId Book ID to update the date for
			void updateLastOpenedDate(_In_ const int bookId);

			// Clears the last opened date for all books in the database. It will clear the history
			void clearLastOpenedDateForAllBooks();

			// Clears the last opened date for the specified book. It will remove the book from the history
			// @param bookId Book ID to remove from the history
			void clearLastOpenedDate(_In_ const int bookId);
#pragma endregion

#pragma region Methods to access cool reader engine
			// Returns a list of reader fonts that are registered by CoolReader engine
			// @return Vector of installed font names
			Windows::Foundation::Collections::IVector<Platform::String^>^ getReaderFonts();

			// Returns currently selected book font
			// @return Name of the currently selected font
			Platform::String^ getCurrentFont();

			// Returns a list of possible font sizes of CoolReader engine
			// @param fontSizes Pointer to reader font sizes, must not be nullptr, the caller must not free the memory
			// @param noOfFonts Number of returned sizes
			void getReaderFontSizes(_Out_ int** fontSizes, _Out_ int& noOfSizes);

			// Returns current font size
			// @return Size of the currently selected font
			const int getCurrentFontSize();

			// Returns a vector of reader colors (background texture + font color)
			// @return A vector of reader colors. Must not be null and must contain at least one color combination
			Windows::Foundation::Collections::IVector<ReaderColor^>^ getReaderColors();

			// Sets a font for the reader to display book text
			// @param fontName Font name for texts (String^). Must be one of the fonts returned by 'getReaderFonts'
			void setReaderFont(_In_ Platform::Object^ fontName);

			// Sets a font size for the reader to display book text
			// @param fontSize Font name for texts (int). Must be one of the fonts returned by 'getReaderFontSizes'
			void setReaderFontSize(_In_ Platform::Object^ fontSize);

			// Sets background image and text color for the reader
			// @param colorIndex Index of the color in the _readerColors vector (unsigned int)
			void setReaderColors(_In_ Platform::Object^ colorIndex);

			// Returns current line spacing
			// @return Current line spacing value in %%. The valid range is [50;200]
			const int getLineSpacing();

			// Sets new line spacing
			// @param lineSpacing New line spacing value in %%. The valid range is [50;200]
			void setLineSpacing(_In_ const int lineSpacing);

			// Returns current page margins. Top, left, bottom, right margins have the same value
			// @return Current page margins value in pixels. The valid range is [0;70]
			const int getPageMargins();

			// Sets new page margins. Top, left, bottom, right margins have the same value
			// @param pageMargins New page margins value in pixels. The valid range is [0;70]
			void setPageMargins(_In_ const int pageMargins);

			// Loads the specified book
			// @param bookPath Path to the file contained a book
			// @param viewSize Size of the view to display a book
			void loadBook(_In_ Platform::String^ bookPath, _In_ const SIZE& viewSize);

			// Closes currently opened book
			void closeBook();

			// Saves a state of the currently loaded book (position, etc.)
			void saveBookState();

			// Returns a page count to display. It depends on the current mode (1-page, 2-page)
			// and page count of the current book
			// @return Page count to display
			int getDisplayedPageCount();

			// Invokes a "page down" command by the cool reader for a current book
			void pageDown();

			// Invokes a "page up" command by the cool reader for a current book
			void pageUp();

			// Get current buffer from the reader to draw
			// @param drawBuffer Pointer to a draw buffer. Must not be nullptr, the caller must not free the memory
			// @param SIZE Size of the returned buffer
			// @param bitsPerPixel Bits per pixel for returned draw buffer
			void getCurrentDrawBuffer(_Out_ void** drawBuffer, _Out_ SIZE& bufferSize, _Out_ int& bitsPerPixel);

			// Resizes a view area for the book page
			// @param viewSize Size of the book view area
			// @param visiblePageCount Number of visible pages on the screen. Valid values are 1 and 2
			void resizeBookPageView(_In_ const SIZE& viewSize, _In_ const int visiblePageCount);

			// Returns currently displayed page(s) index. It depends on the current mode (1-page, 2-page)
			// @return Index of the displayed page(s) in a range 
			// [0;<page count> / <visible page count> + <page count> % <visible page count>]
			int getCurrentPageIndex();

			// Returns a real page count of the currently opened book.
			// @return Book page count
			const int getBookPageCount();
			
			// Returns a current page number
			// @return a value in a range [1;<page count>]
			const int getBookPageNumber();

			// Returns a visible page count
			// @return Visible page count. Must be in a range [1;2]
			const int getVisiblePageCount();

			// Sets a percentage of a reading progress of the currently opened book
			// @param pageNumber A value in a range [1;<page count>]
			void goToPage(_In_ const int pageNumber);

			// Creates and returns a table of content
			// @return Table of content of a currently opened book
			TableOfContent^ getTOC();

			// Creates and returns a bookmark table
			// @return Bookmark of a currently opened book
			BookmarkTable^ getBookmarkTable();

			// Checks whether the bookmark image on the page is clicked
			// @param crPosition Position to check
			// @return true if the specified position inside a bookmark image
			bool isPageBookmarkClicked(_In_ const lvPoint& crPosition);

			// Checks whether the specified page is bookmarked
			// @param pageNumber Number of the page to check. Start from 1
			// @return true if the page is bookmarked
			bool isPageBookmarked(_In_ const int pageNumber);

			// Checks whether the specified page is bookmarked and update page indicator properly
			// @param pageNumber Number of the page to check. Start from 1
			void updatePageBookmarkIndicator(_In_ const int pageNumber);

			// Add/remove bookmark the specified page and change bookmark indicator properly
			// @param pageNumber Number of the page to bookmark. Start from 1
			// @param isPageBookmarked true if the page must be bookmarked, false - otherwise
			void updatePageBookmark(_In_ const int pageNumber, _In_ const bool isPageBookmarked);

			// Gets a search text object. The object is owned by this class and cannot be modified outside
			// @return Search text object
			SearchTextObject* getSearchTextObject();
#pragma endregion

		private:
			// Reads installed fonts
			concurrency::task<void> _readInstalledFontsAsync();

			// Initializes CoolReader font manager. It must be done before the instantiation of LVDocView object
			void _initCRFontManager();

			// Destroys CoolReader font manager
			void _destroyCRFontManager();

			// Creates and initializes CR engine instance
			// @return Task of the CR engine initialization
			concurrency::task<void> _initCREngineAsync();

			// Destroys CR engine
			void _destroyCREingine();

			// Initializes a vector of reader colors
			void _initReaderColors();

			// Initializes a font sizes for the reader
			void _initReaderFontSizes();

			// Returns the current time as a string
			// @return String representation of the current date in the following format: YYYY-MM-DD
			Platform::String^ _getCurrentDate();

			// Sets a background image for the page
			// @param imageFilename Full path to the image file
			void _setBackgroundImage(_In_ const lString16& imageFilename);

			// Sets bookmark images for the page
			// @param activeBookmarkFilename Full path to the file with active bookmark image
			// @param inactiveBookmarkFilename Full path to the file with inactive bookmark image
			void _setBookmarkImages(_In_ const lString16& activeBookmarkFilename, _In_ const lString16& inactiveBookmarkFilename);

			// Determines page index to bookmark corresponding a specified page number
			// @param pageNumber Source page number. Starts from 1
			// @return Target page index to bookmark. Starts from 0
			const int _getPageIdxToBookmark(_In_ const int pageNumber);

		private:
			Platform::Collections::Vector<Platform::String^>^ _readerFonts;
			std::unique_ptr<LVDocView> _lvDocView;
			Platform::Collections::Vector<ReaderColor^>^ _readerColors;
			std::unique_ptr<CRDocViewCallbackTransformer> _CRCallbackTransformer;

			// book storages
			Model::DatabaseStorage^ _bookDatabase;
			Model::FileStorage^ _bookFileStorage;

			Platform::Collections::Vector<Platform::String^>^ _fontFilenames;
			CRPropRef _cr3Props;
			Platform::String^ _cr3HistoryFilename;
			Platform::String^ _cr3SettingsFilename;

			Platform::String^ _CSSFolder;
			Platform::String^ _iconsFolder;

			Platform::String^ _lastOpenedBookFilename;
			SIZE _lastOpenedBookSize;

			BookSet^ _selectedLibraryBookSet; // <! temporarily save the selected library book set to avoid unnecessary reading from the database

			std::unique_ptr<SearchTextObject> _searchTextObject;
		};
	} // ViewModel
}
