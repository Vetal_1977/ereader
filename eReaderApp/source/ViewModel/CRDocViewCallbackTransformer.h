/*
* @file CRDocViewCallbackTransformer.h
* @brief The class implements native callback interface and calls appropriate WinRT callback interface
*
* The class implements CR::LVDocViewCallback and calls appropriate methods of
* ILVDocViewCallbackWinRT callback class
*
*
* @date 2013-01-04
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "lvtinydom.h"
#include "CRDocViewCallback.h"

namespace eReader
{
	class CRDocViewCallbackTransformer : public LVDocViewCallback
	{
	public:
		// Constructs this object
		// @param winRTCallback Instance of a WinRT object that implements callback methods
		CRDocViewCallbackTransformer(_In_ CRDocViewCallback^ winRTCallback);

	public:
		/// on starting file loading
		virtual void OnLoadFileStart(lString16 filename);
		/// format detection finished
		virtual void OnLoadFileFormatDetected(doc_format_t /*fileFormat*/);
		/// file loading is finished successfully - drawCoveTo() may be called there
		virtual void OnLoadFileEnd();
		/// first page is loaded from file an can be formatted for preview
		virtual void OnLoadFileFirstPagesReady();
		/// file progress indicator, called with values 0..100
		virtual void OnLoadFileProgress( int /*percent*/);
		/// document formatting started
		virtual void OnFormatStart();
		/// document formatting finished
		virtual void OnFormatEnd();
		/// format progress, called with values 0..100
		virtual void OnFormatProgress(int /*percent*/);
		/// format progress, called with values 0..100
		virtual void OnExportProgress(int /*percent*/);
		/// file load finiished with error
		virtual void OnLoadFileError(lString16 /*message*/);
		/// Override to handle external links
		virtual void OnExternalLink(lString16 /*url*/, ldomNode * /*node*/);
		/// Called when page images should be invalidated (clearImageCache() called in LVDocView)
		virtual void OnImageCacheClear();
		/// return true if reload will be processed by external code, false to let internal code process it
		virtual bool OnRequestReload();

	private:
		CRDocViewCallback^ _winRTCallback;
	};
}

