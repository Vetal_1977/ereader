﻿/*
* @file BookViewModel.cpp
* @brief View model of a BookPageViewModel
*
* Provides the vector of book pages that need to be displayed in the GUI
*
* @date 2012-12-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookViewModel.h"
#include "Presentation\CRPageDrawBuffer.h"
#include "agents.h"
#include <math.h>
#include "DelegateCommand.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::UI;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Core;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace eReader::Presentation;
using namespace Windows::ApplicationModel;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::Storage::Search;
using namespace eReader::ViewModel;

#define DRAWING_PAGE_NUMBER 3
#define PRESENTATION_UPDATE_TIMESPAN 5000000 // 100 nanoseconds = 0.5 second

namespace eReader
{

BookViewModel::BookViewModel(BookReader^ bookReader) :
	_bookReader(bookReader),
	_selectedPage(0),
	_bookProgressValue(0.0),
	_lastLineSpacingValue(100.0),
	_lastPageMarginsValue(0.0)
{
	assert(_bookReader != nullptr);

	// only 3 pages are needed to ensure "prev. page", "next page" arrows in flip view
	// and smooth page flipping. Those pages are added upon document loading
	// if the document has less than 3 pages, only necessary pages will be added
	App^ currentApp = safe_cast<App^>(Application::Current);
	_page1 = ref new BookPageViewModel(currentApp->Renderer, _bookReader);
	_page2 = ref new BookPageViewModel(currentApp->Renderer, _bookReader);
	_page3 = ref new BookPageViewModel(currentApp->Renderer, _bookReader);
	
	// create only array object; pages will be added later if necessary
	_pages = ref new Vector<BookPageViewModel^>();

	// commands
	_setFontCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookViewModel::_setReaderFont),
		nullptr);
	_setFontSizeCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookViewModel::_setReaderFontSize),
		nullptr);
	_setColorsCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookViewModel::_setReaderColors),
		nullptr);
	_findInBookPrevCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookViewModel::_findInBookPrev),
		nullptr);
	_findInBookNextCommand = ref new DelegateCommand(
		ref new ExecuteDelegate(this, &BookViewModel::_findInBookNext),
		nullptr);

	_isLoadingBook = false;
	
	TimeSpan updateTimSpan;
	updateTimSpan.Duration = PRESENTATION_UPDATE_TIMESPAN;

	_bookProgressUpdateTimer = ref new DispatcherTimer();
	_bookProgressUpdateTimer->Tick += ref new EventHandler<Object^>(this, &BookViewModel::OnBookProgressTimerTick);
	_bookProgressUpdateTimer->Interval = updateTimSpan;

	_lineSpacingUpdateTimer = ref new DispatcherTimer();
	_lineSpacingUpdateTimer->Tick += ref new EventHandler<Object^>(this, &BookViewModel::OnLineSpacingTimerTick);
	_lineSpacingUpdateTimer->Interval = updateTimSpan;

	_pageMarginsUpdateTimer = ref new DispatcherTimer();
	_pageMarginsUpdateTimer->Tick += ref new EventHandler<Object^>(this, &BookViewModel::OnPageMarginsTimerTick);
	_pageMarginsUpdateTimer->Interval = updateTimSpan;

	currentApp->Renderer->DeviceLost += ref new DeviceLostEventHandler(this, &BookViewModel::OnDeviceLost);
}

IBindableObservableVector^ BookViewModel::BookPages::get()
{
	return _pages;
}

bool BookViewModel::IsLoadingBook::get()
{
	return _isLoadingBook;
}

int BookViewModel::SelectedBookPage::get()
{
	return _selectedPage;
}

void BookViewModel::SelectedBookPage::set(int value)
{
	value; // by selection of a new page additional actions needed (see OnPageSelectionChange)
}

void BookViewModel::IsLoadingBook::set(bool value)
{
	_isLoadingBook = value;
	OnPropertyChanged("IsLoadingBook");
}

double BookViewModel::BookPageCount::get()
{
	return _bookReader->getBookPageCount();
}

double BookViewModel::LineSpacing::get()
{
	return static_cast<double>(_bookReader->getLineSpacing());
}

void BookViewModel::LineSpacing::set(double value)
{
	// stop the timer if started
	_lineSpacingUpdateTimer->Stop();

	// start the timer with
	_lastLineSpacingValue = value;
	_lineSpacingUpdateTimer->Start();
}

double BookViewModel::PageMargins::get()
{
	return static_cast<double>(_bookReader->getPageMargins());
}

void BookViewModel::PageMargins::set(double value)
{
	// stop the timer if started
	_pageMarginsUpdateTimer->Stop();

	// start the timer with
	_lastPageMarginsValue = value;
	_pageMarginsUpdateTimer->Start();
}

IObservableVector<ViewModel::TOCItemWrapper^>^ BookViewModel::TOCItems::get()
{
	return _bookReader->getTOC()->getItems();
}

IObservableVector<ViewModel::BookmarkWrapper^>^ BookViewModel::Bookmarks::get()
{
	return _bookReader->getBookmarkTable()->getBookmarks();
}

double BookViewModel::BookReadingProgress::get()
{
	return _bookProgressValue;
}

void BookViewModel::BookReadingProgress::set(double value)
{
	// stop the timer if started
	_bookProgressUpdateTimer->Stop();

	// start the timer with
	_bookProgressValue = value;
	_bookProgressUpdateTimer->Start();
}

ICommand^ BookViewModel::SetFontCommand::get()
{
	return _setFontCommand;
}

ICommand^ BookViewModel::SetFontSizeCommand::get()
{
	return _setFontSizeCommand;
}

ICommand^ BookViewModel::SetColorsCommand::get()
{
	return _setColorsCommand;
}

Windows::UI::Xaml::Input::ICommand^ BookViewModel::FindInBookPrevCommand::get()
{
	return _findInBookPrevCommand;
}

Windows::UI::Xaml::Input::ICommand^ BookViewModel::FindInBookNextCommand::get()
{
	return _findInBookNextCommand;
}


void BookViewModel::OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e)
{
	// show the progress ring
	IsLoadingBook = true;

	// load book asynchronous
	SIZE viewSize = { 
		static_cast<LONG>(Window::Current->Bounds.Width),
		static_cast<LONG>(Window::Current->Bounds.Height)
	}; // current window bounds must be taken on GUI thread
	auto bookFullPath = safe_cast<String^>(e->Parameter);

	auto loadBookTask = create_task([=](void)
	{
		_bookReader->loadBook(bookFullPath, viewSize);

		this->Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new Windows::UI::Core::DispatchedHandler([this]()
		{
			// remove all pages to draw
			_pages->Clear();

			// update draw buffers and put a page onto the screen
			_initPageVector();

			// hide the progress ring
			IsLoadingBook = false;
		}));
	});
}

void BookViewModel::LoadState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap)
{
}

void BookViewModel::SaveState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap)
{
	if (!ReaderPage::IsSuspending)
	{
		_bookReader->saveBookState();
		_bookReader->closeBook();
	}
	// else the book reader will be suspended from 'App::OnSuspending'
}

void BookViewModel::OnPageSelectionChanged(int selectedPageIdx)
{
	if (IsLoadingBook)
	{
		return;
	}

	try
	{
		auto selPageViewModel = _pages->GetAt(selectedPageIdx);

		// update of book progress value must be done before updating pages on the screen
		if (selPageViewModel->DisplayPageDelta >= 1)
		{
			// the last page in the vector reached. to allow a flowing page change,
			// move the first page to the end of the vector and update data buffer for it

			int pageCount = _bookReader->getDisplayedPageCount();
			int currentPageIndex = _bookReader->getCurrentPageIndex();

			if (currentPageIndex < (pageCount - 1)) // pageDown is possible until the next-to-last page
			{
				for (int count = 0; count < selPageViewModel->DisplayPageDelta; count++)
				{
					_bookReader->pageDown();
				}

				currentPageIndex = _bookReader->getCurrentPageIndex();
				if (currentPageIndex == (pageCount - 1)) // last page reached
				{
					_pages->GetAt(0)->updateDisplayPageDelta(-2, true);
					_pages->GetAt(1)->updateDisplayPageDelta(-1, true);
					_pages->GetAt(2)->updateDisplayPageDelta(0, true);

					_selectedPage = 2;
				}
				else
				{
					_pages->GetAt(0)->updateDisplayPageDelta(-1, true);
					_pages->GetAt(1)->updateDisplayPageDelta(0, true);
					_pages->GetAt(2)->updateDisplayPageDelta(1, true);

					_selectedPage = 1;
				}
			}
		}
		else if (selPageViewModel->DisplayPageDelta <= -1)
		{
			// the first page in the vector reached. to allow a flowing page change,
			// move the last page to the beginning of the vector and update data buffer for it

			int currentPageIndex = _bookReader->getCurrentPageIndex();

			if (currentPageIndex > 0) // pageDown is possible until the next-to-first page
			{
				for (int count = 0; count < (-1) * selPageViewModel->DisplayPageDelta; count++)
				{
					_bookReader->pageUp();
				}

				currentPageIndex = _bookReader->getCurrentPageIndex();
				if (currentPageIndex == 0) // first page reached
				{
					_pages->GetAt(0)->updateDisplayPageDelta(0, true);
					_pages->GetAt(1)->updateDisplayPageDelta(1, true);
					_pages->GetAt(2)->updateDisplayPageDelta(2, true);

					_selectedPage = 0;
				}
				else
				{
					_pages->GetAt(0)->updateDisplayPageDelta(-1, true);
					_pages->GetAt(1)->updateDisplayPageDelta(0, true);
					_pages->GetAt(2)->updateDisplayPageDelta(1, true);

					_selectedPage = 1;
				}
			}
		}
		OnPropertyChanged("SelectedBookPage");

		_updateBookProgressValue();
	}
	catch(const std::exception&)
	{
		assert(false);
	}
}

void BookViewModel::OnPageSelectionRestored(int selectedPageIdx)
{
	if (IsLoadingBook)
	{
		return;
	}

	_pages->GetAt(selectedPageIdx)->updateDisplayPageDelta(selectedPageIdx - 1, true);
}

IVector<String^>^ BookViewModel::getReaderFonts()
{
	return _bookReader->getReaderFonts();
}

String^ BookViewModel::getCurrentFont()
{
	return _bookReader->getCurrentFont();
}

void BookViewModel::getReaderFontSizes(int** fontSizes, int& noOfSizes)
{
	_bookReader->getReaderFontSizes(fontSizes, noOfSizes);
}

int32 BookViewModel::getCurrentFontSize()
{
	return _bookReader->getCurrentFontSize();
}

IVector<ReaderColor^>^ BookViewModel::getReaderColors()
{
	return _bookReader->getReaderColors();
}

void BookViewModel::updateBookView(const SIZE& viewSize, const int visiblePageCount)
{
	_bookReader->resizeBookPageView(viewSize, visiblePageCount);
	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::redrawBook()
{
	_updateCurrentPresentation(UpdateFlags::NoProgressUpdate);
}

void BookViewModel::selectTOCItem(Platform::Object^ TOCItem)
{
	auto TOCItemLocal = safe_cast<TOCItemWrapper^>(TOCItem);
	goToPage(TOCItemLocal->PageNumber);
}

void BookViewModel::selectBookmark(Platform::Object^ bookmark)
{
	auto bookmarkLocal = safe_cast<BookmarkWrapper^>(bookmark);
	goToPage(bookmarkLocal->PageNumber);
}

void BookViewModel::pageTapped(Windows::Foundation::Point& position, bool &isHandled)
{
	// check whether a bookmark image selected
	lvPoint crPosition;
	crPosition.x = static_cast<int>(position.X);
	crPosition.y = static_cast<int>(position.Y);
	if (_bookReader->isPageBookmarkClicked(crPosition))
	{
		// add/remove bookmark
		const int currentPageNumber = static_cast<int>(_bookProgressValue);
		bool isPageBookmarked = _bookReader->isPageBookmarked(currentPageNumber);
		_bookReader->updatePageBookmark(currentPageNumber, !isPageBookmarked);
		_updateCurrentPresentation(UpdateFlags::UpdateBookmarkTable);
		isHandled = true;
	}
	else
	{
		isHandled = false;
	}
}

void BookViewModel::goToPage(const int pageNumber)
{
	_bookReader->goToPage(pageNumber);
	_alignCRPageToDispPages();
	_updateCurrentPresentation(UpdateFlags::UpdateBookProgress);
}

void BookViewModel::resetSearch()
{
	auto searchTextObject = _bookReader->getSearchTextObject();
	assert(searchTextObject != nullptr);

	if (searchTextObject != nullptr)
	{
		searchTextObject->reset();
		_updateCurrentPresentation(UpdateFlags::NoProgressUpdate);
	}
}

void BookViewModel::_initPageVector()
{
	// append a proper number of presentation pages
	int pageCount = _bookReader->getDisplayedPageCount();	
	
	auto pages = _pages;
	_pages = nullptr;
	OnPropertyChanged("BookPages");
	_pages = pages;

	_pages->Append(_page1);
	if (pageCount >= 2)
	{
		_pages->Append(_page2);
	}
	if (pageCount >= 3)
	{
		_pages->Append(_page3);
	}

	_alignCRPageToDispPages();

	OnPropertyChanged("BookPages");
	OnPropertyChanged("SelectedBookPage");

	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::_alignCRPageToDispPages()
{
	// get the current page of the reader and adjust presentation properly
	int currentPageIndex = _bookReader->getCurrentPageIndex();
	int maxPageIndex = _bookReader->getDisplayedPageCount() - 1;
	if (currentPageIndex == 0)
	{
		_selectedPage = 0;
	}
	else if (currentPageIndex >= maxPageIndex)
	{
		_bookReader->pageUp(); // move to the next-to-last page 
		_selectedPage = 2;
	}
	else
	{
		_selectedPage = 1;
	}
	
	OnPropertyChanged("SelectedBookPage");
}

void BookViewModel::_setReaderFont(Platform::Object^ fontName)
{
	_bookReader->setReaderFont(fontName);

	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::_setReaderFontSize(Platform::Object^ fontSize)
{
	_bookReader->setReaderFontSize(fontSize);
	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::_setReaderColors(Platform::Object^ colorIndex)
{
	_bookReader->setReaderColors(colorIndex);
	_updateCurrentPresentation(UpdateFlags::NoProgressUpdate);
}

void BookViewModel::_updateCurrentPresentation(const unsigned int updateFlags)
{
	// update TOC items, book progress, bookmark table before updating pages on the screen
	if ((updateFlags & UpdateFlags::UpdateTOC) != 0)
	{
		OnPropertyChanged("TOCItems");
	}

	if ((updateFlags & UpdateFlags::UpdateBookProgress) != 0)
	{
		_updateBookProgressValue();
		OnPropertyChanged("BookPageCount");
	}

	if ((updateFlags & UpdateFlags::UpdateBookmarkTable) != 0)
	{
		OnPropertyChanged("Bookmarks");
	}

	// update pages on the screen
	int pageCount = _bookReader->getDisplayedPageCount();
	int currentPageIndex = _bookReader->getCurrentPageIndex();
	int pageDelta;

	if (currentPageIndex == 0)
	{
		pageDelta = 0;
	}
	else if (currentPageIndex == (pageCount - 1))
	{
		pageDelta = (-1) * (_pages->Size - 1);
	}
	else
	{
		pageDelta = -1;
	}

	for each(BookPageViewModel^ page in _pages)
	{
		int pageNumber = _bookReader->getBookPageNumber() + (pageDelta + 1) * _bookReader->getVisiblePageCount();
		_bookReader->updatePageBookmarkIndicator(static_cast<int>(pageNumber));
		page->updateDisplayPageDelta(pageDelta++, true);
	}
}

void BookViewModel::OnBookProgressTimerTick(Platform::Object^ sender, Platform::Object^ e)
{
	_bookProgressUpdateTimer->Stop();
	
	_bookReader->goToPage(static_cast<const int>(_bookProgressValue));
	_alignCRPageToDispPages();
	_updateCurrentPresentation(UpdateFlags::NoProgressUpdate);
}

void BookViewModel::OnLineSpacingTimerTick(Platform::Object^ sender, Platform::Object^ e)
{
	_lineSpacingUpdateTimer->Stop();
	
	_bookReader->setLineSpacing(static_cast<const int>(_lastLineSpacingValue));

	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::OnPageMarginsTimerTick(Platform::Object^ sender, Platform::Object^ e)
{
	_pageMarginsUpdateTimer->Stop();
	
	_bookReader->setPageMargins(static_cast<const int>(_lastPageMarginsValue));
	_updateCurrentPresentation(UpdateFlags::UpdateTOC | UpdateFlags::UpdateBookProgress | UpdateFlags::UpdateBookmarkTable);
}

void BookViewModel::_updateBookProgressValue()
{
	auto pageCount = _bookReader->getBookPageCount() * _bookReader->getVisiblePageCount();
	_bookProgressValue = _bookReader->getBookPageNumber() + (_selectedPage - 1) * _bookReader->getVisiblePageCount();
	if (_bookProgressValue < 0)
	{
		_bookProgressValue = 0;
	}
	else if (_bookProgressValue > (pageCount - 1))
	{
		_bookProgressValue = pageCount - 1;
	}
	_bookReader->updatePageBookmarkIndicator(static_cast<int>(_bookProgressValue));
	OnPropertyChanged("BookReadingProgress");
}

void BookViewModel::OnDeviceLost()
{
	_bookReader->saveBookState();
	redrawBook();
}

void BookViewModel::_findInBookPrev(_In_ Platform::Object^ text)
{
	auto searchTextObject = _bookReader->getSearchTextObject();
	assert(searchTextObject != nullptr);
	
	if (searchTextObject != nullptr)
	{
		if (searchTextObject->findAndMoveToText((Platform::String^)text, TextSearchDirection::CurrentToFirst, true))
		{
			_alignCRPageToDispPages();
			_updateCurrentPresentation(UpdateFlags::UpdateBookProgress);
		}
	}
}

void BookViewModel::_findInBookNext(_In_ Platform::Object^ text)
{
	auto searchTextObject = _bookReader->getSearchTextObject();
	assert(searchTextObject != nullptr);
	
	if (searchTextObject != nullptr)
	{
		if (searchTextObject->findAndMoveToText((Platform::String^)text, TextSearchDirection::CurrentToLast, true))
		{
			_alignCRPageToDispPages();
			_updateCurrentPresentation(UpdateFlags::UpdateBookProgress);
		}
	}
}

}
