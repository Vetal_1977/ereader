/*
* @file FilteredLibraryPageViewModel.h
* @brief View model of a FilteredLibraryPage
*
* @date 2013-06-11
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "BookOperationBasePageViewModel.h"

namespace eReader
{
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class FilteredLibraryPageViewModel sealed : public BookOperationBasePageViewModel
	{
	internal:
		FilteredLibraryPageViewModel(ViewModel::BookReader^ bookReader);

		property Platform::Object^ DataSource
		{
			Platform::Object^ get();
		}

	protected:
		// Returns book sets of the class
		// @return Observable vector of book sets
		virtual Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ _getBookSets() override;

		// Updates book data from the database into all book sets. 
		// The method just calls '_update...' for all book sets of this class and fires 'OnPropertyChanged'
		virtual void _updateAllBookSets() override;

	private:
		Windows::Foundation::Collections::IVector<Model::BookCollection^>^ _bookCollections;
	};
}
