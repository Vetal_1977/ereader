/*
* @file BookContainer.cpp
* @brief Book container class
*
* Contains book data from the database and additional data from the file storage.
* Provides access for GUI elements only to those fileds, which canbe displayed
*
* @date 2013-02-01
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookContainer.h"

#define UNDEFINED_COLLECTION_ID -1

using namespace Platform;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Windows::Storage;
using namespace Windows::Storage::Streams;
using namespace concurrency;
using namespace eReader::Model;

namespace eReader
{
namespace ViewModel
{
//
// Book container class
//
BookContainer::BookContainer(BookData^ bookData) :
	_bookData(bookData),
	_collectionID(UNDEFINED_COLLECTION_ID)
{
	_bookData = bookData;
	_openCoverImageFileAsync();
}

String^ BookContainer::Title::get()
{
	return _bookData->Title;
}

String^ BookContainer::Authors::get()
{
	return _bookData->Authors;
}

ImageSource^ BookContainer::Image::get()
{
	return _imageSource;
}

String^ BookContainer::FullPath::get()
{
	String^ fullPath = 
		Windows::Storage::ApplicationData::Current->LocalFolder->Path + 
		L"\\" +
		_bookData->Directory +
		L"\\" +
		_bookData->BookFilename;
	
	return fullPath;
}

BookData^ BookContainer::InternalBookData::get()
{
	return _bookData;
}

int BookContainer::CollectionId::get()
{
	return _collectionID;
}

void BookContainer::CollectionId::set(int value)
{
	_collectionID = value;
}

void BookContainer::_openCoverImageFileAsync()
{
	task<StorageFolder^> getBookFolderTask(Windows::Storage::ApplicationData::Current->LocalFolder->
		GetFolderAsync(_bookData->Directory));
	getBookFolderTask.then([=](StorageFolder^ bookFolder)
	{
		// try to get cover image file
		task<StorageFile^> getCoverImageFileTask(bookFolder->GetFileAsync(_bookData->CoverImageFilename));
		getCoverImageFileTask.then([=](StorageFile^ coverImageFile)
		{
			// get file access stream for a cover image file
			task<IRandomAccessStream^> openFileStreamTask(coverImageFile->OpenAsync(FileAccessMode::Read));
			openFileStreamTask.then([=](IRandomAccessStream^ fileAccessStream)
			{
				_imageSource = ref new BitmapImage();
				task<void> setBitmapSourceTask(_imageSource->SetSourceAsync(fileAccessStream));
				setBitmapSourceTask.then([=]()
				{
					delete fileAccessStream;
					OnPropertyChanged("Image");
				});
			});
		}).then([] (task<void> previousTask)
		{
			try
			{
				previousTask.get(); //Get the result of task by forcing it.
			}
			catch(Exception^ e)
			{
				// file with a cover image is not found - just continue
			}
		});
	});
}

} // ViewModel
}
