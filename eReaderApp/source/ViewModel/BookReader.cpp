/*
 * @file BookReader.cpp
 * @brief Book data source class
 *
 * Data source reads, writes books and their info and collections from/into the storage;
 * it must be initialized by a program start and provides methods to
 * access books and collections. SQLite database is used as
 * a storage of the library information
 *
 * @date 2012-12-25
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#include "pch.h"
#include <time.h>
#include <objbase.h>
#include <ATLComTime.h>
#include "BookReader.h"
#include "Presentation\CRPageDrawBuffer.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "ExceptionHandling\TaskExceptionsExtensions.h"

#define LAST_READ_BOOKS L"Last read"
#define LAST_ADDED_BOOKS L"Last added"
#define CR3_FOLDER_NAME L"CR3"
#define CR3_SETTINGS_FILENAME L"cr3.ini"
#define EREADER_CSS_FILENAME L"epub.css"
#define CR3_HIST_FILENAME L"cr3hist.bmk"
#define ER_BOOK_FILENAME L"eR_Book.dat"
#define COVER_IMAGE_FILENAME L"cover.png"
#define DEFAULT_INTERLINE_SPACE 120 // %
#define DEFAULT_PAGE_MARGINS 20 // pix
#define DEFAULT_FONT_SIZE 32
#define DEFAULT_FONT_NAME "Verdana"
#define DEFAULT_FONT_NAME_L L"Verdana"
#define ARGB_TO_UINT(a, r, g, b) (unsigned int)(((a&255)<<24) | ((r&255)<<16) | ((g&255)<<8) | (b&255))

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;
using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace Windows::Storage::Search;
using namespace Windows::ApplicationModel;
using namespace Windows::UI;
using namespace concurrency;
using namespace eReader::Model;
using namespace eReader::Presentation;
using namespace Windows::Globalization::DateTimeFormatting;

namespace eReader
{
namespace ViewModel
{

static int cr_font_sizes[] = { 11, 13, 16, 18, 20, 24, 28, 32, 38, 48, 60, 72 };

BookReader::BookReader() :
	_bookDatabase(ref new DatabaseStorage()),
	_bookFileStorage(ref new FileStorage()),
	_fontFilenames(ref new Vector<String^>),
	_readerFonts(ref new Vector<String^>()),
	_readerColors(ref new Vector<ReaderColor^>()),
	_cr3Props(),
	_lvDocView(),
	_cr3HistoryFilename(L""),
	_cr3SettingsFilename(""),
	_CSSFolder(""),
	_iconsFolder(""),
	_lastOpenedBookFilename(L""),
	_searchTextObject()
{
}

void BookReader::OnLoadFileFormatDetected(doc_format_t fileFormat)
{
	String^ CSSFilename(L"fb2.css");
	switch (fileFormat)
	{
	case doc_format_txt:
		CSSFilename = L"txt.css";
		break;
	case doc_format_rtf:
		CSSFilename = L"rtf.css";
		break;
	case doc_format_epub:
		CSSFilename = L"epub.css";
		break;
	case doc_format_html:
		CSSFilename = L"htm.css";
		break;
	case doc_format_doc:
		CSSFilename = L"doc.css";
		break;
	case doc_format_chm:
		CSSFilename = L"chm.css";
		break;
	default:
		// do nothing
		break;
	}

	String^ CSSFileFullPath = _CSSFolder + CSSFilename;
	lString8 readerCSS;
	if (LVLoadStylesheetFile(CSSFileFullPath->Data(), readerCSS))
	{
		if (!readerCSS.empty())
		{
			_lvDocView->setStyleSheet(readerCSS);
		}
	}
}

BookSet^ BookReader::SelectedLibraryBookSet::get()
{
	return _selectedLibraryBookSet;
}

void BookReader::SelectedLibraryBookSet::set(BookSet^ value)
{
	_selectedLibraryBookSet = value;
}

task<void> BookReader::initAsync()
{
	task<void> readInstalledFontsTask(_readInstalledFontsAsync());
	return readInstalledFontsTask.then([=]()
	{
		_initCRFontManager();

		task<void> initCREngineTask(_initCREngineAsync());
		return initCREngineTask.then([=]()
		{
			_bookDatabase->openBookDatabase();
			_initReaderColors();
			_initReaderFontSizes();
		});
	});
}

task<void> BookReader::resumeAsync()
{
	_bookDatabase->openBookDatabase();
	_initCRFontManager();
	return _initCREngineAsync();
}

void BookReader::suspend()
{
	// remember the last loaded book to load it upon suspend/resume
	if (_lvDocView->isDocumentOpened())
	{
		_lastOpenedBookFilename = ref new String(_lvDocView->getOriginalFilename().data());
		_lastOpenedBookSize.cx = _lvDocView->GetWidth();
		_lastOpenedBookSize.cy = _lvDocView->GetHeight();
	}

	_bookDatabase->closeBookDatabase();
	_destroyCREingine();
	_destroyCRFontManager(); // font manager can be destroyed only after CR engine destroy

	_cr3Props.Clear();
}

const bool BookReader::isLibraryEmpty()
{
	auto books = _bookDatabase->getBooksByTitle();
	return (books->Size <= 0);
}

String^ BookReader::getDateInCurrentLocale(const wchar_t* dateInDBFormat)
{
	COleDateTime oleDate;
	oleDate.ParseDateTime(dateInDBFormat, VAR_DATEVALUEONLY); // parse date string specified as 'YYYY-MM-DD'

	// get the system time
	SYSTEMTIME st;
	oleDate.GetAsSystemTime(st);

	// get file time to get a time format compatible with DateTime
	FILETIME ft;
	SystemTimeToFileTime(&st, &ft);

	// use _ULARGE_INTEGER to get a uint64 to set the DateTime to
	_ULARGE_INTEGER ulint = {ft.dwLowDateTime, ft.dwHighDateTime};

	Windows::Foundation::DateTime date;
	date.UniversalTime = ulint.QuadPart;

	auto formatter = ref new DateTimeFormatter(YearFormat::Full, MonthFormat::Abbreviated, DayFormat::Default, DayOfWeekFormat::None);
	return formatter->Format(date);
}

concurrency::task<bool> BookReader::importBookFromFile(Windows::Storage::StorageFile^ bookStorageFile, Model::BookData^ bookData)
{
	assert(bookData != nullptr);

	GUID dirGuid;
	HRESULT hr = CoCreateGuid(&dirGuid);
	assert(hr == S_OK);

	wchar_t dirGuidStr[80];
	swprintf_s(dirGuidStr, sizeof(dirGuidStr)/sizeof(wchar_t), 
		L"{%08X-%04hX-%04hX-%02X%02X-%02X%02X%02X%02X%02X%02X}", 
		dirGuid.Data1, 
		dirGuid.Data2, 
		dirGuid.Data3, 
		dirGuid.Data4[0], dirGuid.Data4[1], dirGuid.Data4[2], dirGuid.Data4[3], dirGuid.Data4[4], dirGuid.Data4[5], dirGuid.Data4[6], dirGuid.Data4[7]);

	bookData->Directory = ref new String(dirGuidStr);
	bookData->CoverImageFilename = ref new String(COVER_IMAGE_FILENAME);
	bookData->BookFilename = ref new String(ER_BOOK_FILENAME); //ref new String(bookStorageFile->Name->Data());
	bookData->StoreDateFull = _getCurrentDate();
	bookData->LastOpenedDateFull = ref new String(L"");
	
	auto addBookToFileStorageTask(_bookFileStorage->addBookAsync(bookStorageFile, _lvDocView.get(), bookData));
	return addBookToFileStorageTask.then([=]()
	{
		_bookDatabase->addBook(bookData);
		return true;
	});
}

task<void> BookReader::deleteBook(Object^ bookToDelete)
{
	BookData^ bookData = safe_cast<BookData^>(bookToDelete);

	auto deleteBookFromFileStorageTask = _bookFileStorage->deleteBook(bookData, _lvDocView.get());
	return deleteBookFromFileStorageTask.then([=]()
	{
		_bookDatabase->deleteBook(bookData);
		return;
	});
}

IVector<BookData^>^ BookReader::getLastReadBooks(const uint16 maxNoOfBooks)
{
	return _bookDatabase->getLastReadBooks(maxNoOfBooks);
}

IVector<BookData^>^ BookReader::getLastAddedBooks(const uint16 maxNoOfBooks)
{
	return _bookDatabase->getLastAddedBooks(maxNoOfBooks);
}

//IVector<BookData^>^ BookReader::getBooksByDate(String^ date)
//{
//	return _bookDatabase->getBooksByDate(date->Data());
//}

IVector<Platform::String^>^ BookReader::getAuthors()
{
	return _bookDatabase->getAuthors();
}

IVector<BookData^>^ BookReader::getBooksByAuthor(String^ author)
{
	return _bookDatabase->getBooksByAuthor(author->Data());
}

IVector<BookData^>^ BookReader::getBooksByTitle()
{
	return _bookDatabase->getBooksByTitle();
}

//IVector<BookData^>^ BookReader::getBooksByTitle(_In_ const wchar_t startChar)
//{
//	return _bookDatabase->getBooksByTitle(startChar);
//}

IVector<BookCollection^>^ BookReader::getCollections()
{
	return _bookDatabase->getCollections();
}

IVector<BookData^>^ BookReader::getBooksByCollection(BookCollection^ collection)
{
	return _bookDatabase->getBooksByCollection(collection);
}

void BookReader::addBookToCollection(const int bookId, const int collectionId)
{
	_bookDatabase->addBookToCollection(bookId, collectionId);
}

void BookReader::addCollection(Model::BookCollection^ collection)
{
	_bookDatabase->addCollection(collection);
}

void BookReader::removeBookFromCollection(const int bookId, const int collectionId)
{
	_bookDatabase->removeBookFromCollection(bookId, collectionId);
}

void BookReader::deleteCollection(BookCollection^ collection)
{
	_bookDatabase->deleteCollection(collection);
}

void BookReader::updateLastOpenedDate(const int bookId)
{
	_bookDatabase->updateLastOpenedDate(bookId, _getCurrentDate());
}

void BookReader::clearLastOpenedDateForAllBooks()
{
	_bookDatabase->clearLastOpenedDateForAllBooks();
}

void BookReader::clearLastOpenedDate(_In_ const int bookId)
{
	_bookDatabase->updateLastOpenedDate(bookId, L"");
}

IVector<String^>^ BookReader::getReaderFonts()
{
	return _readerFonts;
}

String^ BookReader::getCurrentFont()
{
	assert(_lvDocView != nullptr);

	lString16 fontName;
	if (!_cr3Props->getString(PROP_FONT_FACE, fontName))
	{
		return ref new String(DEFAULT_FONT_NAME_L);
	}
	return ref new String(fontName.c_str());
}

void BookReader::getReaderFontSizes(int** fontSizes, int& noOfSizes)
{
	assert(fontSizes != nullptr);

	*fontSizes = cr_font_sizes;
	noOfSizes = sizeof(cr_font_sizes) / sizeof(int);
}

const int BookReader::getCurrentFontSize()
{
	assert(_lvDocView != nullptr);

	int fontSize = 0;
	if (!_cr3Props->getInt(PROP_FONT_SIZE, fontSize))
	{
		return DEFAULT_FONT_SIZE;
	}
	return fontSize;
}

IVector<ReaderColor^>^ BookReader::getReaderColors()
{
	return _readerColors;
}

void BookReader::setReaderFont(Platform::Object^ fontName)
{
	assert(_cr3Props.get() != nullptr);
	assert(_lvDocView != nullptr);

	auto fontNameLocal = safe_cast<String^>(fontName);
	_cr3Props->setString(PROP_FONT_FACE, fontNameLocal->Data());

	lString8 fontFace(UnicodeToUtf8(fontNameLocal->Data()));
	_lvDocView->setDefaultFontFace(fontFace);
	_lvDocView->setStatusFontFace(fontFace);

	// CR engine behavior: without this call the font is not applied 
	int fontSize = getCurrentFontSize();
	_lvDocView->setFontSize(fontSize);
}

const int BookReader::getLineSpacing()
{
	assert(_cr3Props.get() != nullptr);
	int lineSpacing = 0;
	if (!_cr3Props->getInt(PROP_INTERLINE_SPACE, lineSpacing))
	{
		return DEFAULT_INTERLINE_SPACE;
	}
	return lineSpacing;
}

void BookReader::setLineSpacing(const int lineSpacing)
{
	assert(_cr3Props.get() != nullptr);
	assert(_lvDocView != nullptr);

	_cr3Props->setInt(PROP_INTERLINE_SPACE, lineSpacing);
	_lvDocView->setDefaultInterlineSpace(lineSpacing);
}

const int BookReader::getPageMargins()
{
	assert(_cr3Props.get() != nullptr);
	int pageMargins = 0;
	if (!_cr3Props->getInt(PROP_PAGE_MARGIN_LEFT, pageMargins))
	{
		return DEFAULT_PAGE_MARGINS;
	}
	return pageMargins;
}

void BookReader::setPageMargins(const int pageMargins)
{
	assert(_cr3Props.get() != nullptr);
	assert(_lvDocView != nullptr);

	_cr3Props->setInt(PROP_PAGE_MARGIN_LEFT, pageMargins);
	_cr3Props->setInt(PROP_PAGE_MARGIN_TOP, pageMargins);
	_cr3Props->setInt(PROP_PAGE_MARGIN_RIGHT, pageMargins);
	_cr3Props->setInt(PROP_PAGE_MARGIN_BOTTOM, pageMargins);

	lvRect rcMargins;
	rcMargins.top = rcMargins.left = rcMargins.bottom = rcMargins.right = pageMargins;
	_lvDocView->setPageMargins(rcMargins);
}

void BookReader::setReaderFontSize(Platform::Object^ fontSize)
{
	int fontSizeLocal = safe_cast<int>(fontSize);
	_cr3Props->setInt(PROP_FONT_SIZE, fontSizeLocal);
	_lvDocView->setFontSize(fontSizeLocal);
}

void BookReader::setReaderColors(Platform::Object^ colorIndex)
{
	auto colorIdxLocal = safe_cast<unsigned int>(colorIndex);

	assert(_readerColors != nullptr);
	assert(0 <= colorIdxLocal && colorIdxLocal < _readerColors->Size);

	// set background texture or color
	auto readerColor = _readerColors->GetAt(colorIdxLocal);
	if (readerColor->ColorCombination == ColorCombinationType::Texture_Foreground)
	{
		lString16 imageFilename(readerColor->TextureFilename->Data());
		_setBackgroundImage(imageFilename);
		_cr3Props->setString(PROP_BACKGROUND_IMAGE, imageFilename);
		_cr3Props->setHex(PROP_BACKGROUND_COLOR, 0xFFFFFFFF);
	}
	else
	{
		lUInt32 backgroundColor = ARGB_TO_UINT(
			readerColor->BackgroundColor.A, 
			readerColor->BackgroundColor.R, 
			readerColor->BackgroundColor.G, 
			readerColor->BackgroundColor.B);

		LVImageSourceRef emptyImage; // empty image to reset a background texture
		_lvDocView->setBackgroundImage(emptyImage, true);
		_lvDocView->setBackgroundColor(backgroundColor);

		_cr3Props->setString(PROP_BACKGROUND_IMAGE, L"");
		_cr3Props->setHex(PROP_BACKGROUND_COLOR, backgroundColor);

	}

	// set font color
	lUInt32 fontColor = ARGB_TO_UINT(
		readerColor->FontColor.A, 
		readerColor->FontColor.R, 
		readerColor->FontColor.G, 
		readerColor->FontColor.B);
	_lvDocView->setTextColor(fontColor);
	_cr3Props->setColor(PROP_FONT_COLOR, fontColor);

	// set bookmark icons
	_setBookmarkImages(readerColor->ActiveBookmarkFilename->Data(), readerColor->InactiveBookmarkFilename->Data());
	_cr3Props->setString(PROP_ACTIVE_BOOKMARK_IMAGE, readerColor->ActiveBookmarkFilename->Data());
	_cr3Props->setString(PROP_INACTIVE_BOOKMARK_IMAGE, readerColor->InactiveBookmarkFilename->Data());
}

void BookReader::loadBook(Platform::String^ bookPath, const SIZE& viewSize)
{
	assert(_lvDocView != nullptr);

	lString16 bookNameCR(bookPath->Data());
	
	bool res = _lvDocView->LoadDocument(bookNameCR.c_str());
	if (!res)
	{
		// book opening failed -> create "error" page
		String^ error = "Error while opening document " + bookPath;
		lString16 errorCR(error->Data());
		_lvDocView->createDefaultDocument(lString16::empty_str, errorCR);
	}

	_lvDocView->Resize(viewSize.cx, viewSize.cy);
	_lvDocView->restorePosition();
	_lvDocView->setOriginalFilename(bookNameCR);
	_lvDocView->checkRender(); // this call will rend pages
	_lvDocView->setShowCover(false);
	
	// set background image if needed
	lString16 imageFilename;
	bool useBackgroundImage = false;
	if (_cr3Props->getString(PROP_BACKGROUND_IMAGE, imageFilename))
	{
		if (imageFilename.length() > 0)
		{
			_setBackgroundImage(imageFilename);
			useBackgroundImage = true;
		}
	}
	
	// if no background image specified, set background color
	if (!useBackgroundImage)
	{
		lUInt32 backgroundColor = 0;
		if (_cr3Props->getColor(PROP_BACKGROUND_COLOR, backgroundColor))
		{
			_lvDocView->setBackgroundColor(backgroundColor);
		}
		else
		{
			backgroundColor = static_cast<lUInt32>(ARGB_TO_UINT(0xFF, 255, 253, 208)); // cream
			_lvDocView->setBackgroundColor(backgroundColor);
		}
	}

	// set font color
	lUInt32 fontColor = 0;
	if (_cr3Props->getColor(PROP_FONT_COLOR, fontColor))
	{
		_lvDocView->setTextColor(fontColor);
	}
	else
	{
		fontColor = ARGB_TO_UINT(0xFF, 0x00, 0x00, 0x00);
		_lvDocView->setTextColor(fontColor);
	}
	
	// set bookmark images
	lString16 activeBookmarkFilename;
	if (!_cr3Props->getString(PROP_ACTIVE_BOOKMARK_IMAGE, activeBookmarkFilename))
	{
		String^ bmkDarkFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_dark_fill.png";
		activeBookmarkFilename = bmkDarkFillIconPath->Data();
	}
	
	lString16 inactiveBookmarkFilename;
	if (!_cr3Props->getString(PROP_INACTIVE_BOOKMARK_IMAGE, inactiveBookmarkFilename))
	{
		String^ bmkDarkNoFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_dark_nofill.png";
		inactiveBookmarkFilename = bmkDarkNoFillIconPath->Data();
	}

	_setBookmarkImages(activeBookmarkFilename, inactiveBookmarkFilename);

	// set reader font
	String^ fontFace = getCurrentFont();
	lString8 fontFaceCR = UnicodeToUtf8(fontFace->Data());
	_lvDocView->setDefaultFontFace(fontFaceCR);
	_lvDocView->setStatusFontFace(fontFaceCR);

	// set reader font size
	int fontSize = getCurrentFontSize();
	_lvDocView->setFontSize(fontSize);

	// set interline spacing
	const int lineSpacing = getLineSpacing();
	_lvDocView->setDefaultInterlineSpace(lineSpacing);

	// set page margins
	const int pageMargins = getPageMargins();
	lvRect rcMargins;
	rcMargins.top = rcMargins.left = rcMargins.bottom = rcMargins.right = pageMargins;
	_lvDocView->setPageMargins(rcMargins);
}

void BookReader::closeBook()
{
	assert(_lvDocView != nullptr);
	if (_lvDocView->isDocumentOpened())
	{
		_lvDocView->close();
	}
}

void BookReader::saveBookState()
{
	assert(_lvDocView != nullptr);
	if (_lvDocView->isDocumentOpened())
	{
		_lvDocView->savePosition();
	}

	// save CR engine settings
	LVStreamRef settingsStream = LVOpenFileStream(_cr3SettingsFilename->Data(), LVOM_WRITE);
	if ( !settingsStream.isNull() ) 
	{
		_cr3Props->saveToStream(settingsStream.get());
		settingsStream->Flush(true);
	}

	// save history file
	LVStreamRef historyStream = LVOpenFileStream(_cr3HistoryFilename->Data(), LVOM_WRITE);
	if ( !historyStream.isNull() ) 
	{
		_lvDocView->getHistory()->saveToStream(historyStream.get());
		historyStream->Flush(true);
	}
}

int BookReader::getDisplayedPageCount()
{
	assert(_lvDocView != nullptr);

	_lvDocView->checkPos(); // this call will correct page presentation in CR engine

	const int totalPageCount = _lvDocView->getPageCount();
	const int visiblePageCount = _lvDocView->getVisiblePageCount();
	int pageCount = totalPageCount / visiblePageCount +
		totalPageCount % visiblePageCount;
	return pageCount;
}

void BookReader::pageDown()
{
	assert(_lvDocView != nullptr);
	_lvDocView->doCommand(DCMD_PAGEDOWN, 1);
}

void BookReader::pageUp()
{
	assert(_lvDocView != nullptr);
	_lvDocView->doCommand(DCMD_PAGEUP, 1);
}

void BookReader::getCurrentDrawBuffer(void** drawBuffer, SIZE& bufferSize, int& bitsPerPixel)
{
	assert(drawBuffer != nullptr);

	*drawBuffer = nullptr;
	bufferSize.cx = 0;
	bufferSize.cy = 0;
	bitsPerPixel = 0;

	// Get page image
	LVDocImageRef lvDocImageRef = _lvDocView->getPageImage(0);
	if (lvDocImageRef.isNull())
	{
		assert(false);
		return;
	}

	// Get draw buffer of the image
	LVDrawBuf* lvDrawBuf = lvDocImageRef->getDrawBuf();
	*drawBuffer = lvDrawBuf->GetScanLine(0);
	bufferSize.cx = lvDrawBuf->GetWidth();
	bufferSize.cy = lvDrawBuf->GetHeight();
	bitsPerPixel = lvDrawBuf->GetBitsPerPixel();
}

void BookReader::resizeBookPageView(const SIZE& viewSize, const int visiblePageCount)
{
	assert(_lvDocView != nullptr);
	
	auto oldPageIdx = getCurrentPageIndex();
	auto oldPageCount = _lvDocView->getPageCount();
	auto oldVisiblePageCount = _lvDocView->getVisiblePageCount();
	_lvDocView->setVisiblePageCount(visiblePageCount);
	_lvDocView->Resize(viewSize.cx, viewSize.cy);
	_lvDocView->checkRender();
	
	// because a previous position cannot be exactly mapped into the new sizes
	if (0 < oldPageIdx && oldPageIdx < (oldPageCount - 1) )
	{
		auto newPageCount = _lvDocView->getPageCount();
		auto newVisiblePageCount = _lvDocView->getVisiblePageCount();
		if (newPageCount > oldPageCount)
		{
			pageDown(); 
		}
		else if (newVisiblePageCount < oldVisiblePageCount)
		{
			pageDown(); 
			pageDown(); 
		}
	}
}

int BookReader::getCurrentPageIndex()
{
	assert(_lvDocView != nullptr);
	int currentPageIndex = _lvDocView->getCurPage() / _lvDocView->getVisiblePageCount();
	return currentPageIndex;
}

const int BookReader::getBookPageCount()
{
	assert(_lvDocView != nullptr);
	return _lvDocView->getPageCount();
}

const int BookReader::getBookPageNumber()
{
	assert(_lvDocView != nullptr);
	return (_lvDocView->getCurPage() + 1); // Page numeration starts from 0 by CR engine. Here a user-friendly value is returned
}

const int BookReader::getVisiblePageCount()
{
	assert(_lvDocView != nullptr);
	return _lvDocView->getVisiblePageCount();
}

void BookReader::goToPage(const int pageNumber)
{
	assert(_lvDocView != nullptr);
#ifdef DEBUG
	const int pageCount = _lvDocView->getPageCount();
	assert(1 <= pageNumber && pageNumber <= pageCount);
#endif
	int pageNumberToSet = pageNumber - 1; // page numeration starts from 0 by CR engine 

	assert(pageNumberToSet >=0 );
	_lvDocView->doCommand(DCMD_GO_PAGE, pageNumberToSet);
}

TableOfContent^ BookReader::getTOC()
{
	assert(_lvDocView != nullptr);

	auto TOC = ref new TableOfContent(*_lvDocView->getToc());
	return TOC;
}

BookmarkTable^ BookReader::getBookmarkTable()
{
	assert(_lvDocView != nullptr);

	auto bookmarkTable = ref new BookmarkTable(_lvDocView.get());
	return bookmarkTable;
}

bool BookReader::isPageBookmarkClicked(_In_ const lvPoint& crPosition)
{
	assert(_lvDocView != nullptr);
	return _lvDocView->isPageBookmarkClicked(crPosition);
}

bool BookReader::isPageBookmarked(const int pageNumber)
{
	assert(_lvDocView != nullptr);
	
	int pageIdxToBookmark = _getPageIdxToBookmark(pageNumber);
	LVPtrVector<CRBookmark, false> pageBookmarks;
	_lvDocView->findPageBookmarks(pageIdxToBookmark, pageBookmarks);
	
	return (pageBookmarks.length() > 0);
}

void BookReader::updatePageBookmarkIndicator(const int pageNumber)
{
	assert(_lvDocView != nullptr);
	
	int pageIdxToBookmark = _getPageIdxToBookmark(pageNumber);
	LVPtrVector<CRBookmark, false> pageBookmarks;
	_lvDocView->findPageBookmarks(pageIdxToBookmark, pageBookmarks);
	
	_lvDocView->setPageBookmarkIndicator(pageIdxToBookmark, pageBookmarks.length() > 0);
}

void BookReader::updatePageBookmark(const int pageNumber, const bool isPageBookmarked)
{
	assert(_lvDocView != nullptr);
	
	// add/remove bookmark
	int pageIdxToBookmark = _getPageIdxToBookmark(pageNumber);
	if (isPageBookmarked)
	{
		_lvDocView->savePageBookmark(pageIdxToBookmark, L"");
	}
	else
	{
		LVPtrVector<CRBookmark, false> pageBookmarks;
		_lvDocView->findPageBookmarks(pageIdxToBookmark, pageBookmarks);
		for (int idx = 0; idx < pageBookmarks.length(); idx++)
		{
			_lvDocView->removeBookmark(pageBookmarks.get(idx));
		}
	}

	// change bookmark indicator
	_lvDocView->setPageBookmarkIndicator(pageIdxToBookmark, isPageBookmarked);
}

SearchTextObject* BookReader::getSearchTextObject()
{
	return _searchTextObject.get();
}

task<void> BookReader::_readInstalledFontsAsync()
{
	task<StorageFolder^> getFolderTask(Package::Current->InstalledLocation->
		GetFolderAsync(ref new String(L"res\\fonts")));

	return getFolderTask.then([=](StorageFolder^ fontFolder)
	{
		String^ fontFolderPath = fontFolder->Path;
		auto queryOptions = ref new QueryOptions(CommonFileQuery::OrderByName, 
			ref new Vector<String^>(1, ref new String(L".ttf")));
		auto query = fontFolder->CreateFileQueryWithOptions(queryOptions);
		task<IVectorView<StorageFile^>^> getFileTask(query->GetFilesAsync());

		return getFileTask.then([=](IVectorView<StorageFile^>^ files)
		{
			// register fonts from files
			for each(StorageFile^ file in files)
			{
				String^ fontFileFullName = fontFolder->Path + "\\" + file->Name;
				_fontFilenames->Append(fontFileFullName);
			};
		});
	});
}

void BookReader::_initCRFontManager()
{
	InitFontManager(lString8::empty_str); // must be called before creation of LVDocView

	// register fonts from files
	for each(String^ fontFilename in _fontFilenames)
	{
		lString8 fontFilePathCR( UnicodeToUtf8(fontFilename->Data()));
		fontMan->RegisterFont(fontFilePathCR);
	};

	// initialize reader font array only if not done yet
	if (_readerFonts->Size == 0)
	{
		// now take installed reader font names back from the font manager
		lString16Collection faceList;
		fontMan->getFaceList(faceList);
		for (int fontIdx = 0; fontIdx < faceList.length(); fontIdx++)
		{
			String^ fontName = ref new String(faceList[fontIdx].c_str());
			_readerFonts->Append(fontName);
		}
	}
}

void BookReader::_destroyCRFontManager()
{
	fontMan->gc();
	ShutdownFontManager();
}

task<void> BookReader::_initCREngineAsync()
{
	// open installed CSS folder
	task<StorageFolder^> getCSSFolderTask(Package::Current->InstalledLocation->
		GetFolderAsync(ref new String(L"res\\css")));
	return getCSSFolderTask.then([=](StorageFolder^ CSSFolder)
	{
		_CSSFolder = CSSFolder->Path + L"\\";

		// open installed icons folder
		task<StorageFolder^> getIconsFolderTask(Package::Current->InstalledLocation->
			GetFolderAsync(ref new String(L"res\\icons")));
		return getIconsFolderTask.then([=](StorageFolder^ iconsFolder)
		{
			_iconsFolder = iconsFolder->Path + L"\\";
		
			// create/open CR3 folder to store settings and history
			auto localFolder = Windows::Storage::ApplicationData::Current->LocalFolder;
			task<StorageFolder^> createCR3FolderTask(Windows::Storage::ApplicationData::Current->LocalFolder->
				CreateFolderAsync(CR3_FOLDER_NAME, CreationCollisionOption::OpenIfExists));
			return createCR3FolderTask.then([=](StorageFolder^ cr3Folder)
			{
				// create/open settings file
				task<StorageFile^> createCR3SettingsFileTask(cr3Folder->CreateFileAsync(CR3_SETTINGS_FILENAME, CreationCollisionOption::OpenIfExists));
				return createCR3SettingsFileTask.then([=](StorageFile^ cr3SettingsFile)
				{
					// create/open history file
					task<StorageFile^> createCR3HistoryFileTask(cr3Folder->CreateFileAsync(CR3_HIST_FILENAME, CreationCollisionOption::OpenIfExists));
					return createCR3HistoryFileTask.then([=](StorageFile^ cr3HistoryFile)
					{
						// now we have opened all necessary streams -> initialize CR engine
						assert(_lvDocView.get() == nullptr);
						_lvDocView.reset(new LVDocView(CRPageDrawBuffer::BPP));
						_searchTextObject.reset(new SearchTextObject(_lvDocView.get()));

						// load CR engine settings
						if (_cr3Props.get() == nullptr)
						{
							_cr3Props = LVCreatePropsContainer();
						}

						_cr3SettingsFilename = cr3SettingsFile->Path;
						LVStreamRef settingsStream = LVOpenFileStream(_cr3SettingsFilename->Data(), LVOM_READ);
						if ( !settingsStream.isNull() ) 
						{
							_cr3Props->loadFromStream(settingsStream.get());
						}

						// set properties to CR engine
						_lvDocView->propsUpdateDefaults(_cr3Props);
						_cr3Props->setBool(PROP_SHOW_TIME, true);
						_cr3Props->setBool(PROP_SHOW_TITLE, true);
						_cr3Props->setBool(PROP_SHOW_BATTERY, false);
						_cr3Props->setBool(PROP_STATUS_CHAPTER_MARKS, false);
						_cr3Props->setBool(PROP_SHOW_POS_PERCENT, false);
						_cr3Props->setBool(PROP_SHOW_PAGE_NUMBER, true);
						_cr3Props->setBool(PROP_SHOW_PAGE_COUNT, true);

						_lvDocView->propsApply(_cr3Props);

						// load history
						_cr3HistoryFilename = cr3HistoryFile->Path;
						LVStreamRef historyStream = LVOpenFileStream(_cr3HistoryFilename->Data(), LVOM_READ);
						if ( !historyStream.isNull() ) 
						{
							_lvDocView->getHistory()->loadFromStream(historyStream);
						}

						// set CR engine callback
						_CRCallbackTransformer = std::unique_ptr<CRDocViewCallbackTransformer>(new CRDocViewCallbackTransformer(this));
						_lvDocView->setCallback(_CRCallbackTransformer.get());

						// load last opened book
						if (!_lastOpenedBookFilename->IsEmpty())
						{
							loadBook(_lastOpenedBookFilename, _lastOpenedBookSize);
						}
					});
				});
			});
		});
	});
}

void BookReader::_destroyCREingine()
{
	saveBookState();
	closeBook();

	_lvDocView->setCallback(nullptr);
	CRDocViewCallbackTransformer* callbackPtr = _CRCallbackTransformer.release();
	delete callbackPtr;

	SearchTextObject* searchTextObjectPtr = _searchTextObject.release();
	delete searchTextObjectPtr;

	LVDocView* lvDocViewPtr = _lvDocView.release();
	delete lvDocViewPtr;
}

void BookReader::_initReaderColors()
{
	String^ texturesPath = Package::Current->InstalledLocation->Path + "\\res\\backgrounds\\";
	String^ bmkHellNoFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_hell_nofill.png";
	String^ bmkHellFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_hell_fill.png";
	String^ bmkDarkNoFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_dark_nofill.png";
	String^ bmkDarkFillIconPath = Package::Current->InstalledLocation->Path + "\\res\\icons\\bookmark_70_dark_fill.png";

	// light texture, dark foreground
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_fabric.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_fabric_indigo_fibre.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_gray_sand.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_metal_old_blue.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_metal_red.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_paper.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_rust.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_sand.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_stones.jpg", Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));

	// dark texture, light foreground
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_fabric_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_fabric_indigo_fibre_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_gray_sand_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_metal_old_blue_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_metal_red_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_paper_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_rust_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_sand_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(texturesPath + "tx_stones_dark.jpg", Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath));

	// light background, dark foreground
	_readerColors->Append(ref new ReaderColor(Colors::White, Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 255, 253, 208), Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath)); // cream
	_readerColors->Append(ref new ReaderColor(Colors::Beige, Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 240, 225, 130), Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath)); // buff
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 193, 154, 107), Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath)); // sand
	_readerColors->Append(ref new ReaderColor(Colors::Wheat, Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath)); // wheat
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 245, 255, 250), Colors::Black,
		bmkDarkFillIconPath, bmkDarkNoFillIconPath)); // mint cream

	// dark texture, light foreground
	_readerColors->Append(ref new ReaderColor(Colors::Black, Colors::White,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(Colors::Black, Colors::Beige,
		bmkHellFillIconPath, bmkHellNoFillIconPath));
	_readerColors->Append(ref new ReaderColor(Colors::Black, ColorHelper::FromArgb(0xFF, 245, 201, 175),
		bmkHellFillIconPath, bmkHellNoFillIconPath)); // desert sand
	_readerColors->Append(ref new ReaderColor(Colors::Black, ColorHelper::FromArgb(0xFF, 240, 225, 130),
		bmkHellFillIconPath, bmkHellNoFillIconPath)); // buff
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 50, 20, 20), Colors::WhiteSmoke,
		bmkHellFillIconPath, bmkHellNoFillIconPath)); // seal brown
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 50, 20, 20), ColorHelper::FromArgb(0xFF, 245, 255, 250),
		bmkHellFillIconPath, bmkHellNoFillIconPath)); // seal brown + mint cream
	_readerColors->Append(ref new ReaderColor(ColorHelper::FromArgb(0xFF, 50, 20, 20), ColorHelper::FromArgb(0xFF, 240, 225, 130),
		bmkHellFillIconPath, bmkHellNoFillIconPath)); // seal brown + buff
}

void BookReader::_initReaderFontSizes()
{
	LVArray<int> sizes( cr_font_sizes, sizeof(cr_font_sizes)/sizeof(int) );
	_lvDocView->setFontSizes( sizes, false );

	int fontSize = sizes[sizes.length() * 1 / 3];
	_cr3Props->setInt(PROP_FONT_SIZE, fontSize);
	_lvDocView->setFontSize(fontSize);
}

String^ BookReader::_getCurrentDate()
{
	__time64_t now;
	struct tm when;
	time(&now);
	_localtime64_s(&when, &now);
	wchar_t nowStr[80];
	wcsftime(nowStr, sizeof(nowStr)/sizeof(wchar_t), L"%Y-%m-%d %H:%M:%S", &when);

	return ref new String(nowStr);
}

void BookReader::_setBackgroundImage(const lString16& imageFilename)
{
	LVStreamRef stream = LVOpenFileStream(imageFilename.data(), LVOM_READ);
	if ( !stream.isNull() ) 
	{
		LVImageSourceRef img = LVCreateStreamCopyImageSource(stream);
		if ( !img.isNull() ) 
		{
			_lvDocView->setBackgroundImage(img, true);
		}
	}
}

void BookReader::_setBookmarkImages(const lString16& activeBookmarkFilename, const lString16& inactiveBookmarkFilename)
{
	LVImageSourceRef activeBookmarkImage;
	LVImageSourceRef inactiveBookmarkImage;
	LVStreamRef stream = LVOpenFileStream(activeBookmarkFilename.data(), LVOM_READ);
	if ( !stream.isNull() ) 
	{
		activeBookmarkImage = LVCreateStreamCopyImageSource(stream);
		stream->Release();
	}

	stream = LVOpenFileStream(inactiveBookmarkFilename.data(), LVOM_READ);
	if ( !stream.isNull() ) 
	{
		inactiveBookmarkImage = LVCreateStreamCopyImageSource(stream);
		stream->Release();
	}

	_lvDocView->setBookmarkImages(activeBookmarkImage, inactiveBookmarkImage);
}

const int BookReader::_getPageIdxToBookmark(_In_ const int pageNumber)
{
	assert(_lvDocView != nullptr);


	//_lvDocView->getDocument()->findText(




	int pageIdxToBookmark = (pageNumber > 0 ? (pageNumber - 1) : pageNumber);
	const int visiblePageCount = _lvDocView->getVisiblePageCount();
	if (visiblePageCount > 1)
	{
		// in 2-page mode bookmark always a left page
		if ((pageIdxToBookmark % visiblePageCount) != 0)
		{
			pageIdxToBookmark--;
		}
	}
	return pageIdxToBookmark;
}

} // ViewModel
}
