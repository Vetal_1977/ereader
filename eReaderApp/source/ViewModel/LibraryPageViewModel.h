﻿/*
* @file LibraryPageViewModel.h
* @brief View model of a LibraryPage
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "BookOperationBasePageViewModel.h"

namespace eReader
{
	ref class DelegateCommand;

	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LibraryPageViewModel sealed : public BookOperationBasePageViewModel
	{
	internal:
		LibraryPageViewModel(ViewModel::BookReader^ bookReader);

	public:
		property Windows::UI::Xaml::Input::ICommand^ AuthorsCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ TitlesCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ LastAddedCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ CollectionsCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ DeleteCollectionCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ AddBookToCollectionCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ RemoveBookFromCollectionCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::Foundation::Collections::IObservableVector<Platform::String^>^ BookCollectionNames
		{
			Windows::Foundation::Collections::IObservableVector<Platform::String^>^ get();
		}

		property Platform::String^ SortByString
		{
			Platform::String^ get();
		}

	internal:
		// Forces a reevaluation of commands, especially "canExecute..." parts
		virtual void reEvaluateCommands() override;

		// Populates the binding page with content passed during navigation. Any saved state is also
		// provided when recreating a page from a prior session.
		// @param stateMap A map of state preserved by this page during an earlier session. This will be null the first time a page is visited.
		virtual void LoadState(_In_ Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;

		// Preserves state associated with this page in case the application is suspended or the
		// page is discarded from the navigation cache.  Values must conform to the serialization
		// requirements of 'SuspensionManager.SessionState'.
		// @param stateMap An empty map to be populated with serializable state.
		virtual void SaveState(_In_ Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;

		// Invoked when the user selected a book set to explore. 
		// 'selectedBookSet->Name' specifies an author, start character, date or collection name
		// @param selectedBookSet A book set to select. Must be a type of Model::BookSet
		void OnBookSetSelected(_In_ Platform::Object^ selectedBookSet);

	protected:
		// Returns book sets of the class
		// @return Observable vector of book sets
		virtual Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ _getBookSets() override;

		// Updates book data from the database into all book sets. 
		// The method just calls '_update...' for all book sets of this class and fires 'OnPropertyChanged'
		virtual void _updateAllBookSets() override;

	private:
		// Invokes to delete collection. After deletion all books remain in the library
		// but they are not assigned to a collection anymore
		// @param collectionName Name of the collection to delete (type String^)
		void _deleteCollection(_In_ Platform::Object^ collectionName);

		// Invokes to add books to a collection. Selection of a target collection is provided by this method
		// @param bookSet Book set, where the name is a collection name (new or existing)
		void _addToCollection(_In_ Platform::Object^ bookSet);

		// Invokes to remove books from collections. After removing selected books are not
		// assigned to any collection
		// @param books Vector of BookContainer objects to be removed from collections
		void _removeFromCollection(_In_ Platform::Object^ books);

		// Invokes to determine whether removing from a collection is allowed
		// @param books Vector of BookContainer objects to be removed from collections
		bool _canRemoveFromCollection(_In_ Platform::Object^ books);

		// Instructs the view model to use a book set ordered by authors
		// @param parameter Not used here
		void _orderByAuthor(_In_ Platform::Object^ parameter);

		// Instructs the view model to use a book set ordered by titles
		// @param parameter Not used here
		void _orderByTitle(_In_ Platform::Object^ parameter);

		// Instructs the view model to use a set, limited by number of books, ordered by last added date
		// @param parameter Not used here
		void _orderByLastAddedDate(_In_ Platform::Object^ parameter);

		// Instructs the view model to use a set ordered by collections
		// @param parameter Not used here
		void _orderByCollection(_In_ Platform::Object^ parameter);

		// Updates book data from the database into authors book sets.
		// This method should be called by each activation of the LibraryPage
		void _updateAuthorBookSets();
		
		// Updates book data from the database into titles book sets.
		// This method should be called by each activation of the LibraryPage
		void _updateTitleBookSets();

		// Updates book data from the database into last added book sets.
		// This method should be called by each activation of the LibraryPage
		void _updateLastAddedBookSets();

		// Updates book data from the database into collection book sets.
		// This method should be called by each activation of the LibraryPage
		void _updateCollectionBookSets();

	private:
		enum BookSetIdx
		{
			Authors = 0,
			Titles,
			LastAdded,
			Collections
		};

		static Platform::String^ const _currentBookSetIdxKey;

		Platform::Collections::Vector<ViewModel::BookSet^>^ _authorBookSets; // <! BookSet = author surname + his books
		Platform::Collections::Vector<ViewModel::BookSet^>^ _titleBookSets; // <! BookSet = character + books, which title start with this character
		Platform::Collections::Vector<ViewModel::BookSet^>^ _lastAddedBookSets; // <! BookSet = date + books, which has been added at this date
		Platform::Collections::Vector<ViewModel::BookSet^>^ _collectionBookSets; // <! BookSet = collection + books in this collection
		Platform::Collections::Vector<Platform::String^>^ _bookCollectionNames;
		Windows::Foundation::Collections::IVector<Model::BookCollection^>^ _bookCollections;

		DelegateCommand^ _authorsCommand;
		DelegateCommand^ _titlesCommand;
		DelegateCommand^ _lastAddedCommand;
		DelegateCommand^ _collectionsCommand;
		DelegateCommand^ _deleteCollectionCommand;
		DelegateCommand^ _addBookToCollectionCommand;
		DelegateCommand^ _removeBooksFromCollectionCommand;

		BookSetIdx _currentBookSetIdx;
	};
}
