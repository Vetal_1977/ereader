/*
* @file BookSet.cpp
* @brief Book set class
*
* Class contains a vector of books and has a name. 
* May be used directly or by library subsets. 
* My be bound by GUI elements
*
* @date 2013-01-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookSet.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

namespace eReader
{
namespace ViewModel
{
BookSet::BookSet(BookSetType bookSetType) :
	_bookSetType(bookSetType)
{
	_books = ref new Vector<BookContainer^>();
}

IObservableVector<BookContainer^>^ BookSet::Books::get()
{
	return _books;
}

String^ BookSet::Name::get()
{
	return _name;
}

void BookSet::Name::set(String^ value)
{
	_name = value;
}

const BookSetType BookSet::getSetType() const
{
	return _bookSetType;
}

} // Model
}
