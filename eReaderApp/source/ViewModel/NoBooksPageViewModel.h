﻿/*
* @file NoBooksPageViewModel.h
* @brief View model of a NoBooksPage
*
* @date 2013-02-05
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "ViewModelBase.h"

namespace eReader
{
	//
	// The NoBooksPageViewModel class contains the presentation logic of a StartPage
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class NoBooksPageViewModel sealed : public ViewModelBase
	{
	internal:
		NoBooksPageViewModel(ViewModel::BookReader^ bookReader);

	public:
		property Windows::UI::Xaml::Input::ICommand^ AddBookCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property bool HasStatusToDisplay { bool get(); }

		property Platform::String^ StatusString { Platform::String^ get(); }

		property bool IsOperationInProgress
		{ 
			bool get(); 
		private:
			void set(bool value);
		}

	private:
		// Imports a book into the storage. First the FileOpenPicker is called to 
		// get the file from the disk, then this file is imported into the library
		// @param parameter is not used here
		void _importBookFromFile(Platform::Object^ parameter);

		// Updates status properties
		// @param displayStatus 'true' if the status should be displayed, false - otherwise
		// @param statusStrng New status string
		void _updateStatus(bool displayStatus, Platform::String^ statusString);

	private:
		ViewModel::BookReader^ _bookReader;
		Windows::UI::Xaml::Input::ICommand^ _addBookCommand;
		bool _hasStatusToDisplay;
		Platform::String^ _statusString;
		bool _isOperationInProgress;
	};
}
