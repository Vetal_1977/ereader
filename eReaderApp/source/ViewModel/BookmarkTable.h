/*
* @file BookmarkTable.h
* @brief Bookmark table and bookmark item classes
*
* The classes should be used by corresponding view and
* view model to display bookmarks
*
* @date 2013-04-17
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "hist.h"

namespace eReader
{
	namespace ViewModel
	{
		//
		// Bookmark wrapper
		//
		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class BookmarkWrapper sealed
		{
		internal:
			// Constructs the class instance
			// @param bookmark Bookmark to wrap
			// @param lvDocView Pointer to a document. Must not be nullptr!
			BookmarkWrapper(_In_ CRBookmark& bookmark, _In_ LVDocView* lvDocView);

		public:
			property Platform::String^ ItemName
			{
				Platform::String^ get();
			}

			property int PageNumber
			{
				int get();
			}

			property Windows::UI::Xaml::Thickness LevelMargin
			{
				Windows::UI::Xaml::Thickness get();
			}

			property int ItemWidth
			{
				int get();
			}

		private:
			Platform::String^ _name;
			int _pageNumber;
		};

		//
		// Bookmark table wrapper. Create wrappers for bookmarks in the book
		//
		ref class BookmarkTable
		{
		internal:
			// Constructs the class instance
			// @param lvDocView Pointer to a document. Must not be nullptr!
			BookmarkTable(_In_ LVDocView* lvDocView);

		public:
			Windows::Foundation::Collections::IObservableVector<BookmarkWrapper^>^ getBookmarks();

		internal:
			static const int getBookmarkMenuMaxWidth();

		private:
			// Append child items of the specified item into the bookmark table. Recursive method!
			// @param lvDocView Pointer to a document. Must not be nullptr!
			void _appendBookmarks(_In_ LVDocView* lvDocView);

		private:
			Platform::Collections::Vector<BookmarkWrapper^>^ _bookmarkTable;
		};
	} // ViewModel
}
