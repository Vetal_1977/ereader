﻿/*
* @file StartPageViewModel.h
* @brief View model of a StartPage
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "BookOperationBasePageViewModel.h"

namespace eReader
{
	//
	// The StartPageViewModel class contains the presentation logic of a StartPage
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class StartPageViewModel sealed : public BookOperationBasePageViewModel
	{
	internal:
		StartPageViewModel(ViewModel::BookReader^ bookReader);

	internal:
		// Populates the binding page with content passed during navigation. Any saved state is also
		// provided when recreating a page from a prior session.
		// @param stateMap A map of state preserved by this page during an earlier session. This will be null the first time a page is visited.
		virtual void LoadState(_In_ Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;

		// Preserves state associated with this page in case the application is suspended or the
		// page is discarded from the navigation cache.  Values must conform to the serialization
		// requirements of 'SuspensionManager.SessionState'.
		// @param stateMap An empty map to be populated with serializable state.
		virtual void SaveState(_In_ Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;

		// Invoked when the user selected a book set to explore
		// @param selectedBookSet A book set to select. Must be a type of Model::BookSet
		void OnBookSetSelected(_In_ Platform::Object^ selectedBookSet);

	protected:
		// Returns book sets of the class
		// @return Observable vector of book sets
		virtual Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ _getBookSets() override;

		// Updates book data from the database into all book sets. 
		// The method just calls '_update...' for all book sets of this class and fires 'OnPropertyChanged'
		virtual void _updateAllBookSets() override;

	private:
		enum BookSetIdx
		{
			LastRead = 0,
			Library
		};

		Platform::Collections::Vector<ViewModel::BookSet^>^ _startPageBookSets;
	};
}
