/*
* @file BookmarkTable.cpp
* @brief Bookmark table and bookmark item classes
*
* The classes should be used by corresponding view and
* view model to display bookmarks
*
* @date 2013-04-17
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookmarkTable.h"

#define BOOKMARK_MENU_MAX_WIDTH 300 // pixels
#define BOOKMARK_MAX_DISP_NAME_LENGTH 47 // characters

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;

namespace eReader
{
namespace ViewModel
{
//
// Bookmark wrapper
//
BookmarkWrapper::BookmarkWrapper(CRBookmark& bookmark, LVDocView* lvDocView)
{
	lString16 bookmarkText = bookmark.getPosText();
	if (bookmarkText.length() > BOOKMARK_MAX_DISP_NAME_LENGTH)
	{
		bookmarkText.limit(BOOKMARK_MAX_DISP_NAME_LENGTH);
		bookmarkText.append(L"...");
	}
	_name = ref new String(bookmarkText.data());
	
	ldomXPointer start = lvDocView->getDocument()->createXPointer(bookmark.getStartPos());
	_pageNumber = lvDocView->getBookmarkPage(start) + 1; // CR engine returns page index starting from 0
}

String^ BookmarkWrapper::ItemName::get()
{
	return _name;
}

int BookmarkWrapper::PageNumber::get()
{
	return _pageNumber;
}

Thickness BookmarkWrapper::LevelMargin::get()
{
	return Thickness(0);
}

int BookmarkWrapper::ItemWidth::get()
{
	return BookmarkTable::getBookmarkMenuMaxWidth();
}

//
// Bookmark table
//
BookmarkTable::BookmarkTable(LVDocView* lvDocView) : 
	_bookmarkTable(ref new Vector<BookmarkWrapper^>())
{
	_appendBookmarks(lvDocView);
}

IObservableVector<BookmarkWrapper^>^ BookmarkTable::getBookmarks()
{
	return _bookmarkTable;
}

const int BookmarkTable::getBookmarkMenuMaxWidth()
{
	return static_cast<int>(__min(BOOKMARK_MENU_MAX_WIDTH, Window::Current->Bounds.Width));
}

void BookmarkTable::_appendBookmarks(LVDocView* lvDocView)
{
	CRFileHistRecord* history = lvDocView->getCurrentFileHistRecord();
	if (history == nullptr)
	{
		return;
	}

	auto bookmarks = history->getBookmarks();
	for (int idx=0; idx < bookmarks.length(); idx++ )
	{
		CRBookmark* bookmark = bookmarks.get(idx);
		auto bookmarkWrapper = ref new BookmarkWrapper(*bookmark, lvDocView);
		_bookmarkTable->Append(bookmarkWrapper);
	}
}

} // ViewModel
}
