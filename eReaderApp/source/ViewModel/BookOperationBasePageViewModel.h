﻿/*
* @file BookOperationBasePageViewModel.h
* @brief Base view model for pages that operation on books (e.g. StartPage, LibraryPage view models)
*
* @date 2013-03-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "ViewModelBase.h"
#include "ViewModel\BookReader.h"

namespace eReader
{
	ref class DelegateCommand;

	//
	// The LibraryPageViewModel class contains the presentation logic of a StartPage
	//
	public ref class BookOperationBasePageViewModel : public ViewModelBase
	{
	internal:
		BookOperationBasePageViewModel(ViewModel::BookReader^ bookReader);

	public:
		property Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ BookSets
		{ 
			Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ ImportBookFromFileCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ DeleteBooksCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property bool HasStatusToDisplay { bool get(); }

		property Platform::String^ StatusString { Platform::String^ get(); }

		property bool IsOperationInProgress
		{ 
			bool get(); 
		}

		// Invoked when the user selected a book to read
		// @param selectedBook A book to select. Must be a type of Model::BookData
		void OnBookSelected(_In_ Platform::Object^ selectedBook);

	internal:
		// Forces a reevaluation of commands, especially "canExecute..." parts
		virtual void reEvaluateCommands();

	protected:
		// Returns book sets of the class. The method must be implemented in child classes and is called from the 'BookSets' get property
		// @return Observable vector of book sets
		virtual Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ _getBookSets();

		// Updates book data from the database into all book sets. The method must be implemented in child classes
		virtual void _updateAllBookSets();

		// Invokes to import a new book from an existing file
		// @param parameter Not used here
		virtual void _importBookFromFile(_In_ Platform::Object^ parameter);

		// Invokes to delete books from the library
		// @param books Vector of BookContainer objects to delete from the library
		virtual void _deleteBooks(_In_ Platform::Object^ books);

		// Invokes to determine whether book deletion is allowed
		// @param books Vector of BookContainer objects to be deleted from the library
		virtual bool _canDeleteBooks(_In_ Platform::Object^ books);

		// Checks to determine whether specified vector contain books
		// @param books Vector of BookContainer objects to be deleted from the library
		// return true if there are books in the vector, false - otherwise
		virtual bool _containBooks(_In_ Platform::Object^ books);

		// Updates status properties
		// @param displayStatus 'true' if the status should be displayed, false - otherwise
		// @param statusStrng New status string
		void _updateStatus(bool displayStatus, Platform::String^ statusString);

		// Activates/deactivates the operation in progress ring
		// @param isActive true, if the progress ring should be activated, false - otherwise
		void _activateOperationInProgress(bool isActive);

	private protected:
		enum CommandId
		{
			Yes = 0,
			No
		};

		ViewModel::BookReader^ _bookReader;
		bool _hasStatusToDisplay;
		Platform::String^ _statusString;
		bool _isOperationInProgress;

		DelegateCommand^ _importBookFromFileCommand;
		DelegateCommand^ _deleteBooksCommand;
	};
}
