﻿/*
* @file BookViewModel.h
* @brief View model of a BookPageViewModel
*
* Provides the vector of book pages that need to be displayed in the GUI
*
* @date 2012-12-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "ViewModelBase.h"
#include "BookPageViewModel.h"
#include "TableOfContent.h"
#include "BookmarkTable.h"

namespace eReader
{
	//
	// The BookViewModel class contains the presentation logic of a BookPageViewModel
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class BookViewModel sealed : public ViewModelBase
	{
	private:
		// Update identification flags
		// UpdateTOC: page numbers in TOC should be updated. It may be necessary if the book size is changed
		// UpdateBookProgress: the book progress value should be updated. It may be necessary if the book 
		// size is changed (e.g. another font or line spacing) or the user jumps to a page over TOC
		// UpdateBookmarkTable: page numbers in bookmark table should be updated. It may be necessary if the book size is changed or the table itself has been changed
		enum UpdateFlags
		{
			NoProgressUpdate = 0x00000000,
			UpdateTOC = 0x00000001,
			UpdateBookProgress = 0x00000002,
			UpdateBookmarkTable = 0x00000004
		};

	internal:
		BookViewModel(ViewModel::BookReader^ bookReader);

	public:
		property Windows::UI::Xaml::Interop::IBindableObservableVector^ BookPages
		{
			Windows::UI::Xaml::Interop::IBindableObservableVector^ get();
		}
		
		property bool IsLoadingBook 
		{ 
			bool get(); 
		private:
			void set(bool value);
		}

		property int SelectedBookPage
		{
			int get();
			void set(int value);
		}

		property double BookPageCount
		{
			double get();
		}

		property double BookReadingProgress
		{
			double get();
			void set(double value);
		}

		property double LineSpacing
		{
			double get();
			void set(double value);
		}
		
		property double PageMargins
		{
			double get();
			void set(double value);
		}

		property Windows::Foundation::Collections::IObservableVector<ViewModel::TOCItemWrapper^>^ TOCItems
		{
			Windows::Foundation::Collections::IObservableVector<ViewModel::TOCItemWrapper^>^ get();
		}

		property Windows::Foundation::Collections::IObservableVector<ViewModel::BookmarkWrapper^>^ Bookmarks
		{
			Windows::Foundation::Collections::IObservableVector<ViewModel::BookmarkWrapper^>^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ FindInBookPrevCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ FindInBookNextCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

	internal:
		property Windows::UI::Xaml::Input::ICommand^ SetFontCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ SetFontSizeCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ SetColorsCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

	internal:
		// Invoked when the user is navigated to this page
		// @param e Navigation event arguments.
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

		// Loads state of this class, allocates unmanaged resources if necessary
		// @param stateMap Collection of <name, object> pairs. May be nullptr
		virtual void LoadState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;
		
		// Saves the state of this class, frees unmanaged resources if necessary
		// @param stateMap Collection of <name, object> pairs. Must not be nullptr
		virtual void SaveState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;
		
		// Invoked when the page in the book has been changed
		// @param selectedPageIdx New selected page index
		void OnPageSelectionChanged(int selectedPageIdx);

		// Invoked when the page in the book has been restored
		// @param selectedPageIdx Selected page index
		void OnPageSelectionRestored(int selectedPageIdx);

		// Returns a list of reader fonts that are registered by CoolReader engine
		// @return Vector of installed font names
		Windows::Foundation::Collections::IVector<Platform::String^>^ getReaderFonts();

		// Returns currently selected book font
		// @return Name of the currently selected font
		Platform::String^ getCurrentFont();

		// Returns a list of possible font sizes of CoolReader engine
		// @param fontSizes Pointer to reader font sizes, must not be nullptr, the caller must not free the memory
		// @param noOfFonts Number of returned sizes
		void getReaderFontSizes(_Out_ int** fontSizes, _Out_ int& noOfSizes);

		// Returns current font size
		// @return Size of the currently selected font
		int32 getCurrentFontSize();

		// Returns a vector of reader colors (background texture + font color)
		// @return A vector of reader colors. Must not be null and must contain at least one color combination
		Windows::Foundation::Collections::IVector<ReaderColor^>^ getReaderColors();

		// Invalidates a current page view. To be invoked when, for instance, the windows size has been changed
		// @param viewSize Size of the book view area
		// @param visiblePageCount Number of visible pages on the screen. Valid values are 1 and 2
		void updateBookView(_In_ const SIZE& viewSize, _In_ const int visiblePageCount);

		// Redraws currently active book pages. To be invoked by application resume
		void redrawBook();

		// Invoked when a TOC item has been selected
		// @param TOCItem TOCItem to select. ViewModel::TOCItemWrapper is expected
		void selectTOCItem(_In_ Platform::Object^ TOCItem);

		// Invoked when a bookmark has been selected
		// @param bookmark Bookmark to select. ViewModel::BookmarkWrapper is expected
		void selectBookmark(_In_ Platform::Object^ bookmark);

		// Invoked when the user tapped on the screen. The specified position is checked
		// and requested action (if applicable) is applied
		// @param position Absolute position on the screen
		// @param isHandled true if the page tapped event was handled an no further processing needed
		void pageTapped(_In_ Windows::Foundation::Point& position, _Out_ bool &isHandled);

		// Scrolls to the specified page
		void goToPage(_In_ const int pageNumber);

		// Resets the text search. Must be called each time when the user call the text search
		void resetSearch();

	private:
		// Updates an observable page vector
		void _initPageVector();

		// Aligns current CR pages to display pages
		void _alignCRPageToDispPages();

		// Sets a font for the reader to display book text
		// @param fontName Font name for texts (String^). Must be one of the fonts returned by 'getReaderFonts'
		void _setReaderFont(_In_ Platform::Object^ fontName);

		// Sets a font size for the reader to display book text
		// @param fontSize Font name for texts (int). Must be one of the fonts returned by 'getReaderFontSizes'
		void _setReaderFontSize(_In_ Platform::Object^ fontSize);

		// Sets background image and text color for the reader
		// @param colorIndex Index of the color in the _readerColors vector (unsigned int)
		void _setReaderColors(_In_ Platform::Object^ colorIndex);

		// Updates the presentation of pages. Invoked if, for instance, font parameters are changed
		// @param updateFlags Update identification flags. Value from UpdateFlags enum must be used
		void _updateCurrentPresentation(_In_ const unsigned int updateFlags);

		// Invoked when a timer of a book progress update ticks
		// @param sender Book progress timer
		// @param e Event data
		void OnBookProgressTimerTick(Platform::Object^ sender, Platform::Object^ e);

		// Invoked when a timer of a line spacing update ticks
		// @param sender Line spacing timer
		// @param e Event data
		void OnLineSpacingTimerTick(Platform::Object^ sender, Platform::Object^ e);

		// Invoked when a timer of a page margins update ticks
		// @param sender Page margins timer
		// @param e Event data
		void OnPageMarginsTimerTick(Platform::Object^ sender, Platform::Object^ e);

		// Internally updates a book progress value. The value is calculated using 
		// the current page number in CR engine, currently selected page index in GUI
		// and page mode (1-, 2- pages). It is also necessary to recalculate bookmarks
		// for the currently presented page
		void _updateBookProgressValue();

		// Saves the book state if DX renderer device was lost
		void OnDeviceLost();

		// Searches the previous appearance of the text in the book
		// @param text Text to search
		void _findInBookPrev(_In_ Platform::Object^ text);

		// Searches the next appearance of the text in the book
		// @param text Text to search
		void _findInBookNext(_In_ Platform::Object^ text);

	private:
		ViewModel::BookReader^ _bookReader;

		Platform::Collections::Vector<BookPageViewModel^>^ _pages;
		bool _isLoadingBook;
		int _selectedPage;
		BookPageViewModel^ _page1;
		BookPageViewModel^ _page2;
		BookPageViewModel^ _page3;
		Windows::UI::Xaml::Input::ICommand^ _setFontCommand;
		Windows::UI::Xaml::Input::ICommand^ _setFontSizeCommand;
		Windows::UI::Xaml::Input::ICommand^ _setColorsCommand;
		Windows::UI::Xaml::DispatcherTimer^ _bookProgressUpdateTimer;
		Windows::UI::Xaml::DispatcherTimer^ _lineSpacingUpdateTimer;
		Windows::UI::Xaml::DispatcherTimer^ _pageMarginsUpdateTimer;
		double _bookProgressValue;
		double _lastLineSpacingValue;
		double _lastPageMarginsValue;
		DelegateCommand^ _findInBookPrevCommand;
		DelegateCommand^ _findInBookNextCommand;
	};
}
