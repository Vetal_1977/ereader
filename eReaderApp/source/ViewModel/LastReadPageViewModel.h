﻿/*
* @file LastReadPageViewModel.h
* @brief View model of a LastAddedPage
*
* @date 2013-03-10
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "BookOperationBasePageViewModel.h"

namespace eReader
{
	ref class DelegateCommand;

	//
	// The LastReadPageViewModel class contains the presentation logic of a StartPage
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LastReadPageViewModel sealed : public BookOperationBasePageViewModel
	{
	internal:
		LastReadPageViewModel(ViewModel::BookReader^ bookReader);

	public:
		property Windows::UI::Xaml::Input::ICommand^ RemoveBooksFromHistoryCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

		property Windows::UI::Xaml::Input::ICommand^ ClearHistoryCommand
		{
			Windows::UI::Xaml::Input::ICommand^ get();
		}

	internal:
		// Forces a reevaluation of commands, especially "canExecute..." parts
		virtual void reEvaluateCommands() override;

		// Populates the binding page with content passed during navigation. Any saved state is also
		// provided when recreating a page from a prior session.
		// @param stateMap A map of state preserved by this page during an earlier session. This will be null the first time a page is visited.
		virtual void LoadState(_In_ Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ stateMap) override;

	protected:
		// Returns book sets of the class
		// @return Observable vector of book sets
		virtual Windows::Foundation::Collections::IObservableVector<ViewModel::BookSet^>^ _getBookSets() override;

		// Updates book data from the database into all book sets. 
		// The method just calls '_update...' for all book sets of this class and fires 'OnPropertyChanged'
		virtual void _updateAllBookSets() override;

	private:
		// Invokes to clear the history (last read books)
		// @param parameter Not used here
		void _clearHistory(_In_ Platform::Object^ parameter);

		// Invokes to remove books from the history (last opened books)
		// @param books Vector of BookContainer objects to be removed from the history
		void _removeFromHistory(_In_ Platform::Object^ books);

		// Invokes to determine whether removing from a history is allowed
		// @param books Vector of BookContainer objects to be removed from the history
		bool _canRemoveFromHistory(_In_ Platform::Object^ books);

	private:
		Platform::Collections::Vector<ViewModel::BookSet^>^ _lastReadBookSets; // <! BookSet = date + books, which has been added at this date

		DelegateCommand^ _removeBooksFromHistoryCommand;
		DelegateCommand^ _clearHistoryCommand;
	};
}
