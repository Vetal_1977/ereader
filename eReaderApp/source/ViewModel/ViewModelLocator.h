﻿/*
* @file ViewModelLocator.h
* @brief Locator of known view models
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "StartPageViewModel.h" // Required by generated header
#include "BookViewModel.h" // Required by generated header
#include "LibraryPageViewModel.h" // Required by generated header
#include "NoBooksPageViewModel.h" // Required by generated header
#include "LastReadPageViewModel.h" // Required by generated header
#include "FilteredLibraryPageViewModel.h" // Required by generated header

namespace eReader
{
	//
	// The ViewModelLocator class gives pages access to the app's presentation logic
	// following the Model-View-ViewModel (MVVM) pattern.
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ViewModelLocator sealed
	{
	public:
		ViewModelLocator();
		virtual ~ViewModelLocator();

		property StartPageViewModel^ StartPageVM { StartPageViewModel^ get(); }
		property NoBooksPageViewModel^ NoBooksPageVM { NoBooksPageViewModel^ get(); }
		property BookViewModel^ BookVM { BookViewModel^ get(); }
		property LibraryPageViewModel^ LibraryPageVM { LibraryPageViewModel^ get(); }
		property LastReadPageViewModel^ LastReadPageVM { LastReadPageViewModel^ get(); }
		property FilteredLibraryPageViewModel^ FilteredLibraryPageVM { FilteredLibraryPageViewModel^ get(); }

	private:
	};
}
