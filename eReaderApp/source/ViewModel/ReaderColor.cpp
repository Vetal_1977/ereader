/*
* @file ReaderColor.cpp
* @brief The class represents reader colors (background texture file and font color)
*
* @date 2013-01-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ReaderColor.h"

using namespace Platform;
using namespace Windows::UI;

namespace eReader
{

ReaderColor::ReaderColor(Platform::String^ textureFilename, const Windows::UI::Color& fontColor,
								 Platform::String^ activeBookmarkFilename, Platform::String^ inactiveBookmarkFilename) :
	_textureFilename(textureFilename),
	_fontColor(fontColor),
	_backgroundColor(),
	_colorCombinationType(ColorCombinationType::Texture_Foreground),
	_activeBookmarkFilename(activeBookmarkFilename),
	_inactiveBookmarkFilename(inactiveBookmarkFilename)
{
}

ReaderColor::ReaderColor(const Windows::UI::Color& backgroundColor, const Windows::UI::Color& fontColor,
								 Platform::String^ activeBookmarkFilename, Platform::String^ inactiveBookmarkFilename) :
	_textureFilename(""),
	_fontColor(fontColor),
	_backgroundColor(backgroundColor),
	_colorCombinationType(ColorCombinationType::Background_Foreground),
	_activeBookmarkFilename(activeBookmarkFilename),
	_inactiveBookmarkFilename(inactiveBookmarkFilename)
{
}

String^ ReaderColor::TextureFilename::get()
{
	return _textureFilename;
}

const Color ReaderColor::FontColor::get()
{
	return _fontColor;
}

const Color ReaderColor::BackgroundColor::get()
{
	return _backgroundColor;
}

const ColorCombinationType ReaderColor::ColorCombination::get()
{
	return _colorCombinationType;
}

String^ ReaderColor::ActiveBookmarkFilename::get()
{
	return _activeBookmarkFilename;
}

String^ ReaderColor::InactiveBookmarkFilename::get()
{
	return _inactiveBookmarkFilename;
}

}
