﻿/*
* @file ReaderException.h
* @brief Reader exception is inherited from std:exception and contains additional information about an exception
*
* @date 2013-01-21
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader 
{
	//
	// Possible type of the specified message string value
	//
	enum MessageStringType
	{
		PureText = 0,
		ResourceString
	};

	//
	// Specific Reader exception
	//
	class ReaderException : public std::exception
	{
	public:
		// Constructs this object
		// @param message Exception explnation
		// @param stringType Type of the 'messageString'; it may contain text or resource string to load it
		// @param result Specific result value to extent an explanation
		ReaderException(const wchar_t* messageString, const MessageStringType stringType, const int result);

	public:
		// Return an exception explanation string
		virtual const wchar_t* what();

		// Returns the operation result, which caused the exception
		virtual const int result();

	private:
		const ReaderException& operator=(const ReaderException&) {};

	private:
		std::wstring _what;
		const int _result;
	};
}