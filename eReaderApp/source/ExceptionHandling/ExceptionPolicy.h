﻿/*
* @file ExceptionPolicy.h
* @brief Abstract class defines an exception policy in a scope of the application
*
* @date 2013-01-21
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "ReaderException.h"

namespace eReader
{
	class ExceptionPolicy abstract
	{
	public:
		// Handles a specified exception according to policy rules
		// @param exception Exception contained necessary explanation
		virtual void handleException(std::exception& exception) = 0;

		// Handles a specific eReader exception according to policy rules
		// @param exception Specific eReader exception contained necessary explanation
		virtual void handleException(ReaderException& exception) = 0;

		// Handles a specified exception according to policy rules
		// @param exception Exception contained necessary explanation
		virtual void handleException(Platform::Exception^ exception) = 0;
	};
}