/*
* @file TaskExceptionsExtensions.h
* @brief Task exceptions extensions to catch exceptions thrown within a task. Slightly modified code from Microsoft Hilo project
*
* @date 2013-05-13
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "ExceptionPolicy.h"

namespace eReader 
{
	template<typename T>
	struct ObserveException
	{
		ObserveException(ExceptionPolicy* handler, bool rethrow) : _handler(handler), _rethrow(rethrow)
		{
		}

		concurrency::task<T> operator()(concurrency::task<T> antecedent) const
		{
			T result;
			try 
			{
				result = antecedent.get();
			}
			catch(ReaderException& ex)
			{
				if (_rethrow)
					throw;
				else
					_handler->handleException(ex);
				//throw;
			}
			catch(concurrency::task_canceled&)
			{
				// don't need to run canceled tasks through the policy
			}
			catch(std::exception& ex)
			{
				if (_rethrow)
					throw;
				else
					_handler->handleException(ex);
				//throw;
			}
			catch(Platform::Exception^ ex)
			{
				if (_rethrow)
					throw;
				else
					_handler->handleException(ex);
				//throw;
			}
			return antecedent;
		}

	private:
		ExceptionPolicy* _handler;
		bool _rethrow;
	};

	template<>
	concurrency::task<void> ObserveException<void>::operator()(concurrency::task<void> antecedent) const
	{
		try 
		{
			antecedent.get();
		}
		catch(ReaderException& ex)
		{
			if (_rethrow)
				throw;
			else
				_handler->handleException(ex);
			//throw;
		}
		catch(concurrency::task_canceled&)
		{
			// don't need to run canceled tasks through the policy
		}
		catch(std::exception& ex)
		{
			if (_rethrow)
				throw;
			else
				_handler->handleException(ex);
			//throw;
		}
		catch(Platform::Exception^ ex)
		{
			if (_rethrow)
				throw;
			else
				_handler->handleException(ex);
			//throw;
		}

		return antecedent;
	}
}