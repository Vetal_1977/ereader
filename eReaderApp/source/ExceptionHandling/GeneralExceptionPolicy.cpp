﻿/*
* @file GeneralExceptionPolicy.cpp
* @brief General exception policy implements an abstract ExceptionPolicy class
*
* General policy: if an exception occurs, show a user-friendly message to the user
* and write additional information into debug output window. 
* If 'exception->what()' is a resource string, it will be loaded;
* otherwise it is taken directly
*
* @date 2013-01-21
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "GeneralExceptionPolicy.h"
#include <sstream>
#include <atlbase.h>

using namespace Platform;
using namespace std;
using namespace Windows::UI::Popups;
using namespace eReader::Utils;

namespace eReader
{

void GeneralExceptionPolicy::handleException(std::exception& exception)
{
	// Debug output works only in debug version
	wstringstream stream;
	stream << exception.what() << std::endl;
	OutputDebugString(stream.str().c_str());
}

void GeneralExceptionPolicy::handleException(ReaderException& exception)
{
	// Debug output works only in debug version
	wstringstream stream;
	stream << "[HR: " << exception.result() << "] " << exception.what() << std::endl;
	OutputDebugString(stream.str().c_str());
}

void GeneralExceptionPolicy::handleException(Platform::Exception^ exception)
{
	// Debug output works only in debug version
	wstringstream stream;
	stream << "[HR: " << exception->HResult << "] " << exception->Message->Data() << std::endl;
	OutputDebugString(stream.str().c_str());
}

}
