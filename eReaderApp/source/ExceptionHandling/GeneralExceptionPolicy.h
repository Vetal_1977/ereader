﻿/*
* @file GeneralExceptionPolicy.h
* @brief General exception policy implements an abstract ExceptionPolicy class
*
* General policy: if an exception occurs, show a user-friendly message to the user
* and write additional information into debug output window. 
* If 'exception->what()' is a resource string, it will be loaded;
* otherwise it is taken directly
*
* @date 2013-01-21
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "ExceptionPolicy.h"

namespace eReader 
{
	class GeneralExceptionPolicy : public ExceptionPolicy
	{
	public:
		// Handles a specified exception according to policy rules
		// @param exception Exception contained necessary explanation
		virtual void handleException(std::exception& exception);

		// Handles a specific eReader exception according to policy rules
		// @param exception Specific eReader exception contained necessary explanation
		virtual void handleException(ReaderException& exception) override;

		// Handles a specified exception according to policy rules
		// @param exception Exception contained necessary explanation
		virtual void handleException(Platform::Exception^ exception);
	};
}
