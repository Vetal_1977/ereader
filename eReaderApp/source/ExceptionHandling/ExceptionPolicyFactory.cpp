﻿/*
* @file ExceptionPolicyFactory.cpp
* @brief Exception factory provides static methods to access the exception policy
*
* @date 2012-12-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ExceptionPolicyFactory.h"
#include "GeneralExceptionPolicy.h"

namespace eReader
{

std::unique_ptr<ExceptionPolicy> ExceptionPolicyFactory::_policy = nullptr;

ExceptionPolicy* ExceptionPolicyFactory::getCurrentPolicy()
{
	// lazy initialization
	if (_policy == nullptr)
	{
		_policy = std::unique_ptr<ExceptionPolicy>(new GeneralExceptionPolicy());
	}
	return _policy.get();
}

}
