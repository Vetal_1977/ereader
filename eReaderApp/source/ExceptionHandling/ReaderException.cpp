﻿/*
* @file ReaderException.cpp
* @brief Reader exception is inherited from std:exception and contains additional information about an exception
*
* @date 2013-01-21
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ReaderException.h"

using namespace Platform;
using namespace eReader::Utils;

namespace eReader
{

ReaderException::ReaderException(
	const wchar_t* messageString, 
	const MessageStringType stringType, 
	const int result) : _result(result)
{
	if (stringType == MessageStringType::PureText)
	{
		_what = messageString;
	}
	else
	{
		String^ resourceString = ref new String(messageString);
		String^ what = ResLoader::getString(resourceString);
		if (what->IsEmpty())
		{
			// resource string not found - take it as an error message
			_what = messageString;
		}
		else
		{
			_what = what->Data();
		}
	}
}

const wchar_t* ReaderException::what()
{
	return _what.c_str();
}

const int ReaderException::result()
{
	return _result;
}

} 