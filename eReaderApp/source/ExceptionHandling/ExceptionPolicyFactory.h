﻿/*
* @file ExceptionPolicyFactory.h
* @brief Exception factory provides static methods to access the exception policy
*
* @date 2012-12-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "ExceptionPolicy.h"

namespace eReader
{
	//
	// Exception policy factory class
	//
	class ExceptionPolicyFactory
	{
	public:
		static ExceptionPolicy* getCurrentPolicy();

	private:
		static std::unique_ptr<ExceptionPolicy> _policy;
	};
}