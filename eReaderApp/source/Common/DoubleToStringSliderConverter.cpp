﻿/*
* @file DoubleToStringSliderConverter.cpp
* @brief Converts double value of the slider thumb to string "p. <page number>"
*
* @date 2013-03-29
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DoubleToStringSliderConverter.h"

using namespace eReader::Common;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Interop;

Object^ DoubleToStringSliderConverter::Convert(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
	(void) targetType;	// Unused parameter]
	(void) parameter;	// Unused parameter
	(void) language;	// Unused parameter

	auto boxedDouble = dynamic_cast<Box<double>^>(value);
	String^ stringValue;
	if (boxedDouble == nullptr)
	{
		stringValue = ref new String(L"");
	}
	else
	{
		static String^ format = Utils::ResLoader::getString("SliderPageNumFormat");
		static wchar_t pageNumString[256];

		swprintf_s(pageNumString, format->Data(), static_cast<int>(boxedDouble->Value));
		stringValue = ref new String(pageNumString);
	}
	return stringValue;
}

Object^ DoubleToStringSliderConverter::ConvertBack(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
	(void) targetType;	// Unused parameter
	(void) parameter;	// Unused parameter
	(void) language;	// Unused parameter

	throw ref new NotImplementedException();
}
