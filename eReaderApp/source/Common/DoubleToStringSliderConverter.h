﻿/*
* @file DoubleToStringSliderConverter.h
* @brief Converts double value of the slider thumb to string "p. <page number>"
*
* @date 2013-03-29
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
	namespace Common
	{
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class DoubleToStringSliderConverter sealed : Windows::UI::Xaml::Data::IValueConverter
		{
		public:
			virtual Platform::Object^ Convert(Platform::Object^ value, Windows::UI::Xaml::Interop::TypeName targetType, Platform::Object^ parameter, Platform::String^ language);
			virtual Platform::Object^ ConvertBack(Platform::Object^ value, Windows::UI::Xaml::Interop::TypeName targetType, Platform::Object^ parameter, Platform::String^ language);
		};
	}
}
