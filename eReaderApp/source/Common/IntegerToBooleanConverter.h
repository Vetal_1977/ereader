﻿/*
* @file IntegerToBooleanConverter.h
* @brief Converts integer value to boolean value. It is used in binding
* between XAML controls properties
*
* @date 2013-02-26
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
	namespace Common
	{
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class IntegerToBooleanConverter sealed : Windows::UI::Xaml::Data::IValueConverter
		{
		public:
			virtual Platform::Object^ Convert(Platform::Object^ value, Windows::UI::Xaml::Interop::TypeName targetType, Platform::Object^ parameter, Platform::String^ language);
			virtual Platform::Object^ ConvertBack(Platform::Object^ value, Windows::UI::Xaml::Interop::TypeName targetType, Platform::Object^ parameter, Platform::String^ language);
		};
	}
}
