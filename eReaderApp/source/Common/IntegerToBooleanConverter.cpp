﻿/*
* @file IntegerToBooleanConverter.cpp
* @brief Converts integer value to boolean value. It is used in binding
* between XAML controls properties
*
* @date 2013-02-26
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "IntegerToBooleanConverter.h"

using namespace eReader::Common;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Interop;

Object^ IntegerToBooleanConverter::Convert(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
	(void) targetType;	// Unused parameter
	(void) parameter;	// Unused parameter
	(void) language;	// Unused parameter

	auto boxedInteger = dynamic_cast<Box<int32>^>(value);
	int32 integerValue = 0;
	if (boxedInteger != nullptr)
	{
		integerValue = boxedInteger->Value;
	}
	return (integerValue == 0 ? false : true);
}

Object^ IntegerToBooleanConverter::ConvertBack(Object^ value, TypeName targetType, Object^ parameter, String^ language)
{
	(void) targetType;	// Unused parameter
	(void) parameter;	// Unused parameter
	(void) language;	// Unused parameter

	auto boxedBool = dynamic_cast<Box<boolean>^>(value);
	auto boolValue = (boxedBool != nullptr && boxedBool->Value);
	return (boolValue ? 1 : 0);
}
