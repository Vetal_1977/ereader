#include "pch.h"
#include "Constants.h"

using namespace Platform;

namespace eReader
{
	const int Constants::SNAPPED_WINDOW_MAX_WIDTH = 500;
	const int Constants::FULLSCREEN_PORTRAIT_MAX_WIDTH = 1366;

	String^ Constants::SNAPPED = "Snapped";
	String^ Constants::FULL_SCREEN_PORTRAIT = "FullScreenPortrait";
	String^ Constants::FILLED_OR_NARROW = "FilledOrNarrow";
	String^ Constants::FULL_SCREEN_LANDSCAPE = "FullScreenLandscape";
}