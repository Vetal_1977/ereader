#pragma once

namespace eReader
{
	class Constants
	{
	public:
		static const int SNAPPED_WINDOW_MAX_WIDTH;
		static const int FULLSCREEN_PORTRAIT_MAX_WIDTH;
		
		static Platform::String^ SNAPPED;
		static Platform::String^ FULL_SCREEN_PORTRAIT;
		static Platform::String^ FILLED_OR_NARROW;
		static Platform::String^ FULL_SCREEN_LANDSCAPE;
	};
}
