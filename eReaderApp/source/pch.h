﻿//
// pch.h
// Header for standard system include files.
//

#pragma once

#include <collection.h>
#include <ppltasks.h>
#include <assert.h>

#include "App.xaml.h"
#include "Utils\ResLoader.h"
#include "Utils\ErrorCodes.h"
#include "ExceptionHandling\ReaderException.h"
