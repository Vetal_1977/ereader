﻿
//
// App.xaml.cpp
// Implementation of the App class
//

#include "pch.h"
#include "Common\SuspensionManager.h"
#include "ExceptionHandling\ExceptionPolicyFactory.h"
#include "View\NoBooksPage.xaml.h"
#include "View\StartPage.xaml.h"
#include "View\AboutSettingsFlyout.xaml.h"
#include "View\PrivacyPolicySettingsFlyout.xaml.h"

#include <ppltasks.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

using namespace eReader::Model;
using namespace eReader::Presentation;
using namespace eReader::Common;
using namespace eReader::ViewModel;

using namespace Concurrency;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage;
using namespace Windows::Storage::Search;

using namespace Callisto::Controls::Common;
using namespace Callisto::Controls::SettingsManagement;
using namespace Callisto::Controls;

namespace eReader
{
// The Grid Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234226

App::App()
{
	InitializeComponent();
	Resuming += ref new EventHandler<Object^>(this, &App::OnResuming);
	Suspending += ref new SuspendingEventHandler(this, &App::OnSuspending);
	UnhandledException += ref new UnhandledExceptionEventHandler(this, &App::OnUnhandledException);
}

BookReader^ App::eBookReader::get()
{
	return _bookReader;
}

DXRenderer^ App::Renderer::get()
{ 
	return _renderer;
}

void App::OnLaunched(LaunchActivatedEventArgs^ args)
{
	// application settings
	_appSettings = ref new AppSettings();
	auto aboutSettingFlyout = ref new AboutSettingsFlyout();
	aboutSettingFlyout->Title = Utils::ResLoader::getString("About");
	
	auto privacyPolicySettingFlyout = ref new PrivacyPolicySettingsFlyout();
	privacyPolicySettingFlyout->Title = Utils::ResLoader::getString("PrivacyPolicy");

	_appSettings->AddCommand(aboutSettingFlyout, "About");
	_appSettings->AddCommand(privacyPolicySettingFlyout, "PrivacyPolicy");

	// create DirectX renderer
	Windows::Graphics::Display::DisplayInformation;
	_renderer = ref new DXRenderer(Window::Current->Bounds, Windows::Graphics::Display::DisplayInformation::GetForCurrentView()->LogicalDpi);

	// create and initialize book reader
	_bookReader = ref new BookReader();
	task<void> initBookReader(_bookReader->initAsync());
	initBookReader.then([=]()
	{
		auto rootFrame = dynamic_cast<Frame^>(Window::Current->Content);

		// Do not repeat app initialization when the Window already has content,
		// just ensure that the window is active
		if (rootFrame == nullptr)
		{
			// Create a Frame to act as the navigation context and associate it with
			// a SuspensionManager key
			rootFrame = ref new Frame();
			SuspensionManager::RegisterFrame(rootFrame, "AppFrame");

			auto prerequisite = task<void>([](){});
			if (args->PreviousExecutionState == ApplicationExecutionState::Terminated)
			{
				// Restore the saved session state only when appropriate, scheduling the
				// final launch steps after the restore is complete
				prerequisite = SuspensionManager::RestoreAsync();
			}
			prerequisite.then([=](task<void> prerequisite)
			{
				try
				{
					prerequisite.get();
				}
				catch (Platform::Exception^)
				{
					//Something went wrong restoring state.
					//Assume there is no state and continue
				}
			}, task_continuation_context::use_current()).then([=]()
			{
				if (rootFrame->Content == nullptr)
				{
					// When the navigation stack isn't restored navigate to the first page,
					// configuring the new page by passing required information as a navigation
					// parameter
					if (_bookReader->isLibraryEmpty())
					{
						if (!rootFrame->Navigate(TypeName(NoBooksPage::typeid)))
						{
							throw ref new FailureException("Failed to create initial page");
						}
					}
					else
					{
						if (!rootFrame->Navigate(TypeName(StartPage::typeid)))
						{
							throw ref new FailureException("Failed to create initial page");
						}
					}
				}
				// Place the frame in the current Window
				Window::Current->Content = rootFrame;
				// Ensure the current window is active
				Window::Current->Activate();
			}, task_continuation_context::use_current());
		}
		else
		{
			if (rootFrame->Content == nullptr)
			{
				assert(_bookReader != nullptr);

				// When the navigation stack isn't restored navigate to the first page,
				// configuring the new page by passing required information as a navigation
				// parameter
				if (_bookReader == nullptr || _bookReader->isLibraryEmpty())
				{
					if (!rootFrame->Navigate(TypeName(NoBooksPage::typeid)))
					{
						throw ref new FailureException("Failed to create initial page");
					}
				}
				else
				{
					if (!rootFrame->Navigate(TypeName(StartPage::typeid)))
					{
						throw ref new FailureException("Failed to create initial page");
					}
				}
			}
			// Ensure the current window is active
			Window::Current->Activate();
		}
	});
}

void App::OnSuspending(Object^ sender, SuspendingEventArgs^ e)
{
	(void) sender;	// Unused parameter

	auto deferral = e->SuspendingOperation->GetDeferral();
	ReaderPage::IsSuspending = true;
	SuspensionManager::SaveAsync().then([=]()
	{
		_bookReader->suspend();
		
		auto rootFrame = dynamic_cast<Frame^>(Window::Current->Content);
		auto currentPage = dynamic_cast<ReaderPage^>(rootFrame->Content);
		if (currentPage != nullptr)
		{
			currentPage->suspend();
		}

		ReaderPage::IsSuspending = false;
		deferral->Complete();

		_CrtDumpMemoryLeaks();
	});
}

void App::OnResuming(Platform::Object^ sender, Platform::Object^ e)
{
	_bookReader->resumeAsync().then([=]()
	{
		auto rootFrame = dynamic_cast<Frame^>(Window::Current->Content);
		auto currentPage = dynamic_cast<ReaderPage^>(rootFrame->Content);
		if (currentPage != nullptr)
		{
			currentPage->resume();
		}
	});
}

void App::OnUnhandledException(Platform::Object^ sender, UnhandledExceptionEventArgs^ e)
{
	Platform::Exception^ ex = ref new Platform::Exception(e->Exception.Value, e->Message);
	ExceptionPolicyFactory::getCurrentPolicy()->handleException(ex);
	e->Handled = true;
}

}
