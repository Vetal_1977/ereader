﻿/*
* @file FilteredLibraryPage.xaml.h
* @brief Library page displays library books applying a filter 
*
* The filtered library page displays books stored in the library using a filter.
* A filter may be an author, start character of a title, a date or collection
*
* @date 2013-06-10
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "FilteredLibraryPage.xaml.h"
#include "AddBookToCollectionControl.xaml.h"
#include "DeleteBookCollectionControl.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace concurrency;
using namespace Windows::UI::ViewManagement;
using namespace eReader::Utils;
using namespace Platform::Collections;

namespace eReader
{

FilteredLibraryPage::FilteredLibraryPage()
{
	InitializeComponent();
}

void FilteredLibraryPage::OnNavigatedTo(NavigationEventArgs^ e)
{
	auto vm = dynamic_cast<FilteredLibraryPageViewModel^>(DataContext);
	CollectionsViewSource->Source = vm->DataSource;
}

}
