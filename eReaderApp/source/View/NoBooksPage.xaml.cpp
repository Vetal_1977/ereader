﻿/*
* @file NoBooksPage.xaml.cpp
* @brief This page is displayed if there is no books in the library. 
*
* @date 2013-02-05
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "NoBooksPage.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace concurrency;

namespace eReader
{

NoBooksPage::NoBooksPage()
{
	InitializeComponent();
}

void NoBooksPage::LoadState(Object^ navigationParameter, IMap<String^, Object^>^ pageState)
{
	ReaderPage::LoadState(navigationParameter, pageState);

	double progressRingTop = Window::Current->Bounds.Height/2 - OperationProgress->Height/2;
	double progressRingLeft = Window::Current->Bounds.Width/2 - OperationProgress->Width/2;
	OperationProgress->SetValue(Canvas::TopProperty, progressRingTop);
	OperationProgress->SetValue(Canvas::LeftProperty, progressRingLeft);
}


}
