/*
* @file ReaderPage.cpp
* @brief Base class for all pages in the application
*
* All pages in the application must be inherited from this class because of MVVM pattern
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ReaderPage.h"
#include "Common\SuspensionManager.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;
using namespace Windows::System;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::ApplicationSettings;
using namespace Windows::UI::Popups;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Media::Imaging;

namespace eReader
{

static TypeName pageTypeName = { eReader::ReaderPage::typeid->FullName, TypeKind::Metadata };
static TypeName objectTypeName = { "Object", TypeKind::Primitive };
static Platform::String^ viewModelStateKey = "ViewModelState";

static DependencyProperty^ _dataContextProperty =
	DependencyProperty::Register(
	"readerDataContext",
	objectTypeName, 
	pageTypeName, 
	ref new PropertyMetadata(
	nullptr, 
	ref new PropertyChangedCallback(
	&ReaderPage::OnDataContextPropertyChanged)
	));

void ReaderPage::OnDataContextPropertyChanged(DependencyObject^ element, DependencyPropertyChangedEventArgs^ e)
{
	ReaderPage^ page = static_cast<ReaderPage^>(element);
	ViewModelBase^ oldViewModel = dynamic_cast<ViewModelBase^>(e->OldValue);
	page->_detachPageHandlers(oldViewModel);

	ViewModelBase^ newViewModel = dynamic_cast<ViewModelBase^>(e->NewValue);
	page->_attachPageHandlers(newViewModel);
}

DependencyProperty^ ReaderPage::DataContextProperty::get()
{
	return _dataContextProperty;
}

ReaderPage::ReaderPage() : _hasHandlers(false)
{
	if (Windows::ApplicationModel::DesignMode::DesignModeEnabled)
	{
		return;
	}
}

void ReaderPage::suspend()
{
	// empty implementation
}

void ReaderPage::resume()
{
	// empty implementation
}

void ReaderPage::NavigateBack()
{
	GoBack(nullptr, nullptr);
}

void ReaderPage::NavigateHome()
{
	GoHome(nullptr, nullptr);
}

void ReaderPage::NavigateToPage(TypeName pageType, Platform::Object^ parameter)
{
	if (Frame != nullptr)
	{
		Frame->Navigate(pageType, parameter);
	}
}

void ReaderPage::OnNavigatedTo(NavigationEventArgs^ e)
{
	ViewModelBase^ viewModel = dynamic_cast<ViewModelBase^>(DataContext);
	this->_attachPageHandlers(viewModel);
	if (viewModel != nullptr)
	{
		viewModel->OnNavigatedTo(e);
	}

	LayoutAwarePage::OnNavigatedTo(e);
}

void ReaderPage::OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e)
{
	ViewModelBase^ viewModel = dynamic_cast<ViewModelBase^>(DataContext);
	if (!ReaderPage::IsSuspending) 
	{        
		// Since we never receive destructs, this is the only place to unsubscribe.
		_detachPageHandlers(viewModel);
		viewModel->OnNavigatedFrom(e);
	}

	LayoutAwarePage::OnNavigatedFrom(e);
}

void ReaderPage::LoadState(Object^ navigationParameter, IMap<String^, Object^>^ pageState)
{
	auto vm = dynamic_cast<ViewModelBase^>(DataContext);
	if (vm != nullptr)
	{
		IMap<String^, Object^>^ state = nullptr;
		if (pageState != nullptr)
		{
			state = dynamic_cast<IMap<String^, Object^>^>(pageState->Lookup(viewModelStateKey));
		}

		vm->LoadState(state);
	}
}

void ReaderPage::SaveState(IMap<String^, Object^>^ pageState)
{
	auto vm = dynamic_cast<ViewModelBase^>(DataContext);
	if (vm != nullptr)
	{
		auto vmStateMap = ref new Map<String^, Object^>();
		vm->SaveState(vmStateMap);

		pageState->Insert(viewModelStateKey, vmStateMap);
	}
}

void ReaderPage::_attachPageHandlers(ViewModelBase^ viewModel) 
{
	if (nullptr == viewModel) return;

	// Only re-subscribe if our tokens are empty.
	if (!_hasHandlers)
	{
		_navigateBackEventToken = viewModel->NavigateBack::add(ref new NavigateEventHandler(this, &ReaderPage::NavigateBack));
		_navigateHomeEventToken = viewModel->NavigateHome::add(ref new NavigateEventHandler(this, &ReaderPage::NavigateHome));
		_navigateToPageEventToken = viewModel->NavigateToPage::add(ref new PageNavigateEventHandler(this, &ReaderPage::NavigateToPage));
		_hasHandlers = true;
	}
}

void ReaderPage::_detachPageHandlers(ViewModelBase^ viewModel) 
{
	if (nullptr == viewModel) return;
	assert(!ReaderPage::IsSuspending);

	if (_hasHandlers)
	{
		viewModel->NavigateBack::remove(_navigateBackEventToken);
		viewModel->NavigateHome::remove(_navigateHomeEventToken);
		viewModel->NavigateToPage::remove(_navigateToPageEventToken);
		_hasHandlers = false;
	};
}

}
