﻿/*
* @file LastReadPage.xaml.h
* @brief Last page displays last opened  books. 
*
* It will be displayed last, at maximum 16, opened books. Books are sorted by last opened date.
* The date will be also displayed
*
* @date 2013-02-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\LastReadPage.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LastReadPage sealed
	{
	public:
		LastReadPage();

	protected:
		// Returns currently active list view
		// @return ListViewBase instance (ListView or GridView in our case)
		virtual Windows::UI::Xaml::Controls::ListViewBase^ _getActiveListBaseView() override;

	private:
		// Invoked when a 'simple' appbar button is clicked ('Import from File', 'Delete Book', etc.). 
		// Actual processing is done in the view model. Here only appbar is disappeared. Must be invoked,
		// if any dialog, file picker, etc. appears after invoke
		// @param sender Appbar button.
		// @param e Event data.
		void OnClickSimpleAppBarButton(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a book is selected/un-selected in the grid view. The appbar will be opened
		// @param sender Books grid view.
		// @param e Event data that describes the item clicked.
		void OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);
	};
}
