﻿/*
 * @file BookPage.xaml.h
 * @brief A page to display a book content. 
 *
 * The book content is read with Cool Reader engine and book pages are displayed vie DirectX
 *
 * @date 2012-12-28
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#pragma once

#include "View\BookViewerControl.xaml.h"
#include "View\BookPage.g.h"

namespace eReader
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class BookPage sealed
	{
	public:
		BookPage();

	internal:
		// Invoked by resuming the application. In this case it is necessary
		// to update current drawing buffer for the book display
		virtual void resume() override;

	protected:
		// Populates the page with content passed during navigation. Any saved state is also
		// provided when recreating a page from a prior session.
		// @param navigationParameter The parameter value passed to 'Frame::Navigate(Type, Object)' when this page was initially requested.
		// @param pageState A map of state preserved by this page during an earlier session. This will be null the first time a page is visited.
		virtual void LoadState(
			Platform::Object^ navigationParameter,
			Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ pageState) override;

		// Invoked when the user leaves a page. Here necessary unregister action can be executed
		// @param e Navigation event data
		virtual void OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

	private:
		// Invoked when a popup menu from an appbar button is opened
		// @param sender Popup menu
		// @param e Event data
		void OnPopupOpened(Platform::Object^ sender, Platform::Object^ e);

		// Invoked when a "Font Face" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Font Face" appbar button
		// @param e Event data
		void OnFontFaceClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a menu item from a "Font Face" popup menu has been selected
		// @param sender "Font Face" appbar button
		// @param e Event data
		void OnFontFaceSelected(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a "Font Size" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Font Size" appbar button
		// @param e Event data
		void OnFontSizeClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a menu item from a "Font Size" popup menu has been selected
		// @param sender "Font Size" appbar button
		// @param e Event data
		void OnFontSizeSelected(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a "Colors" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Colors" appbar button
		// @param e Event data
		void OnColorsClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a "Search" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Search" appbar button
		// @param e Event data
		void OnSearchClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Create a grid for the color selection appbar menu item
		// @param panel Grid that will be defined
		// @param noOfRows Number of rows of the grid
		// @param noOfColumns Number of columns of the grid
		void _createColorsGrid(_In_ _Out_ Windows::UI::Xaml::Controls::Grid^ panel, _In_ const int noOfRows, _In_ const int noOfColumns);

		// Invoked when a menu item from a "Colors" popup menu has been selected
		// @param sender "Colors" appbar button
		// @param e Event data
		void OnColorsSelected(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a size of the windows has been changed
		// @param sender Main canvas
		// @param e Event data
		void OnBookPageSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);

		// Invoked when a top appbar size is changed. The size of the book progress slider will be adjusted properly
		// @param sender Top appbar
		// @param e Event data that describes
		void OnTopAppBarSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);

		// Invoked when a "Line Spacing" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Line Spacing" appbar button
		// @param e Event data
		void OnLineSpacingClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a "Page Margins" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Page Margins" appbar button
		// @param e Event data
		void OnPageMarginsClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		
		// Invoked when a "Table of Content" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Table of Content" appbar button
		// @param e Event data
		void OnTableOfContentClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a TOC item has been selected
		// @param sender TOC list view panel
		// @param e Event data. TOCItem as e->ClickedItem is expected
		void OnTOCItemSelected(Platform::Object^ sender, Windows::UI::Xaml::Controls::ItemClickEventArgs^ e);

		// Calculates a TOC menu position on the screen
		void _calculateTOCMenuPosition();

		// Invoked when a "Bookmark Table" appbar button has been clicked. As a reaction, a popup menu will be displayed
		// @param sender "Bookmark Table" appbar button
		// @param e Event data
		void OnBookmarkTableClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a bookmark has been selected
		// @param sender bookmark list view panel
		// @param e Event data. bookmark as e->ClickedItem is expected
		void OnBookmarkSelected(Platform::Object^ sender, Windows::UI::Xaml::Controls::ItemClickEventArgs^ e);

		// Calculates a bookmark menu position on the screen
		void _calculateBookmarkMenuPosition();

		// Invoked when a page selection in the control has been changed
		// @param selectedPage New selected page
		void OnBookViewerPageSelectionChanged(default::int32 selectedPage);

		// Invoked when a page selection in the control has been restored
		// @param selectedPage Selected page
		void OnBookViewerPageSelectionRestored(default::int32 selectedPage);

		// Invoked when a user press a key. Depending on the key scroll the page up and down
		// @param sender Core window
		// @param e Event data
		void OnCoreWindowKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e);

		// Invoked when a search popup menu is closed
		// @param sender Popup menu
		// @param e Event data
		void OnSearchPopupClosed(Platform::Object^ sender, Platform::Object^ e);

		// FUTURE IMPLEMENTATION: highlight/add/remove notes
		//-------------------------------------------------------------
		// Invoked when the "pointer pressed" action has been initiated (mouse click, tap, etc.)
		// @param sender Scroll view contained a page presentation
		// @param e Event data
		//void OnPagePointerPressed(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);

		// Invoked when the "pointer released" action has been initiated (mouse click, tap, etc.)
		// @param sender Scroll view contained a page presentation
		// @param e Event data
		//void OnPagePointerReleased(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);

		// Invoked when the "pointer moved" action has been initiated (mouse move, tap, etc.)
		// @param sender Scroll view contained a page presentation
		// @param e Event data
		//void OnPagePointerMoved(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e);
		//-------------------------------------------------------------

	private:
		Windows::UI::Xaml::Controls::Primitives::Popup^ _selectFontPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _selectFontSizePopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _selectColorsPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _lineSpacingPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _pageMarginsPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _TOCPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _bookmarkTablePopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _searchPopup;
		Windows::Foundation::EventRegistrationToken _selectFontPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _selectColorsPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _pageMarginsPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _lineSpacingPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _keyDownToken;
		Windows::Foundation::EventRegistrationToken _searchPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _searchPopupClosedToken;
};
}
