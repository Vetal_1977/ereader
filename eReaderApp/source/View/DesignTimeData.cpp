﻿/*
* @file DesignTimeData.cpp
* @brief Data that are used during design time
*
* @date 2012-12-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DesignTimeData.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Media::Imaging;

namespace eReader
{

#pragma region DesignTimeBookData Members

String^ DesignTimeBookData::Authors::get()
{
	 return _authors;
}

void DesignTimeBookData::Authors::set(Platform::String^ value)
{
	if (_authors != value)
	{
		_authors = value;
	}
}

String^ DesignTimeBookData::Title::get()
{
	 return _title;
}

void DesignTimeBookData::Title::set(Platform::String^ value)
{
	if (_title != value)
	{
		_title = value;
	}
}

#pragma endregion

#pragma region DesignTimeBookCollection Members

DesignTimeBookCollection::DesignTimeBookCollection(String^ collectionName) :
	_collectionName(collectionName)
{
	_books = ref new Vector<DesignTimeBookData^>();
}

String^ DesignTimeBookCollection::Name::get()
{
	return _collectionName;
}

IObservableVector<DesignTimeBookData^>^ DesignTimeBookCollection::Books::get()
{
	return _books;
}

#pragma endregion

#pragma region DesignTimeData Members

DesignTimeData::DesignTimeData()
{
}

IObservableVector<DesignTimeBookCollection^>^ DesignTimeData::SpecialCollections::get()
{
	String^ bookCollectionsStr = ref new String(L"Design time last read");

	DesignTimeBookData^ book1 = ref new DesignTimeBookData();
	book1->Authors = ref new String(L"J.F. Kennedy");
	book1->Title = ref new String(L"Murder");

	DesignTimeBookData^ book2 = ref new DesignTimeBookData();
	book2->Authors = ref new String(L"Jane Fonda");
	book2->Title = ref new String(L"Music");

	DesignTimeBookData^ book3 = ref new DesignTimeBookData();
	book3->Authors = ref new String(L"Leslie Nilsen");
	book3->Title = ref new String(L"2 1/2 Cops");

	DesignTimeBookCollection^ lastReadBooks = ref new DesignTimeBookCollection(ref new String(L"Design time last read"));
	lastReadBooks->Books->Append(book1);
	lastReadBooks->Books->Append(book2);

	DesignTimeBookCollection^ lastAddedBooks = ref new DesignTimeBookCollection(ref new String(L"Design time library"));
	lastAddedBooks->Books->Append(book3);

	Vector<DesignTimeBookCollection^>^ specialCollections = ref new Vector<DesignTimeBookCollection^>();
	specialCollections->Append(lastReadBooks);
	specialCollections->Append(lastAddedBooks);

	return specialCollections;
}

#pragma endregion

}
