﻿
/*
 * @file BookPage.xaml.cpp
 * @brief A page to display a book content. 
 *
 * The book content is read with Cool Reader engine and book pages are displayed vie DirectX
 *
 * @date 2012-12-28
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#include "pch.h"
#include "BookPage.xaml.h"
#include "SearchControl.xaml.h"
#include "Constants.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::ViewManagement;
using namespace Windows::ApplicationModel;
using namespace Windows::Storage::Pickers;
using namespace concurrency;
using namespace Windows::Storage;
using namespace Windows::UI::Core;
using namespace eReader::Utils;

#define MAX_POPUP_MENU_HEIGHT 300 // pixels
#define MIN_POPUP_MENU_HEIGHT 200 // pixels
#define APP_BAR_MENU_VERTICAL_OFFSET 4 // pixels
#define MIN_LINE_SPACING 50 // %
#define MAX_LINE_SPACING 200 // %
#define MIN_PAGE_MARGIN 0 // pix
#define MAX_PAGE_MARGIN 70 // pix

namespace eReader
{

BookPage::BookPage() :
	_selectFontPopup(nullptr),
	_selectFontSizePopup(nullptr)
{
	InitializeComponent();

	TopAppBar->Loaded += ref new RoutedEventHandler(this, &LayoutAwarePage::OnAppBarLoaded);
	TopAppBar->Unloaded += ref new RoutedEventHandler(this, &LayoutAwarePage::OnAppBarUnloaded);

	BottomAppBar->Loaded += ref new RoutedEventHandler(this, &LayoutAwarePage::OnAppBarLoaded);
	BottomAppBar->Unloaded += ref new RoutedEventHandler(this, &LayoutAwarePage::OnAppBarUnloaded);
}

void BookPage::resume()
{
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->redrawBook();
}

void BookPage::LoadState(Object^ navigationParameter, IMap<String^, Object^>^ pageState)
{
	ReaderPage::LoadState(navigationParameter, pageState);

	BookViewer->PageSize = Window::Current->Bounds;

	double progressRingTop = Window::Current->Bounds.Height/2 - LoadingBookProgress->Height/2;
	double progressRingLeft = Window::Current->Bounds.Width/2 - LoadingBookProgress->Width/2;
	LoadingBookProgress->SetValue(Canvas::TopProperty, progressRingTop);
	LoadingBookProgress->SetValue(Canvas::LeftProperty, progressRingLeft);

	_keyDownToken = Window::Current->CoreWindow->KeyDown += 
		ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &BookPage::OnCoreWindowKeyDown);
}

void BookPage::OnNavigatedFrom(NavigationEventArgs^ e)
{
	if (_selectFontPopup != nullptr)
		_selectFontPopup->Opened -= _selectFontPopupOpenedToken;
	
	if (_selectColorsPopup != nullptr)
		_selectColorsPopup->Opened -= _selectColorsPopupOpenedToken;

	if (_pageMarginsPopup != nullptr)
		_pageMarginsPopup->Opened -= _pageMarginsPopupOpenedToken;

	if (_lineSpacingPopup != nullptr)
		_lineSpacingPopup->Opened -= _lineSpacingPopupOpenedToken;

	if (_searchPopup != nullptr)
	{
		_searchPopup->Opened -= _searchPopupOpenedToken;
		_searchPopup->Closed -= _searchPopupClosedToken;
	}

	Window::Current->CoreWindow->KeyDown -= _keyDownToken;

	ReaderPage::OnNavigatedFrom(e);
}

void BookPage::OnFontFaceClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	StackPanel^ panel = nullptr;
	if (_selectFontPopup == nullptr)
	{
		_selectFontPopup = ref new Popup();
		_selectFontPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

		// insert stack panel into the menu
		_selectFontPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the menu
		_selectFontPopupOpenedToken = _selectFontPopup->Opened += ref new EventHandler<Object^>(this, &BookPage::OnPopupOpened);
	}
	else
	{
		panel = safe_cast<StackPanel^>(_selectFontPopup->Child);
	}
	panel->Children->Clear();
		
	// enumerate reader fonts and insert them into the stack panel
	static const int horButtonMargin = 10;
	static const int verButtonMargin = 5;
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	IVector<String^>^ readerFonts = vm->getReaderFonts();
	String^ currentFontName = vm->getCurrentFont();
	for each(String^ fontName in readerFonts)
	{
		auto fontButton = ref new RadioButton();
		fontButton->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("MenuRadioButtonStyle"));
		fontButton->Content = fontName;
		fontButton->Margin = *(new Thickness(horButtonMargin, verButtonMargin, horButtonMargin, verButtonMargin));
		fontButton->Command = vm->SetFontCommand; // to set the font in the view model
		fontButton->CommandParameter = fontName;
		fontButton->Click += ref new RoutedEventHandler(this, &BookPage::OnFontFaceSelected); // to close the popup menus
		if (String::CompareOrdinal((fontName), currentFontName) == 0)
		{
			fontButton->IsChecked = true;
		}
					
		panel->Children->Append(fontButton);
	}

	// The menu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the menu
	// Get the screen position of the button
	GeneralTransform^ transform = FontFaceBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_selectFontPopup->HorizontalOffset = rootPoint.X;
	_selectFontPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight; 
	
	// show menu
	_selectFontPopup->IsOpen = true;
}

void BookPage::OnPopupOpened(Object^ sender, Object^ e)
{
	auto openedPopup = safe_cast<Popup^>(sender);
	auto childPanel = safe_cast<Panel^>(openedPopup->Child);

	// actual height of the stack panel is known only upon opening of the parent popup
	openedPopup->VerticalOffset -= (childPanel->ActualHeight + APP_BAR_MENU_VERTICAL_OFFSET);
}

void BookPage::OnFontFaceSelected(Object^ sender, RoutedEventArgs^ e)
{
	_selectFontPopup->IsOpen = false;
	TopAppBar->IsOpen = false;
	BottomAppBar->IsOpen = false;
	BookViewer->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void BookPage::OnFontSizeClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	StackPanel^ panel = nullptr;
	ScrollViewer^ scrollViewer = nullptr;
	if (_selectFontSizePopup == nullptr)
	{
		_selectFontSizePopup = ref new Popup();
		_selectFontSizePopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

		scrollViewer = ref new ScrollViewer();
		scrollViewer->Content = panel;
		scrollViewer->Height = MAX_POPUP_MENU_HEIGHT;

		// insert stack panel into the menu
		_selectFontSizePopup->Child = scrollViewer;
	}
	else
	{
		scrollViewer = safe_cast<ScrollViewer^>(_selectFontSizePopup->Child);
		panel = safe_cast<StackPanel^>(scrollViewer->Content);
	}
	panel->Children->Clear();

	// enumerate reader font sizes and insert them into the stack panel
	static const int horButtonMargin = 10;
	static const int verButtonMargin = 5;
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	int noOfSizes = 0;
	String^ currentFontName = vm->getCurrentFont();
	int32 currentFontSize = vm->getCurrentFontSize();
	auto content = ResLoader::getString("MenuItemABC");
	int* fontSizes = nullptr;

	vm->getReaderFontSizes(&fontSizes, noOfSizes);

	for (int sizeIdx = 0; sizeIdx < noOfSizes; sizeIdx++)
	{
		auto sizeButton = ref new RadioButton();
		sizeButton->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("MenuRadioButtonStyle"));
		sizeButton->Content = content;
		sizeButton->Margin = *(new Thickness(horButtonMargin, verButtonMargin, horButtonMargin, verButtonMargin));
		sizeButton->Command = vm->SetFontSizeCommand; // to set the font size in the view model
		sizeButton->CommandParameter = fontSizes[sizeIdx];
		sizeButton->Click += ref new RoutedEventHandler(this, &BookPage::OnFontSizeSelected); // to close the popup menus
		if (fontSizes[sizeIdx] == currentFontSize)
		{
			sizeButton->IsChecked = true;
		}
		sizeButton->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily(currentFontName);
		sizeButton->FontSize = fontSizes[sizeIdx];
					
		panel->Children->Append(sizeButton);
	}

	// calculate the placement of the menu
	// Get the screen position of the button
	GeneralTransform^ transform = FontSizeBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_selectFontSizePopup->HorizontalOffset = rootPoint.X;
	_selectFontSizePopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight -
		scrollViewer->Height - 4;

	// show menu
	_selectFontSizePopup->IsOpen = true;
}

void BookPage::OnFontSizeSelected(Object^ sender, RoutedEventArgs^ e)
{
	_selectFontSizePopup->IsOpen = false;
	TopAppBar->IsOpen = false;
	BottomAppBar->IsOpen = false;
	BookViewer->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void BookPage::OnColorsClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	if (_selectColorsPopup == nullptr)
	{
		_selectColorsPopup = ref new Popup();
		_selectColorsPopup->IsLightDismissEnabled = true;
		
		// create stack panel to put command buttons
		auto panel = ref new StackPanel();
		panel->Background = safe_cast<Windows::UI::Xaml::Media::Brush^>(App::Current->Resources->Lookup("readerPopupMenuBackgroundBrush"));
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

		// textures caption
		auto texturesCaption = ref new TextBlock();
		texturesCaption->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("BasicTextStyle"));
		texturesCaption->Text = Utils::ResLoader::getString("TexturesCaption");
		texturesCaption->FontSize = 20;
		texturesCaption->Margin = Thickness(10, 10, 5, 5);
		
		// backgrounds caption
		auto backgroundsCaption = ref new TextBlock();
		backgroundsCaption->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("BasicTextStyle"));
		backgroundsCaption->Text = Utils::ResLoader::getString("BackgroundsCaption");
		backgroundsCaption->FontSize = 20;
		backgroundsCaption->Margin = Thickness(10, 10, 5, 5);

		// textures grid
		auto texturesGrid = ref new Grid();
		texturesGrid->UseLayoutRounding = true;
		texturesGrid->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

		// backgrounds grid
		auto backgroundsGrid = ref new Grid();
		backgroundsGrid->UseLayoutRounding = true;
		backgroundsGrid->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// enumerate reader fonts and insert them into the grid
		static const int horButtonMargin = 10;
		static const int verButtonMargin = 5;
		auto vm = dynamic_cast<BookViewModel^>(DataContext);
		IVector<ReaderColor^>^ readerColors = vm->getReaderColors();
		auto content = ResLoader::getString("MenuItemABC");

		// calculate grid size and create/update it
		static const int noOfGridColumns = 5; // constant
		static const int noOfGridRows = readerColors->Size / noOfGridColumns + (readerColors->Size % noOfGridColumns > 0 ? 1 : 0);
		_createColorsGrid(texturesGrid, noOfGridRows, noOfGridColumns);
		_createColorsGrid(backgroundsGrid, noOfGridRows, noOfGridColumns);

		int textureGridRow = 0;
		int textureGridColumn = 0;
		int backgroundGridRow = 0;
		int backgroundGridColumn = 0;
		for (unsigned int colorIdx = 0; colorIdx < readerColors->Size; colorIdx++)
		{
			Brush^ backgroundBrush = nullptr;
			auto readerColor = readerColors->GetAt(colorIdx);
			if (readerColor->ColorCombination == ColorCombinationType::Texture_Foreground)
			{
				ImageBrush^ backgroundImage = ref new ImageBrush();
				Uri^ bitmapUri = ref new Uri(readerColor->TextureFilename);
				backgroundImage->ImageSource = ref new Windows::UI::Xaml::Media::Imaging::BitmapImage(bitmapUri);
				backgroundBrush = backgroundImage;
			}
			else
			{
				backgroundBrush = ref new SolidColorBrush(readerColor->BackgroundColor);
			}

			auto colorButton = ref new RadioButton();
			colorButton->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("MenuRadioButtonStyleRectSel"));
			colorButton->Content = content;
			colorButton->Margin = *(new Thickness(horButtonMargin, verButtonMargin, horButtonMargin, verButtonMargin));
			colorButton->Command = vm->SetColorsCommand; // to set colors in the view model
			colorButton->CommandParameter = colorIdx;
			colorButton->Click += ref new RoutedEventHandler(this, &BookPage::OnColorsSelected); // to close the popup menus
			colorButton->Background = backgroundBrush;
			colorButton->Foreground = ref new SolidColorBrush(readerColor->FontColor);
			colorButton->HorizontalContentAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
			colorButton->VerticalContentAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
			colorButton->Width = 80;
			colorButton->Height = 60;

			if (readerColor->ColorCombination == ColorCombinationType::Texture_Foreground)
			{
				texturesGrid->Children->Append(colorButton);
				texturesGrid->SetRow(colorButton, textureGridRow);
				texturesGrid->SetColumn(colorButton, textureGridColumn);

				// increase column index or go to the first column of the next row
				if (++textureGridColumn >= noOfGridColumns)
				{
					++textureGridRow;
					textureGridColumn = 0;
				}
			}
			else
			{
				backgroundsGrid->Children->Append(colorButton);
				backgroundsGrid->SetRow(colorButton, backgroundGridRow);
				backgroundsGrid->SetColumn(colorButton, backgroundGridColumn);

				// increase column index or go to the first column of the next row
				if (++backgroundGridColumn >= noOfGridColumns)
				{
					++backgroundGridRow;
					backgroundGridColumn = 0;
				}
			}
		}

		// insert stack panel into the menu
		panel->Children->Append(texturesCaption);
		panel->Children->Append(texturesGrid);
		panel->Children->Append(backgroundsCaption);
		panel->Children->Append(backgroundsGrid);
		_selectColorsPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the menu
		_selectColorsPopupOpenedToken = _selectColorsPopup->Opened += ref new EventHandler<Object^>(this, &BookPage::OnPopupOpened); 
	}
	
	// The menu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the menu
	// Get the screen position of the button
	GeneralTransform^ transform = ColorsBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_selectColorsPopup->HorizontalOffset = rootPoint.X;
	_selectColorsPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;

	// show menu
	_selectColorsPopup->IsOpen = true;
}

void BookPage::OnSearchClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup sortByMenu
	if (_searchPopup == nullptr)
	{
		_searchPopup = ref new Popup();
		_searchPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		StackPanel^ panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// margins
		static const int horMargin = 10;
		static const int verMargin = 5;

		// insert controls into the stack panel
		auto searchControl = ref new SearchControl();
		searchControl->DataContext = DataContext;
		panel->Children->Append(searchControl);

		// insert stack panel into the sortByMenu
		_searchPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the sortByMenu
		_searchPopupOpenedToken = _searchPopup->Opened += ref new EventHandler<Object^>(this, &BookPage::OnPopupOpened); 
		_searchPopupClosedToken = _searchPopup->Closed += ref new EventHandler<Object^>(this, &BookPage::OnSearchPopupClosed); 
	}
	else
	{
		auto stackPanel = safe_cast<StackPanel^>(_searchPopup->Child);
		auto addBookToCollectionControl = safe_cast<SearchControl^>(stackPanel->Children->GetAt(0));
	}

	// The sortByMenu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the sortByMenu
	// Get the screen position of the button
	GeneralTransform^ transform = SearchBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_searchPopup->HorizontalOffset = rootPoint.X;
	_searchPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;
	
	// show sortByMenu
	_searchPopup->IsOpen = true;
}

void BookPage::_createColorsGrid(Windows::UI::Xaml::Controls::Grid^ panel, const int noOfRows, const int noOfColumns)
{
	for (int row = 0; row < noOfRows; row++)
	{
		auto rowDef = ref new RowDefinition();
		panel->RowDefinitions->Append(rowDef);
	}

	for (int column = 0; column < noOfColumns; column++)
	{
		auto columnDef = ref new ColumnDefinition();
		panel->ColumnDefinitions->Append(columnDef);
	}
}

void BookPage::OnColorsSelected(Object^ sender, RoutedEventArgs^ e)
{
	_selectColorsPopup->IsOpen = false;
	TopAppBar->IsOpen = false;
	BottomAppBar->IsOpen = false;
	BookViewer->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void BookPage::OnBookPageSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e)
{
	// Book view must be updated only if the book is already loaded
	// The loading progress ring activation is used here to determine whether the book is loaded
	if (!LoadingBookProgress->IsActive)
	{
		auto vm = dynamic_cast<BookViewModel^>(DataContext);
		SIZE viewSize = {(LONG)e->NewSize.Width, (LONG)e->NewSize.Height};

		if (e->NewSize.Width <= Constants::SNAPPED_WINDOW_MAX_WIDTH || 
			(e->NewSize.Width <= Constants::FULLSCREEN_PORTRAIT_MAX_WIDTH && e->NewSize.Width < e->NewSize.Height))
		{
			vm->updateBookView(viewSize, 1);
		}
		else
		{
			vm->updateBookView(viewSize, 2);
		}
	}

	BookViewer->PageSize = Windows::Foundation::Rect(0.0f, 0.0f, 
		static_cast<float>(RootGrid->ActualWidth), static_cast<float>(RootGrid->ActualHeight));
}

void BookPage::OnTopAppBarSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e)
{
	// progress slider is in the fourth grid column
	double col1Width = TopAppBarElementGrid->ColumnDefinitions->GetAt(0)->ActualWidth;
	double col2Width = TopAppBarElementGrid->ColumnDefinitions->GetAt(1)->ActualWidth;
	double col3Width = TopAppBarElementGrid->ColumnDefinitions->GetAt(2)->ActualWidth;
	double bookProgressSliderWidth = TopAppBar->ActualWidth - col1Width - col2Width - col3Width -
		BookProgressSlider->Margin.Left - BookProgressSlider->Margin.Right - 
		TopAppBar->Padding.Left - TopAppBar->Padding.Right;
	if (bookProgressSliderWidth < BookProgressSlider->MinWidth)
	{
		bookProgressSliderWidth = BookProgressSlider->MinWidth;
		BookProgressSlider->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	}
	else
	{
		BookProgressSlider->Visibility = Windows::UI::Xaml::Visibility::Visible;
	}
	BookProgressSlider->Width = bookProgressSliderWidth;
	TopAppBarElementGrid->Height = TopAppBar->ActualHeight;
}

void BookPage::OnLineSpacingClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	if (_lineSpacingPopup == nullptr)
	{
		_lineSpacingPopup = ref new Popup();
		_lineSpacingPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		auto panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// add a slider to set up line spacing and set a current value into it
		static const int horMargin = 10;
		static const int verMargin = 5;
		
		auto lineSpacingSlider = ref new Slider();
		lineSpacingSlider->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("readerAppbarSliderStyle"));
		lineSpacingSlider->Margin = *(new Thickness(horMargin, verMargin, horMargin, verMargin));
		lineSpacingSlider->HorizontalContentAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
		lineSpacingSlider->VerticalContentAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		lineSpacingSlider->Height = MIN_POPUP_MENU_HEIGHT;
		lineSpacingSlider->Minimum = MIN_LINE_SPACING;
		lineSpacingSlider->Maximum = MAX_LINE_SPACING;
		lineSpacingSlider->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Stretch;
		lineSpacingSlider->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Stretch;
		lineSpacingSlider->Orientation = Orientation::Vertical;
		lineSpacingSlider->IsThumbToolTipEnabled = false;

		auto binding = ref new Binding();
		binding->Source = DataContext;
		binding->Mode = Data::BindingMode::TwoWay;
		binding->Path = ref new PropertyPath(L"LineSpacing");
		lineSpacingSlider->SetBinding(lineSpacingSlider->ValueProperty, binding);

		panel->Children->Append(lineSpacingSlider);

		// insert stack panel into the menu
		_lineSpacingPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the menu
		_lineSpacingPopupOpenedToken = _lineSpacingPopup->Opened += ref new EventHandler<Object^>(this, &BookPage::OnPopupOpened); 
	}

	// The menu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the menu
	// Get the screen position of the button
	GeneralTransform^ transform = LineSpacingBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_lineSpacingPopup->HorizontalOffset = rootPoint.X;
	_lineSpacingPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;

	// show menu
	_lineSpacingPopup->IsOpen = true;
}

void BookPage::OnPageMarginsClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	if (_pageMarginsPopup == nullptr)
	{
		_pageMarginsPopup = ref new Popup();
		_pageMarginsPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		auto panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// add a slider to set up line spacing and set a current value into it
		static const int horMargin = 10;
		static const int verMargin = 5;
		
		auto pageMarginsSlider = ref new Slider();
		pageMarginsSlider->Style = safe_cast<Windows::UI::Xaml::Style^>(App::Current->Resources->Lookup("readerAppbarSliderStyle"));
		pageMarginsSlider->Margin = *(new Thickness(horMargin, verMargin, horMargin, verMargin));
		pageMarginsSlider->HorizontalContentAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
		pageMarginsSlider->VerticalContentAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		pageMarginsSlider->Height = MIN_POPUP_MENU_HEIGHT;
		pageMarginsSlider->Minimum = MIN_PAGE_MARGIN;
		pageMarginsSlider->Maximum = MAX_PAGE_MARGIN;
		pageMarginsSlider->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Stretch;
		pageMarginsSlider->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Stretch;
		pageMarginsSlider->Orientation = Orientation::Vertical;
		pageMarginsSlider->IsThumbToolTipEnabled = false;

		auto binding = ref new Binding();
		binding->Source = DataContext;
		binding->Mode = Data::BindingMode::TwoWay;
		binding->Path = ref new PropertyPath(L"PageMargins");
		pageMarginsSlider->SetBinding(pageMarginsSlider->ValueProperty, binding);

		panel->Children->Append(pageMarginsSlider);

		// insert stack panel into the menu
		_pageMarginsPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the menu
		_pageMarginsPopupOpenedToken = _pageMarginsPopup->Opened += ref new EventHandler<Object^>(this, &BookPage::OnPopupOpened); 
	}
	
	// The menu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the menu
	// Get the screen position of the button
	GeneralTransform^ transform = PageMarginsBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_pageMarginsPopup->HorizontalOffset = rootPoint.X;
	_pageMarginsPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;

	// show menu
	_pageMarginsPopup->IsOpen = true;
}

void BookPage::OnTableOfContentClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	if (_TOCPopup == nullptr)
	{
		_TOCPopup = ref new Popup();
		_TOCPopup->IsLightDismissEnabled = true;

		// create a list view to show a TOC
		// each stack panel item is a button consisting of 2 parts: 
		// - name of the item shifted right corresponding to the item level
		// - page number. All page number are aligned independent from the item level

		auto panel = ref new ListView();
		panel->Background = safe_cast<Windows::UI::Xaml::Media::Brush^>(App::Current->Resources->Lookup("readerPopupMenuBackgroundBrush"));
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		panel->ItemTemplate = safe_cast<Windows::UI::Xaml::DataTemplate^>(App::Current->Resources->Lookup("TOCItemTemplate"));
		panel->SelectionMode = ListViewSelectionMode::Single;
		panel->IsItemClickEnabled = true;
		panel->ItemClick += ref new ItemClickEventHandler(this, &BookPage::OnTOCItemSelected);

		auto binding = ref new Binding();
		binding->Source = DataContext;
		binding->Mode = Data::BindingMode::OneWay;
		binding->Path = ref new PropertyPath(L"TOCItems");
		panel->SetBinding(panel->ItemsSourceProperty, binding);
		
		// insert stack panel into the menu
		_TOCPopup->Child = panel;

		_calculateTOCMenuPosition();
	}
	else
	{
		auto panel = safe_cast<ListView^>(_TOCPopup->Child);
		panel->SelectedIndex = -1;

		_calculateTOCMenuPosition();
	}
	
	// show menu
	_TOCPopup->IsOpen = true;
}

void BookPage::OnTOCItemSelected(Platform::Object^ sender, Windows::UI::Xaml::Controls::ItemClickEventArgs^ e)
{
	assert(_TOCPopup != nullptr);
	assert(_TOCPopup->IsOpen);
	_TOCPopup->IsOpen = false;
	TopAppBar->IsOpen = false;
	BottomAppBar->IsOpen = false;

	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->selectTOCItem(e->ClickedItem);
}

void BookPage::_calculateTOCMenuPosition()
{
	// calculate list view width and height
	auto panel = safe_cast<ListView^>(_TOCPopup->Child);
	panel->MaxHeight = Window::Current->Bounds.Height - TopAppBar->Height - BottomAppBar->Height - APP_BAR_MENU_VERTICAL_OFFSET * 2;

	// calculate the placement of the menu
	// Get the screen position of the button	
	GeneralTransform^ transform = TableOfContentBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	const int TOCMenuMaxWidth = ViewModel::TableOfContent::getTOCMenuMaxWidth();
	if (rootPoint.X + TOCMenuMaxWidth > Window::Current->Bounds.Right)
	{
		rootPoint.X = 0;
	}

	// Now get the point of the button in the screen
	_TOCPopup->HorizontalOffset = rootPoint.X;
	_TOCPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Top +
		TopAppBar->ActualHeight;
}

void BookPage::OnBookmarkTableClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup menu
	// for a bookmark table TOC styles are used
	if (_bookmarkTablePopup == nullptr)
	{
		_bookmarkTablePopup = ref new Popup();
		_bookmarkTablePopup->IsLightDismissEnabled = true;

		// create a list view to show a table
		// each stack panel item is a button consisting of 2 parts: 
		// - name of the item shifted right corresponding to the item level
		// - page number. All page number are aligned independent from the item level

		auto panel = ref new ListView();
		panel->Background = safe_cast<Windows::UI::Xaml::Media::Brush^>(App::Current->Resources->Lookup("readerPopupMenuBackgroundBrush"));
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		panel->ItemTemplate = safe_cast<Windows::UI::Xaml::DataTemplate^>(App::Current->Resources->Lookup("TOCItemTemplate"));
		panel->SelectionMode = ListViewSelectionMode::Single;
		panel->IsItemClickEnabled = true;
		panel->ItemClick += ref new ItemClickEventHandler(this, &BookPage::OnBookmarkSelected);

		auto binding = ref new Binding();
		binding->Source = DataContext;
		binding->Mode = Data::BindingMode::OneWay;
		binding->Path = ref new PropertyPath(L"Bookmarks");
		panel->SetBinding(panel->ItemsSourceProperty, binding);
		
		// insert stack panel into the menu
		_bookmarkTablePopup->Child = panel;

		_calculateBookmarkMenuPosition();
	}
	else
	{
		auto panel = safe_cast<ListView^>(_bookmarkTablePopup->Child);
		panel->SelectedIndex = -1;

		_calculateBookmarkMenuPosition();
	}
	
	// show menu
	_bookmarkTablePopup->IsOpen = true;
}

void BookPage::OnBookmarkSelected(Platform::Object^ sender, Windows::UI::Xaml::Controls::ItemClickEventArgs^ e)
{
	assert(_bookmarkTablePopup != nullptr);
	assert(_bookmarkTablePopup->IsOpen);
	_bookmarkTablePopup->IsOpen = false;
	TopAppBar->IsOpen = false;
	BottomAppBar->IsOpen = false;

	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->selectBookmark(e->ClickedItem);
}

void BookPage::_calculateBookmarkMenuPosition()
{
	// calculate list view width and height
	auto panel = safe_cast<ListView^>(_bookmarkTablePopup->Child);
	panel->MaxHeight = Window::Current->Bounds.Height - TopAppBar->Height - BottomAppBar->Height - APP_BAR_MENU_VERTICAL_OFFSET * 2;

	// calculate the placement of the menu
	// Get the screen position of the button	
	GeneralTransform^ transform = BookmarkTableBn->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	const int bookmarkMenuMaxWidth = ViewModel::BookmarkTable::getBookmarkMenuMaxWidth();
	if (rootPoint.X + bookmarkMenuMaxWidth > Window::Current->Bounds.Right)
	{
		rootPoint.X = 0;
	}

	// Now get the point of the button in the screen
	_bookmarkTablePopup->HorizontalOffset = rootPoint.X;
	_bookmarkTablePopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Top +
		TopAppBar->ActualHeight;
}

void BookPage::OnBookViewerPageSelectionChanged(default::int32 selectedPage)
{
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->OnPageSelectionChanged(selectedPage);
}

void BookPage::OnBookViewerPageSelectionRestored(default::int32 selectedPage)
{
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->OnPageSelectionRestored(selectedPage);
}

void BookPage::OnCoreWindowKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e)
{
	switch (e->VirtualKey)
	{
	case Windows::System::VirtualKey::Home:
		{
			auto vm = dynamic_cast<BookViewModel^>(DataContext);
			vm->goToPage(1);
			break;
		}

	case Windows::System::VirtualKey::End:
		{
			auto vm = dynamic_cast<BookViewModel^>(DataContext);
			vm->goToPage(static_cast<int>(vm->BookPageCount));
			break;
		}

	default:
		break;
	}
}

void BookPage::OnSearchPopupClosed(Object^ sender, Object^ e)
{
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	vm->resetSearch();
}

// FUTURE IMPLEMENTATION: highlight/add/remove notes
//-------------------------------------------------------------
//void BookPage::OnPagePointerPressed(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
//{
//}
//
//void BookPage::OnPagePointerReleased(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
//{
//
//}
//
//void BookPage::OnPagePointerMoved(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
//{
//
//}

}
