﻿/*
* @file StartPage.xaml.h
* @brief Start page is an overview page of the reader. 
*
* The overview contains quick links to last read books and last put book into the bookshelf
*
* @date 2012-12-23
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\StartPage.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class StartPage sealed
	{
	public:
		StartPage();

	protected:
		// Invoked when a group header (last read or library) is clicked.
		// @param sender The Button used as a group header for the selected group.
		// @param e Event data that describes how the click was initiated.
		void OnHeaderClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		
		// Returns currently active list view
		// @return ListViewBase instance (ListView or GridView in our case)
		virtual Windows::UI::Xaml::Controls::ListViewBase^ _getActiveListBaseView() override;
		
	private:
		// Invoked when a book is selected/un-selected in the grid view. The appbar will be opened
		// @param sender Books grid view.
		// @param e Event data that describes the item clicked.
		void OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);

		// Invoked when a 'simple' appbar button is clicked ('Import from File', 'Delete Book', etc.). 
		// Actual processing is done in the view model. Here only appbar is disappeared. Must be invoked,
		// if any dialog, file picker, etc. appears after invoke
		// @param sender Appbar button.
		// @param e Event data.
		void OnClickSimpleAppBarButton(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);
	};
}
