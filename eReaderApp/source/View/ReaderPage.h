﻿/*
* @file ReaderPage.h
* @brief Base class for all pages in the application
*
* All pages in the application must be inherited from this class because of MVVM pattern
*
* @date 2012-12-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once
#include "Common\LayoutAwarePage.h"
#include "ViewModel\ViewModelBase.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ReaderPage : Common::LayoutAwarePage
	{
	internal:
		ReaderPage();

	public:
		static property Windows::UI::Xaml::DependencyProperty^ DataContextProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		}

	internal:
		virtual void suspend();
		virtual void resume();

	protected:
		virtual void NavigateBack();
		virtual void NavigateHome();
		virtual void NavigateToPage(Windows::UI::Xaml::Interop::TypeName pageType, Platform::Object^ parameter);        
		
		virtual void LoadState(
			Platform::Object^ navigationParameter, 
			Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ pageState) override;
		virtual void SaveState(Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ pageState) override;
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;
		virtual void OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

	internal:
		static property bool IsSuspending;
		static void OnDataContextPropertyChanged(
			Windows::UI::Xaml::DependencyObject^ element, 
			Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

	private:
		void _attachPageHandlers(ViewModelBase^ viewModel);
		void _detachPageHandlers(ViewModelBase^ viewModel);

	private:     
		enum class SettingsCommandId
		{
			CommandAbout = 1,
			CommandPrivacy
		};

		Windows::Foundation::EventRegistrationToken _navigateBackEventToken;
		Windows::Foundation::EventRegistrationToken _navigateHomeEventToken;
		Windows::Foundation::EventRegistrationToken _navigateToPageEventToken;

		bool _hasHandlers;
	};
} 
