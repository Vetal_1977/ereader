﻿/*
* @file LastReadPage.xaml.cpp
* @brief Last page displays last opened  books. 
*
* It will be displayed last, at maximum 16, opened books. Books are sorted by last opened date.
* The date will be also displayed
*
* @date 2013-02-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "LastReadPage.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

namespace eReader
{

LastReadPage::LastReadPage()
{
	InitializeComponent();
}

ListViewBase^ LastReadPage::_getActiveListBaseView()
{
	auto activeBookView = (BookGridView->Visibility == Windows::UI::Xaml::Visibility::Visible ?
		safe_cast<ListViewBase^>(BookGridView) : safe_cast<ListViewBase^>(BookListView));

	return activeBookView;
}

void LastReadPage::OnClickSimpleAppBarButton(Platform::Object^ /*sender*/, Windows::UI::Xaml::RoutedEventArgs^ /*e*/)
{
	if (TopAppBar != nullptr && TopAppBar->IsOpen)
	{
		TopAppBar->IsOpen = false;
	}

	if (BottomAppBar != nullptr && BottomAppBar->IsOpen)
	{
		BottomAppBar->IsOpen = false;
	}
}

void LastReadPage::OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e)
{
	auto activeBookView = _getActiveListBaseView();

	DeleteBooksButton->CommandParameter = activeBookView->SelectedItems;
	RemoveBooksFromHistoryButton->CommandParameter = activeBookView->SelectedItems;

	_updateBottomAppBarButtons();
}

}
