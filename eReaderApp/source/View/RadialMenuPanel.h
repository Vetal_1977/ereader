﻿/*
* @file RadialMenuPanel.h
* @brief Radial menu panel
*
* Panel of a radial menu to display menu items in a radial form
*
* @date 2013-02-12
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class RadialMenuPanel sealed : Windows::UI::Xaml::Controls::Panel
	{
	public:
		RadialMenuPanel();

		static property Windows::UI::Xaml::DependencyProperty^ OuterDiameterProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property float64 OuterDiameter 
		{ 
			float64 get() { return safe_cast<float64>(GetValue(OuterDiameterProperty)); } 
			void set(float64 value) { SetValue(OuterDiameterProperty, value); }
		}

		static property Windows::UI::Xaml::DependencyProperty^ InnerDiameterProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property float64 InnerDiameter 
		{ 
			float64 get() { return safe_cast<float64>(GetValue(InnerDiameterProperty)); }  
			void set(float64 value) { SetValue(InnerDiameterProperty, value); }
		}

		static property Windows::UI::Xaml::DependencyProperty^ StrokeThicknessProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property float64 StrokeThickness 
		{ 
			float64 get() { return safe_cast<float64>(GetValue(StrokeThicknessProperty)); } 
		}

		static property Windows::UI::Xaml::DependencyProperty^ AngleProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property float64 Angle 
		{ 
			float64 get() { return safe_cast<float64>(GetValue(AngleProperty)); }  
			void set(float64 value) { SetValue(AngleProperty, value); }
		}

		static property Windows::UI::Xaml::DependencyProperty^ StartAngleProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property float64 StartAngle 
		{ 
			float64 get() { return safe_cast<float64>(GetValue(StartAngleProperty)); }  
			void set(float64 value) { SetValue(StartAngleProperty, value); }
		};

		static property Windows::UI::Xaml::DependencyProperty^ ShowBorderProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property Platform::Boolean ShowBorder 
		{ 
			Platform::Boolean get() { return safe_cast<Platform::Boolean>(GetValue(ShowBorderProperty)); }  
			void set(Platform::Boolean value) { SetValue(ShowBorderProperty, value); }
		};

		static property Windows::UI::Xaml::DependencyProperty^ ShowPieLinesProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property Platform::Boolean ShowPieLines 
		{ 
			Platform::Boolean get() { return safe_cast<Platform::Boolean>(GetValue(ShowPieLinesProperty)); }  
			void set(Platform::Boolean value) { SetValue(ShowPieLinesProperty, value); }
		};

		/*static property Windows::UI::Xaml::DependencyProperty^ BorderColorProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property Windows::UI::Xaml::Media::Brush^ BorderColor
		{
			Windows::UI::Xaml::Media::Brush^ get() { return safe_cast<Windows::UI::Xaml::Media::Brush^>(GetValue(BorderColorProperty)); }
			void set(Windows::UI::Xaml::Media::Brush^ value) { SetValue(BorderColorProperty, value); }
		};

		static property Windows::UI::Xaml::DependencyProperty^ BackgroundColorProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};
		property Windows::UI::Xaml::Media::Brush^ BackgroundColor
		{
			Windows::UI::Xaml::Media::Brush^ get() { return safe_cast<Windows::UI::Xaml::Media::Brush^>(GetValue(BackgroundColorProperty)); }
			void set(Windows::UI::Xaml::Media::Brush^ value) { SetValue(BackgroundColorProperty, value); }
		};*/

	internal:
		// Invoked when an outer radius property is changed
		// @param d Dependency object instance; must be RadialMenuPanel
		// @param e Event arguments
		static void OuterDiameterChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

		// Invoked when an inner radius property is changed
		// @param d Dependency object instance; must be RadialMenuPanel
		// @param e Event arguments
		static void InnerDiameterChanged(Windows::UI::Xaml::DependencyObject^ d, Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

	protected:
		virtual Windows::Foundation::Size MeasureOverride(Windows::Foundation::Size availableSize) override;
		virtual Windows::Foundation::Size ArrangeOverride(Windows::Foundation::Size finalSize) override;

	private:
		// Converts specified double value of the angle into radian
		static float64 _angleToRadian(float64 angle);

		// Calculates angle per element section
		void _calculateAnglePerSection();

		// Recalculates a stroke thickness and set a new value into the property
		void _recalculateStrokeThickness();

	private:
		float64 _angleEach;
	};
}
