﻿/*
* @file AddBookToCollectionControl.xaml.cpp
* @brief The dialog allows to add a book into a category
*
* The dialog allows to select an existing book category or create a new one
*
* @date 2013-02-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "AddBookToCollectionControl.xaml.h"
#include <sstream>

using namespace eReader::Model;
using namespace eReader::ViewModel;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace std;


namespace eReader
{

AddBookToCollectionControl::AddBookToCollectionControl(RoutedEventHandler^ booksAddedToCollectionHadler)
{
	InitializeComponent();
	AddToCollectionButton->IsEnabled = (NewCollectionText->Text != nullptr && NewCollectionText->Text->Length() > 0);
	AddToCollectionButton->Click += booksAddedToCollectionHadler;
}

void AddBookToCollectionControl::setBooksForCollection(IVector<Object^>^ books)
{
	_collectionBookSet = nullptr;

	// create a book set to be passed as a parameter to 'Add Book to Collection' command
	_collectionBookSet = ref new BookSet(BookSetType::Collections);
	for each(Object^ book in books)
	{
		_collectionBookSet->Books->Append(safe_cast<BookContainer^>(book));
	}
	_collectionBookSet->Name = NewCollectionText->Text;
	AddToCollectionButton->CommandParameter = _collectionBookSet;
}

void AddBookToCollectionControl::OnCategorySelectionChanged(Object^ sender, SelectionChangedEventArgs^ e)
{
	if (e->AddedItems != nullptr && e->AddedItems->Size > 0)
	{
		assert(e->AddedItems->Size == 1);
		NewCollectionText->Text = safe_cast<String^>(e->AddedItems->GetAt(0));
	}
}

void AddBookToCollectionControl::OnNewCollectionTextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	AddToCollectionButton->IsEnabled = (NewCollectionText->Text != nullptr && NewCollectionText->Text->Length() > 0);
	_collectionBookSet->Name = NewCollectionText->Text;
}

}
