﻿/*
* @file NoBooksPage.xaml.h
* @brief This page is displayed if there is no books in the library. 
*
* @date 2013-02-05
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\NoBooksPage.g.h"

namespace eReader
{
	/// <summary>
	/// A basic page that provides characteristics common to most applications.
	/// </summary>
	public ref class NoBooksPage sealed
	{
	public:
		NoBooksPage();

	protected:
		// Populates the page with content passed during navigation. Any saved state is also
		// provided when recreating a page from a prior session.
		// @param navigationParameter The parameter value passed to 'Frame::Navigate(Type, Object)' when this page was initially requested.
		// @param pageState A map of state preserved by this page during an earlier session. This will be null the first time a page is visited.
		virtual void LoadState(
			Platform::Object^ navigationParameter,
			Windows::Foundation::Collections::IMap<Platform::String^, Platform::Object^>^ pageState) override;

	};
}
