﻿/*
* @file LibraryPage.xaml.h
* @brief Library page displays library books. 
*
* The library page displays books stored in the library. There are 4 filters/orders
* that applicable to this page: by authors, by title, last added (max. 10 books), 
* book collections created by the user
*
* @date 2013-02-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\LibraryPage.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LibraryPage sealed
	{
	public:
		LibraryPage();

	protected:
		// Invoked when the user leaves a page. Here necessary unregister action can be executed
		// @param e Navigation event data
		virtual void OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

		// Returns currently active list view
		// @return ListViewBase instance (ListView or GridView in our case)
		virtual Windows::UI::Xaml::Controls::ListViewBase^ _getActiveListBaseView() override;

		// Updates the state of the bottom appbar buttons. To be called by opening of the bottom appbar and changing of the book selection
		virtual void _updateBottomAppBarButtons() override;
		
	private:
		// Invoked when a book is selected/un-selected in the grid view. The appbar will be opened
		// @param sender Books grid view.
		// @param e Event data that describes the item clicked.
		void OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e);

		// Invoked when a 'simple' appbar button is clicked ('Import from File', 'Delete Book', etc.). 
		// Actual processing is done in the view model. Here only appbar is disappeared
		// @param sender Appbar button.
		// @param e Event data.
		void OnClickSimpleAppBarButton(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when user wants to add books to a collection (new or existing). The corresponding popup menu is displayed
		// @param sender Add collection button
		// @param e Event data
		void OnClickAddToCollection(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a menu item from a "Add Book to Collection" popup menu has been selected
		// @sender Add Book to Collection button
		// @param e Event data
		void OnAddBookToCollection(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when user wants to delete a collection. The corresponding popup menu is displayed
		// @param sender Delete Collection button
		// @param e Event data
		void OnClickDeleteCollection(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a menu item from a "Delete Collection" popup menu has been selected
		// @sender Delete Collection button
		// @param e Event data
		void OnDeleteCollection(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a popup menu from an appbar button is opened
		// @sender Popup menu
		// @param e Event data
		void OnPopupOpened(_In_ Platform::Object^ sender, _In_ Platform::Object^ e);

		// Invoked when an onscreen keyboard is showing
		// @param sender Onscreen keyboard
		// @param e Event data. The height of the pane is used 
		void OnKbdShowingHandler(Windows::UI::ViewManagement::InputPane^ sender, 
			Windows::UI::ViewManagement::InputPaneVisibilityEventArgs^ e);
		
		// Invoked when an onscreen keyboard is hiding
		// @param sender Onscreen keyboard
		// @param e Event data
		void OnKbdHidingHandler(Windows::UI::ViewManagement::InputPane^ sender, 
			Windows::UI::ViewManagement::InputPaneVisibilityEventArgs^ e);

		// Invoked when a user clicked on "Sort By" button. The flyout with possible selections will be displayed
		// @param sender "Sort By" drop-down button
		// @param e Event data
		void OnSortByDropDownClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when a user taps on a book set header. Navigate to a filtered library page
		// @param sender Book set header button
		// @param e Event data
		void OnHeaderClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

	private:
		Windows::UI::Xaml::Controls::Primitives::Popup^ _addToCollectionPopup;
		Windows::UI::Xaml::Controls::Primitives::Popup^ _deleteCollectionPopup;
		Windows::Foundation::EventRegistrationToken _addCollectionPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _deleteCollectionPopupOpenedToken;
		Windows::Foundation::EventRegistrationToken _onScreenKbdShowingToken;
		Windows::Foundation::EventRegistrationToken _onScreenKbdHidingToken;
		int _flyoutOffset;
	};
}
