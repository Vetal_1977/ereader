﻿/*
* @file SearchControl.xaml.h
* @brief Initiates the search in the book
*
* @date 2013-11-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "view\SearchControl.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class SearchControl sealed
	{
	public:
		SearchControl();
	};
}
