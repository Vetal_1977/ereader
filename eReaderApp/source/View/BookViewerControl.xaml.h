﻿/*
* @file BookViewerControl.xaml.h
* @brief This control shows a book content
*
* Displayed book content consists of 1 to 3 book pages; 
* the control allows navgation among them and provides properties to bind
*
* @date 2013-06-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "view\BookViewerControl.g.h"
#include <map>

namespace eReader
{
	public delegate void PageSelectionChangedHandler(int selectedPage);

	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class BookViewerControl sealed
	{
	public:
		BookViewerControl();

		static property Windows::UI::Xaml::DependencyProperty^ PageSizeProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};

		static property Windows::UI::Xaml::DependencyProperty^ PagesProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};

		static property Windows::UI::Xaml::DependencyProperty^ SelectedPageProperty
		{
			Windows::UI::Xaml::DependencyProperty^ get();
		};

		property Windows::Foundation::Rect PageSize
		{
			Windows::Foundation::Rect get() { return safe_cast<Windows::Foundation::Rect>(GetValue(PageSizeProperty)); }
			void set(Windows::Foundation::Rect value) { SetValue(PageSizeProperty, value); }
		};

		property Platform::Object^ Pages
		{
			Platform::Object^ get() 
			{ 
				return safe_cast<Platform::Object^>(GetValue(PagesProperty)); 
			}
			void set(Platform::Object^ value) 
			{ 
				SetValue(PagesProperty, value); 
			}
		};

		property int SelectedPage
		{
			int get() { return safe_cast<int>(GetValue(SelectedPageProperty)); }
			void set(int value) { SetValue(SelectedPageProperty, value); }
		};

		event PageSelectionChangedHandler^ PageSelectionChanged;
		event PageSelectionChangedHandler^ PageSelectionRestored;

	internal:
		// Adjusts scroll viewer and stack panel widths according to a currently set width
		// @param d Dependency object - should be this user control
		// @param e Event arguments should contain page width relevant data
		static void UpdateControlLayout(
			Windows::UI::Xaml::DependencyObject^ d, 
			Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

		// Updates page images according to new page contents
		// @param d Dependency object - should be this user control
		// @param e Event arguments should contain new/old page contents
		static void UpdatePages(
			Windows::UI::Xaml::DependencyObject^ d, 
			Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

		// Updates selected page and brings it onto the screen
		// @param d Dependency object - should be this user control
		// @param e Event arguments should contain newly selected page
		static void UpdateSelectedPage(
			Windows::UI::Xaml::DependencyObject^ d, 
			Windows::UI::Xaml::DependencyPropertyChangedEventArgs^ e);

	private:
		// Invoked when a user scrolls to another page
		// @param sender Root scroller
		// @param e Event data. Contains a usable 'is intermediate change' indication
		void OnRootScrollerViewChanged(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::Controls::ScrollViewerViewChangedEventArgs^ e);

		// Invoked when pages data vector changed its content
		// @param vector Vector that was changed
		// @param e ???
		void OnPagesVectorChanged(_In_ Windows::UI::Xaml::Interop::IBindableObservableVector^ vector, _In_ Platform::Object^ e);

		// Invoked when the size of the root stack panel is changed. 
		// It may be caused by showing stack panel children (page images) on the screen.
		// Scrolls to appropriate page here
		// @param sender Root scroll viewer
		// @param e Event data
		void OnRootStackPanelSizeChanged(_In_ Platform::Object^ sender, _In_ Windows::UI::Xaml::SizeChangedEventArgs^ e);

		// Scrolls to a currently selected page
		// @param thisControl Reference to 'this'
		// @param selectedPage Page to be scrolled to
		static void _scrollToSelectedPage(_In_ BookViewerControl^ thisControl, _In_ int selectedPage);

		// Invoked when a user tapped on the screen (touch, mouse click, etc.).
		// Change to the previous or next page
		// @param sender Book viewer control
		// @param e Event data
		void OnRootStackPanelTapped(Platform::Object^ sender, Windows::UI::Xaml::Input::TappedRoutedEventArgs^ e);

		static void _updatePages(BookViewerControl^ thisControl);

		// Invoked when a user press a key. Depending on the key scroll the page up and down
		// @param sender Core window
		// @param e Event data
		void OnCoreWindowKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e);
		
		// Invoked when this control is loaded. Used to capture a keyboard shortcuts
		// @param sender Not used here
		// @param e Event data
		void OnUserControlLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		// Invoked when this control is loaded. Used to un-capture a keyboard shortcuts
		// @param sender Not used here
		// @param e Event data
		void OnUserControlUnloaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

	private:
		Windows::Foundation::EventRegistrationToken _keyDownToken;
};

}
