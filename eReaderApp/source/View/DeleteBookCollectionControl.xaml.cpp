﻿/*
* @file DeleteBookCollectionControl.xaml.cpp
* @brief The dialog allows to delete collection
*
* @date 2013-03-02
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "DeleteBookCollectionControl.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace eReader
{

DeleteBookCollectionControl::DeleteBookCollectionControl(RoutedEventHandler^ collectionDeletedHadler) :
	_collectionDeletedHadler(collectionDeletedHadler)
{
	InitializeComponent();
}

void DeleteBookCollectionControl::OnDeleteBookCollectionControlLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (CollectionComboBox->Items != nullptr && CollectionComboBox->Items->Size > 0)
	{
		CollectionComboBox->SelectedIndex = 0;
		DeleteCollectionButton->IsEnabled = true;
	}
	else
	{
		DeleteCollectionButton->IsEnabled = false;
	}
	DeleteCollectionButton->Click += _collectionDeletedHadler;
}


}
