﻿//
// AboutSettingsFlyout.xaml.h
// Declaration of the AboutSettingsFlyout class
//

#pragma once

#include "view\AboutSettingsFlyout.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class AboutSettingsFlyout sealed
	{
	public:
		AboutSettingsFlyout();
	};
}
