﻿/*
* @file BookOperationBasePage.h
* @brief Base page for book operational page (e.g. BookOperationBasePage, LibraryPage)
*
* Contains common event handlers for appbar popup menus
*
* @date 2013-03-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\ReaderPage.h"

namespace eReader
{
	public ref class BookOperationBasePage : ReaderPage
	{
	internal:
		BookOperationBasePage();

	protected:
		// Returns currently active list view. Must be implemented in child classes
		// @return ListViewBase instance (ListView or GridView in our case)
		virtual Windows::UI::Xaml::Controls::ListViewBase^ _getActiveListBaseView();
		
		// Updates the state of the bottom appbar buttons. To be called by opening of the bottom appbar and changing of the book selection
		virtual void _updateBottomAppBarButtons();

		// Invoked when a book is clicked.
		// @param sender The GridView (or ListView when the application is snapped) displaying the item clicked.
		// @param e Event data that describes the item clicked.
		void OnItemViewItemClick(Platform::Object^ sender, Windows::UI::Xaml::Controls::ItemClickEventArgs^ e);

		// Invoked when a book is selected/un-selected in the grid view. The appbar will be opened
		// @param sender Books grid view.
		// @param e Event data that describes the item clicked.
		void OnBookSelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);

		// Invoked when a bottom appbar is opened. Updates appbar button states
		// @sender Bottom appbar
		// @param e Event data
		void OnBottomAppBarOpened(_In_ Platform::Object^ sender, _In_ Platform::Object^ e);
	};
}
