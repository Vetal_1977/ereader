﻿/*
* @file BookViewerControl.xaml.cpp
* @brief This control shows a book content
*
* Displayed book content consists of 1 to 3 book pages; 
* the control allows navigation among them and provides properties to bind
*
* @date 2013-06-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookViewerControl.xaml.h"
#include <assert.h>
#include "Utils\Helper.h"
#include <sstream>

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Core;

namespace eReader
{

BookViewerControl::BookViewerControl()
{
	InitializeComponent();
}

static DependencyProperty^ _pageSizeProperty =
	DependencyProperty::Register("PageSize", TypeName(Windows::Foundation::Rect::typeid), TypeName(BookViewerControl::typeid),
	ref new PropertyMetadata(nullptr, ref new PropertyChangedCallback(
	&BookViewerControl::UpdateControlLayout)));

DependencyProperty^ BookViewerControl::PageSizeProperty::get()
{
	return _pageSizeProperty;
}

void BookViewerControl::UpdateControlLayout(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
	auto thisControl = safe_cast<BookViewerControl^>(d);
	if (thisControl != nullptr)
	{
		// ScrollViewer behavior: HorizontalScrollBarVisibility must be set to 'Auto' to 
		// change 'ScrollableWidth' properly by size changing
		thisControl->RootScroller->HorizontalScrollBarVisibility = ScrollBarVisibility::Auto;

		thisControl->RootScroller->Width = thisControl->PageSize.Width;
		thisControl->RootScroller->Height = thisControl->PageSize.Height;
	}
}

static DependencyProperty^ _pagesProperty =
	DependencyProperty::Register("Pages", TypeName(Object::typeid), TypeName(BookViewerControl::typeid),
	ref new PropertyMetadata(nullptr, ref new PropertyChangedCallback(
	&BookViewerControl::UpdatePages)));

DependencyProperty^ BookViewerControl::PagesProperty::get()
{
	return _pagesProperty;
}

void BookViewerControl::UpdatePages(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
	auto thisControl = safe_cast<BookViewerControl^>(d);
	if (thisControl != nullptr)
	{
		_updatePages(thisControl);
	}
}

static DependencyProperty^ _selectedPageProperty =
	DependencyProperty::Register("SelectedPage", TypeName(int::typeid), TypeName(BookViewerControl::typeid),
	ref new PropertyMetadata(nullptr, ref new PropertyChangedCallback(
	&BookViewerControl::UpdateSelectedPage)));

DependencyProperty^ BookViewerControl::SelectedPageProperty::get()
{
	return _selectedPageProperty;
}

void BookViewerControl::UpdateSelectedPage(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
	auto thisControl = safe_cast<BookViewerControl^>(d);
	if (thisControl != nullptr)
	{
		auto newSelPage = static_cast<int>(e->NewValue);
		_scrollToSelectedPage(thisControl, newSelPage);
	}
}

void BookViewerControl::OnRootScrollerViewChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::ScrollViewerViewChangedEventArgs^ e)
{
	const double maxOffset = RootStackPanel->ActualWidth - RootScroller->ActualWidth + 0.5f;
	const double minOffset = 0.5f;
	auto offset = RootScroller->HorizontalOffset + 0.5f;

	if (!e->IsIntermediate || offset <= minOffset || maxOffset <= offset )
	{
#ifdef _DEBUG
		std::stringstream stream;
		stream << "Scroll to offset '" << offset << "'";
		Utils::Helper::outputDebugString(stream.str().c_str());
#endif

		if (offset < minOffset)
		{
			offset = minOffset;
		}
		else if (maxOffset < offset)
		{
			offset = maxOffset;
		}

		auto newSelPage = static_cast<int>((offset) / RootScroller->ActualWidth);
		SelectedPage = newSelPage;
		PageSelectionChanged(newSelPage);
		
		// ScrollViewer behavior: HorizontalScrollBarVisibility must be set to 'Auto' to 
		// change 'ScrollableWidth' properly by size changing
		// Here is a proper point to hide the scrollbar again
		RootScroller->HorizontalScrollBarVisibility = ScrollBarVisibility::Hidden;
	}
}

void BookViewerControl::_updatePages(BookViewerControl^ thisControl)
{
	auto pages = dynamic_cast<IBindableObservableVector^>(thisControl->Pages);
	thisControl->RootStackPanel->Children->Clear();

	if (pages != nullptr && pages->Size > 0)
	{
		for (size_t pageIdx = 0; pageIdx < pages->Size; pageIdx++)
		{
			auto vm = dynamic_cast<BookPageViewModel^>(pages->GetAt(pageIdx));
			
			auto binding = ref new Binding();
			binding->Source = vm;
			binding->Mode = Data::BindingMode::OneWay;
			binding->Path = ref new PropertyPath(L"Content");
			
			auto pageImage = ref new Image();
			pageImage->SetBinding(pageImage->SourceProperty, binding);

			thisControl->RootStackPanel->Children->Append(pageImage);
		}

		Utils::Helper::outputDebugString("_updatePages");
	}
}

void BookViewerControl::OnRootStackPanelSizeChanged(Object^ sender, SizeChangedEventArgs^ e)
{
	_scrollToSelectedPage(this, SelectedPage);
}

void BookViewerControl::_scrollToSelectedPage(BookViewerControl^ thisControl, int selectedPage)
{
	if (0 <= selectedPage && selectedPage < static_cast<int>(thisControl->RootStackPanel->Children->Size))
	{
		auto offset = thisControl->RootScroller->ActualWidth * selectedPage;
		if (offset != thisControl->RootScroller->HorizontalOffset)
		{
			//thisControl->RootScroller->ScrollToHorizontalOffset(offset);
			thisControl->RootScroller->ChangeView((double)offset, 0.0, 1.0f, true);
			assert(thisControl->RootScroller->HorizontalOffset != offset);
#ifdef DEBUG
			std::stringstream stream;
			stream << "Scroll to offset '" << offset << "'";
			Utils::Helper::outputDebugString(stream.str().c_str());
#endif
		}
	}
}

void BookViewerControl::OnRootStackPanelTapped(Platform::Object^ sender, Windows::UI::Xaml::Input::TappedRoutedEventArgs^ e)
{
	Point position = e->GetPosition(nullptr);
	auto vm = dynamic_cast<BookViewModel^>(DataContext);
	bool isEventHandled = false;
	vm->pageTapped(position, isEventHandled);

	if (!isEventHandled)
	{
		// go to the next/previous page depending on the tapped position
		// check the x position corresponding to the center of the current window
		const float currentWinWidth = Window::Current->Bounds.Width;
		auto selectedPage = SelectedPage;
		if (position.X < currentWinWidth / 2)
		{
			if (selectedPage > 0)
			{
				_scrollToSelectedPage(this, selectedPage - 1);
			}
		}
		else
		{
			if (selectedPage < static_cast<int>(RootStackPanel->Children->Size - 1))
			{
				_scrollToSelectedPage(this, selectedPage + 1);
			}
		}
	}

	e->Handled = true;
}

void BookViewerControl::OnCoreWindowKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ e)
{
	switch (e->VirtualKey)
	{
	case Windows::System::VirtualKey::Down:
	case Windows::System::VirtualKey::Right:
	case Windows::System::VirtualKey::PageDown:
		{
			auto selectedPage = SelectedPage;
			if (selectedPage < static_cast<int>(RootStackPanel->Children->Size - 1))
			{
				_scrollToSelectedPage(this, selectedPage + 1);
			}
			break;
		}

	case Windows::System::VirtualKey::Up:
	case Windows::System::VirtualKey::Left:
	case Windows::System::VirtualKey::PageUp:
		{
			auto selectedPage = SelectedPage;
			if (selectedPage > 0)
			{
				_scrollToSelectedPage(this, selectedPage - 1);
			}
			break;
		}

	default:
		break;
	}
}

void BookViewerControl::OnUserControlLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	_keyDownToken = Window::Current->CoreWindow->KeyDown += 
		ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &BookViewerControl::OnCoreWindowKeyDown);
}

void BookViewerControl::OnUserControlUnloaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	Window::Current->CoreWindow->KeyDown -= _keyDownToken;
}

}

