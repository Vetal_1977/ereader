﻿/*
* @file DesignTimeData.h
* @brief Data that are used during design time
*
* @date 2012-12-31
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
#pragma region DesignTimeBookData Class
	//
	// Book data for design time
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class DesignTimeBookData sealed
	{
	public:
		property Platform::String^ Authors { Platform::String^ get(); void set(Platform::String^ vlaue); }
		property Platform::String^ Title { Platform::String^ get(); void set(Platform::String^ value); }

	private:
		Platform::String^ _authors;
		Platform::String^ _title;
	};
#pragma endregion

#pragma region DesignTimeBookCollection Class
	//
	// Book collection for design time
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class DesignTimeBookCollection sealed
	{
	public:
		DesignTimeBookCollection(Platform::String^ collectionName);

		property Platform::String^ Name { Platform::String^ get(); }
		property Windows::Foundation::Collections::IObservableVector<DesignTimeBookData^>^ Books
		{ 
			Windows::Foundation::Collections::IObservableVector<DesignTimeBookData^>^ get(); 
		}

	private:
		Platform::String^ _collectionName;
		Platform::Collections::Vector<DesignTimeBookData^>^ _books;
	};
#pragma endregion

#pragma region DesignTimeData Class
	//
	// Root class of design time data
	//
	[Windows::UI::Xaml::Data::Bindable]
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class DesignTimeData sealed
	{
	public:
		DesignTimeData();

		property Windows::Foundation::Collections::IObservableVector<DesignTimeBookCollection^>^ SpecialCollections
		{ 
			Windows::Foundation::Collections::IObservableVector<DesignTimeBookCollection^>^ get();
		}
	};
#pragma endregion

}
