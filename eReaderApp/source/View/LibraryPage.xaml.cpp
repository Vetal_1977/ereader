﻿/*
* @file LibraryPage.xaml.cpp
* @brief Library page displays library books. 
*
* The library page displays books stored in the library. There are 4 filters/orders
* that applicable to this page: by authors, by title, last added (max. 10 books), 
* book collections created by the user
*
* @date 2013-02-15
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "LibraryPage.xaml.h"
#include "AddBookToCollectionControl.xaml.h"
#include "DeleteBookCollectionControl.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace concurrency;
using namespace Windows::UI::ViewManagement;
using namespace eReader::Utils;
using namespace Callisto::Controls;

namespace eReader
{

LibraryPage::LibraryPage() :
	_flyoutOffset(0)
{
	InitializeComponent();
}

void LibraryPage::OnNavigatedFrom(NavigationEventArgs^ e)
{
	if (!ReaderPage::IsSuspending)
	{
		if (_addToCollectionPopup != nullptr)
		{
			_addToCollectionPopup->Opened -= _addCollectionPopupOpenedToken;
			InputPane::GetForCurrentView()->Showing -= _onScreenKbdShowingToken;
			InputPane::GetForCurrentView()->Hiding -= _onScreenKbdHidingToken;
		}
	
		if (_deleteCollectionPopup != nullptr)
			_deleteCollectionPopup->Opened -= _deleteCollectionPopupOpenedToken;
	}

	ReaderPage::OnNavigatedFrom(e);
}

ListViewBase^ LibraryPage::_getActiveListBaseView()
{
	auto activeBookView = (BookGridView->Visibility == Windows::UI::Xaml::Visibility::Visible ?
		safe_cast<ListViewBase^>(BookGridView) : safe_cast<ListViewBase^>(BookListView));

	return activeBookView;
}

void LibraryPage::_updateBottomAppBarButtons()
{
	auto vm = dynamic_cast<LibraryPageViewModel^>(DataContext);
	vm->reEvaluateCommands();

	// AddToCollection button is not bind to a command. Instead "Add Book to Collection" dialog is displayed
	// Because of that the button should enabled/disabled manually
	auto activeBookView = _getActiveListBaseView();
	AddToCollectionButton->IsEnabled = (activeBookView->SelectedItems != nullptr && activeBookView->SelectedItems->Size > 0);

	// DeleteCollection button is not bind to a command. Instead "Delete Collection" dialog is displayed
	// Because of that the button should enabled/disabled manually
	DeleteCollectionButton->IsEnabled = (vm->BookCollectionNames != nullptr && vm->BookCollectionNames->Size > 0);
}

void LibraryPage::OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e)
{
	auto activeBookView = _getActiveListBaseView();

	DeleteBooksButton->CommandParameter = activeBookView->SelectedItems;
	RemoveFromCollectionButton->CommandParameter = activeBookView->SelectedItems;

	_updateBottomAppBarButtons();
}

void LibraryPage::OnClickSimpleAppBarButton(Platform::Object^ /*sender*/, Windows::UI::Xaml::RoutedEventArgs^ /*e*/)
{
	if (TopAppBar != nullptr && TopAppBar->IsOpen)
	{
		TopAppBar->IsOpen = false;
	}

	if (BottomAppBar != nullptr && BottomAppBar->IsOpen)
	{
		BottomAppBar->IsOpen = false;
	}
}

void LibraryPage::OnClickAddToCollection(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto activeBookView = _getActiveListBaseView();
	assert(activeBookView->SelectedItems != nullptr);
	assert(activeBookView->SelectedItems->Size > 0);

	// create popup sortByMenu
	if (_addToCollectionPopup == nullptr)
	{
		_addToCollectionPopup = ref new Popup();
		_addToCollectionPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		StackPanel^ panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// margins
		static const int horMargin = 10;
		static const int verMargin = 5;

		// insert controls into the stack panel
		auto addBookToCollectionControl = ref new AddBookToCollectionControl(
			ref new RoutedEventHandler(this, &LibraryPage::OnAddBookToCollection));
		addBookToCollectionControl->DataContext = DataContext;
		addBookToCollectionControl->setBooksForCollection(safe_cast<IVector<Object^>^>(activeBookView->SelectedItems));
		panel->Children->Append(addBookToCollectionControl);

		// insert stack panel into the sortByMenu
		_addToCollectionPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the sortByMenu
		_addCollectionPopupOpenedToken = _addToCollectionPopup->Opened += ref new EventHandler<Object^>(this, &LibraryPage::OnPopupOpened); 
		_onScreenKbdShowingToken = InputPane::GetForCurrentView()->Showing += 
			ref new TypedEventHandler<InputPane^,InputPaneVisibilityEventArgs^>(this, &LibraryPage::OnKbdShowingHandler);
		_onScreenKbdHidingToken = InputPane::GetForCurrentView()->Hiding += 
			ref new TypedEventHandler<InputPane^,InputPaneVisibilityEventArgs^>(this, &LibraryPage::OnKbdHidingHandler);
	}
	else
	{
		auto stackPanel = safe_cast<StackPanel^>(_addToCollectionPopup->Child);
		auto addBookToCollectionControl = safe_cast<AddBookToCollectionControl^>(stackPanel->Children->GetAt(0));
		addBookToCollectionControl->setBooksForCollection(safe_cast<IVector<Object^>^>(activeBookView->SelectedItems));
	}

	// The sortByMenu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the sortByMenu
	// Get the screen position of the button
	GeneralTransform^ transform = AddToCollectionButton->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_addToCollectionPopup->HorizontalOffset = rootPoint.X;
	_addToCollectionPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;
	
	// show sortByMenu
	_addToCollectionPopup->IsOpen = true;
}

void LibraryPage::OnAddBookToCollection(Object^ sender, RoutedEventArgs^ e)
{
	_addToCollectionPopup->IsOpen = false;
	BottomAppBar->IsOpen = false;
	_getActiveListBaseView()->Focus(Windows::UI::Xaml::FocusState::Programmatic);;
}

void LibraryPage::OnClickDeleteCollection(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// create popup sortByMenu
	if (_deleteCollectionPopup == nullptr)
	{
		_deleteCollectionPopup = ref new Popup();
		_deleteCollectionPopup->IsLightDismissEnabled = true;

		// create stack panel to put command buttons
		StackPanel^ panel = ref new StackPanel();
		panel->Background = BottomAppBar->Background;
		panel->UseLayoutRounding = true;
		panel->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
		
		// margins
		static const int horMargin = 10;
		static const int verMargin = 5;

		// insert controls into the stack panel
		auto deleteCollectionControl = ref new DeleteBookCollectionControl(
			ref new RoutedEventHandler(this, &LibraryPage::OnDeleteCollection));
		deleteCollectionControl->DataContext = DataContext;
		panel->Children->Append(deleteCollectionControl);

		// insert stack panel into the sortByMenu
		_deleteCollectionPopup->Child = panel;

		// upon opening of the popup, reposition it corresponding to the actual height of the stack panel
		// it must be done only once upon creating of the sortByMenu
		_deleteCollectionPopupOpenedToken = _deleteCollectionPopup->Opened += ref new EventHandler<Object^>(this, &LibraryPage::OnPopupOpened); 
	}
	
	// The sortByMenu position calculation must be always done because of the possibility
	// to turn the display

	// calculate the placement of the sortByMenu
	// Get the screen position of the button
	GeneralTransform^ transform = DeleteCollectionButton->TransformToVisual(this);
	Point rootPoint = transform->TransformPoint(Point(0, 0));
	Rect rootRect = transform->TransformBounds(Rect());
	
	// Now get the point of the button in the screen
	_deleteCollectionPopup->HorizontalOffset = rootPoint.X;
	_deleteCollectionPopup->VerticalOffset = 
		Window::Current->CoreWindow->Bounds.Bottom - 
		BottomAppBar->ActualHeight;
	
	// show sortByMenu
	_deleteCollectionPopup->IsOpen = true;
}

void LibraryPage::OnDeleteCollection(Object^ sender, RoutedEventArgs^ e)
{
	_deleteCollectionPopup->IsOpen = false;
	BottomAppBar->IsOpen = false;
	_getActiveListBaseView()->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void LibraryPage::OnPopupOpened(Object^ sender, Object^ e)
{
	auto openedPopup = safe_cast<Popup^>(sender);
	auto childPanel = safe_cast<StackPanel^>(openedPopup->Child);

	// actual height of the stack panel is known only upon opening of the parent popup
	openedPopup->VerticalOffset -= (childPanel->ActualHeight + 4);
}

void LibraryPage::OnKbdShowingHandler(InputPane^ sender, InputPaneVisibilityEventArgs^ e)
{
	_flyoutOffset = static_cast<int>(e->OccludedRect.Height);

	if (_addToCollectionPopup->IsOpen)
	{
		_addToCollectionPopup->VerticalOffset -= _flyoutOffset;
	}
}

void LibraryPage::OnKbdHidingHandler(Windows::UI::ViewManagement::InputPane^ sender, 
			Windows::UI::ViewManagement::InputPaneVisibilityEventArgs^ e)
{
	if (_addToCollectionPopup->IsOpen)
	{
		_addToCollectionPopup->VerticalOffset += _flyoutOffset;
	}
}

void LibraryPage::OnSortByDropDownClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	// Display flyout sortByMenu allowing a user to select an entity to sorting
	auto sortByFlyout = ref new Callisto::Controls::Flyout();
	sortByFlyout->PlacementTarget = dynamic_cast<UIElement^>(sender);
	sortByFlyout->Placement = PlacementMode::Bottom;

	auto sortByMenu = ref new Menu();
	auto vm = dynamic_cast<LibraryPageViewModel^>(DataContext);

	auto sortByMIDate = ref new MenuItem();
	sortByMIDate->Text = Utils::ResLoader::getString("Date");
	sortByMIDate->Command = vm->LastAddedCommand;

	auto sortByMIAuthor = ref new MenuItem();
	sortByMIAuthor->Text = Utils::ResLoader::getString("Author");
	sortByMIAuthor->Command = vm->AuthorsCommand;

	auto sortByMITitle = ref new MenuItem();
	sortByMITitle->Text = Utils::ResLoader::getString("Title");
	sortByMITitle->Command = vm->TitlesCommand;

	auto sortByMICollection = ref new MenuItem();
	sortByMICollection->Text = Utils::ResLoader::getString("Collection");
	sortByMICollection->Command = vm->CollectionsCommand;

	sortByMenu->Items->Append(sortByMIDate);
	sortByMenu->Items->Append(sortByMIAuthor);
	sortByMenu->Items->Append(sortByMITitle);
	sortByMenu->Items->Append(sortByMICollection);

	//sortByFlyout.HostMargin = new Thickness(0); // on sortByMenu flyouts set HostMargin to 0
	sortByFlyout->HostMargin = Thickness(0, 0, 0, 0);
	sortByFlyout->Content = sortByMenu;
	sortByFlyout->IsOpen = true;
}

void LibraryPage::OnHeaderClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto vm = dynamic_cast<LibraryPageViewModel^>(DataContext);
	auto bookSet = safe_cast<FrameworkElement^>(sender)->DataContext;
	vm->OnBookSetSelected(bookSet);
}

}
