﻿/*
* @file RadialMenuPanel.cpp
* @brief Radial menu control
*
* Panel of a radial menu to display menu items in a radial form
*
* @date 2013-02-12
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "RadialMenuPanel.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;

namespace eReader
{

RadialMenuPanel::RadialMenuPanel()
{
}

//
// Dependency property of the radius of the outer circle 
//
static DependencyProperty^ _outerDiameter =
	DependencyProperty::Register("OuterDiameter", TypeName(default::float64::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(120.0, ref new PropertyChangedCallback(
	&RadialMenuPanel::OuterDiameterChanged)));

DependencyProperty^ RadialMenuPanel::OuterDiameterProperty::get()
{
	return _outerDiameter;
}

void RadialMenuPanel::OuterDiameterChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
	auto radialMenu = safe_cast<RadialMenuPanel^>(d);
	if (radialMenu != nullptr)
	{
		radialMenu->Width = (float64)e->NewValue;
		radialMenu->Height = (float64)e->NewValue;
		radialMenu->_recalculateStrokeThickness();
	}
}

// 
// Dependency property of the radius of the inner circle 
//
static DependencyProperty^ _innerDiameter =
	DependencyProperty::Register("InnerDiameter", TypeName(default::float64::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(60.0, ref new PropertyChangedCallback(
	&RadialMenuPanel::InnerDiameterChanged)));

DependencyProperty^ RadialMenuPanel::InnerDiameterProperty::get()
{
	return _innerDiameter;
}

void RadialMenuPanel::InnerDiameterChanged(DependencyObject^ d, DependencyPropertyChangedEventArgs^ e)
{
	auto radialMenu = safe_cast<RadialMenuPanel^>(d);
	if (radialMenu != nullptr)
	{
		radialMenu->_recalculateStrokeThickness();
	}
}

//
// Dependency property of the radius of the inner circle 
//
static DependencyProperty^ _strokeThickness =
	DependencyProperty::Register("StrokeThickness", TypeName(default::float64::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(30.0));

DependencyProperty^ RadialMenuPanel::StrokeThicknessProperty::get()
{
	return _strokeThickness;
}

//
// Dependency property of the radial angle to place items. 360.0 means placement in a full circle, 180.0 - in a half-circle, etc.
//
static DependencyProperty^ _angle =
	DependencyProperty::Register("Angle", TypeName(default::float64::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(360.0));

DependencyProperty^ RadialMenuPanel::AngleProperty::get()
{
	return _angle;
}

//
// Dependency property of the start angle to place items
//
static DependencyProperty^ _startAngle =
	DependencyProperty::Register("StartAngle", TypeName(default::float64::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(0.0));

DependencyProperty^ RadialMenuPanel::StartAngleProperty::get()
{
	return _startAngle;
}

//
// Dependency property of the show border for a menu
//
static DependencyProperty^ _showBorder =
	DependencyProperty::Register("ShowBorder", TypeName(Platform::Boolean::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(true));

DependencyProperty^ RadialMenuPanel::ShowBorderProperty::get()
{
	return _showBorder;
}

//
// Dependency property of the show pie lines for a menu
//
static DependencyProperty^ _showPieLines =
	DependencyProperty::Register("ShowPieLines", TypeName(Platform::Boolean::typeid), TypeName(RadialMenuPanel::typeid),
	ref new PropertyMetadata(false));

DependencyProperty^ RadialMenuPanel::ShowPieLinesProperty::get()
{
	return _showPieLines;
}

////
//// Dependency property of the border color
////
//static DependencyProperty^ _borderColor =
//	DependencyProperty::Register("BorderColor", TypeName(Brush::typeid), TypeName(RadialMenuPanel::typeid),
//	ref new PropertyMetadata(ref new SolidColorBrush(Colors::Gray)));
//
//DependencyProperty^ RadialMenuPanel::BorderColorProperty::get()
//{
//	return _borderColor;
//}
//
////
//// Dependency property of the border color
////
//static DependencyProperty^ _backgroundColor =
//	DependencyProperty::Register("BackgroundColor", TypeName(Brush::typeid), TypeName(RadialMenuPanel::typeid),
//	ref new PropertyMetadata(ref new SolidColorBrush(Colors::LightGray)));
//
//DependencyProperty^ RadialMenuPanel::BackgroundColorProperty::get()
//{
//	return _backgroundColor;
//}

Size RadialMenuPanel::MeasureOverride(Size availableSize)
{
	_calculateAnglePerSection();

	for each (UIElement^ child in Children)
	{
		child->Measure(availableSize);
	}

	return Size((float)(OuterDiameter), (float)(OuterDiameter));
}

Size RadialMenuPanel::ArrangeOverride(Windows::Foundation::Size /*finalSize*/)
{
	_calculateAnglePerSection();

	auto startAngle = _angleToRadian(StartAngle);
	auto outerRadius = OuterDiameter / 2;
	auto innerRadius = InnerDiameter / 2;
	auto startPointX = outerRadius + sin(startAngle) * (outerRadius + innerRadius) / 2;
	auto startPointY = (outerRadius - innerRadius) / 2 + (1 - cos(startAngle)) * (outerRadius + innerRadius) / 2;
	Point currentPosition((float)startPointX, (float)startPointY);

	int childCount = Children->Size;
	if (childCount > 0)
	{
		auto perAngle = _angleToRadian(Angle) / childCount;

		for (int i = 0; i < childCount; i++)
		{
			UIElement^ child = Children->GetAt(i);

			auto angle = (i + 1) * perAngle + startAngle;
			auto offsetX = sin(angle) * (outerRadius + innerRadius) / 2;
			auto offsetY = (1 - cos(angle)) * (outerRadius + innerRadius) / 2;

			Rect childRect(Point(currentPosition.X - child->DesiredSize.Width / 2,
				currentPosition.Y - child->DesiredSize.Height / 2),
				Point(currentPosition.X + child->DesiredSize.Width / 2,
				currentPosition.Y + child->DesiredSize.Height / 2));
			child->Arrange(childRect);
			currentPosition.X = (float)(offsetX + outerRadius);
			currentPosition.Y = (float)(offsetY + (outerRadius - innerRadius) / 2);
		}
	}

	return Size((float)(OuterDiameter), (float)(OuterDiameter));
}

void RadialMenuPanel::_calculateAnglePerSection()
{
	if (Children->Size > 0)
	{
		_angleEach = Angle / Children->Size;
	}
	else
	{
		_angleEach = 360.0;
	}
}

float64 RadialMenuPanel::_angleToRadian(float64 angle)
{
	return angle * M_PI / 180;
}

void RadialMenuPanel::_recalculateStrokeThickness()
{
	float64 strokeThickness = 1.0;
	if (OuterDiameter > InnerDiameter)
	{
		strokeThickness = (OuterDiameter - InnerDiameter) / 2;
	}
	SetValue(StrokeThicknessProperty, strokeThickness);
}

}
