﻿/*
 * @file StartPage.xaml.cpp
 * @brief Start page is an overview page of the reader. 
 *
 * The overview contains quick links to last read books and last put book into the bookshelf
 *
 * @date 2012-12-23
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#include "pch.h"
#include "StartPage.xaml.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace concurrency;
using namespace Windows::UI::ViewManagement;

namespace eReader
{

StartPage::StartPage()
{
	InitializeComponent();
}

void StartPage::OnHeaderClick(Object^ sender, RoutedEventArgs^ e)
{
	auto vm = dynamic_cast<StartPageViewModel^>(DataContext);
	auto bookSet = dynamic_cast<FrameworkElement^>(sender)->DataContext;
	vm->OnBookSetSelected(bookSet);
}

ListViewBase^ StartPage::_getActiveListBaseView()
{
	auto activeBookView = (BookGridView->Visibility == Windows::UI::Xaml::Visibility::Visible ?
		safe_cast<ListViewBase^>(BookGridView) : safe_cast<ListViewBase^>(BookListView));

	return activeBookView;
}

void StartPage::OnMainGridSizeChanged(Platform::Object^ sender, Windows::UI::Xaml::SizeChangedEventArgs^ e)
{
	auto activeBookView = _getActiveListBaseView();

	DeleteBooksButton->CommandParameter = activeBookView->SelectedItems;

	_updateBottomAppBarButtons();
}

void StartPage::OnClickSimpleAppBarButton(Platform::Object^ /*sender*/, Windows::UI::Xaml::RoutedEventArgs^ /*e*/)
{
	if (TopAppBar != nullptr && TopAppBar->IsOpen)
	{
		TopAppBar->IsOpen = false;
	}

	if (BottomAppBar != nullptr && BottomAppBar->IsOpen)
	{
		BottomAppBar->IsOpen = false;
	}
}

}
