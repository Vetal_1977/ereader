﻿/*
* @file AddBookToCollectionControl.xaml.h
* @brief The dialog allows to add a book into a category
*
* The dialog allows to select an existing book category or create a new one
*
* @date 2013-02-25
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\AddBookToCollectionControl.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class AddBookToCollectionControl sealed
	{
	public:
		// Constructs this object
		// @param booksAddedToCollectionHadler Handler to be called after user clicked on 'add books to collection' button
		AddBookToCollectionControl(_In_ Windows::UI::Xaml::RoutedEventHandler^ booksAddedToCollectionHadler);

	internal:
		// Sets books that will be added into a new or existing collection. This method should
		// be called before the control is displayed
		// @param books Books for the collection
		void setBooksForCollection(_In_ Windows::Foundation::Collections::IVector<Platform::Object^>^ books);

	private:
		// By changing a selection of a collection put it name into the text box, i.e.
		// this simulates a behavior an editable combo box using 2 controls. WinRT combo box 
		// is not editable
		// @param sender Book collections combo box
		// @param e Arguments of the event
		void OnCategorySelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);

		// By changing a text in the new collection text box, updates a state of the 'Add' button
		// @param sender New collection text box
		// @param e Arguments of the event
		void OnNewCollectionTextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);

	private:
		ViewModel::BookSet^ _collectionBookSet;
	};
}
