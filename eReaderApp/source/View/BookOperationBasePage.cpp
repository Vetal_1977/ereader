﻿/*
* @file BookOperationBasePage.cpp
* @brief Base page for book operational page (e.g. BookOperationBasePage, LibraryPage)
*
* Contains common event handlers for appbar popup menus
*
* @date 2013-03-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage;
using namespace concurrency;

namespace eReader
{

BookOperationBasePage::BookOperationBasePage() : ReaderPage()
{
}

ListViewBase^ BookOperationBasePage::_getActiveListBaseView()
{
	throw ref new NotImplementedException();
}
		
void BookOperationBasePage::OnItemViewItemClick(Object^ /*sender*/, ItemClickEventArgs^ e)
{
	auto vm = dynamic_cast<BookOperationBasePageViewModel^>(DataContext);
	vm->OnBookSelected(e->ClickedItem);
}

void BookOperationBasePage::OnBookSelectionChanged(Object^ sender, SelectionChangedEventArgs^ e)
{
	auto senderListViewBase = safe_cast<ListViewBase^>(sender);
	if (senderListViewBase->SelectedItems != nullptr && senderListViewBase->SelectedItems->Size > 0)
	{
		if (BottomAppBar->IsOpen)
		{
			_updateBottomAppBarButtons();
		}

		BottomAppBar->IsSticky = true;
		BottomAppBar->IsOpen = true;
	}
	else
	{
		BottomAppBar->IsOpen = false;
	}
}

void BookOperationBasePage::OnBottomAppBarOpened(Platform::Object^ sender, Platform::Object^ e)
{
	_updateBottomAppBarButtons();
}

void BookOperationBasePage::_updateBottomAppBarButtons()
{
	auto vm = dynamic_cast<BookOperationBasePageViewModel^>(DataContext);
	vm->reEvaluateCommands();
}

}
