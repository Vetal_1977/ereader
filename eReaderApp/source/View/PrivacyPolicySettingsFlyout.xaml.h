﻿//
// PrivacyPolicySettingsFlyout.xaml.h
// Declaration of the PrivacyPolicySettingsFlyout class
//

#pragma once

#include "view\PrivacyPolicySettingsFlyout.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class PrivacyPolicySettingsFlyout sealed
	{
	public:
		PrivacyPolicySettingsFlyout();
	};
}
