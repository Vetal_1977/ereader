﻿/*
* @file FilteredLibraryPage.xaml.h
* @brief Library page displays library books applying a filter 
*
* The filtered library page displays books stored in the library using a filter.
* A filter may be an author, start character of a title, a date or collection
*
* @date 2013-06-10
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "view\FilteredLibraryPage.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class FilteredLibraryPage sealed
	{
	public:
		FilteredLibraryPage();

	protected:
		// Invoked when the user navigates to a page. Here necessary unregister action can be executed
		// @param e Navigation event data
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;
	};
}
