﻿/*
* @file DeleteBookCollectionControl.xaml.h
* @brief The dialog allows to delete collection
*
* @date 2013-03-02
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "View\DeleteBookCollectionControl.g.h"

namespace eReader
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class DeleteBookCollectionControl sealed
	{
	public:
		// Constructs this object
		// @param collectionDeletedHadler Handler to be called after user clicked on 'delete collection' button
		DeleteBookCollectionControl(_In_ Windows::UI::Xaml::RoutedEventHandler^ collectionDeletedHadler);
	
	private:
		// Invoked when the control is loaded
		// @sender This control
		// @param e Event data
		void OnDeleteBookCollectionControlLoaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

	private:
		Windows::UI::Xaml::RoutedEventHandler^ _collectionDeletedHadler;
	};
}
