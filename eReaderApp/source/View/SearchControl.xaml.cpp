﻿/*
* @file SearchControl.xaml.cpp
* @brief Initiates the search in the book
*
* @date 2013-11-30
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "SearchControl.xaml.h"

using namespace eReader;

namespace eReader
{

SearchControl::SearchControl()
{
	InitializeComponent();
}

}