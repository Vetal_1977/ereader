/*
* @file CRPageDrawBuffer.cpp
* @brief A data buffer contained CoolReader page image
*
*
* @date 2013-01-07
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include <math.h>
#include "CRPageDrawBuffer.h"

using namespace Platform;
using namespace Windows::ApplicationModel;
using namespace Windows::UI::Xaml;
using namespace Windows::Foundation;
using namespace Microsoft::WRL;
using namespace eReader::ViewModel;

#define CRPAGEDRAW_BPP 32

namespace eReader
{
namespace Presentation
{

CRPageDrawBuffer::CRPageDrawBuffer(Presentation::DXRenderer^ renderer, BookReader^ bookReader) :
	_bookReader(bookReader),
	_renderer(renderer),
	_pageDelta(0)
{
	initialize();
}

int CRPageDrawBuffer::BPP::get()
{
	return CRPAGEDRAW_BPP;
}

unsigned int CRPageDrawBuffer::DisplayPageDelta::get()
{
	return _pageDelta;
}

void CRPageDrawBuffer::updateDisplayPageDelta(int newPageDelta, const bool updateBuffer)
{
	_pageDelta = newPageDelta;
	if (updateBuffer)
	{
#ifndef TEST_WITH_BITMAP
		_copyPageImageToBitmap();
#endif
	}
}

void CRPageDrawBuffer::initialize()
{
#ifdef TEST_WITH_BITMAP
	_imageFile = new Utils::ImageFile();
	_imageFile->LoadAsync(Package::Current->InstalledLocation, "backgrounds\\tx_fabric.jpg");
#endif
}

bool CRPageDrawBuffer::draw(D2D1::Matrix3x2F const& transform)
{
#ifdef TEST_WITH_BITMAP
	assert(_renderer != nullptr);

	ComPtr<ID2D1DeviceContext> d2dDeviceContext;
	ComPtr<IWICImagingFactory2> wicFactory;

	// Obtain Direct2D device context and WIC imaging factory
	_renderer->getD2DDeviceContext(&d2dDeviceContext);
	_renderer->getWICImagingFactory(&wicFactory);
	if (_bitmap == nullptr)
	{
		if (!_imageFile->Ready())
		{
			// The image file is still be loaded, requesting redraw upon return.
			return true;
		}

		// Use Windows Imaging Component to decode an image file into a
		// WIC bitmap and then make a copy as a Direct2D bitmap in video
		// memory.
		ComPtr<IWICBitmapDecoder> decoder;
		ComPtr<IWICBitmapFrameDecode> frame;
		ComPtr<IWICFormatConverter> wicBitmap;

		DX::ThrowIfFailed(
			wicFactory->CreateDecoderFromStream(
				_imageFile.Get(),
				nullptr,
				WICDecodeMetadataCacheOnDemand,
				&decoder
				)
			);

		DX::ThrowIfFailed(
				decoder->GetFrame(0, &frame)
			);

		DX::ThrowIfFailed(
				wicFactory->CreateFormatConverter(&wicBitmap)
			);

		DX::ThrowIfFailed(
				wicBitmap->Initialize(
				frame.Get(),
				GUID_WICPixelFormat32bppPBGRA,
				WICBitmapDitherTypeNone,
				nullptr,
				0,
				WICBitmapPaletteTypeCustom
				)
			);

		//D2D1_BITMAP_PROPERTIES1
		D2D1_BITMAP_PROPERTIES1 prop = {D2D1::PixelFormat(), 96.0, 96.0};
		DX::ThrowIfFailed(
			d2dDeviceContext->CreateBitmapFromWicBitmap(
				wicBitmap.Get(),
				&prop,
				&_bitmap
				) 
			);
	}

	_renderer->drawBitmapFillMode(_bitmap.Get(), transform);

	return false;
#else
	_renderer->drawBitmapFillMode(_bitmap.Get(), transform);
	return false;
#endif
}

void CRPageDrawBuffer::measure(_In_ SIZE &parentSize, _Out_ RECT &bounds)
{
	if (_bitmap != nullptr)
	{
		D2D1_SIZE_F bitmapSize = _bitmap->GetSize();
		bounds.left = 0;
		bounds.top = 0;
		bounds.right = static_cast<LONG>(__min(parentSize.cx, bitmapSize.width + 0.5f));
		bounds.bottom = static_cast<LONG>(__min(parentSize.cy, bitmapSize.height + 0.5f));
	}
	else
	{
		bounds.left = 0;
		bounds.top = 0;
		bounds.right = parentSize.cx;
		bounds.bottom = parentSize.cy;
	}
}

bool CRPageDrawBuffer::_checkDrawBufSize(const SIZE& drawBufferSize, const int drawBufferBPP)
{
	assert(_renderer != nullptr);

	D2D1_SIZE_F bitmapSize = {0.0f, 0.0f};
	if (_bitmap != nullptr)
	{
		bitmapSize = _bitmap->GetSize();
	}

	if (drawBufferSize.cx != static_cast<int>(bitmapSize.width + 0.5) ||
		 drawBufferSize.cy != static_cast<int>(bitmapSize.height + 0.5) ||
		 drawBufferBPP != CRPageDrawBuffer::BPP)
	{
		// create a bitmap to match a draw buffer size
		if (_bitmap != nullptr)
		{
			_bitmap.Reset();
		}

		ComPtr<ID2D1DeviceContext> d2dDeviceContext;
		_renderer->getD2DDeviceContext(&d2dDeviceContext);

		Rect bounds = Window::Current->Bounds;
		D2D1_BITMAP_PROPERTIES1 bitmapProps = D2D1::BitmapProperties1(
			D2D1_BITMAP_OPTIONS_TARGET,
			D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));
		DX::ThrowIfFailed(
			d2dDeviceContext->CreateBitmap(
				D2D1::SizeU(static_cast<uint32>(bounds.Width), static_cast<uint32>(bounds.Height)),
				nullptr,    // sourceData
				0,          // pitch
				&bitmapProps,
				&_bitmap)
			);
	}

	return true;
}

void CRPageDrawBuffer::_copyPageImageToBitmap()
{
	// move page up/down to get the correct buffer data
	_goToPage(_pageDelta);

	// Get the draw buffer
	void* drawBuffer = nullptr;
	SIZE drawBufferSize;
	int drawBuferBPP = 0;

	_bookReader->getCurrentDrawBuffer(&drawBuffer, drawBufferSize, drawBuferBPP);
	if (drawBuffer == nullptr)
	{
		assert(false);

		// revert changes of the page changing
		_goToPage(_pageDelta * (-1));
		return;
	}
	assert(drawBufferSize.cx > 0);
	assert(drawBufferSize.cy > 0);
	assert(drawBuferBPP > 0);

	// check size of the draw buffer
	if (!_checkDrawBufSize(drawBufferSize, drawBuferBPP))
	{
		assert(false);

		// revert changes of the page changing
		_goToPage(_pageDelta * (-1));
		return;
	}

	// Copy memory into bitmap
	D2D1_SIZE_F bitmapSize = _bitmap->GetSize();
	D2D1_RECT_U dstRect = {0, 0, static_cast<UINT32>(bitmapSize.width), static_cast<UINT32>(bitmapSize.height)};

	DX::ThrowIfFailed(
		_bitmap->CopyFromMemory(
			&dstRect, 
			drawBuffer, 
			drawBufferSize.cx * (drawBuferBPP/8) 
		)
	);

	// revert changes of the page changing
	_goToPage(_pageDelta * (-1));
}

void CRPageDrawBuffer::_goToPage(int delta)
{
	if (delta > 0)
	{
		for (int count = 0; count < delta; count++)
		{
			_bookReader->pageDown();
		}
	}
	else
	{
		for (int count = 0; count < abs(static_cast<int>(delta)); count++)
		{
			_bookReader->pageUp();
		}
	}
}

} // Presentation
}
