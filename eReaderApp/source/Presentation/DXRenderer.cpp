/*
* @file DXDXRenderer.cpp
* @brief DirectX DXRenderer of book pages
*
* Rendering of the book content is done with a DirectX
*
* @date 2012-12-28
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "Utils\ImageFile.h"
#include "Utils\Helper.h"
#include <sstream>

using namespace Windows::ApplicationModel;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml;
using namespace Platform;
using namespace eReader::Utils;
using namespace concurrency;

namespace eReader
{
namespace Presentation
{

// Initialize all DirectX resources
DXRenderer::DXRenderer(
	Rect windowBounds,
	float dpi
	) :
	_windowBounds(windowBounds),
	_dpi(dpi),
	_featureLevel(),
	_d2dFactory(),
	_d2dDevice(),
	_d2dDeviceContext(),
	_wicFactory(),
	_dxgiDevice()
{
	_createDeviceIndependentResources();
	_createDeviceResources();
}

void DXRenderer::handleDeviceLost()
{
	_createDeviceResources();
	DeviceLost();
}

void DXRenderer::updateWindowSize()
{
	_windowBounds.X = _windowBounds.Y = 0.0f;
	_windowBounds.Width = Window::Current->Bounds.Width;
	_windowBounds.Height = Window::Current->Bounds.Height;
}

void DXRenderer::setDpi(_In_ float dpi)
{
	if (dpi != _dpi)
	{
		_dpi = dpi;

		_d2dDeviceContext->SetDpi(_dpi, _dpi);
	}
}

void DXRenderer::drawBitmapFillMode(_In_ ID2D1Bitmap* bitmap, D2D1::Matrix3x2F const& transform)
{
	assert(bitmap != nullptr);

	D2D1_RECT_F destBounds = { _windowBounds.Left, _windowBounds.Top, _windowBounds.Right, _windowBounds.Bottom };
	D2D_SIZE_F srcSize = bitmap->GetSize();
	D2D1_RECT_F srcBounds = {0, 0, srcSize.width, srcSize.height};

	_d2dDeviceContext->SetTransform(&transform);
	_d2dDeviceContext->DrawBitmap(
		bitmap,
		&destBounds,
		1.0f,
		D2D1_INTERPOLATION_MODE_HIGH_QUALITY_CUBIC,
		&srcBounds
		);

#if 0
	std::stringstream stream;
	stream << "drawBitmapFillMode, srcSize.cx=" << srcSize.width << " srcSize.cy=" << srcSize.height << "; destBounds.cx=" << destBounds.right << " destBounds.cy=" << destBounds.bottom;
	Utils::Helper::outputDebugString(stream.str().c_str());
#endif
}

void DXRenderer::getDXGIDevice(_Outptr_ IDXGIDevice** dxgiDevice)
{
	*dxgiDevice = ComPtr<IDXGIDevice>(_dxgiDevice).Detach();
}

void DXRenderer::getD2DDeviceContext(_Outptr_ ID2D1DeviceContext** d2dDeviceContext)
{
	*d2dDeviceContext = ComPtr<ID2D1DeviceContext>(_d2dDeviceContext).Detach();
}

void DXRenderer::getWICImagingFactory(_Outptr_ IWICImagingFactory2** wicFactory)
{
	*wicFactory = ComPtr<IWICImagingFactory2>(_wicFactory).Detach();
}

void DXRenderer::_createDeviceIndependentResources()
{
	D2D1_FACTORY_OPTIONS options;
	ZeroMemory(&options, sizeof(D2D1_FACTORY_OPTIONS));

#if defined(_DEBUG)
	// If the project is in a debug build, enable Direct2D debugging via Direct2D SDK layer.
	// Enabling SDK debug layer can help catch coding mistakes such as invalid calls and
	// resource leaking that needs to be fixed during the development cycle.
	options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif

	DX::ThrowIfFailed(
		D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		__uuidof(ID2D1Factory1),
		&options,
		&_d2dFactory
		)
		);

	DX::ThrowIfFailed(
		CoCreateInstance(
		CLSID_WICImagingFactory,
		nullptr,
		CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&_wicFactory)
		)
		);
}

void DXRenderer::_createDeviceResources()
{
	// This flag adds support for surfaces with a different color channel ordering than the API default.
	// It is recommended usage, and is required for compatibility with Direct2D.
	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if defined(_DEBUG)
	// If the project is in a debug build, enable debugging via SDK Layers with this flag.
	creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// This array defines the set of DirectX hardware feature levels this app will support.
	// Note the ordering should be preserved.
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	// Create the D3D11 API device object, and get a corresponding context.
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dContext;
	DX::ThrowIfFailed(
		D3D11CreateDevice(
		nullptr,                    // specify null to use the default adapter
		D3D_DRIVER_TYPE_HARDWARE,
		0,                          // leave as 0 unless software device
		creationFlags,              // optionally set debug and Direct2D compatibility flags
		featureLevels,              // list of feature levels this app can support
		ARRAYSIZE(featureLevels),   // number of entries in above list
		D3D11_SDK_VERSION,          // always set this to D3D11_SDK_VERSION for Metro style apps
		&d3dDevice,                 // returns the Direct3D device created
		&_featureLevel,            // returns feature level of device created
		&d3dContext                 // returns the device immediate context
		)
		);

	// Obtain the underlying DXGI device of the Direct3D11.1 device.
	DX::ThrowIfFailed(
		d3dDevice.As(&_dxgiDevice)
		);

	// Obtain the Direct2D device for 2-D rendering.
	DX::ThrowIfFailed(
		_d2dFactory->CreateDevice(_dxgiDevice.Get(), &_d2dDevice)
		);

	// And get its corresponding device context object.
	DX::ThrowIfFailed(
		_d2dDevice->CreateDeviceContext(
		D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
		&_d2dDeviceContext
		)
		);

	// Since this device context will be used to draw content onto XAML surface image source,
	// it needs to operate as pixels. Setting pixel unit mode is a way to tell Direct2D to treat
	// the incoming coordinates and vectors, typically as DIPs, as in pixels.
	_d2dDeviceContext->SetUnitMode(D2D1_UNIT_MODE_PIXELS);

	// Despite treating incoming values as pixels, it is still very important to tell Direct2D
	// the logical DPI the application operates on. Direct2D uses the DPI value as a hint to
	// optimize internal rendering policy such as to determine when is appropriate to enable
	// symmetric text rendering modes. Not specifying the appropriate DPI in this case will hurt
	// application performance.
	_d2dDeviceContext->SetDpi(_dpi, _dpi);

	// When an application performs animation or image composition of graphics content, it is important
	// to use Direct2D grayscale text rendering mode rather than ClearType. The ClearType technique
	// operates on the color channels and not the alpha channel, and therefore unable to correctly perform
	// image composition or sub-pixel animation of text. ClearType is still a method of choice when it
	// comes to direct rendering of text to the destination surface with no subsequent composition required.
	_d2dDeviceContext->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE);
}

} // Presentation
}
