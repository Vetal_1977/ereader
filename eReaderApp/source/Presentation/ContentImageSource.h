/*
* @file ContentImageSource.h
* @brief Content image source is responsible for drawing a page content.
*
* The page content is drawn as an image (via renderer) and providing it for displaying
*
* @date 2013-01-02
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include <windows.ui.xaml.media.dxinterop.h>
#include <wrl.h>
#include "CRPageDrawBuffer.h"

namespace eReader
{
	namespace Presentation
	{
		ref class DXRenderer;

		//  Native host of VirtualSurfaceImageSource, which implements update callback.
		//  The callback is called whenever the application needs to populate new content to
		//  the image source which occurs when the image source hosting UI element repaints.
		//
		class ContentImageSource : public IVirtualSurfaceUpdatesCallbackNative
		{
		public:
			// Constructs this object
			// @param renderer DirectX renderer
			// @param content Content buffer to draw
			ContentImageSource(DXRenderer^ renderer, CRPageDrawBuffer^ content);

			//
			// IVirtualSurfaceUpdatesCallbackNative
			//

			// This method is called when the framework needs to update region managed by
			// the virtual surface image source.
			virtual HRESULT STDMETHODCALLTYPE UpdatesNeeded() override;

			//
			// IUnknown
			//
			virtual HRESULT STDMETHODCALLTYPE QueryInterface(
				REFIID uuid,
				_Outptr_ void** object
				) override;

			virtual ULONG STDMETHODCALLTYPE AddRef() override;

			virtual ULONG STDMETHODCALLTYPE Release() override;

			//
			// ContentImageSource
			//

			// Returns loaded image source
			// @return Image source object that can be operated by GUI elements
			Windows::UI::Xaml::Media::ImageSource^ getImageSource();

			// Returns current image size
			// @return SIZE structure containing the size of the current image
			//SIZE getImageSize();

			// Updates data draw buffer for this image source
			// @param content Data buffer that should be drawn on the screen
			void updateContent(_In_ CRPageDrawBuffer^ content);

		private:
			// Initializes drawing capabilities
			void _initialize();

			// Measures the content size
			// @param contentSize The size of the drawing content
			void _measure(_Out_ SIZE* contentSize);

			// Draws a content within the specified bounds
			// @param drawingBounds Bounds for the drawing content
			bool _draw(RECT const& drawingBounds);

		private:
			// This class is used to ensure proper pairing of IVirtualSurfaceImageSourceNative::BeginDraw
			// and EndDraw within an error condition. The XAML framework expects EndDraw to be called whenever
			// BeginDraw is called - even in failure cases
			class ImageSourceDrawHelper
			{
			public:
				ImageSourceDrawHelper(
					IVirtualSurfaceImageSourceNative *imageSourceNative,
					_In_ RECT updateRect,
					_Out_ IDXGISurface **pSurface,
					_Out_ POINT *offset,
					HRESULT *hr
					);
				~ImageSourceDrawHelper();

			private:
				Microsoft::WRL::ComPtr<IVirtualSurfaceImageSourceNative> _imageSourceNative;
				HRESULT *_hr;
			};

			ULONG _refCount;
			DXRenderer^ _renderer;
			CRPageDrawBuffer^ _content;
			SIZE _contentSize;
			Windows::UI::Xaml::Media::Imaging::VirtualSurfaceImageSource^ _imageSource;
			Microsoft::WRL::ComPtr<IVirtualSurfaceImageSourceNative> _imageSourceNative;
			bool _isOpaque;
		};

	} // Presentation
}
