/*
* @file ContentImageSource.cpp
* @brief Content image source is responsible for drawing a page content.
*
* The page content is drawn as an image (via _renderer) and providing it for displaying
*
* @date 2013-01-03
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "ContentImageSource.h"
#include "Utils\Helper.h"

using namespace Microsoft::WRL;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Platform;
using namespace eReader::Model;

namespace eReader
{
namespace Presentation
{

ContentImageSource::ContentImageSource(DXRenderer^ _renderer, CRPageDrawBuffer^ content) :
	_refCount(),
	_renderer(_renderer),
	_content(content),
	_contentSize(),
	_imageSource(nullptr),
	_imageSourceNative(),
	_isOpaque(false)
{
}

HRESULT STDMETHODCALLTYPE ContentImageSource::UpdatesNeeded()
{
	HRESULT hr = S_OK;

	Utils::Helper::outputDebugString("UpdatesNeeded");
	try
	{
		ULONG drawingBoundsCount = 0;

		DX::ThrowIfFailed(
			_imageSourceNative->GetUpdateRectCount(&drawingBoundsCount)
			);

		std::unique_ptr<RECT[]> drawingBounds(new RECT[drawingBoundsCount]);

		DX::ThrowIfFailed(
			_imageSourceNative->GetUpdateRects(drawingBounds.get(), drawingBoundsCount)
			);

		// This code doesn't try to coalesce multiple drawing bounds into one. Although that
		// extra process will reduce the number of draw calls, it requires the virtual surface
		// image source to manage non-uniform tile size, which requires it to make extra copy
		// operations to the compositor. By using the drawing bounds it directly returns, which are
		// of non-overlapping uniform tile size, the compositor is able to use these tiles directly,
		// which can greatly reduce the amount of memory needed by the virtual surface image source.
		// It will result in more draw calls though, but Direct2D will be able to accommodate that
		// without significant impact on presentation frame rate.
		for (ULONG i = 0; i < drawingBoundsCount; ++i)
		{
			if (_draw(drawingBounds[i]))
			{
				// Drawing isn't complete. This can happen when the content is still being
				// asynchronously loaded. Inform the image source to invalidate the drawing
				// bounds so that it calls back to redraw.
				DX::ThrowIfFailed(
					_imageSourceNative->Invalidate(drawingBounds[i])
					);
			}
		}
	}
	catch (Exception^ exception)
	{
		hr = exception->HResult;

		if (hr == D2DERR_RECREATE_TARGET ||
			hr == DXGI_ERROR_DEVICE_REMOVED)
		{
			// If a device lost error is encountered, notify the _renderer. The _renderer will then
			// recreate the device and fire the DeviceLost event so that all listeners can take
			// an appropriate action.

			_renderer->handleDeviceLost();

			// Invalidate the image source so that it calls back to redraw
			RECT bounds = { 0, 0, _contentSize.cx, _contentSize.cy };
			_imageSourceNative->Invalidate(bounds);
		}
	}

	return hr;
}

HRESULT STDMETHODCALLTYPE ContentImageSource::QueryInterface(
	REFIID uuid,
	_Outptr_ void** object
	)
{
	if (    uuid == IID_IUnknown
		||  uuid == __uuidof(IVirtualSurfaceUpdatesCallbackNative)
		)
	{
		*object = this;
		AddRef();
		return S_OK;
	}
	else
	{
		*object = nullptr;
		return E_NOINTERFACE;
	}
}

ULONG STDMETHODCALLTYPE ContentImageSource::AddRef()
{
	return static_cast<ULONG>(InterlockedIncrement(&_refCount));
}

ULONG STDMETHODCALLTYPE ContentImageSource::Release()
{
	ULONG newCount = static_cast<ULONG>(InterlockedDecrement(&_refCount));

	if (newCount == 0)
		delete this;

	return newCount;
}

ImageSource^ ContentImageSource::getImageSource()
{
	if (_imageSource == nullptr)
	{
		_initialize();
	}

	return _imageSource;
}

void ContentImageSource::updateContent(_In_ CRPageDrawBuffer^ content)
{
	if (_imageSourceNative != nullptr)
	{
		_content = content;

		_measure(&_contentSize);

		DX::ThrowIfFailed(
			_imageSourceNative->Resize(_contentSize.cx, _contentSize.cy)
			);

		_renderer->updateWindowSize();

		RECT bounds = {0};
		bounds.right =  _contentSize.cx;
		bounds.bottom = _contentSize.cy;

		DX::ThrowIfFailed(
			_imageSourceNative->Invalidate(bounds)
			);

		Utils::Helper::outputDebugString("updateContent");
		try
		{	
			if (_draw(bounds))
			{
				// Drawing isn't complete. This can happen when the content is still being
				// asynchronously loaded. Inform the image source to invalidate the drawing
				// bounds so that it calls back to redraw.
				DX::ThrowIfFailed(
					_imageSourceNative->Invalidate(bounds)
					);
			}
		}
		catch (Exception^ exception)
		{
			HRESULT hr = exception->HResult;

			if (hr == D2DERR_RECREATE_TARGET ||
				hr == DXGI_ERROR_DEVICE_REMOVED)
			{
				// If a device lost error is encountered, notify the _renderer. The _renderer will then
				// recreate the device and fire the DeviceLost event so that all listeners can take
				// an appropriate action.

				_renderer->handleDeviceLost();

				// Invalidate the image source so that it calls back to redraw
				RECT bounds = { 0, 0, _contentSize.cx, _contentSize.cy };
				_imageSourceNative->Invalidate(bounds);
			}
		}
	}
}

void ContentImageSource::_initialize()
{
	assert(_renderer != nullptr);

	// measure the content and store its size.
	_measure(&_contentSize);

	// Create an image source at the initial pixel size.
	// The last parameter helps improve rendering performance when the image source is known to be opaque.
	_imageSource = ref new VirtualSurfaceImageSource(_contentSize.cx, _contentSize.cy, _isOpaque);

	ComPtr<IUnknown> unknown(reinterpret_cast<IUnknown*>(_imageSource));
	unknown.As(&_imageSourceNative);

	// Set DXGI device to the image source
	ComPtr<IDXGIDevice> dxgiDevice;
	_renderer->getDXGIDevice(&dxgiDevice);
	_imageSourceNative->SetDevice(dxgiDevice.Get());

	// Register image source's update callback so update can be made to it.
	_imageSourceNative->RegisterForUpdatesNeeded(this);
}

void ContentImageSource::_measure(_Out_ SIZE* contentSize)
{
	RECT contentBounds = {0};
	SIZE parentSize = {
		static_cast<LONG>(Window::Current->Bounds.Width), 
		static_cast<LONG>(Window::Current->Bounds.Height)};

	// measure the drawing content
	_content->measure(parentSize, contentBounds);

	contentSize->cx = contentBounds.right - contentBounds.left;
	contentSize->cy = contentBounds.bottom - contentBounds.top;
}

bool ContentImageSource::_draw(RECT const& drawingBounds)
{
	ComPtr<IDXGISurface> dxgiSurface;
	POINT surfaceOffset = {0};
	bool needRedraw = false;

	HRESULT hr = S_OK;
	{
		// If the underlying device is lost, the virtual surface image source may return a
		// device lost error from BeginDraw. In this case, the error will be thrown here
		// and caught in UpdatesNeeded
		ImageSourceDrawHelper imageSourceDrawHelper(
			_imageSourceNative.Get(),
			drawingBounds,
			&dxgiSurface,
			&surfaceOffset,
			&hr
			);

		ComPtr<ID2D1DeviceContext> d2dDeviceContext;
		_renderer->getD2DDeviceContext(&d2dDeviceContext);

		ComPtr<ID2D1Bitmap1> bitmap;
		DX::ThrowIfFailed(
			d2dDeviceContext->CreateBitmapFromDxgiSurface(
			dxgiSurface.Get(),
			nullptr,
			&bitmap
			)
			);

		// Begin the drawing batch
		d2dDeviceContext->BeginDraw();

		// Scale content design coordinate to the display coordinate,
		// then translate the drawing to the designated place on the surface.
		D2D1::Matrix3x2F transform =
			D2D1::Matrix3x2F::Scale(
			1.0f,
			1.0f
			) *
			D2D1::Matrix3x2F::Translation(
			static_cast<float>(surfaceOffset.x - drawingBounds.left),
			static_cast<float>(surfaceOffset.y - drawingBounds.top)
			);

		// Prepare to draw content. This is the appropriate time for content element
		// to draw to an intermediate if there is any. It is important for performance
		// reason that SetTarget isn't called too frequently. Preparing the intermediates
		// upfront reduces the number of times the render target switches back and forth.
		/*needRedraw = _content->PrepareToDraw(
			m_document,
			transform
			);*/

		if (!needRedraw)
		{
			// Set the render target to surface given by the framework
			d2dDeviceContext->SetTarget(bitmap.Get());

			d2dDeviceContext->SetTransform(D2D1::IdentityMatrix());

			// Constrain the drawing only to the designated portion of the surface
			d2dDeviceContext->PushAxisAlignedClip(
				D2D1::RectF(
				static_cast<float>(surfaceOffset.x),
				static_cast<float>(surfaceOffset.y),
				static_cast<float>(surfaceOffset.x + (drawingBounds.right - drawingBounds.left)),
				static_cast<float>(surfaceOffset.y + (drawingBounds.bottom - drawingBounds.top))
				),
				D2D1_ANTIALIAS_MODE_ALIASED
				);

			// The Clear call must follow and not precede the PushAxisAlignedClip call.
			// Placing the Clear call before the clip is set violates the contract of the
			// virtual surface image source in that the application draws outside the
			// designated portion of the surface the image source hands over to it. This
			// violation won't actually cause the content to spill outside the designated
			// area because the image source will safeguard it. But this extra protection
			// has a runtime cost associated with it, and in some drivers this cost can be
			// very expensive. So the best performance strategy here is to never create a
			// situation where this protection is required. Not drawing outside the appropriate
			// clip does that the right way.
			d2dDeviceContext->Clear(D2D1::ColorF(0, 0));

			// draw the content
			needRedraw = _content->draw(transform);

			d2dDeviceContext->PopAxisAlignedClip();

			d2dDeviceContext->SetTarget(nullptr);
		}

		// End the drawing
		// UpdatesNeeded handles D2DERR_RECREATETARGET, so it is okay to throw this error here.
		DX::ThrowIfFailed(
			d2dDeviceContext->EndDraw()
			);
	}

	DX::ThrowIfFailed(hr);

	return needRedraw;
}

ContentImageSource::ImageSourceDrawHelper::ImageSourceDrawHelper(
	IVirtualSurfaceImageSourceNative *imageSourceNative,
	_In_ RECT updateRect,
	_Out_ IDXGISurface **pSurface,
	_Out_ POINT *offset,
	_Inout_ HRESULT *hr
	) :
_imageSourceNative(imageSourceNative),
	_hr(hr)
{
	DX::ThrowIfFailed(
		imageSourceNative->BeginDraw(
		updateRect,
		pSurface,
		offset
		)
		);
}

ContentImageSource::ImageSourceDrawHelper::~ImageSourceDrawHelper()
{
	// It is not safe to throw from a destructor. Instead we pass the error back to the caller
	// and let the caller throw the error.
	*_hr = _imageSourceNative->EndDraw();
}

} // Presentation
}
