/*
* @file DXRenderer.h
* @brief DirectX renderer of book pages
*
* Rendering of the book content is done with a DirectX
*
* @date 2012-12-28
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include <initguid.h>
#include <wrl.h>
#include <d3d11_1.h>
#include <dxgi1_2.h>
#include <d2d1_1.h>
#include <d2d1effects.h>
#include <dwrite_1.h>
#include <wincodec.h>


namespace eReader
{
	namespace Presentation
	{
		delegate void DeviceLostEventHandler();

		//
		// Helper class that initializes and maintains the DirectX API objects in one place.
		//
		ref class DXRenderer sealed
		{
		internal:
			// "Device Lost" event
			event DeviceLostEventHandler^ DeviceLost;

		internal:
			// Constructs DirectX renderer
			// @param windowBoaunds Window bounds for a DirectX presentation
			// @param dpi Dots per inch for the DirectX device context
			DXRenderer(Windows::Foundation::Rect windowBounds, float dpi);

		internal:
			// Handles "Device Lost" - reallocates necessary resources in this case
			void handleDeviceLost();

			// Invoked when the window size has been updated and the DirectX needs to adjust itself to a new size
			void updateWindowSize();

			// Sets DPI for DirectX device context
			// @param dpi Dots per inch value
			void setDpi(_In_ float dpi);

			// Draws a bitmap in "fill" mode: the bitmap will be stretched or squeezed into window bounds
			// @param bitmap Bitmap to draw
			// @param transform Transformation matrix to display page properly
			void drawBitmapFillMode(_In_ ID2D1Bitmap* bitmap, D2D1::Matrix3x2F const& transform);

			//-----------------------------------------------------
			// TODO: remove "get..." methods and provide methods to draw, prepare, etc. DirectX
			// without exposing DirectX or Direct2D interfaces

			// Gets the DirectX Graphics Infrastructure interface for object that produces image data
			// @param dxgiDevice Requested DXGI interface
			void getDXGIDevice(_Outptr_ IDXGIDevice** dxgiDevice);

			// Gets the Direct2D device context
			// @param d2dDeviceContext Direct2D device context. It can be used for drawing operations
			void getD2DDeviceContext(_Outptr_ ID2D1DeviceContext** d2dDeviceContext);

			// Gets the WIC imaging factory object
			// @param wicFactory WIC imaging factory interface. It can be used for drawing operations
			void getWICImagingFactory(_Outptr_ IWICImagingFactory2** wicFactory);
			//-----------------------------------------------------
			
		private:
			// Create resources required independent of hardware (Direct2D factory, etc.)
			void _createDeviceIndependentResources();

			// These are the resources that depend on hardware (Direct2D device, device context, etc.)
			void _createDeviceResources();

		private:
			Microsoft::WRL::ComPtr<ID2D1Factory1> _d2dFactory;
			Microsoft::WRL::ComPtr<ID2D1Device> _d2dDevice;
			Microsoft::WRL::ComPtr<ID2D1DeviceContext> _d2dDeviceContext;
			Microsoft::WRL::ComPtr<IWICImagingFactory2> _wicFactory; // <! WIC [Windows Imaging Component] factory
			Microsoft::WRL::ComPtr<IDXGIDevice> _dxgiDevice; // <! DXGI [DirectX Graphic Infrastructure] device
			D3D_FEATURE_LEVEL _featureLevel;
			Windows::Foundation::Rect _windowBounds;
			float _dpi;
		};

		//
		// Helper utilities to make DX APIs work with exceptions in the samples apps.
		//
		namespace DX
		{
			inline void ThrowIfFailed(HRESULT hr)
			{
				if (FAILED(hr))
				{
					// Set a breakpoint on this line to catch DX API errors.
					throw Platform::Exception::CreateException(hr);
				}
			}
		}
	} // Presentation
}
