/*
* @file CRPageDrawBuffer.h
* @brief A data buffer contained CoolReader page image
*
*
* @date 2013-01-07
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

#include "lvdocview.h"
#include "DXRenderer.h"
#include "Utils\ImageFile.h"

//#define TEST_WITH_BITMAP

namespace eReader
{
	namespace Presentation
	{
		ref class CRPageDrawBuffer
		{
		internal:
			// Constructs this object
			// @param renderer Renderer to bring pages on the screen
			// @param bookReader Instance of a BookReader class. It is used to get buffer to display and flip pages
			CRPageDrawBuffer(Presentation::DXRenderer^ renderer, ViewModel::BookReader^ bookReader);

			static property int BPP { int get(); }
			
		internal:
			// The page delta to display must be in a range [0;2]
			property unsigned int DisplayPageDelta { unsigned int get(); }

			// Update a display page number
			// @param newPageDelta New page delta, must be in a range [0;2]
			// @param updateBuffer true, if the draw buffer must be updated with a new content
			void updateDisplayPageDelta(int newPageDelta, const bool updateBuffer);

			// Initializes draw buffer data
			void initialize();

			// Draws buffer on the screen
			// @param transform Transformation matrix to display pages properly by flipping
			bool draw(D2D1::Matrix3x2F const& transform);

			// Measures bounds to draw the buffer
			// @param parentSize Size of the parent window (application window in a case of store app)
			// @param bounds Bounds for the drawing area of the buffer; should not exceed the parent size for the reader
			void measure(_In_ SIZE &parentSize, _Out_ RECT &bounds);

		private:
			// Check the size of the draw buffer with a size of a target bitmap.
			// The sizes and BPP must match
			// @param drawBufferSize Size of the draw buffer of the current book page
			// @param drawBufferBPP Bits per pixel for the draw buffer
			// @return true, if the sizes and BPP are identical, false - otherwise
			bool _checkDrawBufSize(_In_ const SIZE& drawBufferSize, _In_ const int drawBufferBPP);

			// Copies the CR page image into _bitmap
			void _copyPageImageToBitmap();

			// Scroll to the page relative to the specified delta
			void _goToPage(int delta);

		private:
			ViewModel::BookReader^ _bookReader;
			Presentation::DXRenderer^ _renderer;
			Microsoft::WRL::ComPtr<ID2D1Bitmap1> _bitmap; // <! target bitmap to draw a page
			int _pageDelta;

#ifdef TEST_WITH_BITMAP
			Platform::String^ _filename;
			Microsoft::WRL::ComPtr<Utils::ImageFile> _imageFile;
#endif

		};
	} // Presentation
}

