/*
 * @file ResLoader.h
 * @brief Static class to load resources specified by a string
 *
 * @date 2012-12-25
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#pragma once

namespace eReader
{
namespace Utils
{

ref class ResLoader
{
public:
	static Platform::String^ getString(Platform::String^ resourceString);

private:
	static Windows::ApplicationModel::Resources::ResourceLoader^ _resourceLoader;
};

} // Utils
}

