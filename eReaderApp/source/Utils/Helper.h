/*
 * @file Helper.h
 * @brief Static class of diverse helper functions to be used in a project scope
 *
 * @date 2013-05-12
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#pragma once

namespace eReader
{
namespace Utils
{

class Helper
{
public:
	// Creates and writes a debug string into VS output window.
	// The output string format is <current time>: <functionName>
	// @param functionName Name of the calling function
	static void outputDebugString(const char* functionName);
};

} // Utils
}

