/*
* @file ErrorCodes.h
* @brief Internal error codes to be used in a scope of the application
*
* @date 2013-01-27
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#pragma once

namespace eReader
{
	//
	// Internal error codes of reader
	//
	enum ErrorCodes
	{
		// File storage
		FS_ErrAddBook = -100
	};

}
