
#include "pch.h"
#include "ResLoader.h"

using namespace Platform;
using namespace Windows::ApplicationModel::Resources;

namespace eReader
{
namespace Utils
{

ResourceLoader^ ResLoader::_resourceLoader = nullptr;

String^ ResLoader::getString(String^ resourceString)
{
	// lazy initialization
	if (_resourceLoader == nullptr)
	{
		_resourceLoader = ref new ResourceLoader();
	}
	return _resourceLoader->GetString(resourceString);
}

} // Utils
}