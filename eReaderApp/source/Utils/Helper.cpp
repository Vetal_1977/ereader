/*
 * @file Helper.cpp
 * @brief Static class of diverse helper functions to be used in a project scope
 *
 * @date 2013-05-12
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#include "pch.h"
#include "Helper.h"
#include <sstream>
#include <atlbase.h>


namespace eReader
{
namespace Utils
{

void Helper::outputDebugString(const char* functionName)
{
#ifdef _DEBUG 
	static __time64_t now;
	static struct tm when;
	time(&now);
	_localtime64_s(&when, &now);
	
	static wchar_t nowStr[80];
	wcsftime(nowStr, sizeof(nowStr)/sizeof(wchar_t), L"%H:%M:%S", &when);

	static std::wstringstream stream;
	stream << nowStr << ": " << functionName << std::endl;
	OutputDebugString(stream.str().c_str());
#else
	functionName; // to avoid compiler warning
#endif
}

} // Utils
}