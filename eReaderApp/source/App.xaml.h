﻿//
// App.xaml.h
// Declaration of the App class
//

#pragma once

#include "App.g.h"
#include "View\ReaderPage.h" // Required by generated header
#include "View\BookOperationBasePage.h" // Required by generated header
#include "View\LibraryPage.g.h" // Required by generated header (for FilteredLibraryPage)
#include "View\DesignTimeData.h"
#include "ViewModel\ViewModelLocator.h"

#include "Common\IntegerToBooleanConverter.h"
#include "Common\DoubleToStringSliderConverter.h"
#include "Common\BooleanToVisibilityConverter.h"

namespace eReader
{
	/// <summary>
	/// Provides application-specific behavior to supplement the default Application class.
	/// </summary>
	ref class App sealed
	{
	public:
		// Initializes the singleton application object. This is the first line of authored code
		// executed, and as such is the logical equivalent of main() or WinMain().
		App();

		// Invoked when the application is launched normally by the end user. Other entry points will
		// be used when the application is launched to open a specific file, to display search results,
		// and so forth.
		// @param args Details about the launch request and process
		virtual void OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args) override;

	public:
		property ViewModel::BookReader^ eBookReader { ViewModel::BookReader^ get(); }
		property Presentation::DXRenderer^ Renderer { Presentation::DXRenderer^ get(); }

	private:
		// Invoked when application execution is being suspended.  Application state is saved
		// without knowing whether the application will be terminated or resumed with the contents
		// of memory still intact.
		// @param sender The source of the suspend request
		// @param e Details about the suspend request
		void OnSuspending(Platform::Object^ sender, Windows::ApplicationModel::SuspendingEventArgs^ e);

		// Invoked when application suspending is being resumed. It is a good point of restoring
		// some internally saved states
		// @param sender The source of the resume request
		// @param e Details about the resume request
		void OnResuming(Platform::Object^ sender, Platform::Object^ e);

		// Invoked when unhandled application occurred
		// @param sender The source of the exception
		// @param e Details about the unhandled exception
		void OnUnhandledException(Platform::Object^ sender, Windows::UI::Xaml::UnhandledExceptionEventArgs^ e);

	private:
		ViewModel::BookReader^ _bookReader;
		Presentation::DXRenderer^ _renderer;
		Callisto::Controls::SettingsManagement::AppSettings^ _appSettings;
	};
}
