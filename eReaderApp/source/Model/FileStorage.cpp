/*
* @file FileStorage.cpp
* @brief Database storage to save book information, settings, etc.
*
* @date 2013-01-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "FileStorage.h"

using namespace Platform;
using namespace concurrency;
using namespace Windows::UI::Xaml::Media::Imaging;
using namespace Windows::Storage;
using namespace Windows::ApplicationModel;
using namespace eReader::Utils;

namespace eReader
{
namespace Model
{
///
/// FileStorage
///
String^ FileStorage::CR3_SETTINGS_DIRECTORY = ref new String(L"CR3");

FileStorage::FileStorage() :
	_csBookOperation()
{
}

task<void> FileStorage::addBookAsync(StorageFile^ srcStorageFile, LVDocView* lvDocView, BookData^ bookData)
{
	assert(srcStorageFile != nullptr);
	assert(bookData != nullptr);
	assert(lvDocView != nullptr);

	auto localFolder = Windows::Storage::ApplicationData::Current->LocalFolder;
	String^ fullDirName = localFolder->Path + L"\\" + bookData->Directory;

	// create a directory to save book data
	task<StorageFolder^> createTargetFolderTask(localFolder->CreateFolderAsync(bookData->Directory, CreationCollisionOption::FailIfExists));
	return createTargetFolderTask.then([=](StorageFolder^ targetBookFolder)
	{
		// copy book to target folder
		String^ bookPath = fullDirName + L"\\" + bookData->BookFilename;
		task<StorageFile^> copyBookTask(srcStorageFile->CopyAsync(targetBookFolder, bookData->BookFilename));
		return copyBookTask.then([=](StorageFile^ targetBook)
		{
			_csBookOperation.lock(); // LVDocView is used in exclusive mode

			String^ bookPath = fullDirName + L"\\" + bookData->BookFilename;
			if (!lvDocView->LoadDocument(bookPath->Data()))
			{
				_csBookOperation.unlock();
				return deleteBook(bookData, lvDocView).then([=]()
				{
					throw ReaderException(L"CannotAddBook", MessageStringType::ResourceString, ErrorCodes::FS_ErrAddBook);
				});
			}

			// get and save book information
			bookData->Title = ref new String(lvDocView->getTitle().c_str());
			if (bookData->Title->IsEmpty())
			{
				bookData->Title = ResLoader::getString("UnknownTitle");
			}
			bookData->Authors = ref new String(lvDocView->getAuthors().c_str());
			if (bookData->Authors->IsEmpty())
			{
				bookData->Authors = ResLoader::getString("UnknownAuthor");
			}
			bookData->Language = ref new String(lvDocView->getLanguage().c_str());
			if (bookData->Language->IsEmpty())
			{
				bookData->Language = ref new String(L"");
			}
			bookData->Annotation = ref new String(L"");

			// extract cover image and save it
			LVStreamRef imageStream = lvDocView->getCoverPageImageStream();
			
			if ( imageStream.isNull() ) 
			{
				// first, close the current document und unlock the doc view
				lvDocView->close(); // close the CR LVDocView - we do not need it anymore
				_csBookOperation.unlock(); // allow another thread to use LVDocView

				// second, copy 'no cover image'
				task<StorageFile^> getResFileTask(Package::Current->InstalledLocation->
					GetFileAsync(ref new String(L"res\\NoCoverImage.png")));
				return getResFileTask.then([=](StorageFile^ resFile)
				{
					task<StorageFile^> copyResFileTask(
						resFile->CopyAsync(targetBookFolder, bookData->CoverImageFilename, NameCollisionOption::ReplaceExisting));
					return copyResFileTask.then([=](StorageFile^ coverImageFile)
					{
					});
				});
			}
			else
			{
				// first, load the cover image
				String^ coverImagePath = fullDirName + L"\\" + bookData->CoverImageFilename;
				LVStreamRef outStream = LVOpenFileStream(coverImagePath->Data(), LVOM_WRITE);
				if ( !outStream.isNull() ) 
				{
					LVPumpStream( outStream.get(), imageStream.get() );
				}

				// second, close the current document und unlock the doc view
				lvDocView->close(); // close the CR LVDocView - we do not need it anymore
				_csBookOperation.unlock(); // allow another thread to use LVDocView

				task<void> emptyTask = create_task([=]()
				{
				});
				return emptyTask;
			}
		});
	});
}

task<void> FileStorage::deleteBook(BookData^ bookData,  LVDocView* lvDocView)
{
	assert(bookData != nullptr);
	assert(lvDocView != nullptr);

	// get the book folder
	task<StorageFolder^> getBookFolderTask(Windows::Storage::ApplicationData::Current->LocalFolder->
		GetFolderAsync(bookData->Directory));
	return getBookFolderTask.then([=](StorageFolder^ bookFolder)
	{
		// delete folder
		task<void> deleteBookFolderTask(bookFolder->DeleteAsync(StorageDeleteOption::Default));
		return deleteBookFolderTask.then([=]()
		{
			// clear history for the book
			//CRFileHist* bookHistory = lvDocView->
			return;
		});
	});
}

} // Model
}
