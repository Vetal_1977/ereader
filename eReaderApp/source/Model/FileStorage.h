/*
* @file FileStorage.h
* @brief File storage to save books, CoolReader settings, etc. 
*
* @date 2013-01-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include <collection.h>
#include "lvdocview.h"
#include "BookData.h"

#pragma once

namespace eReader
{
	namespace Model
	{
		//
		// Class for accessing a book data source
		//
		 ref class FileStorage sealed
		{
		public:
			FileStorage();

		internal:
			// Adds a book into the file storage. All file operation are asynchronous
			// @param fullSrcBookPath Full path (directory + name) of the book to add (source)
			// @param lvDocView CR document view. Must not be nullptr and must be already initialized. 
			// @param BookData Parameters of the library book. 
			// Following parameters are "IN": book directory, book name, cover image file name. 
			// Following parameters are "OUT": title, authors, language 
			// It is used to extract additional information about a book (cover image, etc.)
			// @return Task of adding a book
			// @exception ReaderException Thrown if the book cannot be added
			concurrency::task<void> addBookAsync(_In_ Windows::Storage::StorageFile^ srcStorageFile, _In_ LVDocView* lvDocView, _In_ _Out_ BookData^ bookData);

			// Deletes specified book from the file storage
			// @param bookData Data of a book to delete
			// @param lvDocView CR document view. Must not be nullptr and must be already initialized. It is used to delete book history
			// @return Book deletion task
			concurrency::task<void> deleteBook(_In_ BookData^ bookData, _In_ LVDocView* lvDocView);

		private:
			//Windows::UI::Xaml::Media::Imaging::BitmapImage _createBitmapFromStream(_In_ const LVStreamRef &imageStream);

		private:
			static Platform::String^ CR3_SETTINGS_DIRECTORY;
			concurrency::critical_section _csBookOperation;
		};
	} // Model
}
