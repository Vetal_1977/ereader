/*
* @file DatabaseStorage.cpp
* @brief Database storage to save book information, settings, etc.
*
* @date 2013-01-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include <regex>
#include "DatabaseStorage.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;
using namespace Windows::Foundation;

namespace eReader
{
namespace Model
{
///
/// DatabaseStorage
///
String^ DatabaseStorage::STORAGE_NAME = ref new String(L"\\Library.db");

DatabaseStorage::DatabaseStorage() :
	_libraryDB(nullptr)
{
}

void DatabaseStorage::openBookDatabase()
{
	String^ dbPath = String::Concat(Windows::Storage::ApplicationData::Current->LocalFolder->Path, STORAGE_NAME);
	sqlite3_open16(dbPath->Data(), &_libraryDB);

	// check and create database structure
	_createDatabaseStructure();
}

void DatabaseStorage::closeBookDatabase()
{
	sqlite3_close(_libraryDB);
	_libraryDB = nullptr;
}

IVector<BookData^>^ DatabaseStorage::getLastReadBooks(const uint16 maxNoOfBooks)
{
	wchar_t sqlStmt[256];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"SELECT * FROM Books WHERE LastOpenedDate<>'' ORDER BY LastOpenedDate DESC LIMIT %d", maxNoOfBooks);
	
	IVector<BookData^>^ books = _getBooks(sqlStmt);
	return books;
}

IVector<BookData^>^ DatabaseStorage::getLastAddedBooks(const uint16 maxNoOfBooks)
{
	wchar_t sqlStmt[256];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"SELECT * FROM Books ORDER BY StoreDate DESC LIMIT %d", maxNoOfBooks);
	
	IVector<BookData^>^ books = _getBooks(sqlStmt);
	return books;
}

IVector<BookData^>^ DatabaseStorage::getBooksByTitle()
{
	const wchar_t* sqlStmt = L"SELECT * FROM Books ORDER BY Title ASC";
	IVector<BookData^>^ books = _getBooks(sqlStmt);
	return books;
}

IVector<String^>^ DatabaseStorage::getAuthors()
{
	assert(_libraryDB != nullptr);
	
	const wchar_t* sqlStmt = L"SELECT Authors FROM Books";
	sqlite3_stmt* stmt = nullptr;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	Vector<String^>^ authors = ref new Vector<String^>;
	do
	{
		result = sqlite3_step(stmt);
		if (result == SQLITE_ROW)
		{
			const wchar_t* bookAuthors = (const wchar_t*)sqlite3_column_text16(stmt, 0);

			// extract all authors of the book
			BookData::extractAuthorsFromString(bookAuthors, authors);
		}
	} 
	while (result == SQLITE_ROW);

	sqlite3_finalize(stmt);

	return authors;
}

IVector<BookData^>^ DatabaseStorage::getBooksByAuthor(const wchar_t* author)
{
	wchar_t sqlStmt[1024];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), L"SELECT * FROM Books WHERE Authors LIKE '%%%s%%' ORDER BY Title ASC", author);

	IVector<BookData^>^ books = _getBooks(sqlStmt);
	return books;
}

void DatabaseStorage::addBook(BookData^ book)
{
	assert(_libraryDB != nullptr);

	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"INSERT INTO Books ("
		L"Directory, Title, Authors, CoverImageFilename, BookFilename, "
		L"Annotation, Language, StoreDate, LastOpenedDate"
		L") "
		L"VALUES ("
		L"'%s', '%s', '%s', '%s', '%s', "
		L"'%s', '%s', '%s', '%s')",
		book->Directory->Data(), book->Title->Data(), book->Authors->Data(), book->CoverImageFilename->Data(), book->BookFilename->Data(),
		book->Annotation->Data(), book->Language->Data(), book->StoreDateFull->Data(), book->LastOpenedDateFull->Data());

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result == SQLITE_DONE)
	{
		sqlite3_int64 bookId = sqlite3_last_insert_rowid(_libraryDB);
		book->BookId = static_cast<int>(bookId & 0x00000000FFFFFFFF);
	}
	else
	{
		throw ReaderException(L"CannotAddBook", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::deleteBook(BookData^ book)
{
	assert(_libraryDB != nullptr);

	// delete all references to the book
	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"DELETE FROM BookCategoryRef WHERE BookId=%d",
		book->BookId);

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	if (result != SQLITE_OK)
	{
		throw ReaderException(L"CannotDeleteBook", MessageStringType::ResourceString, result);
	}

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotDeleteBook", MessageStringType::ResourceString, result);
	}

	// delete the book from the database
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"DELETE FROM Books WHERE BookId=%d", book->BookId);

	result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	if (result != SQLITE_OK)
	{
		throw ReaderException(L"CannotDeleteBook", MessageStringType::ResourceString, result);
	}

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotDeleteBook", MessageStringType::ResourceString, result);
	}
}

IVector<BookCollection^>^ DatabaseStorage::getCollections()
{
	assert(_libraryDB != nullptr);
	
	const wchar_t* sqlStmt = L"SELECT * FROM BookCollections ORDER BY CollectionId ASC";
	sqlite3_stmt* stmt = nullptr;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	auto collections = ref new Vector<BookCollection^>;
	do
	{
		result = sqlite3_step(stmt);
		if (result == SQLITE_ROW)
		{
			auto collection = ref new BookCollection();

			collection->CollectionId = sqlite3_column_int(stmt, 0);
			collection->CollectionName = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 1));
			collection->Description = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 2));

			collections->Append(collection);
		}
	} 
	while (result == SQLITE_ROW);

	sqlite3_finalize(stmt);

	return collections;
}

IVector<BookData^>^ DatabaseStorage::getBooksByCollection(BookCollection^ collection)
{
	wchar_t sqlStmt[1024];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"SELECT * FROM Books WHERE BookId IN (SELECT BookId FROM BookCategoryRef WHERE CollectionId=%d)", 
		collection->CollectionId);

	IVector<BookData^>^ books = _getBooks(sqlStmt);
	return books;
}

void DatabaseStorage::addBookToCollection(const int bookId, const int collectionId)
{
	assert(_libraryDB != nullptr);

	// check whether the reference is already set
	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"SELECT * FROM BookCategoryRef WHERE BookId=%d AND CollectionId=%d",
		bookId, collectionId);

	sqlite3_stmt* stmt = nullptr;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);
	if (result == SQLITE_ROW)
	{
		// the reference already exists
		return;
	}

	// insert a new book reference to a collection
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"INSERT INTO BookCategoryRef ("
		L"BookId, CollectionId"
		L") "
		L"VALUES ("
		L"%d, %d)",
		bookId, collectionId);

	result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotAddBookToCollection", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::removeBookFromCollection(const int bookId, const int collectionId)
{
	assert(_libraryDB != nullptr);

	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"DELETE FROM BookCategoryRef WHERE BookId=%d AND CollectionID=%d",
		bookId, collectionId);

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotRemoveBookFromCollection", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::addCollection(BookCollection^ collection)
{
	assert(_libraryDB != nullptr);

	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"INSERT INTO BookCollections ("
		L"CollectionName, Description"
		L") "
		L"VALUES ("
		L"'%s', '%s')",
		collection->CollectionName->Data(), collection->Description->Data());

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result == SQLITE_DONE)
	{
		sqlite3_int64 collectionId = sqlite3_last_insert_rowid(_libraryDB);
		collection->CollectionId = static_cast<int>(collectionId & 0x00000000FFFFFFFF);
	}
	else
	{
		throw ReaderException(L"CannotAddCollection", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::deleteCollection(BookCollection^ collection)
{
	assert(_libraryDB != nullptr);

	// delete all references to the category
	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"DELETE FROM BookCategoryRef WHERE CollectionId=%d",
		collection->CollectionId);

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	if (result != SQLITE_OK)
	{
		throw ReaderException(L"CannotDeleteCollection", MessageStringType::ResourceString, result);
	}

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotDeleteCollection", MessageStringType::ResourceString, result);
	}

	// delete collection from the database
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"DELETE FROM BookCollections WHERE CollectionId=%d", collection->CollectionId);

	result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	if (result != SQLITE_OK)
	{
		throw ReaderException(L"CannotDeleteCollection", MessageStringType::ResourceString, result);
	}

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotDeleteCollection", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::updateLastOpenedDate(const int bookId, Platform::String^ lastOpenedDate)
{
	assert(_libraryDB != nullptr);

	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"UPDATE Books SET LastOpenedDate='%s' WHERE BookId=%d",
		lastOpenedDate->Data(), bookId);

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotUpdateBookProperties", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::clearLastOpenedDateForAllBooks()
{
	assert(_libraryDB != nullptr);

	wchar_t sqlStmt[2048];
	swprintf_s(sqlStmt, sizeof(sqlStmt)/sizeof(wchar_t), 
		L"UPDATE Books SET LastOpenedDate=''");

	sqlite3_stmt* stmt;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (result != SQLITE_DONE)
	{
		throw ReaderException(L"CannotUpdateBookProperties", MessageStringType::ResourceString, result);
	}
}

void DatabaseStorage::_createDatabaseStructure()
{
	ColumnInfo bookCollectionCols[] =
	{
		{"CollectionId", "INTEGER", "PRIMARY KEY"},
		{"CollectionName", "TEXT", "NOT NULL"},
		{"Description", "TEXT", ""}
	};
	_createTable("BookCollections", bookCollectionCols, sizeof(bookCollectionCols)/sizeof(ColumnInfo));
	
	ColumnInfo bookCols[] =
	{
		{"BookId", "INTEGER", "PRIMARY KEY"},
		{"Directory", "TEXT", "NOT NULL"},
		{"Title", "TEXT", "NOT NULL"},
		{"Authors", "TEXT", "NOT NULL"},
		{"CoverImageFilename", "TEXT", ""},
		{"BookFilename", "TEXT", "NOT NULL"},
		{"Annotation", "TEXT", "NOT NULL"},
		{"Language", "TEXT", "NOT NULL"},
		{"StoreDate", "TEXT", "NOT NULL"},
		{"LastOpenedDate", "TEXT", "NOT NULL"}
	};
	_createTable("Books", bookCols, sizeof(bookCols)/sizeof(ColumnInfo));

	ColumnInfo bookCategoryRefCols[] =
	{
		{"ID", "INTEGER", "PRIMARY KEY"},
		{"BookId", "INTEGER", "NOT NULL"},
		{"CollectionId", "INTEGER", "NOT NULL"}
	};
	_createTable("BookCategoryRef", bookCategoryRefCols, sizeof(bookCategoryRefCols)/sizeof(ColumnInfo));

	/*ColumnInfo settingCols[] =
	{
		{"SettingId", "INTEGER", "PRIMARY KEY"},
		{"Key", "TEXT", "NOT NULL"},
		{"Value", "TEXT", "NOT NULL"}
	};
	_createTable("Settings", settingCols, sizeof(settingCols)/sizeof(ColumnInfo));*/
}

void DatabaseStorage::_createTable(const char* tableName, const ColumnInfo* columns, const unsigned short noOfColumns)
{
	assert(_libraryDB != nullptr);
	assert(tableName != nullptr);
	assert(columns != nullptr);
	assert(noOfColumns > 0);

	char query[2048];
	const unsigned int queryMaxLen = sizeof(query)/sizeof(char);
	sqlite3_stmt* stmt = nullptr;
	
	sprintf_s(query, queryMaxLen, "PRAGMA table_info(%s)", tableName);
	sqlite3_prepare(_libraryDB, query, sizeof(query), &stmt, NULL);

	int result = SQLITE_DONE;
	int noOfFoundColumns = 0;
	do
	{
		result = sqlite3_step(stmt);
		if (result == SQLITE_ROW)
		{
			const char* retColumnName = (const char*)sqlite3_column_text(stmt, 1);
			const char* retColumnType = (const char*)sqlite3_column_text(stmt, 2);

			for (unsigned short colIdx = 0; colIdx < noOfColumns; colIdx++)
			{
				if (0 == _stricmp(retColumnName, columns[colIdx].Name) &&
					 0 == _stricmp(retColumnType, columns[colIdx].DataType))
				{
					noOfFoundColumns++;
					break;
				}
			}
		}
	} 
	while (result == SQLITE_ROW);

	sqlite3_finalize(stmt);

	if (noOfFoundColumns == noOfColumns)
	{
		// all columns found
		return;
	}

	sprintf_s(query, queryMaxLen, "CREATE TABLE %s(", tableName);

	char queryHelper[512];
	const unsigned int queryHelperMaxLen = sizeof(queryHelper)/sizeof(char);
	for (unsigned short colIdx = 0; colIdx < noOfColumns; colIdx++)
	{
		if (colIdx == 0)
		{
			// first column definition
			sprintf_s(queryHelper, queryHelperMaxLen, "%s %s %s", columns[colIdx].Name, columns[colIdx].DataType, columns[colIdx].Options);
		}
		else
		{
			sprintf_s(queryHelper, queryHelperMaxLen, ",%s %s %s", columns[colIdx].Name, columns[colIdx].DataType, columns[colIdx].Options);
		}
		strcat_s(query, queryHelper); 
	}
	strcat_s(query, ");");

	result = sqlite3_exec(_libraryDB, query, nullptr, nullptr, nullptr);
	if (result != SQLITE_OK)
	{
		throw ReaderException(L"CannotCreateDatabase", MessageStringType::ResourceString, result);
	}
}

IVector<BookData^>^ DatabaseStorage::_getBooks(const wchar_t* sqlStmt)
{
	assert(_libraryDB != nullptr);
	assert(sqlStmt != nullptr);

	sqlite3_stmt* stmt = nullptr;
	int result = sqlite3_prepare16(_libraryDB, sqlStmt, static_cast<int>(wcslen(sqlStmt) * sizeof(wchar_t)), &stmt, NULL);
	assert(result == SQLITE_OK);

	Vector<BookData^>^ books = ref new Vector<BookData^>;
	do
	{
		result = sqlite3_step(stmt);
		if (result == SQLITE_ROW)
		{
			BookData^ bookData = ref new BookData();

			bookData->BookId = sqlite3_column_int(stmt, 0);
			bookData->Directory = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 1));
			bookData->Title = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 2));
			bookData->Authors = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 3));
			bookData->CoverImageFilename = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 4));
			bookData->BookFilename = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 5));
			bookData->Annotation= ref new String((const wchar_t*)sqlite3_column_text16(stmt, 6));
			bookData->Language = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 7));
			bookData->StoreDateFull = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 8));
			bookData->LastOpenedDateFull = ref new String((const wchar_t*)sqlite3_column_text16(stmt, 9));

			books->Append(bookData);
		}
	} 
	while (result == SQLITE_ROW);

	sqlite3_finalize(stmt);

	return books;
}

} // Model
}
