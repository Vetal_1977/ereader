/*
* @file DatabaseStorage.h
* @brief Database storage to save book information, settings, etc.
*
* @date 2013-01-22
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include <collection.h>
#include "sqlite3.h"
#include "BookCollection.h"

#pragma once

namespace eReader
{
	namespace Model
	{
		//
		// Class for accessing a book data source
		//
		public ref class DatabaseStorage sealed
		{
		internal:
			DatabaseStorage();

		internal:
			// Opens data source: SQLite database contained information about books and collections of them
			// This method should be called by the app start
			// @exception ReaderException Thrown if the book database cannot be opened or created
			void openBookDatabase();

			// Closes the data source. This method must be called to store unsaved changes and close the database
			void closeBookDatabase();

			// Reads last read books from the database
			// @param maxNoOfBooks Maximum number of books to return by this method
			// @return Data of last read books from the database
			Windows::Foundation::Collections::IVector<BookData^>^ getLastReadBooks(_In_ const uint16 maxNoOfBooks);

			// Reads last added books from the database
			// @param maxNoOfBooks Maximum number of books to return by this method
			// @return Data of last added books from the database
			Windows::Foundation::Collections::IVector<BookData^>^ getLastAddedBooks(_In_ const uint16 maxNoOfBooks);

			// Reads all books in the database ordered by title
			// @return Vector of books ordered by title
			Windows::Foundation::Collections::IVector<BookData^>^ getBooksByTitle();

			// Reads authors of existing books in the database
			// @return Vector of found authors
			Windows::Foundation::Collections::IVector<Platform::String^>^ getAuthors();

			// Reads books of the specified author
			// @param author The author to read the book of. Must be a valid not null string
			// @return Vector of books of the author. At least one book must be returned
			Windows::Foundation::Collections::IVector<BookData^>^ getBooksByAuthor(_In_ const wchar_t* author);

			// Adds a book into the database
			// @param book Book to add. The BookId must not be specified, it is saved into the object after adding into the database
			// @exception ReaderException Thrown if the book cannot be added
			void addBook(_In_ _Out_ BookData^ book);

			// Deletes a book from the database
			// @param book Book to delete
			// @exception ReaderException Thrown if the book cannot be deleted
			void deleteBook(_In_ BookData^ book);

			// Reads collections from the database
			// @return Vector of found collections ordered by CollectionId
			Windows::Foundation::Collections::IVector<BookCollection^>^ getCollections();

			// Reads books of the specified collection
			// @param collection The collection to read the book of. Must be a valid and not nullptr
			// @return Vector of books of the collection. A collection my be empty 
			Windows::Foundation::Collections::IVector<BookData^>^ getBooksByCollection(_In_ BookCollection^ collection);

			// Adds a book into the collection. If the book is already there, nothing occurs. 
			// Adding into collection means an update of the Book-Collection reference table
			// @param bookId Book ID to add into the collection
			// @param collectionId Collection ID to add the book into
			// @exception ReaderException Thrown if book-collection reference cannot be added
			void addBookToCollection(_In_ const int bookId, const int collectionId);

			// Removes a book from the collection. If the book is not in the collection, nothing occurs
			// @param bookId Book ID  to remove from the collection
			// @param collectionId Collection ID to remove the book from
			// @exception ReaderException Thrown if book-collection reference cannot be deleted
			void removeBookFromCollection(_In_ const int bookId, _In_ const int collectionId);

			// Adds a collection into the database
			// @param collection Collection to add. The CollectionId must not be specified, it is saved into the object after adding into the database
			// @exception ReaderException Thrown if the collection cannot be added
			void addCollection(_In_ _Out_ BookCollection^ collection);

			// Deletes a collection from the database
			// @param collection Collection to delete
			// @exception ReaderException Thrown if the collection cannot be deleted
			void deleteCollection(_In_ BookCollection^ collection);

			// Updates last opened date field for the book
			// @param bookId Book ID to update the date for
			// @param lastOpenedDate String representation of the date
			void updateLastOpenedDate(_In_ const int bookId, _In_ Platform::String^ lastOpenedDate);

			// Clears the last opened date for all books in the database. It will clear the history
			void clearLastOpenedDateForAllBooks();

		private:
			struct ColumnInfo
			{
			public:
				char* Name;
				char* DataType; // INTEGER, TEXT, VARCHR(50), etc.
				char* Options; // empty string or additional options ("NOT NULL", "PRIMARY KEY", their combination, etc.); must not be nullptr
			};

			// Checks the database structure and creates it if necessary (tables, columns)
			void _createDatabaseStructure();

			// Checks whether the specified table exist and columns match and creates the table if necessary
			// @param tableName Table name, must not be nullptr, empty string or conatains invalid characters
			// @param columns Array of table columns, must not be nullptr or contains no columns
			// @param noOfColumns Number of columns in 'columns' array
			void _createTable(_In_ const char* tableName, _In_ const ColumnInfo* columns, _In_ const unsigned short noOfColumns);

			// Executes specified SQL statement to get books from the database
			// @param sqlStmt SQL statement to query book data from the database. List of BookData must be expected as a result of the statement
			// @return Vector of book data corresponding to the specified statement
			Windows::Foundation::Collections::IVector<BookData^>^ _getBooks(_In_ const wchar_t* sqlStmt);

		private:
			static Platform::String^ STORAGE_NAME;

			sqlite3* _libraryDB;
		};
	} // Model
}
