/*
 * @file BookData.h
 * @brief Book data class
 *
 * Contains the data about a book: author, title, language, genre, date of saving, description.
 * It is a part of the data model of the reader
 *
 * @date 2012-12-23
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#pragma once

#include "Common\BindableBase.h"

namespace eReader
{
	namespace Model
	{
		/// <summary>
		/// Data class for a book
		/// </summary>
		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class BookData sealed : eReader::Common::BindableBase
		{
		internal:
			BookData();

		public:
			property int BookId { int get(); void set(int value); }
			property Platform::String^ Directory { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ Title { Platform::String^ get(); void set(Platform::String^ value); }
			property Windows::Foundation::Collections::IVector<Platform::String^>^ AuthorArray 
			{ 
				Windows::Foundation::Collections::IVector<Platform::String^>^ get();
			}
			property Platform::String^ Authors { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ CoverImageFilename { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ BookFilename { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ Annotation { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ Language { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ StoreDateFull { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ StoreDateOnly { Platform::String^ get(); }
			property Platform::String^ LastOpenedDateFull { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ LastOpenedDateOnly { Platform::String^ get(); }
			 
		internal:
			// Extract all authors from the string.
			// @param authors Strings contained author list. The string has a format '[Author 1][Author 2]'
			// @param authorVector Vector of authors to change. Must not be nullptr. The double insert of an author will be avoided
			static void extractAuthorsFromString(
				_In_ const wchar_t* authors,
				_In_ _Out_ Windows::Foundation::Collections::IVector<Platform::String^>^ authorVector);

		private:
			int _bookId;
			Platform::String^ _directory; // <! storage place of book data
			Platform::String^ _title;
			Platform::String^ _authors;
			Platform::String^ _coverImageFilename;
			Platform::String^ _bookFilename;
			Platform::String^ _annotation;
			Platform::String^ _language;
			Platform::String^ _storeDateFull;
			Platform::String^ _storeDateOnly;
			Platform::String^ _lastOpenedDateFull;
			Platform::String^ _lastOpenedDateOnly;
		};
	} // Model
}

