/*
 * @file BookCollection.h
 * @brief Book collection class
 *
 * Book collection can be created by the user. Any book can be added to any collection
 * A book references a collection using its ID
 *
 * @date 2012-12-24
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#pragma once

#include "Common\BindableBase.h"
#include "BookData.h"

namespace eReader
{
	namespace Model
	{
	 	/// <summary>
		/// Book group data class
		/// </summary>
		[Windows::UI::Xaml::Data::Bindable]
		[Windows::Foundation::Metadata::WebHostHidden]
		public ref class BookCollection sealed : eReader::Common::BindableBase
		{
		internal:
			BookCollection();

		public:
			property int CollectionId { int get(); void set(int value); }
			property Platform::String^ CollectionName { Platform::String^ get(); void set(Platform::String^ value); }
			property Platform::String^ Description { Platform::String^ get(); void set(Platform::String^ value); }

		private:
			// Event handler. Invoked when the group of books is changed
			// @param sender A vector that fired the event
			// @param args Arguments of the event
			void OnBookVectorChanged(
				Windows::Foundation::Collections::IObservableVector<BookData^>^ sender, 
				Windows::Foundation::Collections::IVectorChangedEventArgs^ args);

		private:
			int _collectionId;
			Platform::String^ _collectionName;
			Platform::String^ _description;
			Platform::Collections::Vector<BookData^>^ _books;
		};
	} // Model
}
