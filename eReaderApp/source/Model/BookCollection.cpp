/*
* @file BookCollection.cpp
* @brief Book group data class
*
* Contains the the book group string (can be author, genre) and an array of
* books that belong to the group. Book group arrays are not stored, they are
* created dynamically be reading books from the data source
*
* @date 2012-12-24
* @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
*/

#include "pch.h"
#include "BookCollection.h"

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

namespace eReader
{
namespace Model
{

BookCollection::BookCollection()
{
}

int BookCollection::CollectionId::get()
{
	return _collectionId;
}

void BookCollection::CollectionId::set(int value)
{
	_collectionId = value;
}

String^ BookCollection::CollectionName::get()
{
	return _collectionName;
}

void BookCollection::CollectionName::set(String^ value)
{
	_collectionName = value;
}

String^ BookCollection::Description::get()
{
	return _description;
}

void BookCollection::Description::set(String^ value)
{
	_description = value;
}

void BookCollection::OnBookVectorChanged(IObservableVector<BookData^>^ sender, IVectorChangedEventArgs^ args)
{
	OnPropertyChanged("BookVector");
}
 

} // Model
}
