/*
 * @file BookData.cpp
 * @brief Book data class
 *
 * Contains the data about a book: author, title, language, genre, date of saving, description.
 * It is a part of the data model of the reader
 *
 * @date 2012-12-23
 * @author Vitaly Bezgachev, vitaly.bezgachev@gmail.com
 */

#include "pch.h"
#include "BookData.h"
#include "BookCollection.h"
#include <ATLComTime.h>

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

#define AUTHORS_DELIMITER L", " // same delimiter is used by cool reader engine (see lvtinydom.cpp\extractDocAuthors)

namespace eReader
{
namespace Model
{
///
/// Book data class
///
BookData::BookData(void)
{
}

int BookData::BookId::get()
{
	return _bookId;
}

void BookData::BookId::set(int value)
{
	_bookId = value;
}

String^ BookData::Directory::get()
{
	return _directory;
}

void BookData::Directory::set(String^ value)
{
	_directory = value;
}

String^ BookData::Title::get()
{
	return _title;
}

void BookData::Title::set(String^ value)
{
	_title = value;
}

IVector<String^>^ BookData::AuthorArray::get()
{
	auto authorArray = ref new Vector<String^>();
	extractAuthorsFromString(_authors->Data(), authorArray);

	return authorArray;
}

String^ BookData::Authors::get()
{
	return _authors;
}

void BookData::Authors::set(String^ value)
{
	_authors = value;
}

String^ BookData::CoverImageFilename::get()
{
	return _coverImageFilename;
}

void BookData::CoverImageFilename::set(String^ value)
{
	_coverImageFilename = value;
}

String^ BookData::BookFilename::get()
{
	return _bookFilename;
}

void BookData::BookFilename::set(String^ value)
{
	_bookFilename = value;
}

String^ BookData::Annotation::get()
{
	return _annotation;
}

void BookData::Annotation::set(String^ value)
{
	_annotation = value;
}

String^ BookData::Language::get()
{
	return _language;
}

void BookData::Language::set(String^ value)
{
	_language = value;
}

String^ BookData::StoreDateFull::get()
{
	return _storeDateFull;
}

void BookData::StoreDateFull::set(String^ value)
{
	_storeDateFull = value;

	COleDateTime oleDate;
	oleDate.ParseDateTime(_storeDateFull->Data(), VAR_DATEVALUEONLY); // parse date string specified as 'YYYY-MM-DD'
	
	wchar_t dateOnly[80];
	swprintf_s(dateOnly, sizeof(dateOnly)/sizeof(wchar_t), L"%04d-%02d-%02d", oleDate.GetYear(), oleDate.GetMonth(), oleDate.GetDay());

	_storeDateOnly = ref new String(dateOnly);
}

String^ BookData::StoreDateOnly::get()
{
	return _storeDateOnly;
}

String^ BookData::LastOpenedDateFull::get()
{
	return _lastOpenedDateFull;
}

void BookData::LastOpenedDateFull::set(String^ value)
{
	_lastOpenedDateFull = value;

	COleDateTime oleDate;
	oleDate.ParseDateTime(_lastOpenedDateFull->Data(), VAR_DATEVALUEONLY); // parse date string specified as 'YYYY-MM-DD'
	
	wchar_t dateOnly[80];
	swprintf_s(dateOnly, sizeof(dateOnly)/sizeof(wchar_t), L"%04d-%02d-%02d", oleDate.GetYear(), oleDate.GetMonth(), oleDate.GetDay());

	_lastOpenedDateOnly = ref new String(dateOnly);
}

String^ BookData::LastOpenedDateOnly::get()
{
	return _lastOpenedDateOnly;
}

void BookData::extractAuthorsFromString(const wchar_t* authors, IVector<Platform::String^>^ authorVector)
{
	assert(authors != nullptr);
	assert(authorVector != nullptr);

	static const size_t DELIMITER_LENGTH = wcslen(AUTHORS_DELIMITER);
	const wchar_t* authorNameStart = authors;
	const wchar_t* authorNameEnd = nullptr;
	wchar_t extractedName[256];
	const uint16 maxNameLen = sizeof(extractedName)/sizeof(wchar_t);

	while(authorNameStart != nullptr)
	{
		authorNameEnd = wcsstr(authorNameStart, AUTHORS_DELIMITER);
		if (authorNameEnd == nullptr)
		{
			wcsncpy_s(extractedName, maxNameLen, authorNameStart, wcslen(authorNameStart));
			authorNameStart = nullptr;
		}
		else
		{
			wcsncpy_s(extractedName, maxNameLen, authorNameStart, authorNameEnd - authorNameStart);
			authorNameStart = authorNameEnd;
			authorNameStart += DELIMITER_LENGTH;
		}

		// put found author into the vector if he is not there yet
		bool authorAlreadyKnown = false;
		for each (String^ author in authorVector)
		{
			if (0 == _wcsicmp(author->Data(), extractedName))
			{
				authorAlreadyKnown = true;
				break;
			}
		}
		if (!authorAlreadyKnown)
		{
			authorVector->Append(ref new String(extractedName));
		}
	}
}

} // Model
}
